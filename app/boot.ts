import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {AppModule} from "./startpage/app.module";



platformBrowserDynamic().bootstrapModule(AppModule); // JIT Compilation
// AoT Compilation is coming soon