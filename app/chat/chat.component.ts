import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {DatePipe} from '@angular/common';
import {ConfigService} from "../sharedService/config.service";
import {MainService} from "../main/main.service";
import {ChatService} from "./chat.service";
import {Subscription} from "rxjs/Subscription";
import {TranslateService} from "ng2-translate";

@Component({
    selector: 'chat',
    templateUrl: 'app/chat/chat.component.html',
    styleUrls: ['app/chat/chat.component.scss']
})

export class ChatComponent implements OnInit, OnDestroy {
    public _subKeepCorrespondence: Subscription;
    public _subMarkChatCorrespondenceAsRead: Subscription;
    public message: any = '';
    public defaultAvatarImage: string = './app/img/default-avatar-image.jpg';
    public defaultDatePipeLocale: string = 'en';

    // Date time format by current selected locale
    public dateFormatByLocale: any = {
        en: 'MM-dd-y H:mm:ss',
        es: 'MM-dd-y H:mm:ss',
        ru: 'dd-MM-y H:mm:ss',
        ch: 'dd-MM-y H:mm:ss'
    };

    constructor(public configService: ConfigService,
                public mainService: MainService,
                public chatService: ChatService,
                public translate: TranslateService) {}

    ngOnInit() {}

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subKeepCorrespondence) {
            this._subKeepCorrespondence.unsubscribe();
        }
        if (this._subMarkChatCorrespondenceAsRead) {
            this._subMarkChatCorrespondenceAsRead.unsubscribe();
        }
    }

    /**
     * Send message to receiver
     */
    sendMessage() {
        // Create date pipe to manipulate date's format and use another date's services
        let datePipe = new DatePipe(this.defaultDatePipeLocale);

        // Check if message hasn't spaces or tabs, not empty, not null type and not undefined
        if (this.message && this.message.trim() != '' && this.message != '' && this.message !== null && typeof this.message != 'undefined') {

            // Message's object
            let chatMessage = {};

            // Check if current receiver not exist in "chat rooms" (we doesn't talked with him early),
            // then initialize empty array where save chat messages to current receiver
            if (!!this.chatService.chatRooms[this.chatService.receiverId] == false) {
                this.chatService.chatRooms[this.chatService.receiverId] = [];
            }

            chatMessage = {
                from: this.chatService.senderId,
                fromFirstName: this.configService.user.firstName,
                fromSecondName: this.configService.user.secondName,
                fromLastName: this.configService.user.lastName,
                fromUsername: this.configService.user.username,
                fromAvatarImage: this.configService.user.link_avatar_image,
                to: this.chatService.receiverId,
                toFirstName: this.chatService.receiverFirstName,
                toSecondName: this.chatService.receiverSecondName,
                toLastName: this.chatService.receiverLastName,
                toUsername: this.chatService.receiverUsername,
                message: this.message,
                isNew: true,
                time: {
                    date: datePipe.transform(new Date(), 'yyyy-M-d H:m:s')
                }
            };

            // Send message's object (emit) to "NodeJS" server via "socket" connection, to instant message sending to receiver
            this.mainService.sendMessage(chatMessage);

            // Put message's object to current receiver's chat rooms
            this.chatService.chatRooms[this.chatService.receiverId].push(chatMessage);

            // Clear message
            this.message = '';

            // Save chat's message in database
            this._subKeepCorrespondence = this.chatService.keepCorrespondence(chatMessage).subscribe((data: any) => {
                console.info(data);
            });
        } else {
            this.message = '';
        }
    }

    /**
     * Close chat window
     * @param receiverId
     */
    onCloseChat(receiverId) {
        // Before close chat's window, need mark messages like as read
        if (!!this.chatService.chatRooms[receiverId] && this.chatService.chatRooms[receiverId].length > 0) {
            this._subMarkChatCorrespondenceAsRead = this.chatService.markChatCorrespondenceAsRead(this.chatService.senderId, receiverId).subscribe((data: any) => {
                console.info(data);
            });
        }
        this.chatService.chatWindowsState[receiverId] = false;
    }

    /**
     * Send message by clicking on "Enter" key
     * @param event
     */
    onClickEnterSendMessage(event) {
        // If chat's message was send through clicking by "Enter"
        if(event.keyCode == 13) {
            this.sendMessage();
        }
    }
}