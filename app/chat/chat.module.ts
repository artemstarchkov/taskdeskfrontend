import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import { CHAT_ROUTES } from './chat.route';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {ChatService} from "./chat.service";
import {ChatComponent} from "./chat.component";

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        CHAT_ROUTES,
        RouterModule,
        FormsModule
    ],
    providers: [
        ChatService,
    ],
    declarations: [
        ChatComponent
    ]
})
export class ChatModule { }