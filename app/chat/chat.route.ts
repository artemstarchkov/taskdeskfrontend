import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {ChatComponent} from "./chat.component";

const chatRoutes:Routes = [
    {
        path: '',
        component: ChatComponent
    }
];

export const CHAT_ROUTES:ModuleWithProviders = RouterModule.forChild(chatRoutes);