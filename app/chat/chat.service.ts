import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {Subject} from "rxjs/Subject";
import {ConfigService} from "../sharedService/config.service";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/mergeMap";

@Injectable()
export class ChatService {
    public senderId: string = '';
    public receiverId: string = '';
    public receiverFirstName: string = '';
    public receiverSecondName: string = '';
    public receiverLastName: string = '';
    public receiverUsername: string = '';

    // ChatRooms - where stored receivers's chat messages while application working, to fast get all messages for
    // current receiver
    public chatRooms: any = [];

    // In chatWindowsState stored the states of all receivers's chat window (active/inactive, to show/hide chat window)
    public chatWindowsState: any = [];

    // In chatMessagesCounter stores the number of new messages from each user from the list
    public chatMessagesCounter: any = [];

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService) {}

    /**
     * Save chat's message in database
     * @param chatMessage
     * @returns {Observable<any>}
     */
    public keepCorrespondence(chatMessage: any) {
        return this.http.post(this.configService.domen + 'chat/keepcorrespondence',
            {
                chatMessage: chatMessage
            })
            .map((res: any) => res.json());
    }

    /**
     * Get all chat's messages (all correspondence) between me and current user (current receiver when open chat window)
     * @param {string} from
     * @param {string} to
     * @returns {Observable<any>}
     */
    public getChatCorrespondence(from: string, to: string) {
        return this.http.post(this.configService.domen + 'chat/getchatcorrespondence',
            {
                from: from,
                to: to
            })
            .map((res: any) => res.json());
    }

    /**
     * Mark the correspondence between me and the user as read
     * @param {string} from
     * @param {string} to
     * @returns {Observable<any>}
     */
    public markChatCorrespondenceAsRead(from: string, to: string) {
        return this.http.post(this.configService.domen + 'chat/markchatcorrespondenceasread',
            {
                from: from,
                to: to
            })
            .map((res: any) => res.json());
    }

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.chatRooms = [];
        this.chatWindowsState = [];
        this.chatMessagesCounter = [];
        this.senderId = '';
        this.receiverId = '';
        this.receiverFirstName = '';
        this.receiverSecondName = '';
        this.receiverLastName = '';
        this.receiverUsername = '';
    }
}