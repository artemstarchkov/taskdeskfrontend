System.register(["@angular/core", "@angular/http", "@angular/common", "./leftSideBarMenu.route", "@angular/router", "@angular/forms", "./leftSideBarMenu.service", "./leftSidebarMenu.component"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, common_1, leftSideBarMenu_route_1, router_1, forms_1, leftSideBarMenu_service_1, leftSidebarMenu_component_1, LeftSideBarMenuModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (leftSideBarMenu_route_1_1) {
                leftSideBarMenu_route_1 = leftSideBarMenu_route_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (leftSideBarMenu_service_1_1) {
                leftSideBarMenu_service_1 = leftSideBarMenu_service_1_1;
            },
            function (leftSidebarMenu_component_1_1) {
                leftSidebarMenu_component_1 = leftSidebarMenu_component_1_1;
            }
        ],
        execute: function () {
            LeftSideBarMenuModule = class LeftSideBarMenuModule {
            };
            LeftSideBarMenuModule = __decorate([
                core_1.NgModule({
                    imports: [
                        common_1.CommonModule,
                        http_1.HttpModule,
                        leftSideBarMenu_route_1.LEFT_SIDE_BAR_MENU_ROUTES,
                        router_1.RouterModule,
                        forms_1.FormsModule
                    ],
                    providers: [
                        leftSideBarMenu_service_1.LeftSideBarMenuService,
                    ],
                    declarations: [
                        leftSidebarMenu_component_1.LeftSideBarMenuComponent
                    ]
                })
            ], LeftSideBarMenuModule);
            exports_1("LeftSideBarMenuModule", LeftSideBarMenuModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBd0JhLHFCQUFxQixHQUFsQzthQUFzQyxDQUFBO1lBQXpCLHFCQUFxQjtnQkFmakMsZUFBUSxDQUFDO29CQUNOLE9BQU8sRUFBTzt3QkFDVixxQkFBWTt3QkFDWixpQkFBVTt3QkFDVixpREFBeUI7d0JBQ3pCLHFCQUFZO3dCQUNaLG1CQUFXO3FCQUNkO29CQUNELFNBQVMsRUFBRTt3QkFDUCxnREFBc0I7cUJBQ3pCO29CQUNELFlBQVksRUFBRTt3QkFDVixvREFBd0I7cUJBQzNCO2lCQUNKLENBQUM7ZUFDVyxxQkFBcUIsQ0FBSTs7UUFBQSxDQUFDIiwiZmlsZSI6ImxlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SHR0cE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBMRUZUX1NJREVfQkFSX01FTlVfUk9VVEVTIH0gZnJvbSAnLi9sZWZ0U2lkZUJhck1lbnUucm91dGUnO1xyXG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge0Zvcm1zTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHtMZWZ0U2lkZUJhck1lbnVTZXJ2aWNlfSBmcm9tIFwiLi9sZWZ0U2lkZUJhck1lbnUuc2VydmljZVwiO1xyXG5pbXBvcnQge0xlZnRTaWRlQmFyTWVudUNvbXBvbmVudH0gZnJvbSBcIi4vbGVmdFNpZGViYXJNZW51LmNvbXBvbmVudFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6ICAgICAgW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBIdHRwTW9kdWxlLFxyXG4gICAgICAgIExFRlRfU0lERV9CQVJfTUVOVV9ST1VURVMsXHJcbiAgICAgICAgUm91dGVyTW9kdWxlLFxyXG4gICAgICAgIEZvcm1zTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgTGVmdFNpZGVCYXJNZW51U2VydmljZSxcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBMZWZ0U2lkZUJhck1lbnVDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIExlZnRTaWRlQmFyTWVudU1vZHVsZSB7IH0iXX0=
