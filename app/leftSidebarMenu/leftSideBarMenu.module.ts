import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import { LEFT_SIDE_BAR_MENU_ROUTES } from './leftSideBarMenu.route';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {LeftSideBarMenuService} from "./leftSideBarMenu.service";
import {LeftSideBarMenuComponent} from "./leftSidebarMenu.component";

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        LEFT_SIDE_BAR_MENU_ROUTES,
        RouterModule,
        FormsModule
    ],
    providers: [
        LeftSideBarMenuService,
    ],
    declarations: [
        LeftSideBarMenuComponent
    ]
})
export class LeftSideBarMenuModule { }