System.register(["@angular/router", "./leftSidebarMenu.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, leftSidebarMenu_component_1, leftSideBarMenuRoutes, LEFT_SIDE_BAR_MENU_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (leftSidebarMenu_component_1_1) {
                leftSidebarMenu_component_1 = leftSidebarMenu_component_1_1;
            }
        ],
        execute: function () {
            leftSideBarMenuRoutes = [
                {
                    path: '',
                    component: leftSidebarMenu_component_1.LeftSideBarMenuComponent
                }
            ];
            exports_1("LEFT_SIDE_BAR_MENU_ROUTES", LEFT_SIDE_BAR_MENU_ROUTES = router_1.RouterModule.forChild(leftSideBarMenuRoutes));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUucm91dGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7WUFJTSxxQkFBcUIsR0FBVTtnQkFDakM7b0JBQ0ksSUFBSSxFQUFFLEVBQUU7b0JBQ1IsU0FBUyxFQUFFLG9EQUF3QjtpQkFDdEM7YUFDSixDQUFDO1lBRUYsdUNBQWEseUJBQXlCLEdBQXVCLHFCQUFZLENBQUMsUUFBUSxDQUFDLHFCQUFxQixDQUFDLEVBQUM7UUFBQSxDQUFDIiwiZmlsZSI6ImxlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUucm91dGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1JvdXRlcywgUm91dGVyTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7TW9kdWxlV2l0aFByb3ZpZGVyc30gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtMZWZ0U2lkZUJhck1lbnVDb21wb25lbnR9IGZyb20gXCIuL2xlZnRTaWRlYmFyTWVudS5jb21wb25lbnRcIjtcclxuXHJcbmNvbnN0IGxlZnRTaWRlQmFyTWVudVJvdXRlczpSb3V0ZXMgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgcGF0aDogJycsXHJcbiAgICAgICAgY29tcG9uZW50OiBMZWZ0U2lkZUJhck1lbnVDb21wb25lbnRcclxuICAgIH1cclxuXTtcclxuXHJcbmV4cG9ydCBjb25zdCBMRUZUX1NJREVfQkFSX01FTlVfUk9VVEVTOk1vZHVsZVdpdGhQcm92aWRlcnMgPSBSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQobGVmdFNpZGVCYXJNZW51Um91dGVzKTsiXX0=
