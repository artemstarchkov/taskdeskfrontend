import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {LeftSideBarMenuComponent} from "./leftSidebarMenu.component";

const leftSideBarMenuRoutes:Routes = [
    {
        path: '',
        component: LeftSideBarMenuComponent
    }
];

export const LEFT_SIDE_BAR_MENU_ROUTES:ModuleWithProviders = RouterModule.forChild(leftSideBarMenuRoutes);