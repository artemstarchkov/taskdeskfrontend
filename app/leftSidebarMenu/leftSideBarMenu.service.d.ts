import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Http } from '@angular/http';
import "rxjs/add/operator/map";
import { ConfigService } from "../sharedService/config.service";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/mergeMap";
import { SocketService } from "../sharedService/socket.service";
export declare class LeftSideBarMenuService {
    router: Router;
    http: Http;
    configService: ConfigService;
    socketService: SocketService;
    contacts: any;
    tasks: any;
    taskKeys: any;
    constructor(router: Router, http: Http, configService: ConfigService, socketService: SocketService);
    getContactsList(currentUserId: number): Observable<any>;
    getTasks(currentUserId: number): Observable<any>;
    getTasksWithStatusDone(currentUserId: number): Observable<any>;
    getTasksAppointedByMe(currentUserId: number): Observable<any>;
    removeUserFromContactList(relationshipId: number, relationshipRequestId: number, currentUserId: number, targetUserId: number): Observable<{}>;
    deleteUserFromContactList(data: any): void;
    socketOnUserWasDeletedFromContactList(): Observable<any>;
    resetData(): void;
}
