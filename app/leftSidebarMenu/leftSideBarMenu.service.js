System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "../sharedService/config.service", "rxjs/add/operator/switchMap", "rxjs/add/operator/mergeMap", "../sharedService/socket.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, config_service_1, socket_service_1, LeftSideBarMenuService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (_2) {
            },
            function (_3) {
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            }
        ],
        execute: function () {
            LeftSideBarMenuService = class LeftSideBarMenuService {
                constructor(router, http, configService, socketService) {
                    this.router = router;
                    this.http = http;
                    this.configService = configService;
                    this.socketService = socketService;
                    this.contacts = [];
                    this.tasks = {};
                    this.taskKeys = [];
                }
                getContactsList(currentUserId) {
                    return this.http.post(this.configService.domen + 'relationship/getcontactslist', { currentUserId: currentUserId })
                        .map(res => res.json());
                }
                getTasks(currentUserId) {
                    return this.http.post(this.configService.domen + 'task/gettasks', { currentUserId: currentUserId })
                        .map(res => res.json());
                }
                getTasksWithStatusDone(currentUserId) {
                    return this.http.post(this.configService.domen + 'task/get_tasks_with_status_done', { currentUserId: currentUserId })
                        .map(res => res.json());
                }
                getTasksAppointedByMe(currentUserId) {
                    return this.http.post(this.configService.domen + 'task/get_tasks_appointed_by_me', { currentUserId: currentUserId })
                        .map(res => res.json());
                }
                removeUserFromContactList(relationshipId, relationshipRequestId, currentUserId, targetUserId) {
                    return this.http.post(this.configService.domen + 'relationship/removeuserfromcontactlist', {
                        relationshipId: relationshipId,
                        relationshipRequestId: relationshipRequestId,
                        currentUserId: currentUserId,
                        targetUserId: targetUserId
                    })
                        .map((res) => res.json())
                        .flatMap(result => {
                        if (!result.status['error']) {
                            return this.getContactsList(currentUserId);
                        }
                        else {
                            return result;
                        }
                    });
                }
                deleteUserFromContactList(data) {
                    this.socketService.socket.emit("delete_user_from_my_list", data);
                }
                socketOnUserWasDeletedFromContactList() {
                    if (this.socketService.socket) {
                        return this.socketService
                            .fromEvent("user_was_deleted_from_contact_list")
                            .map(data => data);
                    }
                }
                resetData() {
                    this.contacts = [];
                    this.tasks = {};
                    this.taskKeys = [];
                }
            };
            LeftSideBarMenuService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router,
                    http_1.Http,
                    config_service_1.ConfigService,
                    socket_service_1.SocketService])
            ], LeftSideBarMenuService);
            exports_1("LeftSideBarMenuService", LeftSideBarMenuService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQVlhLHNCQUFzQixHQUFuQztnQkFNSSxZQUFtQixNQUFjLEVBQ2QsSUFBVSxFQUNWLGFBQTRCLEVBQzVCLGFBQTRCO29CQUg1QixXQUFNLEdBQU4sTUFBTSxDQUFRO29CQUNkLFNBQUksR0FBSixJQUFJLENBQU07b0JBQ1Ysa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBQzVCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO29CQVB4QyxhQUFRLEdBQVEsRUFBRSxDQUFDO29CQUNuQixVQUFLLEdBQVEsRUFBRSxDQUFDO29CQUNoQixhQUFRLEdBQVEsRUFBRSxDQUFDO2dCQUt3QixDQUFDO2dCQU81QyxlQUFlLENBQUMsYUFBcUI7b0JBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyw4QkFBOEIsRUFBRSxFQUFDLGFBQWEsRUFBRSxhQUFhLEVBQUMsQ0FBQzt5QkFDM0csR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ2hDLENBQUM7Z0JBT00sUUFBUSxDQUFDLGFBQXFCO29CQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsZUFBZSxFQUFFLEVBQUMsYUFBYSxFQUFFLGFBQWEsRUFBQyxDQUFDO3lCQUM1RixHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFPTSxzQkFBc0IsQ0FBQyxhQUFxQjtvQkFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLGlDQUFpQyxFQUFFLEVBQUMsYUFBYSxFQUFFLGFBQWEsRUFBQyxDQUFDO3lCQUM5RyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFPTSxxQkFBcUIsQ0FBQyxhQUFxQjtvQkFDOUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLGdDQUFnQyxFQUFFLEVBQUMsYUFBYSxFQUFFLGFBQWEsRUFBQyxDQUFDO3lCQUM3RyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFTTSx5QkFBeUIsQ0FBQyxjQUFzQixFQUFFLHFCQUE2QixFQUFFLGFBQXFCLEVBQUUsWUFBb0I7b0JBQy9ILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyx3Q0FBd0MsRUFDckY7d0JBQ0ksY0FBYyxFQUFFLGNBQWM7d0JBQzlCLHFCQUFxQixFQUFFLHFCQUFxQjt3QkFDNUMsYUFBYSxFQUFFLGFBQWE7d0JBQzVCLFlBQVksRUFBRSxZQUFZO3FCQUM3QixDQUFDO3lCQUNELEdBQUcsQ0FBQyxDQUFDLEdBQVEsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO3lCQUM3QixPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2QsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDMUIsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQy9DLENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osTUFBTSxDQUFDLE1BQU0sQ0FBQzt3QkFDbEIsQ0FBQztvQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFDWCxDQUFDO2dCQU1NLHlCQUF5QixDQUFDLElBQUk7b0JBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDckUsQ0FBQztnQkFNTSxxQ0FBcUM7b0JBQ3hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt3QkFDNUIsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhOzZCQUNwQixTQUFTLENBQU0sb0NBQW9DLENBQUM7NkJBQ3BELEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMzQixDQUFDO2dCQUNMLENBQUM7Z0JBS00sU0FBUztvQkFDWixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztvQkFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO2dCQUN2QixDQUFDO2FBQ0osQ0FBQTtZQXhHWSxzQkFBc0I7Z0JBRGxDLGlCQUFVLEVBQUU7aURBT2tCLGVBQU07b0JBQ1IsV0FBSTtvQkFDSyw4QkFBYTtvQkFDYiw4QkFBYTtlQVR0QyxzQkFBc0IsQ0F3R2xDOztRQUFBLENBQUMiLCJmaWxlIjoibGVmdFNpZGViYXJNZW51L2xlZnRTaWRlQmFyTWVudS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzL09ic2VydmFibGVcIjtcclxuaW1wb3J0IHtIdHRwLCBIZWFkZXJzLCBSZXNwb25zZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCBcInJ4anMvYWRkL29wZXJhdG9yL21hcFwiO1xyXG5pbXBvcnQge1N1YmplY3R9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHtDb25maWdTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9jb25maWcuc2VydmljZVwiO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9zd2l0Y2hNYXBcIjtcclxuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvbWVyZ2VNYXBcIjtcclxuaW1wb3J0IHtTb2NrZXRTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9zb2NrZXQuc2VydmljZVwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTGVmdFNpZGVCYXJNZW51U2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIGNvbnRhY3RzOiBhbnkgPSBbXTtcclxuICAgIHB1YmxpYyB0YXNrczogYW55ID0ge307XHJcbiAgICBwdWJsaWMgdGFza0tleXM6IGFueSA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBodHRwOiBIdHRwLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgc29ja2V0U2VydmljZTogU29ja2V0U2VydmljZSkge31cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB1c2VyJ3MgY29udGFjdCBsaXN0XHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gY3VycmVudFVzZXJJZFxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldENvbnRhY3RzTGlzdChjdXJyZW50VXNlcklkOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWdTZXJ2aWNlLmRvbWVuICsgJ3JlbGF0aW9uc2hpcC9nZXRjb250YWN0c2xpc3QnLCB7Y3VycmVudFVzZXJJZDogY3VycmVudFVzZXJJZH0pXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHJlcy5qc29uKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHVzZXIncyB0YXNrcyB3aXRoIHN0YXR1c2VzIG5ldyBvciBpbiBwcm9ncmVzc1xyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGN1cnJlbnRVc2VySWRcclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRUYXNrcyhjdXJyZW50VXNlcklkOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWdTZXJ2aWNlLmRvbWVuICsgJ3Rhc2svZ2V0dGFza3MnLCB7Y3VycmVudFVzZXJJZDogY3VycmVudFVzZXJJZH0pXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHJlcy5qc29uKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHVzZXIncyB0YXNrcyB3aXRoIHN0YXR1cyBcImRvbndcIlxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGN1cnJlbnRVc2VySWRcclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRUYXNrc1dpdGhTdGF0dXNEb25lKGN1cnJlbnRVc2VySWQ6IG51bWJlcikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZ1NlcnZpY2UuZG9tZW4gKyAndGFzay9nZXRfdGFza3Nfd2l0aF9zdGF0dXNfZG9uZScsIHtjdXJyZW50VXNlcklkOiBjdXJyZW50VXNlcklkfSlcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGFza3MgYXBwb2ludGVkIGJ5IG1lIChjdXJyZW50IHVzZXIpXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gY3VycmVudFVzZXJJZFxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldFRhc2tzQXBwb2ludGVkQnlNZShjdXJyZW50VXNlcklkOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWdTZXJ2aWNlLmRvbWVuICsgJ3Rhc2svZ2V0X3Rhc2tzX2FwcG9pbnRlZF9ieV9tZScsIHtjdXJyZW50VXNlcklkOiBjdXJyZW50VXNlcklkfSlcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZW1vdmUgdXNlciBmcm9tIHVzZXIncyBjb250YWN0IGxpc3RcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gcmVsYXRpb25zaGlwSWRcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSByZWxhdGlvbnNoaXBSZXF1ZXN0SWRcclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyByZW1vdmVVc2VyRnJvbUNvbnRhY3RMaXN0KHJlbGF0aW9uc2hpcElkOiBudW1iZXIsIHJlbGF0aW9uc2hpcFJlcXVlc3RJZDogbnVtYmVyLCBjdXJyZW50VXNlcklkOiBudW1iZXIsIHRhcmdldFVzZXJJZDogbnVtYmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHRoaXMuY29uZmlnU2VydmljZS5kb21lbiArICdyZWxhdGlvbnNoaXAvcmVtb3ZldXNlcmZyb21jb250YWN0bGlzdCcsXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uc2hpcElkOiByZWxhdGlvbnNoaXBJZCxcclxuICAgICAgICAgICAgICAgIHJlbGF0aW9uc2hpcFJlcXVlc3RJZDogcmVsYXRpb25zaGlwUmVxdWVzdElkLFxyXG4gICAgICAgICAgICAgICAgY3VycmVudFVzZXJJZDogY3VycmVudFVzZXJJZCxcclxuICAgICAgICAgICAgICAgIHRhcmdldFVzZXJJZDogdGFyZ2V0VXNlcklkXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5tYXAoKHJlczogYW55KSA9PiByZXMuanNvbigpKVxyXG4gICAgICAgICAgICAuZmxhdE1hcChyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFyZXN1bHQuc3RhdHVzWydlcnJvciddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q29udGFjdHNMaXN0KGN1cnJlbnRVc2VySWQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEV2ZW50IG9uIGRlbGV0ZWQgdXNlciBmcm9tIG15IGNvbnRhY3QgbGlzdFxyXG4gICAgICogQHBhcmFtIGRhdGFcclxuICAgICAqL1xyXG4gICAgcHVibGljIGRlbGV0ZVVzZXJGcm9tQ29udGFjdExpc3QoZGF0YSkge1xyXG4gICAgICAgIHRoaXMuc29ja2V0U2VydmljZS5zb2NrZXQuZW1pdChcImRlbGV0ZV91c2VyX2Zyb21fbXlfbGlzdFwiLCBkYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExpc3RlbiB0byB0aGUgZXZlbnQgdG8gZGVsZXRlIG1lIGJ5IGFub3RoZXIgdXNlciBmcm9tIHRoZSBsaXN0IG9mIHVzZXJzIChpZiBhbm90aGVyIHVzZXIgZGVsZXRlIG1lIGZyb20gaGltIHVzZXJzIGxpc3QsIHRoZW4gY2F0Y2ggdGhpcyBldmVudClcclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzb2NrZXRPblVzZXJXYXNEZWxldGVkRnJvbUNvbnRhY3RMaXN0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnNvY2tldFNlcnZpY2Uuc29ja2V0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNvY2tldFNlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5mcm9tRXZlbnQ8YW55PihcInVzZXJfd2FzX2RlbGV0ZWRfZnJvbV9jb250YWN0X2xpc3RcIilcclxuICAgICAgICAgICAgICAgIC5tYXAoZGF0YSA9PiBkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXNldCBhbGwgZGF0YS4gVGhpcyBmdW5jdGlvbiBjYWxsIHdoZW4gdXNlciBleGl0IGZyb20gcHJvZ3JhbSBhbmQgbmVlZCB0byBjbGVhciBkYXRhLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcmVzZXREYXRhKCkge1xyXG4gICAgICAgIHRoaXMuY29udGFjdHMgPSBbXTtcclxuICAgICAgICB0aGlzLnRhc2tzID0ge307XHJcbiAgICAgICAgdGhpcy50YXNrS2V5cyA9IFtdO1xyXG4gICAgfVxyXG59Il19
