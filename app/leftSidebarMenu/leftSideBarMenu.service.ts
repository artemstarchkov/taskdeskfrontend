import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {Subject} from "rxjs/Subject";
import {ConfigService} from "../sharedService/config.service";
import "rxjs/add/operator/switchMap";
import "rxjs/add/operator/mergeMap";
import {SocketService} from "../sharedService/socket.service";

@Injectable()
export class LeftSideBarMenuService {

    public contacts: any = [];
    public tasks: any = {};
    public taskKeys: any = [];

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService,
                public socketService: SocketService) {}

    /**
     * Get user's contact list
     * @param {number} currentUserId
     * @returns {Observable<any>}
     */
    public getContactsList(currentUserId: number) {
        return this.http.post(this.configService.domen + 'relationship/getcontactslist', {currentUserId: currentUserId})
            .map(res => res.json());
    }

    /**
     * Get user's tasks with statuses new or in progress
     * @param {number} currentUserId
     * @returns {Observable<any>}
     */
    public getTasks(currentUserId: number) {
        return this.http.post(this.configService.domen + 'task/gettasks', {currentUserId: currentUserId})
            .map(res => res.json());
    }

    /**
     * Get user's tasks with status "donw"
     * @param {number} currentUserId
     * @returns {Observable<any>}
     */
    public getTasksWithStatusDone(currentUserId: number) {
        return this.http.post(this.configService.domen + 'task/get_tasks_with_status_done', {currentUserId: currentUserId})
            .map(res => res.json());
    }

    /**
     * Get tasks appointed by me (current user)
     * @param {number} currentUserId
     * @returns {Observable<any>}
     */
    public getTasksAppointedByMe(currentUserId: number) {
        return this.http.post(this.configService.domen + 'task/get_tasks_appointed_by_me', {currentUserId: currentUserId})
            .map(res => res.json());
    }

    /**
     * Remove user from user's contact list
     *
     * @param {number} relationshipId
     * @param {number} relationshipRequestId
     * @returns {Observable<any>}
     */
    public removeUserFromContactList(relationshipId: number, relationshipRequestId: number, currentUserId: number, targetUserId: number) {
        return this.http.post(this.configService.domen + 'relationship/removeuserfromcontactlist',
            {
                relationshipId: relationshipId,
                relationshipRequestId: relationshipRequestId,
                currentUserId: currentUserId,
                targetUserId: targetUserId
            })
            .map((res: any) => res.json())
            .flatMap(result => {
                if (!result.status['error']) {
                    return this.getContactsList(currentUserId);
                } else {
                    return result;
                }
            });
    }

    /**
     * Event on deleted user from my contact list
     * @param data
     */
    public deleteUserFromContactList(data) {
        this.socketService.socket.emit("delete_user_from_my_list", data);
    }

    /**
     * Listen to the event to delete me by another user from the list of users (if another user delete me from him users list, then catch this event)
     * @returns {Observable<any>}
     */
    public socketOnUserWasDeletedFromContactList() {
        if (this.socketService.socket) {
            return this.socketService
                .fromEvent<any>("user_was_deleted_from_contact_list")
                .map(data => data);
        }
    }

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.contacts = [];
        this.tasks = {};
        this.taskKeys = [];
    }
}