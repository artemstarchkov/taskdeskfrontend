System.register(["@angular/core", "./leftSideBarMenu.service", "../sharedService/config.service", "../chat/chat.service", "../sharedService/socket.service", "../task/task.service", "../task/task.model", "ng2-translate"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, leftSideBarMenu_service_1, config_service_1, chat_service_1, socket_service_1, task_service_1, task_model_1, ng2_translate_1, LeftSideBarMenuComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (leftSideBarMenu_service_1_1) {
                leftSideBarMenu_service_1 = leftSideBarMenu_service_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (chat_service_1_1) {
                chat_service_1 = chat_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            },
            function (task_service_1_1) {
                task_service_1 = task_service_1_1;
            },
            function (task_model_1_1) {
                task_model_1 = task_model_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            }
        ],
        execute: function () {
            LeftSideBarMenuComponent = class LeftSideBarMenuComponent {
                constructor(leftSideBarMenuService, configService, chatService, socketService, taskService, taskModel, translate) {
                    this.leftSideBarMenuService = leftSideBarMenuService;
                    this.configService = configService;
                    this.chatService = chatService;
                    this.socketService = socketService;
                    this.taskService = taskService;
                    this.taskModel = taskModel;
                    this.translate = translate;
                    this.menu = 'contacts';
                    this.taskTitleLength = 23;
                    this.taskFromUsernameLength = 25;
                    this.usernameLength = 25;
                    this.fullNameLength = 23;
                    this.isHideLeftSidebarMenuComponent = false;
                }
                ngOnInit() {
                    this._subGetContactsList = this.leftSideBarMenuService.getContactsList(this.configService.user.id).subscribe(data => {
                        let counterMessage = [];
                        let counterTask = [];
                        data.contacts.forEach(function (contact) {
                            counterMessage[contact.id] = Number(contact.addInfo.countNewMessages);
                            counterTask[contact.id] = Number(contact.addInfo.countNewTasks);
                        });
                        this.chatService.chatMessagesCounter = counterMessage;
                        this.taskService.tasksCounter = counterTask;
                        this.leftSideBarMenuService.contacts = data.contacts;
                    });
                    if (this.socketService.socket) {
                        this._subUserWasDeletedFromContactList = this.leftSideBarMenuService.socketOnUserWasDeletedFromContactList().subscribe((data) => {
                            this.onGetContactList();
                            if (data.from in this.chatService.chatRooms) {
                                this.chatService.chatRooms.splice(data.from, 1);
                            }
                            if (data.from in this.chatService.chatWindowsState) {
                                this.chatService.chatWindowsState.splice(data.from, 1);
                            }
                            if (data.from in this.taskService.taskWindowsState) {
                                this.taskService.taskWindowsState.splice(data.from, 1);
                            }
                        });
                        this._subGetNewTask = this.taskService.socketOnGetNewTask().subscribe((data) => {
                            this.taskService.tasksCounter[data.from] += 1;
                        });
                    }
                }
                ngOnDestroy() {
                    if (this._subGetContactsList) {
                        this._subGetContactsList.unsubscribe();
                    }
                    if (this._subRemoveUserFromContactList) {
                        this._subRemoveUserFromContactList.unsubscribe();
                    }
                    if (this._subGetChatCorrespondence) {
                        this._subGetChatCorrespondence.unsubscribe();
                    }
                    if (this._subMarkChatCorrespondenceAsRead) {
                        this._subMarkChatCorrespondenceAsRead.unsubscribe();
                    }
                    if (this._subGetNewTask) {
                        this._subGetNewTask.unsubscribe();
                    }
                    if (this._subGetTasks) {
                        this._subGetTasks.unsubscribe();
                    }
                    if (this._subGetDoneTasks) {
                        this._subGetDoneTasks.unsubscribe();
                    }
                    if (this._subGetAppointedByMeTasks) {
                        this._subGetAppointedByMeTasks.unsubscribe();
                    }
                }
                onRemoveUserFromContactList(relationshipId, relationshipRequestId, currentUserId, targetUserId) {
                    this._subRemoveUserFromContactList = this.leftSideBarMenuService.removeUserFromContactList(relationshipId, relationshipRequestId, currentUserId, targetUserId)
                        .subscribe((data) => {
                        this.leftSideBarMenuService.contacts = data.contacts;
                        this.chatService.chatRooms.splice(targetUserId, 1);
                        this.chatService.chatWindowsState.splice(targetUserId, 1);
                        this.taskService.taskWindowsState.splice(targetUserId, 1);
                        let deletedUserInfo = {
                            to: targetUserId,
                            from: currentUserId,
                            relationshipId: relationshipId,
                            relationshipRequestId: relationshipRequestId
                        };
                        this.leftSideBarMenuService.deleteUserFromContactList(deletedUserInfo);
                    });
                }
                onGetContactList() {
                    this._subGetContactsList = this.leftSideBarMenuService.getContactsList(this.configService.user.id).subscribe(data => {
                        let counterMessage = [];
                        let counterTask = [];
                        data.contacts.forEach(function (contact) {
                            console.info(contact);
                            counterMessage[contact.id] = Number(contact.addInfo.countNewMessages);
                            counterTask[contact.id] = Number(contact.addInfo.countNewTasks);
                        });
                        this.chatService.chatMessagesCounter = counterMessage;
                        this.taskService.tasksCounter = counterTask;
                        this.leftSideBarMenuService.contacts = data.contacts;
                    });
                }
                onGetTasks() {
                    this._subGetTasks = this.leftSideBarMenuService.getTasks(this.configService.user.id).subscribe((data) => {
                        this.leftSideBarMenuService.taskKeys = Object.keys(data.groupedByFromUserTasks);
                        this.leftSideBarMenuService.tasks = data.groupedByFromUserTasks;
                        this.taskService.tasksList = data.tasksList;
                    });
                }
                onGetTasksWithStatusDone() {
                    this._subGetDoneTasks = this.leftSideBarMenuService.getTasksWithStatusDone(this.configService.user.id).subscribe((data) => {
                        this.taskService.doneTasks = data.doneTasks;
                        this.taskService.isShowDoneTasksWindow = true;
                        this.taskService.isShowAppointedByMeTasksWindow = false;
                        this.chatService.chatWindowsState[this.chatService.receiverId] = false;
                        this.taskService.taskWindowsState[this.taskModel.to] = false;
                        this.taskService.currentTaskId = '';
                        this.taskModel.resetModel();
                    });
                }
                onShowHideLeftSidebarMenuComponent(event) {
                    let searchInputElement = event.target.closest('#custom-search-input');
                    let searchBodyElement = event.target.closest('#custom-search-body');
                    this.isHideLeftSidebarMenuComponent = !!searchInputElement == true || !!searchBodyElement == true ? true : false;
                }
                onStartChat(receiverId = '', receiverFirstName = '', receiverSecondName = '', receiverLastName = '', receiverUsername = '') {
                    if (!!this.chatService.receiverId && !!this.chatService.chatRooms[this.chatService.receiverId] && this.chatService.chatRooms[this.chatService.receiverId].length > 0 && this.chatService.receiverId != receiverId) {
                        this._subMarkChatCorrespondenceAsRead = this.chatService.markChatCorrespondenceAsRead(this.chatService.senderId, this.chatService.receiverId).subscribe((data) => {
                            console.info(data);
                        });
                    }
                    this.chatService.receiverId = receiverId;
                    this.chatService.receiverFirstName = receiverFirstName;
                    this.chatService.receiverSecondName = receiverSecondName;
                    this.chatService.receiverLastName = receiverLastName;
                    this.chatService.receiverUsername = receiverUsername;
                    if (!this.chatService.chatRooms[receiverId] || this.chatService.chatRooms[receiverId].length <= 0) {
                        this._subGetChatCorrespondence = this.chatService.getChatCorrespondence(this.chatService.senderId, this.chatService.receiverId).subscribe((data) => {
                            this.chatService.chatRooms[receiverId] = [];
                            this.chatService.chatRooms[receiverId] = data.correspondence;
                            if (!this.chatService.chatWindowsState[receiverId]) {
                                this.chatService.chatWindowsState[receiverId] = true;
                                this.taskService.isShowAppointedByMeTasksWindow = false;
                                this.taskService.taskWindowsState[this.taskModel.to] = false;
                                this.taskService.currentTaskId = '';
                                this.taskModel.resetModel();
                                this.taskService.isShowDoneTasksWindow = false;
                            }
                            this.chatService.chatMessagesCounter[receiverId] = 0;
                        });
                    }
                    else {
                        if (!this.chatService.chatWindowsState[receiverId]) {
                            this.chatService.chatWindowsState[receiverId] = true;
                            this.chatService.chatMessagesCounter[receiverId] = 0;
                            this.taskService.isShowAppointedByMeTasksWindow = false;
                            this.taskService.taskWindowsState[this.taskModel.to] = false;
                            this.taskService.currentTaskId = '';
                            this.taskModel.resetModel();
                            this.taskService.isShowDoneTasksWindow = false;
                        }
                    }
                }
                onOpenTaskWindow(currentUserId, targetUserId) {
                    if (!this.taskService.taskWindowsState[targetUserId]) {
                        if (this.taskService.taskWindowsState[this.taskModel.to]) {
                            this.taskService.taskWindowsState[this.taskModel.to] = false;
                        }
                        this.taskModel.resetModel();
                        this.taskModel.to = targetUserId;
                        this.taskModel.from = currentUserId;
                        this.taskService.taskWindowsState[targetUserId] = true;
                        this.taskService.isShowAppointedByMeTasksWindow = false;
                        this.chatService.chatWindowsState[this.chatService.receiverId] = false;
                        this.taskService.currentTaskId = '';
                        this.taskService.isShowDoneTasksWindow = false;
                    }
                    else {
                        this.taskService.taskWindowsState[targetUserId] = false;
                    }
                }
                onSelectTask(taskId) {
                    if (taskId != this.taskService.currentTaskId) {
                        this.taskService.currentTaskId = taskId;
                        this.taskModel.status = this.taskService.tasksList[taskId].status;
                        this.taskService.isShowAppointedByMeTasksWindow = false;
                        this.chatService.chatWindowsState[this.chatService.receiverId] = false;
                        this.taskService.taskWindowsState[this.taskModel.to] = false;
                        this.taskService.isShowDoneTasksWindow = false;
                    }
                }
                onGetTasksAppointedByMe() {
                    this._subGetAppointedByMeTasks = this.leftSideBarMenuService.getTasksAppointedByMe(this.configService.user.id).subscribe((data) => {
                        this.taskService.appointedByMeTasks = data.tasks;
                        this.taskService.isShowAppointedByMeTasksWindow = true;
                        this.taskService.isShowDoneTasksWindow = false;
                        this.chatService.chatWindowsState[this.chatService.receiverId] = false;
                        this.taskService.taskWindowsState[this.taskModel.to] = false;
                        this.taskService.currentTaskId = '';
                        this.taskModel.resetModel();
                    });
                }
                onGetClassColorByTaskStatus(taskStatus) {
                    let statusClasses = {
                        'new': 'task-status-indicator-new',
                        'in progress': 'task-status-indicator-in-progress',
                        'pause': 'task-status-indicator-pause'
                    };
                    return statusClasses[taskStatus];
                }
                onGetClassTextColorByTaskStatus(taskStatus) {
                    let statusClasses = {
                        'new': 'task-text-color-status-new',
                        'in progress': 'task-text-color-status-in-progress',
                        'pause': 'task-text-color-status-pause'
                    };
                    return statusClasses[taskStatus];
                }
                onCutStringByLength(str, length) {
                    return str.length <= length ? str : str.substring(0, length);
                }
            };
            LeftSideBarMenuComponent = __decorate([
                core_1.Component({
                    selector: 'leftSideBarMenu',
                    templateUrl: 'app/leftSidebarMenu/leftSidebarMenu.component.html',
                    styleUrls: ['app/leftSidebarMenu/leftSideBarMenu.component.scss'],
                    host: {
                        '(document:click)': 'onShowHideLeftSidebarMenuComponent($event)',
                    }
                }),
                __metadata("design:paramtypes", [leftSideBarMenu_service_1.LeftSideBarMenuService,
                    config_service_1.ConfigService,
                    chat_service_1.ChatService,
                    socket_service_1.SocketService,
                    task_service_1.TaskService,
                    task_model_1.TaskModel,
                    ng2_translate_1.TranslateService])
            ], LeftSideBarMenuComponent);
            exports_1("LeftSideBarMenuComponent", LeftSideBarMenuComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZWJhck1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBb0JhLHdCQUF3QixHQUFyQztnQkFpQkksWUFBbUIsc0JBQThDLEVBQzlDLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLFNBQW9CLEVBQ3BCLFNBQTJCO29CQU4zQiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO29CQUM5QyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFDNUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7b0JBQ3hCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO29CQUM1QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtvQkFDeEIsY0FBUyxHQUFULFNBQVMsQ0FBVztvQkFDcEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7b0JBdEJ2QyxTQUFJLEdBQVEsVUFBVSxDQUFDO29CQUN2QixvQkFBZSxHQUFXLEVBQUUsQ0FBQztvQkFDN0IsMkJBQXNCLEdBQVcsRUFBRSxDQUFDO29CQUNwQyxtQkFBYyxHQUFXLEVBQUUsQ0FBQztvQkFDNUIsbUJBQWMsR0FBVyxFQUFFLENBQUM7b0JBQzVCLG1DQUE4QixHQUFZLEtBQUssQ0FBQztnQkFpQk4sQ0FBQztnQkFFbEQsUUFBUTtvQkFFSixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBR2hILElBQUksY0FBYyxHQUFHLEVBQUUsQ0FBQzt3QkFHeEIsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO3dCQUdyQixJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLE9BQU87NEJBQ25DLGNBQWMsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs0QkFDdEUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDcEUsQ0FBQyxDQUFDLENBQUM7d0JBR0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsR0FBRyxjQUFjLENBQUM7d0JBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQzt3QkFDNUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUN6RCxDQUFDLENBQUMsQ0FBQztvQkFFSCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7d0JBRzVCLElBQUksQ0FBQyxpQ0FBaUMsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMscUNBQXFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTs0QkFDakksSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7NEJBRXhCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dDQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQzs0QkFDcEQsQ0FBQzs0QkFFRCxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dDQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDOzRCQUMzRCxDQUFDOzRCQUVELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7Z0NBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7NEJBQzNELENBQUM7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7d0JBR0gsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7NEJBQ2hGLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2xELENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUM7Z0JBQ0wsQ0FBQztnQkFLRCxXQUFXO29CQUNQLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7d0JBQzNCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDM0MsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO3dCQUNyQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3JELENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQzt3QkFDakMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNqRCxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7d0JBQ3hDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDeEQsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzt3QkFDdEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDdEMsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDcEMsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO3dCQUN4QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3hDLENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQzt3QkFDakMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNqRCxDQUFDO2dCQUNMLENBQUM7Z0JBU0QsMkJBQTJCLENBQUMsY0FBc0IsRUFBRSxxQkFBNkIsRUFBRSxhQUFxQixFQUFFLFlBQW9CO29CQUMxSCxJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHlCQUF5QixDQUFDLGNBQWMsRUFBRSxxQkFBcUIsRUFBRSxhQUFhLEVBQUUsWUFBWSxDQUFDO3lCQUN6SixTQUFTLENBQUMsQ0FBQyxJQUFRLEVBQUUsRUFBRTt3QkFDcEIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO3dCQUNyRCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNsRCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3pELElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBQyxDQUFDLENBQUMsQ0FBQzt3QkFFekQsSUFBSSxlQUFlLEdBQUc7NEJBQ3BCLEVBQUUsRUFBRSxZQUFZOzRCQUNoQixJQUFJLEVBQUUsYUFBYTs0QkFDbkIsY0FBYyxFQUFFLGNBQWM7NEJBQzlCLHFCQUFxQixFQUFFLHFCQUFxQjt5QkFDN0MsQ0FBQzt3QkFHRixJQUFJLENBQUMsc0JBQXNCLENBQUMseUJBQXlCLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQzNFLENBQUMsQ0FBQyxDQUFBO2dCQUNWLENBQUM7Z0JBS0QsZ0JBQWdCO29CQUNaLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFFaEgsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDO3dCQUd4QixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7d0JBR3JCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsT0FBTzs0QkFDcEMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzs0QkFDckIsY0FBYyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUN0RSxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUNwRSxDQUFDLENBQUMsQ0FBQzt3QkFHSCxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixHQUFHLGNBQWMsQ0FBQzt3QkFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEdBQUcsV0FBVyxDQUFDO3dCQUM1QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ3pELENBQUMsQ0FBQyxDQUFBO2dCQUNOLENBQUM7Z0JBS0QsVUFBVTtvQkFDTixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7d0JBQ3pHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQzt3QkFDaEYsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUM7d0JBQ2hFLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ2hELENBQUMsQ0FBQyxDQUFBO2dCQUNOLENBQUM7Z0JBS0Qsd0JBQXdCO29CQUNwQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO3dCQUMzSCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO3dCQUM1QyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzt3QkFHOUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7d0JBRXhELElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBRXZFLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBRTdELElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQzt3QkFFcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLENBQUE7Z0JBQ04sQ0FBQztnQkFNRCxrQ0FBa0MsQ0FBQyxLQUFLO29CQUNwQyxJQUFJLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7b0JBQ3RFLElBQUksaUJBQWlCLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFFcEUsSUFBSSxDQUFDLDhCQUE4QixHQUFHLENBQUMsQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3BILENBQUM7Z0JBUUQsV0FBVyxDQUFDLGFBQW9CLEVBQUUsRUFBRSxvQkFBNEIsRUFBRSxFQUFFLHFCQUE2QixFQUFFLEVBQUUsbUJBQTJCLEVBQUUsRUFBRSxtQkFBMkIsRUFBRTtvQkFFN0osRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDO3dCQUNoTixJQUFJLENBQUMsZ0NBQWdDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyw0QkFBNEIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFOzRCQUNsSyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUN2QixDQUFDLENBQUMsQ0FBQztvQkFDUCxDQUFDO29CQUVELElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztvQkFDekMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztvQkFDekQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztvQkFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQztvQkFJckQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEcsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTs0QkFDcEosSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDOzRCQUM1QyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDOzRCQUU3RCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQztnQ0FHckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7Z0NBRXhELElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7Z0NBRTdELElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztnQ0FFcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQ0FFNUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7NEJBQ25ELENBQUM7NEJBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQ3pELENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDakQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7NEJBQ3JELElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDOzRCQUdyRCxJQUFJLENBQUMsV0FBVyxDQUFDLDhCQUE4QixHQUFHLEtBQUssQ0FBQzs0QkFFeEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQzs0QkFFN0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDOzRCQUVwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUU1QixJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQzt3QkFDbkQsQ0FBQztvQkFDTCxDQUFDO2dCQUNMLENBQUM7Z0JBT0QsZ0JBQWdCLENBQUMsYUFBYSxFQUFFLFlBQVk7b0JBQ3hDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBR25ELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBQ2pFLENBQUM7d0JBR0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDNUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEdBQUcsWUFBWSxDQUFDO3dCQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7d0JBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDO3dCQUd2RCxJQUFJLENBQUMsV0FBVyxDQUFDLDhCQUE4QixHQUFHLEtBQUssQ0FBQzt3QkFFeEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUssQ0FBQzt3QkFFdkUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO3dCQUVwQyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztvQkFDbkQsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxHQUFHLEtBQUssQ0FBQztvQkFDNUQsQ0FBQztnQkFDTCxDQUFDO2dCQU1ELFlBQVksQ0FBQyxNQUFjO29CQUN2QixFQUFFLENBQUMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO3dCQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7d0JBQ3hDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQzt3QkFHbEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7d0JBRXhELElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBRXZFLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBRTdELElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO29CQUNuRCxDQUFDO2dCQUNMLENBQUM7Z0JBS0QsdUJBQXVCO29CQUNuQixJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO3dCQUNuSSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7d0JBQ2pELElBQUksQ0FBQyxXQUFXLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDO3dCQUd2RCxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQzt3QkFFL0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUssQ0FBQzt3QkFFdkUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQzt3QkFFN0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO3dCQUVwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNoQyxDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDO2dCQUtELDJCQUEyQixDQUFDLFVBQWtCO29CQUMxQyxJQUFJLGFBQWEsR0FBUTt3QkFDckIsS0FBSyxFQUFFLDJCQUEyQjt3QkFDbEMsYUFBYSxFQUFFLG1DQUFtQzt3QkFDbEQsT0FBTyxFQUFFLDZCQUE2QjtxQkFDekMsQ0FBQztvQkFFRixNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDO2dCQUtELCtCQUErQixDQUFDLFVBQWtCO29CQUM5QyxJQUFJLGFBQWEsR0FBUTt3QkFDckIsS0FBSyxFQUFFLDRCQUE0Qjt3QkFDbkMsYUFBYSxFQUFFLG9DQUFvQzt3QkFDbkQsT0FBTyxFQUFFLDhCQUE4QjtxQkFDMUMsQ0FBQztvQkFFRixNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDO2dCQVFELG1CQUFtQixDQUFDLEdBQVcsRUFBRSxNQUFjO29CQUMzQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ2hFLENBQUM7YUFDSixDQUFBO1lBalhZLHdCQUF3QjtnQkFUcEMsZ0JBQVMsQ0FBQztvQkFDUCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixXQUFXLEVBQUUsb0RBQW9EO29CQUNqRSxTQUFTLEVBQUUsQ0FBQyxvREFBb0QsQ0FBQztvQkFDakUsSUFBSSxFQUFFO3dCQUNGLGtCQUFrQixFQUFFLDRDQUE0QztxQkFDbkU7aUJBQ0osQ0FBQztpREFtQjZDLGdEQUFzQjtvQkFDL0IsOEJBQWE7b0JBQ2YsMEJBQVc7b0JBQ1QsOEJBQWE7b0JBQ2YsMEJBQVc7b0JBQ2Isc0JBQVM7b0JBQ1QsZ0NBQWdCO2VBdkJyQyx3QkFBd0IsQ0FpWHBDOztRQUFBLENBQUMiLCJmaWxlIjoibGVmdFNpZGViYXJNZW51L2xlZnRTaWRlYmFyTWVudS5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3l9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge0xlZnRTaWRlQmFyTWVudVNlcnZpY2V9IGZyb20gXCIuL2xlZnRTaWRlQmFyTWVudS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7Q29uZmlnU2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZFNlcnZpY2UvY29uZmlnLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtTdWJzY3JpcHRpb259IGZyb20gXCJyeGpzL1N1YnNjcmlwdGlvblwiO1xyXG5pbXBvcnQge0NoYXRTZXJ2aWNlfSBmcm9tIFwiLi4vY2hhdC9jaGF0LnNlcnZpY2VcIjtcclxuaW1wb3J0IHtTb2NrZXRTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9zb2NrZXQuc2VydmljZVwiO1xyXG5pbXBvcnQge1Rhc2tTZXJ2aWNlfSBmcm9tIFwiLi4vdGFzay90YXNrLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtUYXNrTW9kZWx9IGZyb20gXCIuLi90YXNrL3Rhc2subW9kZWxcIjtcclxuaW1wb3J0IHtPYmplY3RLZXlzUGlwZX0gZnJvbSBcIi4uL3NoYXJlZFBpcGUvb2JqZWN0S2V5cy5waXBlXCI7XHJcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSBcIm5nMi10cmFuc2xhdGVcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdsZWZ0U2lkZUJhck1lbnUnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhcHAvbGVmdFNpZGViYXJNZW51L2xlZnRTaWRlYmFyTWVudS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnYXBwL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGhvc3Q6IHtcclxuICAgICAgICAnKGRvY3VtZW50OmNsaWNrKSc6ICdvblNob3dIaWRlTGVmdFNpZGViYXJNZW51Q29tcG9uZW50KCRldmVudCknLFxyXG4gICAgfVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIExlZnRTaWRlQmFyTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIHB1YmxpYyBtZW51OiBhbnkgPSAnY29udGFjdHMnO1xyXG4gICAgcHVibGljIHRhc2tUaXRsZUxlbmd0aDogbnVtYmVyID0gMjM7XHJcbiAgICBwdWJsaWMgdGFza0Zyb21Vc2VybmFtZUxlbmd0aDogbnVtYmVyID0gMjU7XHJcbiAgICBwdWJsaWMgdXNlcm5hbWVMZW5ndGg6IG51bWJlciA9IDI1O1xyXG4gICAgcHVibGljIGZ1bGxOYW1lTGVuZ3RoOiBudW1iZXIgPSAyMztcclxuICAgIHB1YmxpYyBpc0hpZGVMZWZ0U2lkZWJhck1lbnVDb21wb25lbnQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIHByaXZhdGUgX3N1YkdldENvbnRhY3RzTGlzdDogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViUmVtb3ZlVXNlckZyb21Db250YWN0TGlzdDogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViR2V0Q2hhdENvcnJlc3BvbmRlbmNlOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJNYXJrQ2hhdENvcnJlc3BvbmRlbmNlQXNSZWFkOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJVc2VyV2FzRGVsZXRlZEZyb21Db250YWN0TGlzdDogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViR2V0TmV3VGFzazogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViR2V0VGFza3M6IFN1YnNjcmlwdGlvbjtcclxuICAgIHByaXZhdGUgX3N1YkdldERvbmVUYXNrczogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViR2V0QXBwb2ludGVkQnlNZVRhc2tzOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGxlZnRTaWRlQmFyTWVudVNlcnZpY2U6IExlZnRTaWRlQmFyTWVudVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBjaGF0U2VydmljZTogQ2hhdFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgc29ja2V0U2VydmljZTogU29ja2V0U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0YXNrU2VydmljZTogVGFza1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgdGFza01vZGVsOiBUYXNrTW9kZWwsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlKSB7fVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIC8vV2hlbiB0aGUgXCJMZWZ0U2lkZUJhck1lbnVDb21wb25lbnRcIiBpcyBpbml0aWFsaXplZCwgd2UgaW1tZWRpYXRlbHkgZ2V0IGEgbGlzdCBvZiB1c2Vyc1xyXG4gICAgICAgIHRoaXMuX3N1YkdldENvbnRhY3RzTGlzdCA9IHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5nZXRDb250YWN0c0xpc3QodGhpcy5jb25maWdTZXJ2aWNlLnVzZXIuaWQpLnN1YnNjcmliZShkYXRhID0+IHtcclxuXHJcbiAgICAgICAgICAgIC8vIFRoaXMgaXMgYSB0ZW1wb3Jhcnkgc3RvcmUgZm9yIHRoZSBudW1iZXIgb2YgbmV3IG1lc3NhZ2VzIGZyb20gdXNlcnMgaW4gdGhlIGxpc3RcclxuICAgICAgICAgICAgbGV0IGNvdW50ZXJNZXNzYWdlID0gW107XHJcblxyXG4gICAgICAgICAgICAvLyBUaGlzIGlzIGEgdGVtcG9yYXJ5IHN0b3JlIGZvciB0aGUgbnVtYmVyIG9mIG5ldyB0YXNrcyBmcm9tIHVzZXJzIGluIHRoZSBsaXN0XHJcbiAgICAgICAgICAgIGxldCBjb3VudGVyVGFzayA9IFtdO1xyXG5cclxuICAgICAgICAgICAgLy8gV2UgcGFzcyBvbiBlYWNoIGNvbnRhY3QodXNlcikgYW5kIGZvcm0gYXJyYXlzIGJ5IHRoZSBudW1iZXIgb2YgbmV3IG1lc3NhZ2VzIGFuZCBuZXcgdGFza3NcclxuICAgICAgICAgICAgZGF0YS5jb250YWN0cy5mb3JFYWNoKGZ1bmN0aW9uIChjb250YWN0KSB7XHJcbiAgICAgICAgICAgICAgICBjb3VudGVyTWVzc2FnZVtjb250YWN0LmlkXSA9IE51bWJlcihjb250YWN0LmFkZEluZm8uY291bnROZXdNZXNzYWdlcyk7XHJcbiAgICAgICAgICAgICAgICBjb3VudGVyVGFza1tjb250YWN0LmlkXSA9IE51bWJlcihjb250YWN0LmFkZEluZm8uY291bnROZXdUYXNrcyk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gQXNzaWduIHRoZSBjb250YWN0cyBhbmQgZ2VuZXJhdGVkIGFycmF5cyB3aXRoIHRoZSBudW1iZXIgb2YgbmV3IG1lc3NhZ2VzIGFuZCB0YXNrcyB0byB0aGUgc2VydmljZSB2YXJpYWJsZXMgdG8gcXVpY2tseSBhY2Nlc3MgdGhpcyBkYXRhXHJcbiAgICAgICAgICAgIHRoaXMuY2hhdFNlcnZpY2UuY2hhdE1lc3NhZ2VzQ291bnRlciA9IGNvdW50ZXJNZXNzYWdlO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnRhc2tzQ291bnRlciA9IGNvdW50ZXJUYXNrO1xyXG4gICAgICAgICAgICB0aGlzLmxlZnRTaWRlQmFyTWVudVNlcnZpY2UuY29udGFjdHMgPSBkYXRhLmNvbnRhY3RzO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zb2NrZXRTZXJ2aWNlLnNvY2tldCkge1xyXG4gICAgICAgICAgICAvLyBTdWJzY3JpYmVkIG9uIGV2ZW50IHdoZW4gdXNlciB3YXMgZGVsZXRlZCBmcm9tIGNvbnRhY3QgbGlzdCBieSBtZSBvciByZW1vdGUgdXNlciB0byB1cGRhdGUgZGF0YSBieSBjb250YWN0IGxpc3QsIG51bWJlciBvZiBuZXcgbWVzc2FnZXMgYW5kIHRhc2tzLlxyXG4gICAgICAgICAgICAvLyBSZW1vdmUgY2hhdCdzIGNvcnJlc3BvbmRlbmNlLCBjaGF0J3Mgd2luZG93IHN0YXRlIGFuZCB0YXNrJ3Mgd2luZG93IHN0YXRlIGZvciBkZWxldGVkIHVzZXIuXHJcbiAgICAgICAgICAgIHRoaXMuX3N1YlVzZXJXYXNEZWxldGVkRnJvbUNvbnRhY3RMaXN0ID0gdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLnNvY2tldE9uVXNlcldhc0RlbGV0ZWRGcm9tQ29udGFjdExpc3QoKS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vbkdldENvbnRhY3RMaXN0KCk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGEuZnJvbSBpbiB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRSb29tcykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFNlcnZpY2UuY2hhdFJvb21zLnNwbGljZShkYXRhLmZyb20sIDEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmKGRhdGEuZnJvbSBpbiB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRXaW5kb3dzU3RhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRXaW5kb3dzU3RhdGUuc3BsaWNlKGRhdGEuZnJvbSwgMSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoZGF0YS5mcm9tIGluIHRoaXMudGFza1NlcnZpY2UudGFza1dpbmRvd3NTdGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UudGFza1dpbmRvd3NTdGF0ZS5zcGxpY2UoZGF0YS5mcm9tLCAxKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBTdWJzY3JpYmVkIHRvIHJlY2VpdmUgbmV3IHRhc2tzIGZyb20gdGhlIHVzZXIgYW5kIGluY3JlYXNlIHRoZSBudW1iZXIgb2YgbmV3IHRhc2tzIGZyb20gdGhlIHNlbmRlclxyXG4gICAgICAgICAgICB0aGlzLl9zdWJHZXROZXdUYXNrID0gdGhpcy50YXNrU2VydmljZS5zb2NrZXRPbkdldE5ld1Rhc2soKS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50YXNrc0NvdW50ZXJbZGF0YS5mcm9tXSArPSAxO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCZWZvcmUgZGVzdHJveSBjb21wb25lbnQgdW5zdWJzY3JpYmVkIGZyb20gZXZlbnRzXHJcbiAgICAgKi9cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9zdWJHZXRDb250YWN0c0xpc3QpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViR2V0Q29udGFjdHNMaXN0LnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJSZW1vdmVVc2VyRnJvbUNvbnRhY3RMaXN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YlJlbW92ZVVzZXJGcm9tQ29udGFjdExpc3QudW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX3N1YkdldENoYXRDb3JyZXNwb25kZW5jZSkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJHZXRDaGF0Q29ycmVzcG9uZGVuY2UudW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX3N1Yk1hcmtDaGF0Q29ycmVzcG9uZGVuY2VBc1JlYWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViTWFya0NoYXRDb3JyZXNwb25kZW5jZUFzUmVhZC51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fc3ViR2V0TmV3VGFzaykge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJHZXROZXdUYXNrLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJHZXRUYXNrcykge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJHZXRUYXNrcy51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fc3ViR2V0RG9uZVRhc2tzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YkdldERvbmVUYXNrcy51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fc3ViR2V0QXBwb2ludGVkQnlNZVRhc2tzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YkdldEFwcG9pbnRlZEJ5TWVUYXNrcy51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSB1c2VyIGZyb20gY29udGFjdCBsaXN0IGFuZCByZXR1cm4gdXBkYXRlZCBjb250YWN0IGxpc3RcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSByZWxhdGlvbnNoaXBJZFxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHJlbGF0aW9uc2hpcFJlcXVlc3RJZFxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGN1cnJlbnRVc2VySWRcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB0YXJnZXRVc2VySWRcclxuICAgICAqL1xyXG4gICAgb25SZW1vdmVVc2VyRnJvbUNvbnRhY3RMaXN0KHJlbGF0aW9uc2hpcElkOiBudW1iZXIsIHJlbGF0aW9uc2hpcFJlcXVlc3RJZDogbnVtYmVyLCBjdXJyZW50VXNlcklkOiBudW1iZXIsIHRhcmdldFVzZXJJZDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fc3ViUmVtb3ZlVXNlckZyb21Db250YWN0TGlzdCA9IHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5yZW1vdmVVc2VyRnJvbUNvbnRhY3RMaXN0KHJlbGF0aW9uc2hpcElkLCByZWxhdGlvbnNoaXBSZXF1ZXN0SWQsIGN1cnJlbnRVc2VySWQsIHRhcmdldFVzZXJJZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YTphbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5jb250YWN0cyA9IGRhdGEuY29udGFjdHM7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRSb29tcy5zcGxpY2UodGFyZ2V0VXNlcklkLDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0V2luZG93c1N0YXRlLnNwbGljZSh0YXJnZXRVc2VySWQsMSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnRhc2tXaW5kb3dzU3RhdGUuc3BsaWNlKHRhcmdldFVzZXJJZCwxKTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgZGVsZXRlZFVzZXJJbmZvID0ge1xyXG4gICAgICAgICAgICAgICAgICB0bzogdGFyZ2V0VXNlcklkLFxyXG4gICAgICAgICAgICAgICAgICBmcm9tOiBjdXJyZW50VXNlcklkLFxyXG4gICAgICAgICAgICAgICAgICByZWxhdGlvbnNoaXBJZDogcmVsYXRpb25zaGlwSWQsXHJcbiAgICAgICAgICAgICAgICAgIHJlbGF0aW9uc2hpcFJlcXVlc3RJZDogcmVsYXRpb25zaGlwUmVxdWVzdElkXHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIC8vV2Ugc2VuZCBhIG1lc3NhZ2UgdG8gdGhlIHJlbW90ZSB1c2VyIHRoYXQgaXQgd2FzIHJlbW92ZWQgZnJvbSBteSBsaXN0IGFuZCBJIHdpbGwgYWxzbyBiZSByZW1vdmVkIGZyb20gaXRzIHVzZXIgbGlzdC5cclxuICAgICAgICAgICAgICAgIHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5kZWxldGVVc2VyRnJvbUNvbnRhY3RMaXN0KGRlbGV0ZWRVc2VySW5mbyk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdXNlcidzIGNvbnRhY3QgbGlzdFxyXG4gICAgICovXHJcbiAgICBvbkdldENvbnRhY3RMaXN0KCkge1xyXG4gICAgICAgIHRoaXMuX3N1YkdldENvbnRhY3RzTGlzdCA9IHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5nZXRDb250YWN0c0xpc3QodGhpcy5jb25maWdTZXJ2aWNlLnVzZXIuaWQpLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgLy8gVGhpcyBpcyBhIHRlbXBvcmFyeSBzdG9yZSBmb3IgdGhlIG51bWJlciBvZiBuZXcgbWVzc2FnZXMgZnJvbSB1c2VycyBpbiB0aGUgbGlzdFxyXG4gICAgICAgICAgICBsZXQgY291bnRlck1lc3NhZ2UgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIC8vIFRoaXMgaXMgYSB0ZW1wb3Jhcnkgc3RvcmUgZm9yIHRoZSBudW1iZXIgb2YgbmV3IHRhc2tzIGZyb20gdXNlcnMgaW4gdGhlIGxpc3RcclxuICAgICAgICAgICAgbGV0IGNvdW50ZXJUYXNrID0gW107XHJcblxyXG4gICAgICAgICAgICAvLyBXZSBwYXNzIG9uIGVhY2ggY29udGFjdCh1c2VyKSBhbmQgZm9ybSBhcnJheXMgYnkgdGhlIG51bWJlciBvZiBuZXcgbWVzc2FnZXMgYW5kIG5ldyB0YXNrc1xyXG4gICAgICAgICAgICBkYXRhLmNvbnRhY3RzLmZvckVhY2goZnVuY3Rpb24gKGNvbnRhY3QpIHtcclxuICAgICAgICAgICAgICAgY29uc29sZS5pbmZvKGNvbnRhY3QpO1xyXG4gICAgICAgICAgICAgICAgY291bnRlck1lc3NhZ2VbY29udGFjdC5pZF0gPSBOdW1iZXIoY29udGFjdC5hZGRJbmZvLmNvdW50TmV3TWVzc2FnZXMpO1xyXG4gICAgICAgICAgICAgICAgY291bnRlclRhc2tbY29udGFjdC5pZF0gPSBOdW1iZXIoY29udGFjdC5hZGRJbmZvLmNvdW50TmV3VGFza3MpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIEFzc2lnbiB0aGUgY29udGFjdHMgYW5kIGdlbmVyYXRlZCBhcnJheXMgd2l0aCB0aGUgbnVtYmVyIG9mIG5ldyBtZXNzYWdlcyBhbmQgdGFza3MgdG8gdGhlIHNlcnZpY2UgdmFyaWFibGVzIHRvIHF1aWNrbHkgYWNjZXNzIHRoaXMgZGF0YVxyXG4gICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRNZXNzYWdlc0NvdW50ZXIgPSBjb3VudGVyTWVzc2FnZTtcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50YXNrc0NvdW50ZXIgPSBjb3VudGVyVGFzaztcclxuICAgICAgICAgICAgdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLmNvbnRhY3RzID0gZGF0YS5jb250YWN0cztcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IHVzZXIncyB0YXNrcyAod2l0aCBzdGF0dXNlcyBcIm5ld1wiLCBcImluIHByb2dyZXNzXCIsIFwicGF1c2VcIilcclxuICAgICAqL1xyXG4gICAgb25HZXRUYXNrcygpIHtcclxuICAgICAgICB0aGlzLl9zdWJHZXRUYXNrcyA9IHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5nZXRUYXNrcyh0aGlzLmNvbmZpZ1NlcnZpY2UudXNlci5pZCkuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLnRhc2tLZXlzID0gT2JqZWN0LmtleXMoZGF0YS5ncm91cGVkQnlGcm9tVXNlclRhc2tzKTtcclxuICAgICAgICAgICAgdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLnRhc2tzID0gZGF0YS5ncm91cGVkQnlGcm9tVXNlclRhc2tzO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnRhc2tzTGlzdCA9IGRhdGEudGFza3NMaXN0O1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGFza3Mgd2l0aCBzdGF0dXMgXCJkb25lXCJcclxuICAgICAqL1xyXG4gICAgb25HZXRUYXNrc1dpdGhTdGF0dXNEb25lKCkge1xyXG4gICAgICAgIHRoaXMuX3N1YkdldERvbmVUYXNrcyA9IHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5nZXRUYXNrc1dpdGhTdGF0dXNEb25lKHRoaXMuY29uZmlnU2VydmljZS51c2VyLmlkKS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmRvbmVUYXNrcyA9IGRhdGEuZG9uZVRhc2tzO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmlzU2hvd0RvbmVUYXNrc1dpbmRvdyA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAvLyBDbG9zZSB3aW5kb3cgd2l0aCB0YXNrcyBhcHBvaW50ZWQgYnkgbWVcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5pc1Nob3dBcHBvaW50ZWRCeU1lVGFza3NXaW5kb3cgPSBmYWxzZTtcclxuICAgICAgICAgICAgLy8gQ2xvc2UgY3VycmVudCBjaGF0IHdpbmRvd1xyXG4gICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRXaW5kb3dzU3RhdGVbdGhpcy5jaGF0U2VydmljZS5yZWNlaXZlcklkXSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyBDbG9zZSBjdXJyZW50IGFkZCB0YXNrIHdpbmRvd1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnRhc2tXaW5kb3dzU3RhdGVbdGhpcy50YXNrTW9kZWwudG9dID0gZmFsc2U7XHJcbiAgICAgICAgICAgIC8vIENsb3NlIGluZm8gdGFzayB3aW5kb3dcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5jdXJyZW50VGFza0lkID0gJyc7XHJcbiAgICAgICAgICAgIC8vIFJlc2V0IHRhc2sgbW9kZWwgZGF0YVxyXG4gICAgICAgICAgICB0aGlzLnRhc2tNb2RlbC5yZXNldE1vZGVsKCk7XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIG9uIGNsaWNrIGJ5IHBhcnRzIG9mIGRvY3VtZW50IGFyZWEgYW5kIHNob3cvaGlkZSBjb21wb25lbnRcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICovXHJcbiAgICBvblNob3dIaWRlTGVmdFNpZGViYXJNZW51Q29tcG9uZW50KGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IHNlYXJjaElucHV0RWxlbWVudCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcjY3VzdG9tLXNlYXJjaC1pbnB1dCcpO1xyXG4gICAgICAgIGxldCBzZWFyY2hCb2R5RWxlbWVudCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcjY3VzdG9tLXNlYXJjaC1ib2R5Jyk7XHJcblxyXG4gICAgICAgIHRoaXMuaXNIaWRlTGVmdFNpZGViYXJNZW51Q29tcG9uZW50ID0gISFzZWFyY2hJbnB1dEVsZW1lbnQgPT0gdHJ1ZSB8fCAhIXNlYXJjaEJvZHlFbGVtZW50ID09IHRydWU/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJ5IGNsaWNraW5nIGdldCB1c2VyIGlkLCBmaXJzdCBuYW1lLCBzZWNvbmQgbmFtZShyZWNlaXZlciBpZCwgZmlyc3QgbmFtZSwgc2Vjb25kIG5hbWUpIGFuZCBzdGFydCBjaGF0XHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmVjZWl2ZXJJZFxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlY2VpdmVyRmlyc3ROYW1lXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmVjZWl2ZXJTZWNvbmROYW1lXHJcbiAgICAgKi9cclxuICAgIG9uU3RhcnRDaGF0KHJlY2VpdmVySWQ6c3RyaW5nID0gJycsIHJlY2VpdmVyRmlyc3ROYW1lOiBzdHJpbmcgPSAnJywgcmVjZWl2ZXJTZWNvbmROYW1lOiBzdHJpbmcgPSAnJywgcmVjZWl2ZXJMYXN0TmFtZTogc3RyaW5nID0gJycsIHJlY2VpdmVyVXNlcm5hbWU6IHN0cmluZyA9ICcnKSB7XHJcbiAgICAgICAgLy8gQmVmb3JlIHN3aXRjaCB0byBhbm90aGVyIHVzZXIgYW5kIHN0YXJ0IGNoYXQsIG5lZWQgbWFyayBtZXNzYWdlcyBsaWtlIGFzIHJlYWRcclxuICAgICAgICBpZiAoISF0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVySWQgJiYgISF0aGlzLmNoYXRTZXJ2aWNlLmNoYXRSb29tc1t0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVySWRdICYmIHRoaXMuY2hhdFNlcnZpY2UuY2hhdFJvb21zW3RoaXMuY2hhdFNlcnZpY2UucmVjZWl2ZXJJZF0ubGVuZ3RoID4gMCAmJiB0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVySWQgIT0gcmVjZWl2ZXJJZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJNYXJrQ2hhdENvcnJlc3BvbmRlbmNlQXNSZWFkID0gdGhpcy5jaGF0U2VydmljZS5tYXJrQ2hhdENvcnJlc3BvbmRlbmNlQXNSZWFkKHRoaXMuY2hhdFNlcnZpY2Uuc2VuZGVySWQsIHRoaXMuY2hhdFNlcnZpY2UucmVjZWl2ZXJJZCkuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhkYXRhKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVySWQgPSByZWNlaXZlcklkO1xyXG4gICAgICAgIHRoaXMuY2hhdFNlcnZpY2UucmVjZWl2ZXJGaXJzdE5hbWUgPSByZWNlaXZlckZpcnN0TmFtZTtcclxuICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVyU2Vjb25kTmFtZSA9IHJlY2VpdmVyU2Vjb25kTmFtZTtcclxuICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVyTGFzdE5hbWUgPSByZWNlaXZlckxhc3ROYW1lO1xyXG4gICAgICAgIHRoaXMuY2hhdFNlcnZpY2UucmVjZWl2ZXJVc2VybmFtZSA9IHJlY2VpdmVyVXNlcm5hbWU7XHJcblxyXG4gICAgICAgIC8vIGNoZWNrIGlmIGVhcmx5IGhhdmUgY2hhdCBtZXNzYWdlcyB3aXRoIHRoaXMgdXNlciwgYW5kIGlmIGhhdmVuJ3QsIHRoZW5cclxuICAgICAgICAvLyBnZXQgY2hhdCBtZXNzYWdlcyBmcm9tIGRhdGFiYXNlIGFuZCByZXN0b3JlIGNvcnJlc3BvbmRlbmNlXHJcbiAgICAgICAgaWYgKCF0aGlzLmNoYXRTZXJ2aWNlLmNoYXRSb29tc1tyZWNlaXZlcklkXSB8fCB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRSb29tc1tyZWNlaXZlcklkXS5sZW5ndGggPD0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJHZXRDaGF0Q29ycmVzcG9uZGVuY2UgPSB0aGlzLmNoYXRTZXJ2aWNlLmdldENoYXRDb3JyZXNwb25kZW5jZSh0aGlzLmNoYXRTZXJ2aWNlLnNlbmRlcklkLCB0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVySWQpLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRSb29tc1tyZWNlaXZlcklkXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0Um9vbXNbcmVjZWl2ZXJJZF0gPSBkYXRhLmNvcnJlc3BvbmRlbmNlO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5jaGF0U2VydmljZS5jaGF0V2luZG93c1N0YXRlW3JlY2VpdmVySWRdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0V2luZG93c1N0YXRlW3JlY2VpdmVySWRdID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gQ2xvc2Ugd2luZG93IHdpdGggdGFza3MgYXBwb2ludGVkIGJ5IG1lXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5pc1Nob3dBcHBvaW50ZWRCeU1lVGFza3NXaW5kb3cgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBDbG9zZSBjdXJyZW50IGFkZCB0YXNrIHdpbmRvd1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UudGFza1dpbmRvd3NTdGF0ZVt0aGlzLnRhc2tNb2RlbC50b10gPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBDbG9zZSBpbmZvIHRhc2sgd2luZG93XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5jdXJyZW50VGFza0lkID0gJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gUmVzZXQgdGFzayBtb2RlbCBkYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YXNrTW9kZWwucmVzZXRNb2RlbCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIENsb3NlIHRhc2sgbGlzdCB3aXRoIHN0YXR1cyBcImRvbmVcIiB3aW5kb3dcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmlzU2hvd0RvbmVUYXNrc1dpbmRvdyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMuY2hhdFNlcnZpY2UuY2hhdE1lc3NhZ2VzQ291bnRlcltyZWNlaXZlcklkXSA9IDA7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5jaGF0U2VydmljZS5jaGF0V2luZG93c1N0YXRlW3JlY2VpdmVySWRdKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRXaW5kb3dzU3RhdGVbcmVjZWl2ZXJJZF0gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0TWVzc2FnZXNDb3VudGVyW3JlY2VpdmVySWRdID0gMDtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBDbG9zZSB3aW5kb3cgd2l0aCB0YXNrcyBhcHBvaW50ZWQgYnkgbWVcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuaXNTaG93QXBwb2ludGVkQnlNZVRhc2tzV2luZG93ID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAvLyBDbG9zZSBjdXJyZW50IGFkZCB0YXNrIHdpbmRvd1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50YXNrV2luZG93c1N0YXRlW3RoaXMudGFza01vZGVsLnRvXSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgLy8gQ2xvc2UgaW5mbyB0YXNrIHdpbmRvd1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5jdXJyZW50VGFza0lkID0gJyc7XHJcbiAgICAgICAgICAgICAgICAvLyBSZXNldCB0YXNrIG1vZGVsIGRhdGFcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza01vZGVsLnJlc2V0TW9kZWwoKTtcclxuICAgICAgICAgICAgICAgIC8vIENsb3NlIHRhc2sgbGlzdCB3aXRoIHN0YXR1cyBcImRvbmVcIiB3aW5kb3dcclxuICAgICAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuaXNTaG93RG9uZVRhc2tzV2luZG93ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPcGVuIHRhc2sgd2luZG93XHJcbiAgICAgKiBAcGFyYW0gY3VycmVudFVzZXJJZFxyXG4gICAgICogQHBhcmFtIHRhcmdldFVzZXJJZFxyXG4gICAgICovXHJcbiAgICBvbk9wZW5UYXNrV2luZG93KGN1cnJlbnRVc2VySWQsIHRhcmdldFVzZXJJZCkge1xyXG4gICAgICAgIGlmICghdGhpcy50YXNrU2VydmljZS50YXNrV2luZG93c1N0YXRlW3RhcmdldFVzZXJJZF0pIHtcclxuICAgICAgICAgICAgLy8gQmVmb3JlIG9wZW4gbmV3IHRhc2sgd2luZG93LCBjaGVjayBpZiBiZWZvcmUgdGhpcyB3aW5kb3cgd2FzIG9wZW4gYW5vdGhlciB3aW5kb3cgdGFzayBmb3IgYW5vdGhlciB1c2VyLFxyXG4gICAgICAgICAgICAvLyBpZiB3YXMsIHRoZW4gY2xvc2UgdGhlIHByZXZpb3VzbHkgb3BlbmVkIHdpbmRvdyBhbmQgdGhlbiBvcGVuIG5ldyB0YXNrIHdpbmRvd1xyXG4gICAgICAgICAgICBpZiAodGhpcy50YXNrU2VydmljZS50YXNrV2luZG93c1N0YXRlW3RoaXMudGFza01vZGVsLnRvXSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50YXNrV2luZG93c1N0YXRlW3RoaXMudGFza01vZGVsLnRvXSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBCZWZvcmUgdGFzaydzIHdpbmRvdyBvcGVuZWQsIHJlc2V0IHRhc2sncyBtb2RlbCBkYXRhLCB0byBzaG93IG9uIHRoZSBmb3JtXHJcbiAgICAgICAgICAgIHRoaXMudGFza01vZGVsLnJlc2V0TW9kZWwoKTtcclxuICAgICAgICAgICAgdGhpcy50YXNrTW9kZWwudG8gPSB0YXJnZXRVc2VySWQ7XHJcbiAgICAgICAgICAgIHRoaXMudGFza01vZGVsLmZyb20gPSBjdXJyZW50VXNlcklkO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnRhc2tXaW5kb3dzU3RhdGVbdGFyZ2V0VXNlcklkXSA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAvLyBDbG9zZSB3aW5kb3cgd2l0aCB0YXNrcyBhcHBvaW50ZWQgYnkgbWVcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5pc1Nob3dBcHBvaW50ZWRCeU1lVGFza3NXaW5kb3cgPSBmYWxzZTtcclxuICAgICAgICAgICAgLy8gQ2xvc2UgY3VycmVudCBjaGF0IHdpbmRvd1xyXG4gICAgICAgICAgICB0aGlzLmNoYXRTZXJ2aWNlLmNoYXRXaW5kb3dzU3RhdGVbdGhpcy5jaGF0U2VydmljZS5yZWNlaXZlcklkXSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyBDbG9zZSBpbmZvIHRhc2sgd2luZG93XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuY3VycmVudFRhc2tJZCA9ICcnO1xyXG4gICAgICAgICAgICAvLyBDbG9zZSB0YXNrIGxpc3Qgd2l0aCBzdGF0dXMgXCJkb25lXCIgd2luZG93XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuaXNTaG93RG9uZVRhc2tzV2luZG93ID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50YXNrV2luZG93c1N0YXRlW3RhcmdldFVzZXJJZF0gPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWxlY3QgdGFzaydzIGlkIHRvIHNob3cgaW5mb3JtYXRpb24gYWJvdXQgaXRcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB0YXNrSWRcclxuICAgICAqL1xyXG4gICAgb25TZWxlY3RUYXNrKHRhc2tJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHRhc2tJZCAhPSB0aGlzLnRhc2tTZXJ2aWNlLmN1cnJlbnRUYXNrSWQpIHtcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5jdXJyZW50VGFza0lkID0gdGFza0lkO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tNb2RlbC5zdGF0dXMgPSB0aGlzLnRhc2tTZXJ2aWNlLnRhc2tzTGlzdFt0YXNrSWRdLnN0YXR1cztcclxuXHJcbiAgICAgICAgICAgIC8vIENsb3NlIHdpbmRvdyB3aXRoIHRhc2tzIGFwcG9pbnRlZCBieSBtZVxyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmlzU2hvd0FwcG9pbnRlZEJ5TWVUYXNrc1dpbmRvdyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyBDbG9zZSBjdXJyZW50IGNoYXQgd2luZG93XHJcbiAgICAgICAgICAgIHRoaXMuY2hhdFNlcnZpY2UuY2hhdFdpbmRvd3NTdGF0ZVt0aGlzLmNoYXRTZXJ2aWNlLnJlY2VpdmVySWRdID0gZmFsc2U7XHJcbiAgICAgICAgICAgIC8vIENsb3NlIGN1cnJlbnQgYWRkIHRhc2sgd2luZG93XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UudGFza1dpbmRvd3NTdGF0ZVt0aGlzLnRhc2tNb2RlbC50b10gPSBmYWxzZTtcclxuICAgICAgICAgICAgLy8gQ2xvc2UgdGFzayBsaXN0IHdpdGggc3RhdHVzIFwiZG9uZVwiIHdpbmRvd1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmlzU2hvd0RvbmVUYXNrc1dpbmRvdyA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldCB0YXNrcyBhcHBvaW50ZWQgYnkgbWVcclxuICAgICAqL1xyXG4gICAgb25HZXRUYXNrc0FwcG9pbnRlZEJ5TWUoKSB7XHJcbiAgICAgICAgdGhpcy5fc3ViR2V0QXBwb2ludGVkQnlNZVRhc2tzID0gdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLmdldFRhc2tzQXBwb2ludGVkQnlNZSh0aGlzLmNvbmZpZ1NlcnZpY2UudXNlci5pZCkuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS5hcHBvaW50ZWRCeU1lVGFza3MgPSBkYXRhLnRhc2tzO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLmlzU2hvd0FwcG9pbnRlZEJ5TWVUYXNrc1dpbmRvdyA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICAvLyBDbG9zZSB0YXNrIGxpc3Qgd2l0aCBzdGF0dXMgXCJkb25lXCIgd2luZG93XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuaXNTaG93RG9uZVRhc2tzV2luZG93ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIC8vIENsb3NlIGN1cnJlbnQgY2hhdCB3aW5kb3dcclxuICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0V2luZG93c1N0YXRlW3RoaXMuY2hhdFNlcnZpY2UucmVjZWl2ZXJJZF0gPSBmYWxzZTtcclxuICAgICAgICAgICAgLy8gQ2xvc2UgY3VycmVudCBhZGQgdGFzayB3aW5kb3dcclxuICAgICAgICAgICAgdGhpcy50YXNrU2VydmljZS50YXNrV2luZG93c1N0YXRlW3RoaXMudGFza01vZGVsLnRvXSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAvLyBDbG9zZSBpbmZvIHRhc2sgd2luZG93XHJcbiAgICAgICAgICAgIHRoaXMudGFza1NlcnZpY2UuY3VycmVudFRhc2tJZCA9ICcnO1xyXG4gICAgICAgICAgICAvLyBSZXNldCB0YXNrIG1vZGVsIGRhdGFcclxuICAgICAgICAgICAgdGhpcy50YXNrTW9kZWwucmVzZXRNb2RlbCgpO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gY2xhc3MgbmFtZSAoYmFja2dyb3VuZCBjb2xvcikgYnkgdGFzaydzIHN0YXR1c1xyXG4gICAgICovXHJcbiAgICBvbkdldENsYXNzQ29sb3JCeVRhc2tTdGF0dXModGFza1N0YXR1czogc3RyaW5nKSB7XHJcbiAgICAgICAgbGV0IHN0YXR1c0NsYXNzZXM6IGFueSA9IHtcclxuICAgICAgICAgICAgJ25ldyc6ICd0YXNrLXN0YXR1cy1pbmRpY2F0b3ItbmV3JyxcclxuICAgICAgICAgICAgJ2luIHByb2dyZXNzJzogJ3Rhc2stc3RhdHVzLWluZGljYXRvci1pbi1wcm9ncmVzcycsXHJcbiAgICAgICAgICAgICdwYXVzZSc6ICd0YXNrLXN0YXR1cy1pbmRpY2F0b3ItcGF1c2UnXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHN0YXR1c0NsYXNzZXNbdGFza1N0YXR1c107XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gY2xhc3MgbmFtZSAodGV4dCBjb2xvcikgYnkgdGFzaydzIHN0YXR1c1xyXG4gICAgICovXHJcbiAgICBvbkdldENsYXNzVGV4dENvbG9yQnlUYXNrU3RhdHVzKHRhc2tTdGF0dXM6IHN0cmluZykge1xyXG4gICAgICAgIGxldCBzdGF0dXNDbGFzc2VzOiBhbnkgPSB7XHJcbiAgICAgICAgICAgICduZXcnOiAndGFzay10ZXh0LWNvbG9yLXN0YXR1cy1uZXcnLFxyXG4gICAgICAgICAgICAnaW4gcHJvZ3Jlc3MnOiAndGFzay10ZXh0LWNvbG9yLXN0YXR1cy1pbi1wcm9ncmVzcycsXHJcbiAgICAgICAgICAgICdwYXVzZSc6ICd0YXNrLXRleHQtY29sb3Itc3RhdHVzLXBhdXNlJ1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBzdGF0dXNDbGFzc2VzW3Rhc2tTdGF0dXNdO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3V0IHN0cmluZyBieSBsZW5ndGgsIGlmIHN0cmluZyBtb3JlIHRoZW4gbGVuZ3RoXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc3RyXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gbGVuZ3RoXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBvbkN1dFN0cmluZ0J5TGVuZ3RoKHN0cjogc3RyaW5nLCBsZW5ndGg6IG51bWJlcikge1xyXG4gICAgICAgIHJldHVybiBzdHIubGVuZ3RoIDw9IGxlbmd0aD8gc3RyIDogc3RyLnN1YnN0cmluZygwLCBsZW5ndGgpO1xyXG4gICAgfVxyXG59Il19
