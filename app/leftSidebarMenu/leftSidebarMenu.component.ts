import {Component, OnInit, OnDestroy} from '@angular/core';
import {LeftSideBarMenuService} from "./leftSideBarMenu.service";
import {ConfigService} from "../sharedService/config.service";
import {Subscription} from "rxjs/Subscription";
import {ChatService} from "../chat/chat.service";
import {SocketService} from "../sharedService/socket.service";
import {TaskService} from "../task/task.service";
import {TaskModel} from "../task/task.model";
import {ObjectKeysPipe} from "../sharedPipe/objectKeys.pipe";
import {TranslateService} from "ng2-translate";

@Component({
    selector: 'leftSideBarMenu',
    templateUrl: 'app/leftSidebarMenu/leftSidebarMenu.component.html',
    styleUrls: ['app/leftSidebarMenu/leftSideBarMenu.component.scss'],
    host: {
        '(document:click)': 'onShowHideLeftSidebarMenuComponent($event)',
    }
})

export class LeftSideBarMenuComponent implements OnInit, OnDestroy {
    public menu: any = 'contacts';
    public taskTitleLength: number = 23;
    public taskFromUsernameLength: number = 25;
    public usernameLength: number = 25;
    public fullNameLength: number = 23;
    public isHideLeftSidebarMenuComponent: boolean = false;
    private _subGetContactsList: Subscription;
    private _subRemoveUserFromContactList: Subscription;
    private _subGetChatCorrespondence: Subscription;
    private _subMarkChatCorrespondenceAsRead: Subscription;
    private _subUserWasDeletedFromContactList: Subscription;
    private _subGetNewTask: Subscription;
    private _subGetTasks: Subscription;
    private _subGetDoneTasks: Subscription;
    private _subGetAppointedByMeTasks: Subscription;

    constructor(public leftSideBarMenuService: LeftSideBarMenuService,
                public configService: ConfigService,
                public chatService: ChatService,
                public socketService: SocketService,
                public taskService: TaskService,
                public taskModel: TaskModel,
                public translate: TranslateService) {}

    ngOnInit() {
        //When the "LeftSideBarMenuComponent" is initialized, we immediately get a list of users
        this._subGetContactsList = this.leftSideBarMenuService.getContactsList(this.configService.user.id).subscribe(data => {

            // This is a temporary store for the number of new messages from users in the list
            let counterMessage = [];

            // This is a temporary store for the number of new tasks from users in the list
            let counterTask = [];

            // We pass on each contact(user) and form arrays by the number of new messages and new tasks
            data.contacts.forEach(function (contact) {
                counterMessage[contact.id] = Number(contact.addInfo.countNewMessages);
                counterTask[contact.id] = Number(contact.addInfo.countNewTasks);
            });

            // Assign the contacts and generated arrays with the number of new messages and tasks to the service variables to quickly access this data
            this.chatService.chatMessagesCounter = counterMessage;
            this.taskService.tasksCounter = counterTask;
            this.leftSideBarMenuService.contacts = data.contacts;
        });

        if (this.socketService.socket) {
            // Subscribed on event when user was deleted from contact list by me or remote user to update data by contact list, number of new messages and tasks.
            // Remove chat's correspondence, chat's window state and task's window state for deleted user.
            this._subUserWasDeletedFromContactList = this.leftSideBarMenuService.socketOnUserWasDeletedFromContactList().subscribe((data: any) => {
                this.onGetContactList();

                if (data.from in this.chatService.chatRooms) {
                    this.chatService.chatRooms.splice(data.from, 1);
                }

                if(data.from in this.chatService.chatWindowsState) {
                    this.chatService.chatWindowsState.splice(data.from, 1);
                }

                if(data.from in this.taskService.taskWindowsState) {
                    this.taskService.taskWindowsState.splice(data.from, 1);
                }
            });

            // Subscribed to receive new tasks from the user and increase the number of new tasks from the sender
            this._subGetNewTask = this.taskService.socketOnGetNewTask().subscribe((data: any) => {
                this.taskService.tasksCounter[data.from] += 1;
            });
        }
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subGetContactsList) {
            this._subGetContactsList.unsubscribe();
        }
        if (this._subRemoveUserFromContactList) {
            this._subRemoveUserFromContactList.unsubscribe();
        }
        if (this._subGetChatCorrespondence) {
            this._subGetChatCorrespondence.unsubscribe();
        }
        if (this._subMarkChatCorrespondenceAsRead) {
            this._subMarkChatCorrespondenceAsRead.unsubscribe();
        }
        if (this._subGetNewTask) {
            this._subGetNewTask.unsubscribe();
        }
        if (this._subGetTasks) {
            this._subGetTasks.unsubscribe();
        }
        if (this._subGetDoneTasks) {
            this._subGetDoneTasks.unsubscribe();
        }
        if (this._subGetAppointedByMeTasks) {
            this._subGetAppointedByMeTasks.unsubscribe();
        }
    }

    /**
     * Remove user from contact list and return updated contact list
     * @param {number} relationshipId
     * @param {number} relationshipRequestId
     * @param {number} currentUserId
     * @param {number} targetUserId
     */
    onRemoveUserFromContactList(relationshipId: number, relationshipRequestId: number, currentUserId: number, targetUserId: number) {
        this._subRemoveUserFromContactList = this.leftSideBarMenuService.removeUserFromContactList(relationshipId, relationshipRequestId, currentUserId, targetUserId)
            .subscribe((data:any) => {
                this.leftSideBarMenuService.contacts = data.contacts;
                this.chatService.chatRooms.splice(targetUserId,1);
                this.chatService.chatWindowsState.splice(targetUserId,1);
                this.taskService.taskWindowsState.splice(targetUserId,1);

                let deletedUserInfo = {
                  to: targetUserId,
                  from: currentUserId,
                  relationshipId: relationshipId,
                  relationshipRequestId: relationshipRequestId
                };

                //We send a message to the remote user that it was removed from my list and I will also be removed from its user list.
                this.leftSideBarMenuService.deleteUserFromContactList(deletedUserInfo);
            })
    }

    /**
     * Get user's contact list
     */
    onGetContactList() {
        this._subGetContactsList = this.leftSideBarMenuService.getContactsList(this.configService.user.id).subscribe(data => {
            // This is a temporary store for the number of new messages from users in the list
            let counterMessage = [];

            // This is a temporary store for the number of new tasks from users in the list
            let counterTask = [];

            // We pass on each contact(user) and form arrays by the number of new messages and new tasks
            data.contacts.forEach(function (contact) {
               console.info(contact);
                counterMessage[contact.id] = Number(contact.addInfo.countNewMessages);
                counterTask[contact.id] = Number(contact.addInfo.countNewTasks);
            });

            // Assign the contacts and generated arrays with the number of new messages and tasks to the service variables to quickly access this data
            this.chatService.chatMessagesCounter = counterMessage;
            this.taskService.tasksCounter = counterTask;
            this.leftSideBarMenuService.contacts = data.contacts;
        })
    }

    /**
     * Get user's tasks (with statuses "new", "in progress", "pause")
     */
    onGetTasks() {
        this._subGetTasks = this.leftSideBarMenuService.getTasks(this.configService.user.id).subscribe((data: any) => {
            this.leftSideBarMenuService.taskKeys = Object.keys(data.groupedByFromUserTasks);
            this.leftSideBarMenuService.tasks = data.groupedByFromUserTasks;
            this.taskService.tasksList = data.tasksList;
        })
    }

    /**
     * Get tasks with status "done"
     */
    onGetTasksWithStatusDone() {
        this._subGetDoneTasks = this.leftSideBarMenuService.getTasksWithStatusDone(this.configService.user.id).subscribe((data: any) => {
            this.taskService.doneTasks = data.doneTasks;
            this.taskService.isShowDoneTasksWindow = true;

            // Close window with tasks appointed by me
            this.taskService.isShowAppointedByMeTasksWindow = false;
            // Close current chat window
            this.chatService.chatWindowsState[this.chatService.receiverId] = false;
            // Close current add task window
            this.taskService.taskWindowsState[this.taskModel.to] = false;
            // Close info task window
            this.taskService.currentTaskId = '';
            // Reset task model data
            this.taskModel.resetModel();
        })
    }

    /**
     * Check on click by parts of document area and show/hide component
     * @param event
     */
    onShowHideLeftSidebarMenuComponent(event) {
        let searchInputElement = event.target.closest('#custom-search-input');
        let searchBodyElement = event.target.closest('#custom-search-body');

        this.isHideLeftSidebarMenuComponent = !!searchInputElement == true || !!searchBodyElement == true? true : false;
    }

    /**
     * By clicking get user id, first name, second name(receiver id, first name, second name) and start chat
     * @param {string} receiverId
     * @param {string} receiverFirstName
     * @param {string} receiverSecondName
     */
    onStartChat(receiverId:string = '', receiverFirstName: string = '', receiverSecondName: string = '', receiverLastName: string = '', receiverUsername: string = '') {
        // Before switch to another user and start chat, need mark messages like as read
        if (!!this.chatService.receiverId && !!this.chatService.chatRooms[this.chatService.receiverId] && this.chatService.chatRooms[this.chatService.receiverId].length > 0 && this.chatService.receiverId != receiverId) {
            this._subMarkChatCorrespondenceAsRead = this.chatService.markChatCorrespondenceAsRead(this.chatService.senderId, this.chatService.receiverId).subscribe((data: any) => {
                console.info(data);
            });
        }

        this.chatService.receiverId = receiverId;
        this.chatService.receiverFirstName = receiverFirstName;
        this.chatService.receiverSecondName = receiverSecondName;
        this.chatService.receiverLastName = receiverLastName;
        this.chatService.receiverUsername = receiverUsername;

        // check if early have chat messages with this user, and if haven't, then
        // get chat messages from database and restore correspondence
        if (!this.chatService.chatRooms[receiverId] || this.chatService.chatRooms[receiverId].length <= 0) {
            this._subGetChatCorrespondence = this.chatService.getChatCorrespondence(this.chatService.senderId, this.chatService.receiverId).subscribe((data: any) => {
                this.chatService.chatRooms[receiverId] = [];
                this.chatService.chatRooms[receiverId] = data.correspondence;

                if (!this.chatService.chatWindowsState[receiverId]) {
                    this.chatService.chatWindowsState[receiverId] = true;

                    // Close window with tasks appointed by me
                    this.taskService.isShowAppointedByMeTasksWindow = false;
                    // Close current add task window
                    this.taskService.taskWindowsState[this.taskModel.to] = false;
                    // Close info task window
                    this.taskService.currentTaskId = '';
                    // Reset task model data
                    this.taskModel.resetModel();
                    // Close task list with status "done" window
                    this.taskService.isShowDoneTasksWindow = false;
                }

                this.chatService.chatMessagesCounter[receiverId] = 0;
            });
        } else {
            if (!this.chatService.chatWindowsState[receiverId]) {
                this.chatService.chatWindowsState[receiverId] = true;
                this.chatService.chatMessagesCounter[receiverId] = 0;

                // Close window with tasks appointed by me
                this.taskService.isShowAppointedByMeTasksWindow = false;
                // Close current add task window
                this.taskService.taskWindowsState[this.taskModel.to] = false;
                // Close info task window
                this.taskService.currentTaskId = '';
                // Reset task model data
                this.taskModel.resetModel();
                // Close task list with status "done" window
                this.taskService.isShowDoneTasksWindow = false;
            }
        }
    }

    /**
     * Open task window
     * @param currentUserId
     * @param targetUserId
     */
    onOpenTaskWindow(currentUserId, targetUserId) {
        if (!this.taskService.taskWindowsState[targetUserId]) {
            // Before open new task window, check if before this window was open another window task for another user,
            // if was, then close the previously opened window and then open new task window
            if (this.taskService.taskWindowsState[this.taskModel.to]) {
                this.taskService.taskWindowsState[this.taskModel.to] = false;
            }

            // Before task's window opened, reset task's model data, to show on the form
            this.taskModel.resetModel();
            this.taskModel.to = targetUserId;
            this.taskModel.from = currentUserId;
            this.taskService.taskWindowsState[targetUserId] = true;

            // Close window with tasks appointed by me
            this.taskService.isShowAppointedByMeTasksWindow = false;
            // Close current chat window
            this.chatService.chatWindowsState[this.chatService.receiverId] = false;
            // Close info task window
            this.taskService.currentTaskId = '';
            // Close task list with status "done" window
            this.taskService.isShowDoneTasksWindow = false;
        } else {
            this.taskService.taskWindowsState[targetUserId] = false;
        }
    }

    /**
     * Select task's id to show information about it
     * @param {string} taskId
     */
    onSelectTask(taskId: string) {
        if (taskId != this.taskService.currentTaskId) {
            this.taskService.currentTaskId = taskId;
            this.taskModel.status = this.taskService.tasksList[taskId].status;

            // Close window with tasks appointed by me
            this.taskService.isShowAppointedByMeTasksWindow = false;
            // Close current chat window
            this.chatService.chatWindowsState[this.chatService.receiverId] = false;
            // Close current add task window
            this.taskService.taskWindowsState[this.taskModel.to] = false;
            // Close task list with status "done" window
            this.taskService.isShowDoneTasksWindow = false;
        }
    }

    /**
     * Get tasks appointed by me
     */
    onGetTasksAppointedByMe() {
        this._subGetAppointedByMeTasks = this.leftSideBarMenuService.getTasksAppointedByMe(this.configService.user.id).subscribe((data: any) => {
            this.taskService.appointedByMeTasks = data.tasks;
            this.taskService.isShowAppointedByMeTasksWindow = true;

            // Close task list with status "done" window
            this.taskService.isShowDoneTasksWindow = false;
            // Close current chat window
            this.chatService.chatWindowsState[this.chatService.receiverId] = false;
            // Close current add task window
            this.taskService.taskWindowsState[this.taskModel.to] = false;
            // Close info task window
            this.taskService.currentTaskId = '';
            // Reset task model data
            this.taskModel.resetModel();
        })
    }

    /**
     * Return class name (background color) by task's status
     */
    onGetClassColorByTaskStatus(taskStatus: string) {
        let statusClasses: any = {
            'new': 'task-status-indicator-new',
            'in progress': 'task-status-indicator-in-progress',
            'pause': 'task-status-indicator-pause'
        };

        return statusClasses[taskStatus];
    }

    /**
     * Return class name (text color) by task's status
     */
    onGetClassTextColorByTaskStatus(taskStatus: string) {
        let statusClasses: any = {
            'new': 'task-text-color-status-new',
            'in progress': 'task-text-color-status-in-progress',
            'pause': 'task-text-color-status-pause'
        };

        return statusClasses[taskStatus];
    }

    /**
     * Cut string by length, if string more then length
     * @param {string} str
     * @param {number} length
     * @returns {string}
     */
    onCutStringByLength(str: string, length: number) {
        return str.length <= length? str : str.substring(0, length);
    }
}