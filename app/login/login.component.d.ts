import { OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { LoginService } from "./login.service";
import { LoginModel } from "./login.model";
import { ToolbarService } from "../toolbar/toolbar.service";
import { ConfigService } from "../sharedService/config.service";
import { TranslateService } from 'ng2-translate';
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
export declare class LoginComponent implements OnInit, OnDestroy {
    loginService: LoginService;
    loginModel: LoginModel;
    toolbarService: ToolbarService;
    router: Router;
    configService: ConfigService;
    translate: TranslateService;
    private spinnerService;
    private _subLogIn;
    private _subRelationshipRequestsUser;
    private _subChangedLanguage;
    private sharedObjectVal;
    isLogin: boolean;
    private usernameLength;
    constructor(loginService: LoginService, loginModel: LoginModel, toolbarService: ToolbarService, router: Router, configService: ConfigService, translate: TranslateService, spinnerService: Ng4LoadingSpinnerService);
    ngOnInit(): void;
    onLogin(): void;
    ngOnDestroy(): void;
}
