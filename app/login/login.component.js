System.register(["@angular/core", "@angular/router", "./login.service", "./login.model", "../toolbar/toolbar.service", "../sharedService/config.service", "ng2-translate", "ng4-loading-spinner"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, login_service_1, login_model_1, toolbar_service_1, config_service_1, ng2_translate_1, ng4_loading_spinner_1, LoginComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (login_model_1_1) {
                login_model_1 = login_model_1_1;
            },
            function (toolbar_service_1_1) {
                toolbar_service_1 = toolbar_service_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (ng4_loading_spinner_1_1) {
                ng4_loading_spinner_1 = ng4_loading_spinner_1_1;
            }
        ],
        execute: function () {
            LoginComponent = class LoginComponent {
                constructor(loginService, loginModel, toolbarService, router, configService, translate, spinnerService) {
                    this.loginService = loginService;
                    this.loginModel = loginModel;
                    this.toolbarService = toolbarService;
                    this.router = router;
                    this.configService = configService;
                    this.translate = translate;
                    this.spinnerService = spinnerService;
                    this.isLogin = true;
                    this.usernameLength = 14;
                    this.sharedObjectVal = require('electron').remote.getGlobal('sharedObject').someProperty;
                    console.info(this.sharedObjectVal);
                    translate.setDefaultLang(localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en');
                    translate.use(localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en');
                }
                ngOnInit() {
                    this._subChangedLanguage = this.toolbarService.getChangedLanguage().subscribe(() => {
                        this.translate.setDefaultLang(localStorage.getItem('locale'));
                        this.translate.use(localStorage.getItem('locale'));
                    });
                }
                onLogin() {
                    this.spinnerService.show();
                    this._subLogIn = this.loginService.login(this.loginModel).subscribe((data) => {
                        this.isLogin = this.loginService.isExistUser(data);
                        if (data.status.isExistUser) {
                            this.toolbarService.currentUser = data.user;
                            this.toolbarService.username = this.toolbarService.currentUser.username.length >= this.usernameLength ? this.toolbarService.currentUser.username : this.toolbarService.currentUser.username.substr(0, this.usernameLength);
                            this.toolbarService.userEmail = this.toolbarService.currentUser.email;
                            this.configService.user = data.user;
                            this._subRelationshipRequestsUser = this.toolbarService.getUserRelationshipRequestsStatusNew(this.toolbarService.currentUser.id).subscribe((rRequests) => {
                                this.toolbarService.relationshipRequests = rRequests.preparedRR;
                                this.spinnerService.hide();
                                this.router.navigate(['/main-page']);
                            });
                        }
                        else {
                            this.spinnerService.hide();
                        }
                    }, (err) => {
                        this.spinnerService.hide();
                        console.info(err);
                    });
                }
                ngOnDestroy() {
                    if (this._subLogIn) {
                        this._subLogIn.unsubscribe();
                    }
                    if (this._subRelationshipRequestsUser) {
                        this._subRelationshipRequestsUser.unsubscribe();
                    }
                    if (this._subChangedLanguage) {
                        this._subChangedLanguage.unsubscribe();
                    }
                }
            };
            LoginComponent = __decorate([
                core_1.Component({
                    selector: 'login',
                    templateUrl: 'app/login/login.component.html',
                    styleUrls: ['app/login/login.component.scss']
                }),
                __metadata("design:paramtypes", [login_service_1.LoginService,
                    login_model_1.LoginModel,
                    toolbar_service_1.ToolbarService,
                    router_1.Router,
                    config_service_1.ConfigService,
                    ng2_translate_1.TranslateService,
                    ng4_loading_spinner_1.Ng4LoadingSpinnerService])
            ], LoginComponent);
            exports_1("LoginComponent", LoginComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQWdCYSxjQUFjLEdBQTNCO2dCQVNJLFlBQW1CLFlBQTBCLEVBQzFCLFVBQXNCLEVBQ3RCLGNBQThCLEVBQzlCLE1BQWMsRUFDZCxhQUE0QixFQUM1QixTQUEyQixFQUMxQixjQUF3QztvQkFOekMsaUJBQVksR0FBWixZQUFZLENBQWM7b0JBQzFCLGVBQVUsR0FBVixVQUFVLENBQVk7b0JBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtvQkFDOUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFDZCxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFDNUIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7b0JBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUEwQjtvQkFUckQsWUFBTyxHQUFZLElBQUksQ0FBQztvQkFDdkIsbUJBQWMsR0FBVyxFQUFFLENBQUM7b0JBV2hDLElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsWUFBWSxDQUFDO29CQUN6RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFFbkMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFBLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDaEcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFBLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDekYsQ0FBQztnQkFFRCxRQUFRO29CQUNKLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGtCQUFrQixFQUFFLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTt3QkFDL0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3dCQUM5RCxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZELENBQUMsQ0FBQyxDQUFBO2dCQUNOLENBQUM7Z0JBS0QsT0FBTztvQkFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTt3QkFDdEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFHbkQsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDOzRCQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDOzRCQUc1QyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzRCQUMzTixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7NEJBQ3RFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7NEJBR3BDLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLG9DQUFvQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFFO2dDQUNySixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixHQUFHLFNBQVMsQ0FBQyxVQUFVLENBQUM7Z0NBQ2hFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7Z0NBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzs0QkFDekMsQ0FBQyxDQUFDLENBQUM7d0JBQ1AsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDSixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMvQixDQUFDO29CQUNMLENBQUMsRUFDTCxDQUFDLEdBQVEsRUFBRSxFQUFFO3dCQUNULElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQzNCLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3RCLENBQUMsQ0FDQSxDQUFDO2dCQUNWLENBQUM7Z0JBS0QsV0FBVztvQkFDUCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDakMsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDO3dCQUNwQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3BELENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUMzQyxDQUFDO2dCQUNMLENBQUM7YUFDSixDQUFBO1lBaEZZLGNBQWM7Z0JBTjFCLGdCQUFTLENBQUM7b0JBQ1AsUUFBUSxFQUFFLE9BQU87b0JBQ2pCLFdBQVcsRUFBRSxnQ0FBZ0M7b0JBQzdDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO2lCQUNoRCxDQUFDO2lEQVdtQyw0QkFBWTtvQkFDZCx3QkFBVTtvQkFDTixnQ0FBYztvQkFDdEIsZUFBTTtvQkFDQyw4QkFBYTtvQkFDakIsZ0NBQWdCO29CQUNWLDhDQUF3QjtlQWZuRCxjQUFjLENBZ0YxQjs7UUFBQSxDQUFDIiwiZmlsZSI6ImxvZ2luL2xvZ2luLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Um91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7TG9naW5TZXJ2aWNlfSBmcm9tIFwiLi9sb2dpbi5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7TG9naW5Nb2RlbH0gZnJvbSBcIi4vbG9naW4ubW9kZWxcIjtcclxuaW1wb3J0IHtTdWJzY3JpcHRpb259IGZyb20gXCJyeGpzL1N1YnNjcmlwdGlvblwiO1xyXG5pbXBvcnQge1Rvb2xiYXJTZXJ2aWNlfSBmcm9tIFwiLi4vdG9vbGJhci90b29sYmFyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtDb25maWdTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9jb25maWcuc2VydmljZVwiO1xyXG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gJ25nMi10cmFuc2xhdGUnO1xyXG5pbXBvcnQge05nNExvYWRpbmdTcGlubmVyU2VydmljZX0gZnJvbSBcIm5nNC1sb2FkaW5nLXNwaW5uZXJcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdsb2dpbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIHByaXZhdGUgX3N1YkxvZ0luOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJSZWxhdGlvbnNoaXBSZXF1ZXN0c1VzZXI6IFN1YnNjcmlwdGlvbjtcclxuICAgIHByaXZhdGUgX3N1YkNoYW5nZWRMYW5ndWFnZTogU3Vic2NyaXB0aW9uO1xyXG5cclxuICAgIHByaXZhdGUgc2hhcmVkT2JqZWN0VmFsOiBhbnk7XHJcbiAgICBwdWJsaWMgaXNMb2dpbjogYm9vbGVhbiA9IHRydWU7XHJcbiAgICBwcml2YXRlIHVzZXJuYW1lTGVuZ3RoOiBudW1iZXIgPSAxNDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgbG9naW5TZXJ2aWNlOiBMb2dpblNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgbG9naW5Nb2RlbDogTG9naW5Nb2RlbCxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0b29sYmFyU2VydmljZTogVG9vbGJhclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHNwaW5uZXJTZXJ2aWNlOiBOZzRMb2FkaW5nU3Bpbm5lclNlcnZpY2UpIHtcclxuXHJcbiAgICAgICAgLy8gR2V0dGluZyB0ZXN0IGdsb2JhbCB2YWx1ZVxyXG4gICAgICAgIHRoaXMuc2hhcmVkT2JqZWN0VmFsID0gcmVxdWlyZSgnZWxlY3Ryb24nKS5yZW1vdGUuZ2V0R2xvYmFsKCdzaGFyZWRPYmplY3QnKS5zb21lUHJvcGVydHk7XHJcbiAgICAgICAgY29uc29sZS5pbmZvKHRoaXMuc2hhcmVkT2JqZWN0VmFsKTtcclxuXHJcbiAgICAgICAgdHJhbnNsYXRlLnNldERlZmF1bHRMYW5nKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKT8gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xvY2FsZScpIDogJ2VuJyk7XHJcbiAgICAgICAgdHJhbnNsYXRlLnVzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9jYWxlJyk/IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKSA6ICdlbicpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuX3N1YkNoYW5nZWRMYW5ndWFnZSA9IHRoaXMudG9vbGJhclNlcnZpY2UuZ2V0Q2hhbmdlZExhbmd1YWdlKCkuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy50cmFuc2xhdGUuc2V0RGVmYXVsdExhbmcobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xvY2FsZScpKTtcclxuICAgICAgICAgICAgdGhpcy50cmFuc2xhdGUudXNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKSk7XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvZ2luIHVzZXIsIHNldCBkYXRhIGFuZCByZWRpcmVjdCB0byBtYWluIHBhZ2UgaWYgdXNlciBleGlzdFxyXG4gICAgICovXHJcbiAgICBvbkxvZ2luKCkge1xyXG4gICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgIHRoaXMuX3N1YkxvZ0luID0gdGhpcy5sb2dpblNlcnZpY2UubG9naW4odGhpcy5sb2dpbk1vZGVsKS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNMb2dpbiA9IHRoaXMubG9naW5TZXJ2aWNlLmlzRXhpc3RVc2VyKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBDaGVjayBpZiBleGlzdCB1c2VyXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuc3RhdHVzLmlzRXhpc3RVc2VyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIgPSBkYXRhLnVzZXI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBHZXQgYW5kIGN1dCB1c2VybmFtZSBieSBsZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b29sYmFyU2VydmljZS51c2VybmFtZSA9IHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIudXNlcm5hbWUubGVuZ3RoID49IHRoaXMudXNlcm5hbWVMZW5ndGggPyB0aGlzLnRvb2xiYXJTZXJ2aWNlLmN1cnJlbnRVc2VyLnVzZXJuYW1lIDogdGhpcy50b29sYmFyU2VydmljZS5jdXJyZW50VXNlci51c2VybmFtZS5zdWJzdHIoMCwgdGhpcy51c2VybmFtZUxlbmd0aCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UudXNlckVtYWlsID0gdGhpcy50b29sYmFyU2VydmljZS5jdXJyZW50VXNlci5lbWFpbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb25maWdTZXJ2aWNlLnVzZXIgPSBkYXRhLnVzZXI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBHZXQgbmV3IHVzZXIncyByZWxhdGlvbnNoaXAgcmVxdWVzdHMgYW5kIHJlZGlyZWN0IHRvIFwibWFpbi1wYWdlXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyID0gdGhpcy50b29sYmFyU2VydmljZS5nZXRVc2VyUmVsYXRpb25zaGlwUmVxdWVzdHNTdGF0dXNOZXcodGhpcy50b29sYmFyU2VydmljZS5jdXJyZW50VXNlci5pZCkuc3Vic2NyaWJlKChyUmVxdWVzdHMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UucmVsYXRpb25zaGlwUmVxdWVzdHMgPSByUmVxdWVzdHMucHJlcGFyZWRSUjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbWFpbi1wYWdlJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoZXJyOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5pbmZvKGVycik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJlZm9yZSBkZXN0cm95IGNvbXBvbmVudCB1bnN1YnNjcmliZWQgZnJvbSBldmVudHNcclxuICAgICAqL1xyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3N1YkxvZ0luKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YkxvZ0luLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJSZWxhdGlvbnNoaXBSZXF1ZXN0c1VzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJDaGFuZ2VkTGFuZ3VhZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViQ2hhbmdlZExhbmd1YWdlLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59Il19
