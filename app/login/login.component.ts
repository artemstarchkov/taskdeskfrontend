import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {LoginService} from "./login.service";
import {LoginModel} from "./login.model";
import {Subscription} from "rxjs/Subscription";
import {ToolbarService} from "../toolbar/toolbar.service";
import {ConfigService} from "../sharedService/config.service";
import {TranslateService} from 'ng2-translate';
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
    selector: 'login',
    templateUrl: 'app/login/login.component.html',
    styleUrls: ['app/login/login.component.scss']
})

export class LoginComponent implements OnInit, OnDestroy {
    private _subLogIn: Subscription;
    private _subRelationshipRequestsUser: Subscription;
    private _subChangedLanguage: Subscription;

    private sharedObjectVal: any;
    public isLogin: boolean = true;
    private usernameLength: number = 14;

    constructor(public loginService: LoginService,
                public loginModel: LoginModel,
                public toolbarService: ToolbarService,
                public router: Router,
                public configService: ConfigService,
                public translate: TranslateService,
                private spinnerService: Ng4LoadingSpinnerService) {

        // Getting test global value
        this.sharedObjectVal = require('electron').remote.getGlobal('sharedObject').someProperty;
        console.info(this.sharedObjectVal);

        translate.setDefaultLang(localStorage.getItem('locale')? localStorage.getItem('locale') : 'en');
        translate.use(localStorage.getItem('locale')? localStorage.getItem('locale') : 'en');
    }

    ngOnInit() {
        this._subChangedLanguage = this.toolbarService.getChangedLanguage().subscribe(() => {
            this.translate.setDefaultLang(localStorage.getItem('locale'));
            this.translate.use(localStorage.getItem('locale'));
        })
    }

    /**
     * Login user, set data and redirect to main page if user exist
     */
    onLogin() {
        this.spinnerService.show();
        this._subLogIn = this.loginService.login(this.loginModel).subscribe((data: any) => {
                    this.isLogin = this.loginService.isExistUser(data);

                    // Check if exist user
                    if (data.status.isExistUser) {
                        this.toolbarService.currentUser = data.user;

                        // Get and cut username by length
                        this.toolbarService.username = this.toolbarService.currentUser.username.length >= this.usernameLength ? this.toolbarService.currentUser.username : this.toolbarService.currentUser.username.substr(0, this.usernameLength);
                        this.toolbarService.userEmail = this.toolbarService.currentUser.email;
                        this.configService.user = data.user;

                        // Get new user's relationship requests and redirect to "main-page"
                        this._subRelationshipRequestsUser = this.toolbarService.getUserRelationshipRequestsStatusNew(this.toolbarService.currentUser.id).subscribe((rRequests) => {
                            this.toolbarService.relationshipRequests = rRequests.preparedRR;
                            this.spinnerService.hide();
                            this.router.navigate(['/main-page']);
                        });
                    } else {
                        this.spinnerService.hide();
                    }
                },
            (err: any) => {
                this.spinnerService.hide();
                console.info(err);
            }
            );
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subLogIn) {
            this._subLogIn.unsubscribe();
        }
        if (this._subRelationshipRequestsUser) {
            this._subRelationshipRequestsUser.unsubscribe();
        }
        if (this._subChangedLanguage) {
            this._subChangedLanguage.unsubscribe();
        }
    }
}