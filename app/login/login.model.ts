export class LoginModel {
    firstName: string;
    secondName: string;
    lastName: string;
    email: string;
    password: string;
    passwordConfirmation: string;
}