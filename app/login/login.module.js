System.register(["@angular/core", "@angular/http", "@angular/common", "./login.route", "./login.component", "./login.service", "./login.model", "@angular/router", "@angular/forms", "ng2-translate"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, common_1, login_route_1, login_component_1, login_service_1, login_model_1, router_1, forms_1, ng2_translate_1, LoginModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (login_route_1_1) {
                login_route_1 = login_route_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (login_model_1_1) {
                login_model_1 = login_model_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            }
        ],
        execute: function () {
            LoginModule = class LoginModule {
            };
            LoginModule = __decorate([
                core_1.NgModule({
                    imports: [
                        common_1.CommonModule,
                        http_1.HttpModule,
                        login_route_1.LOGIN_ROUTES,
                        router_1.RouterModule,
                        forms_1.FormsModule,
                        ng2_translate_1.TranslateModule.forRoot({
                            provide: ng2_translate_1.TranslateLoader,
                            useFactory: (http) => new ng2_translate_1.TranslateStaticLoader(http, './assets/i18n', '.json'),
                            deps: [http_1.Http]
                        })
                    ],
                    providers: [
                        login_service_1.LoginService,
                        login_model_1.LoginModel
                    ],
                    declarations: [
                        login_component_1.LoginComponent
                    ]
                })
            ], LoginModule);
            exports_1("LoginModule", LoginModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xvZ2luL2xvZ2luLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQWdDYSxXQUFXLEdBQXhCO2FBQTRCLENBQUE7WUFBZixXQUFXO2dCQXJCdkIsZUFBUSxDQUFDO29CQUNOLE9BQU8sRUFBTzt3QkFDVixxQkFBWTt3QkFDWixpQkFBVTt3QkFDViwwQkFBWTt3QkFDWixxQkFBWTt3QkFDWixtQkFBVzt3QkFDWCwrQkFBZSxDQUFDLE9BQU8sQ0FBQzs0QkFDcEIsT0FBTyxFQUFFLCtCQUFlOzRCQUN4QixVQUFVLEVBQUUsQ0FBQyxJQUFVLEVBQUUsRUFBRSxDQUFDLElBQUkscUNBQXFCLENBQUMsSUFBSSxFQUFFLGVBQWUsRUFBRSxPQUFPLENBQUM7NEJBQ3JGLElBQUksRUFBRSxDQUFDLFdBQUksQ0FBQzt5QkFDZixDQUFDO3FCQUNMO29CQUNELFNBQVMsRUFBRTt3QkFDUCw0QkFBWTt3QkFDWix3QkFBVTtxQkFDYjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsZ0NBQWM7cUJBQ2pCO2lCQUNKLENBQUM7ZUFDVyxXQUFXLENBQUk7O1FBQUEsQ0FBQyIsImZpbGUiOiJsb2dpbi9sb2dpbi5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtIdHRwLCBIdHRwTW9kdWxlfSBmcm9tICdAYW5ndWxhci9odHRwJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IExPR0lOX1JPVVRFUyB9IGZyb20gJy4vbG9naW4ucm91dGUnO1xyXG5pbXBvcnQge0xvZ2luQ29tcG9uZW50fSBmcm9tIFwiLi9sb2dpbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtMb2dpblNlcnZpY2V9IGZyb20gXCIuL2xvZ2luLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtMb2dpbk1vZGVsfSBmcm9tIFwiLi9sb2dpbi5tb2RlbFwiO1xyXG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge0Zvcm1zTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHtUcmFuc2xhdGVMb2FkZXIsIFRyYW5zbGF0ZU1vZHVsZSwgVHJhbnNsYXRlU3RhdGljTG9hZGVyfSBmcm9tIFwibmcyLXRyYW5zbGF0ZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6ICAgICAgW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBIdHRwTW9kdWxlLFxyXG4gICAgICAgIExPR0lOX1JPVVRFUyxcclxuICAgICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvclJvb3Qoe1xyXG4gICAgICAgICAgICBwcm92aWRlOiBUcmFuc2xhdGVMb2FkZXIsXHJcbiAgICAgICAgICAgIHVzZUZhY3Rvcnk6IChodHRwOiBIdHRwKSA9PiBuZXcgVHJhbnNsYXRlU3RhdGljTG9hZGVyKGh0dHAsICcuL2Fzc2V0cy9pMThuJywgJy5qc29uJyksXHJcbiAgICAgICAgICAgIGRlcHM6IFtIdHRwXVxyXG4gICAgICAgIH0pXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgTG9naW5TZXJ2aWNlLFxyXG4gICAgICAgIExvZ2luTW9kZWxcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBMb2dpbkNvbXBvbmVudFxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9naW5Nb2R1bGUgeyB9Il19
