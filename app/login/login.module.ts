import {NgModule} from '@angular/core';
import {Http, HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import { LOGIN_ROUTES } from './login.route';
import {LoginComponent} from "./login.component";
import {LoginService} from "./login.service";
import {LoginModel} from "./login.model";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from "ng2-translate";

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        LOGIN_ROUTES,
        RouterModule,
        FormsModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, './assets/i18n', '.json'),
            deps: [Http]
        })
    ],
    providers: [
        LoginService,
        LoginModel
    ],
    declarations: [
        LoginComponent
    ]
})
export class LoginModule { }