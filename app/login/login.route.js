System.register(["@angular/router", "./login.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, login_component_1, loginRoutes, LOGIN_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            }
        ],
        execute: function () {
            loginRoutes = [
                {
                    path: '',
                    component: login_component_1.LoginComponent
                }
            ];
            exports_1("LOGIN_ROUTES", LOGIN_ROUTES = router_1.RouterModule.forChild(loginRoutes));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xvZ2luL2xvZ2luLnJvdXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O1lBSU0sV0FBVyxHQUFVO2dCQUN2QjtvQkFDSSxJQUFJLEVBQUUsRUFBRTtvQkFDUixTQUFTLEVBQUUsZ0NBQWM7aUJBQzVCO2FBS0osQ0FBQztZQUVGLDBCQUFhLFlBQVksR0FBdUIscUJBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUM7UUFBQSxDQUFDIiwiZmlsZSI6ImxvZ2luL2xvZ2luLnJvdXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSb3V0ZXMsIFJvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge01vZHVsZVdpdGhQcm92aWRlcnN9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7TG9naW5Db21wb25lbnR9IGZyb20gXCIuL2xvZ2luLmNvbXBvbmVudFwiO1xyXG5cclxuY29uc3QgbG9naW5Sb3V0ZXM6Um91dGVzID0gW1xyXG4gICAge1xyXG4gICAgICAgIHBhdGg6ICcnLFxyXG4gICAgICAgIGNvbXBvbmVudDogTG9naW5Db21wb25lbnRcclxuICAgIH1cclxuICAgIC8qe1xyXG4gICAgICAgIHBhdGg6ICdyZWdpc3RlcicsXHJcbiAgICAgICAgbG9hZENoaWxkcmVuOiAnYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLm1vZHVsZSNSZWdpc3Rlck1vZHVsZSdcclxuICAgIH0qL1xyXG5dO1xyXG5cclxuZXhwb3J0IGNvbnN0IExPR0lOX1JPVVRFUzpNb2R1bGVXaXRoUHJvdmlkZXJzID0gUm91dGVyTW9kdWxlLmZvckNoaWxkKGxvZ2luUm91dGVzKTsiXX0=
