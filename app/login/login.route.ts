import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {LoginComponent} from "./login.component";

const loginRoutes:Routes = [
    {
        path: '',
        component: LoginComponent
    }
    /*{
        path: 'register',
        loadChildren: 'app/register/register.module#RegisterModule'
    }*/
];

export const LOGIN_ROUTES:ModuleWithProviders = RouterModule.forChild(loginRoutes);