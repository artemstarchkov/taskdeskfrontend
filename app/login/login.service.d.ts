import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/map";
import { LoginModel } from "./login.model";
import { ConfigService } from "../sharedService/config.service";
export declare class LoginService {
    router: Router;
    http: Http;
    configService: ConfigService;
    constructor(router: Router, http: Http, configService: ConfigService);
    isExistUser(data: any): any;
    login(loginModel: LoginModel): Observable<Response>;
}
