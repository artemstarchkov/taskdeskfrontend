System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "../sharedService/config.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, config_service_1, LoginService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            }
        ],
        execute: function () {
            LoginService = class LoginService {
                constructor(router, http, configService) {
                    this.router = router;
                    this.http = http;
                    this.configService = configService;
                }
                isExistUser(data) {
                    console.info(data);
                    if (data.status.isExistUser) {
                        localStorage.setItem('user', JSON.stringify(data.user));
                        return data.status.isExistUser;
                    }
                    else {
                        return data.status.isExistUser;
                    }
                }
                login(loginModel) {
                    return this.http.post(this.configService.domen + 'user/login', loginModel)
                        .map(res => res.json());
                }
            };
            LoginService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router,
                    http_1.Http,
                    config_service_1.ConfigService])
            ], LoginService);
            exports_1("LoginService", LoginService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL2xvZ2luL2xvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQVNhLFlBQVksR0FBekI7Z0JBRUksWUFBbUIsTUFBYyxFQUNkLElBQVUsRUFDVixhQUE0QjtvQkFGNUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFDZCxTQUFJLEdBQUosSUFBSSxDQUFNO29CQUNWLGtCQUFhLEdBQWIsYUFBYSxDQUFlO2dCQUFHLENBQUM7Z0JBTzVDLFdBQVcsQ0FBQyxJQUFTO29CQUN4QixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQzFCLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztvQkFDbkMsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7b0JBQ25DLENBQUM7Z0JBQ0wsQ0FBQztnQkFPTSxLQUFLLENBQUMsVUFBc0I7b0JBQ2hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxZQUFZLEVBQUUsVUFBVSxDQUFDO3lCQUNwRSxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQzthQUNKLENBQUE7WUE5QlksWUFBWTtnQkFEeEIsaUJBQVUsRUFBRTtpREFHa0IsZUFBTTtvQkFDUixXQUFJO29CQUNLLDhCQUFhO2VBSnRDLFlBQVksQ0E4QnhCOztRQUFBLENBQUMiLCJmaWxlIjoibG9naW4vbG9naW4uc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tIFwicnhqcy9PYnNlcnZhYmxlXCI7XHJcbmltcG9ydCB7SHR0cCwgUmVzcG9uc2V9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9tYXBcIjtcclxuaW1wb3J0IHtMb2dpbk1vZGVsfSBmcm9tIFwiLi9sb2dpbi5tb2RlbFwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBMb2dpblNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBodHRwOiBIdHRwLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2UpIHt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiB1c2VyIGV4aXN0LCB0aGFuIHNldCBkYXRhIHRvIGxvY2FsU3RvcmFnZShzZXNzaW9uKSwgaGFuZGxlIFwiU3ViamVjdFwiIGFuZCByZWRpcmVjdCB0byBtYWluIHBhZ2VcclxuICAgICAqIEBwYXJhbSBkYXRhOiBhbnlcclxuICAgICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBpc0V4aXN0VXNlcihkYXRhOiBhbnkpIHtcclxuICAgICAgICBjb25zb2xlLmluZm8oZGF0YSk7XHJcbiAgICAgICAgaWYgKGRhdGEuc3RhdHVzLmlzRXhpc3RVc2VyKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCd1c2VyJywgSlNPTi5zdHJpbmdpZnkoZGF0YS51c2VyKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBkYXRhLnN0YXR1cy5pc0V4aXN0VXNlcjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0YS5zdGF0dXMuaXNFeGlzdFVzZXI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCBwb3N0IHJlcXVlc3QgdG8gY2hlY2sgaWYgdXNlciBleGlzdCBhbmQgcmV0dXJuIFwidXNlclwiIG9iamVjdCBhbmQgXCJzdGF0dXNcIlxyXG4gICAgICogQHBhcmFtIGxvZ2luTW9kZWw6IExvZ2luTW9kZWxcclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPFI+fVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgbG9naW4obG9naW5Nb2RlbDogTG9naW5Nb2RlbCk6IE9ic2VydmFibGU8UmVzcG9uc2U+IHtcclxuICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZ1NlcnZpY2UuZG9tZW4gKyAndXNlci9sb2dpbicsIGxvZ2luTW9kZWwpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHJlcy5qc29uKCkpO1xyXG4gICAgfVxyXG59Il19
