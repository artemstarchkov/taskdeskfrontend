import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {LoginModel} from "./login.model";
import {ConfigService} from "../sharedService/config.service";

@Injectable()
export class LoginService {

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService) {}

    /**
     * Check if user exist, than set data to localStorage(session), handle "Subject" and redirect to main page
     * @param data: any
     * @returns {any}
     */
    public isExistUser(data: any) {
        console.info(data);
        if (data.status.isExistUser) {
            localStorage.setItem('user', JSON.stringify(data.user));
            return data.status.isExistUser;
        } else {
            return data.status.isExistUser;
        }
    }

    /**
     * Send post request to check if user exist and return "user" object and "status"
     * @param loginModel: LoginModel
     * @returns {Observable<R>}
     */
    public login(loginModel: LoginModel): Observable<Response> {
       return this.http.post(this.configService.domen + 'user/login', loginModel)
            .map(res => res.json());
    }
}