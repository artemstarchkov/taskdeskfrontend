import { OnInit, OnDestroy } from '@angular/core';
import "rxjs/add/operator/map";
import { ConfigService } from "../sharedService/config.service";
import { MainService } from "./main.service";
import { SocketService } from "../sharedService/socket.service";
import { ChatService } from "../chat/chat.service";
import { ToolbarService } from "../toolbar/toolbar.service";
export declare class MainComponent implements OnInit, OnDestroy {
    configService: ConfigService;
    mainService: MainService;
    socketService: SocketService;
    chatService: ChatService;
    toolbarService: ToolbarService;
    private _subGetMsg;
    private _subKeepCorrespondence;
    private _subOnEmitNewRelationshipRequest;
    private _subRelationshipRequestsUser;
    defaultDatePipeLocale: string;
    constructor(configService: ConfigService, mainService: MainService, socketService: SocketService, chatService: ChatService, toolbarService: ToolbarService);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
