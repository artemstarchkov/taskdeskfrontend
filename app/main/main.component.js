System.register(["@angular/core", "rxjs/add/operator/map", "../sharedService/config.service", "./main.service", "../sharedService/socket.service", "../chat/chat.service", "@angular/common", "../toolbar/toolbar.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, config_service_1, main_service_1, socket_service_1, chat_service_1, common_1, toolbar_service_1, MainComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (_1) {
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (main_service_1_1) {
                main_service_1 = main_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            },
            function (chat_service_1_1) {
                chat_service_1 = chat_service_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (toolbar_service_1_1) {
                toolbar_service_1 = toolbar_service_1_1;
            }
        ],
        execute: function () {
            MainComponent = class MainComponent {
                constructor(configService, mainService, socketService, chatService, toolbarService) {
                    this.configService = configService;
                    this.mainService = mainService;
                    this.socketService = socketService;
                    this.chatService = chatService;
                    this.toolbarService = toolbarService;
                    this.defaultDatePipeLocale = 'en';
                }
                ngOnInit() {
                    if (this.configService.user) {
                        this.socketService.connection(this.configService.user.id);
                        this.chatService.senderId = this.configService.user.id;
                    }
                    this.socketService.socket.emit("user_online", this.configService.user.id);
                    if (this.socketService.socket) {
                        let datePipe = new common_1.DatePipe(this.defaultDatePipeLocale);
                        this._subGetMsg = this.mainService.getMessage()
                            .subscribe(data => {
                            let chatMessage = {};
                            if (!!this.chatService.chatRooms[data.from] == false) {
                                this.chatService.chatRooms[data.from] = [];
                            }
                            chatMessage = {
                                from: data.from,
                                fromFirstName: data.fromFirstName,
                                fromSecondName: data.fromSecondName,
                                fromLastName: data.fromLastName,
                                fromUsername: data.fromUsername,
                                to: data.to,
                                toFirstName: data.toFirstName,
                                toSecondName: data.toSecondName,
                                toLastName: data.toLastName,
                                toUsername: data.toUsername,
                                message: data.msg,
                                isNew: data.isNew,
                                time: {
                                    date: data.time.date
                                }
                            };
                            if (!this.chatService.chatWindowsState[data.from] || this.chatService.chatWindowsState[data.from] == false) {
                                if (!this.chatService.chatMessagesCounter[data.from]) {
                                    this.chatService.chatMessagesCounter[data.from] = 0;
                                }
                                this.chatService.chatMessagesCounter[data.from] += 1;
                            }
                            else {
                                this.chatService.chatMessagesCounter[data.from] = 0;
                                chatMessage.isNew = false;
                            }
                            this.chatService.chatRooms[data.from].push(chatMessage);
                        });
                        this._subOnEmitNewRelationshipRequest = this.toolbarService.onEmitNewRelationshipRequest().subscribe((data) => {
                            if (data.from && data.to) {
                                this._subRelationshipRequestsUser = this.toolbarService.getUserRelationshipRequestsStatusNew(this.toolbarService.currentUser.id).subscribe((rRequests) => {
                                    this.toolbarService.relationshipRequests = rRequests.preparedRR;
                                });
                            }
                        });
                    }
                }
                ngOnDestroy() {
                    if (this._subGetMsg) {
                        this._subGetMsg.unsubscribe();
                    }
                    if (this._subKeepCorrespondence) {
                        this._subKeepCorrespondence.unsubscribe();
                    }
                    if (this._subOnEmitNewRelationshipRequest) {
                        this._subOnEmitNewRelationshipRequest.unsubscribe();
                    }
                    if (this._subRelationshipRequestsUser) {
                        this._subRelationshipRequestsUser.unsubscribe();
                    }
                }
            };
            MainComponent = __decorate([
                core_1.Component({
                    selector: 'main',
                    templateUrl: 'app/main/main.component.html',
                    styleUrls: ['app/main/main.component.scss']
                }),
                __metadata("design:paramtypes", [config_service_1.ConfigService,
                    main_service_1.MainService,
                    socket_service_1.SocketService,
                    chat_service_1.ChatService,
                    toolbar_service_1.ToolbarService])
            ], MainComponent);
            exports_1("MainComponent", MainComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL21haW4vbWFpbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQWdCYSxhQUFhLEdBQTFCO2dCQVFJLFlBQW1CLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLGFBQTRCLEVBQzVCLFdBQXdCLEVBQ3hCLGNBQThCO29CQUo5QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFDNUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7b0JBQ3hCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO29CQUM1QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtvQkFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO29CQU4xQywwQkFBcUIsR0FBVyxJQUFJLENBQUM7Z0JBTVEsQ0FBQztnQkFFckQsUUFBUTtvQkFHSixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQzFCLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUMxRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7b0JBRTNELENBQUM7b0JBR0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFFMUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUU1QixJQUFJLFFBQVEsR0FBRyxJQUFJLGlCQUFRLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7d0JBR3hELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUU7NkJBQzFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTs0QkFFZCxJQUFJLFdBQVcsR0FBTyxFQUFFLENBQUM7NEJBSXpCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQztnQ0FDbkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQzs0QkFDL0MsQ0FBQzs0QkFFRCxXQUFXLEdBQUc7Z0NBQ1YsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dDQUNmLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYTtnQ0FDakMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO2dDQUNuQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVk7Z0NBQy9CLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWTtnQ0FDL0IsRUFBRSxFQUFDLElBQUksQ0FBQyxFQUFFO2dDQUNWLFdBQVcsRUFBQyxJQUFJLENBQUMsV0FBVztnQ0FDNUIsWUFBWSxFQUFDLElBQUksQ0FBQyxZQUFZO2dDQUM5QixVQUFVLEVBQUMsSUFBSSxDQUFDLFVBQVU7Z0NBQzFCLFVBQVUsRUFBQyxJQUFJLENBQUMsVUFBVTtnQ0FDMUIsT0FBTyxFQUFFLElBQUksQ0FBQyxHQUFHO2dDQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0NBQ2pCLElBQUksRUFBRTtvQ0FDRixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJO2lDQUN2Qjs2QkFDSixDQUFDOzRCQUdGLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQztnQ0FHekcsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0NBQ25ELElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQ0FDeEQsQ0FBQztnQ0FHRCxJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3pELENBQUM7NEJBQUMsSUFBSSxDQUFDLENBQUM7Z0NBR0osSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dDQUNwRCxXQUFXLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzs0QkFDOUIsQ0FBQzs0QkFHRCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUM1RCxDQUFDLENBQUMsQ0FBQzt3QkFHUCxJQUFJLENBQUMsZ0NBQWdDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyw0QkFBNEIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFOzRCQUMvRyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dDQUV2QixJQUFJLENBQUMsNEJBQTRCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQ0FBb0MsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtvQ0FDckosSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDO2dDQUNwRSxDQUFDLENBQUMsQ0FBQzs0QkFDUCxDQUFDO3dCQUNMLENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUM7Z0JBQ0wsQ0FBQztnQkFLRCxXQUFXO29CQUNQLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO3dCQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNsQyxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7d0JBQzlCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDOUMsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsQ0FBQyxDQUFDO3dCQUN4QyxJQUFJLENBQUMsZ0NBQWdDLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3hELENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNwRCxDQUFDO2dCQUNMLENBQUM7YUFDSixDQUFBO1lBOUdZLGFBQWE7Z0JBTnpCLGdCQUFTLENBQUM7b0JBQ1AsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLFdBQVcsRUFBRSw4QkFBOEI7b0JBQzNDLFNBQVMsRUFBRSxDQUFDLDhCQUE4QixDQUFDO2lCQUM5QyxDQUFDO2lEQVVvQyw4QkFBYTtvQkFDZiwwQkFBVztvQkFDVCw4QkFBYTtvQkFDZiwwQkFBVztvQkFDUixnQ0FBYztlQVp4QyxhQUFhLENBOEd6Qjs7UUFBQSxDQUFDIiwiZmlsZSI6Im1haW4vbWFpbi5jb21wb25lbnQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3l9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9tYXBcIjtcclxuaW1wb3J0IHtDb25maWdTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9jb25maWcuc2VydmljZVwiO1xyXG5pbXBvcnQge01haW5TZXJ2aWNlfSBmcm9tIFwiLi9tYWluLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtTb2NrZXRTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9zb2NrZXQuc2VydmljZVwiO1xyXG5pbXBvcnQge1N1YnNjcmlwdGlvbn0gZnJvbSBcInJ4anMvU3Vic2NyaXB0aW9uXCI7XHJcbmltcG9ydCB7Q2hhdFNlcnZpY2V9IGZyb20gXCIuLi9jaGF0L2NoYXQuc2VydmljZVwiO1xyXG5pbXBvcnQge0RhdGVQaXBlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge1Rvb2xiYXJTZXJ2aWNlfSBmcm9tIFwiLi4vdG9vbGJhci90b29sYmFyLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtYWluJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnYXBwL21haW4vbWFpbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnYXBwL21haW4vbWFpbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTWFpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIHByaXZhdGUgX3N1YkdldE1zZzogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViS2VlcENvcnJlc3BvbmRlbmNlOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJPbkVtaXROZXdSZWxhdGlvbnNoaXBSZXF1ZXN0OiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJSZWxhdGlvbnNoaXBSZXF1ZXN0c1VzZXI6IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgICBwdWJsaWMgZGVmYXVsdERhdGVQaXBlTG9jYWxlOiBzdHJpbmcgPSAnZW4nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBjb25maWdTZXJ2aWNlOiBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIG1haW5TZXJ2aWNlOiBNYWluU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBzb2NrZXRTZXJ2aWNlOiBTb2NrZXRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNoYXRTZXJ2aWNlOiBDaGF0U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0b29sYmFyU2VydmljZTogVG9vbGJhclNlcnZpY2UpIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy8gSWYgbG9nIGluIHdhcyBzdWNjZXNzIGFuZCB1c2VyIGV4aXN0LCB0aGVuIGNyZWF0ZSB1c2VyJ3Mgc29ja2V0IGNvbm5lY3Rpb24uXHJcbiAgICAgICAgLy8gU2V0IHRvIGNoYXRTZXJ2aWNlJ3MgdmFyaWFibGUgXCJzZW5kZXJJZFwiIGRhdGEgKHVzZXIgaWQpXHJcbiAgICAgICAgaWYgKHRoaXMuY29uZmlnU2VydmljZS51c2VyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc29ja2V0U2VydmljZS5jb25uZWN0aW9uKHRoaXMuY29uZmlnU2VydmljZS51c2VyLmlkKTtcclxuICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5zZW5kZXJJZCA9IHRoaXMuY29uZmlnU2VydmljZS51c2VyLmlkO1xyXG5cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEVtaXQgZXZlbnQgdGhhdCB1c2VyIG9ubGluZSBpbiB0aGUgc3lzdGVtXHJcbiAgICAgICAgdGhpcy5zb2NrZXRTZXJ2aWNlLnNvY2tldC5lbWl0KFwidXNlcl9vbmxpbmVcIiwgdGhpcy5jb25maWdTZXJ2aWNlLnVzZXIuaWQpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5zb2NrZXRTZXJ2aWNlLnNvY2tldCkge1xyXG4gICAgICAgICAgICAvLyBDcmVhdGUgZGF0ZSBwaXBlIHRvIG1hbmlwdWxhdGUgZGF0ZSdzIGZvcm1hdCBhbmQgdXNlIGFub3RoZXIgZGF0ZSdzIHNlcnZpY2VzXHJcbiAgICAgICAgICAgIGxldCBkYXRlUGlwZSA9IG5ldyBEYXRlUGlwZSh0aGlzLmRlZmF1bHREYXRlUGlwZUxvY2FsZSk7XHJcblxyXG4gICAgICAgICAgICAvLyBTdWJzY3JpYmUgdG8gdGhlIGV2ZW50IGFib3V0IHJlY2VpdmluZyBtZXNzYWdlcyBmcm9tIGFub3RoZXIgdXNlciB2aWEgdGhlIHNvY2tldCBjb25uZWN0aW9uXHJcbiAgICAgICAgICAgIHRoaXMuX3N1YkdldE1zZyA9IHRoaXMubWFpblNlcnZpY2UuZ2V0TWVzc2FnZSgpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIE1lc3NhZ2UncyBvYmplY3RcclxuICAgICAgICAgICAgICAgICAgICBsZXQgY2hhdE1lc3NhZ2U6YW55ID0ge307XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIENoZWNrIGlmIGN1cnJlbnQgc2VuZGVyKHVzZXIgd2hvIHNlbmQgbWUgbWVzc2FnZSkgbm90IGV4aXN0IGluIFwiY2hhdCByb29tc1wiICh3ZSBkb2Vzbid0IHRhbGtlZCB3aXRoIGhpbSBlYXJseSksXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhlbiBpbml0aWFsaXplIGVtcHR5IGFycmF5IHdoZXJlIHNhdmUgY2hhdCBtZXNzYWdlcyB0byBjdXJyZW50IHNlbmRlclxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghIXRoaXMuY2hhdFNlcnZpY2UuY2hhdFJvb21zW2RhdGEuZnJvbV0gPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0Um9vbXNbZGF0YS5mcm9tXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY2hhdE1lc3NhZ2UgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZyb206IGRhdGEuZnJvbSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZnJvbUZpcnN0TmFtZTogZGF0YS5mcm9tRmlyc3ROYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmcm9tU2Vjb25kTmFtZTogZGF0YS5mcm9tU2Vjb25kTmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZnJvbUxhc3ROYW1lOiBkYXRhLmZyb21MYXN0TmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZnJvbVVzZXJuYW1lOiBkYXRhLmZyb21Vc2VybmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG86ZGF0YS50byxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9GaXJzdE5hbWU6ZGF0YS50b0ZpcnN0TmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9TZWNvbmROYW1lOmRhdGEudG9TZWNvbmROYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b0xhc3ROYW1lOmRhdGEudG9MYXN0TmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9Vc2VybmFtZTpkYXRhLnRvVXNlcm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGRhdGEubXNnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc05ldzogZGF0YS5pc05ldyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGltZToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZTogZGF0YS50aW1lLmRhdGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vQ2hlY2sgaWYgdGhlIGNoYXQgd2luZG93IHdhcyBub3Qgb3BlbmVkIHdoZW4gdGhlIG1lc3NhZ2UgY2FtZSBmcm9tIHRoZSBzZW5kZXJcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXRoaXMuY2hhdFNlcnZpY2UuY2hhdFdpbmRvd3NTdGF0ZVtkYXRhLmZyb21dIHx8IHRoaXMuY2hhdFNlcnZpY2UuY2hhdFdpbmRvd3NTdGF0ZVtkYXRhLmZyb21dID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFdlIGNoZWNrIGlmIHRoZSBjb3VudGVyIG9mIHRoZSBudW1iZXIgb2YgbmV3IG1lc3NhZ2VzIHdhcyBub3QgcHJldmlvdXNseSBjcmVhdGVkIGZvciB0aGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gc2VuZGVyICh0aGF0IGlzLCBubyBuZXcgbWVzc2FnZXMgZnJvbSB0aGlzIHVzZXIgd2VyZSByZWNlaXZlZCBhZnRlciBJIGxvZ2dlZCBpbiksIHRoZW4gaW5pdGlhbGl6ZSB0aGUgY291bnRlciB3aXRoIHRoZSB2YWx1ZSAwXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy5jaGF0U2VydmljZS5jaGF0TWVzc2FnZXNDb3VudGVyW2RhdGEuZnJvbV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2hhdFNlcnZpY2UuY2hhdE1lc3NhZ2VzQ291bnRlcltkYXRhLmZyb21dID0gMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gYW5kIGluY3JlYXNlIHRoZSBjb3VudGVyIHRvIDEgbmV3IG1lc3NhZ2UgZnJvbSB0aGUgY3VycmVudCBzZW5kZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0TWVzc2FnZXNDb3VudGVyW2RhdGEuZnJvbV0gKz0gMTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJZiBhdCB0aGUgdGltZSB0aGUgbmV3IG1lc3NhZ2UgYXJyaXZlZCB0aGUgY2hhdCB3aW5kb3cgd2l0aCB0aGUgc2VuZGVyIHdhcyBvcGVuZWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHRoZW4gdGhlIGNvdW50ZXIgaXMgcmVzZXQgdG8gdGhlIHZhbHVlIDAgZm9yIHRoZSBzZW5kZXJcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0TWVzc2FnZXNDb3VudGVyW2RhdGEuZnJvbV0gPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjaGF0TWVzc2FnZS5pc05ldyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gUHV0IG1lc3NhZ2UncyBvYmplY3QgdG8gY3VycmVudCBzZW5kZXIncyBjaGF0IHJvb21zXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jaGF0U2VydmljZS5jaGF0Um9vbXNbZGF0YS5mcm9tXS5wdXNoKGNoYXRNZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgLy8gU3Vic2NyaWJlIHRvIHRoZSBldmVudCBvZiB0aGUgcmVxdWVzdCB0byBhZGQgdG8gY29udGFjdHMgZnJvbSBhbm90aGVyIHVzZXIgKHRoZSByZXF1ZXN0IGV2ZW50IGNvbWVzIHRocm91Z2ggdGhlIHNvY2tldCBjb25uZWN0aW9uKVxyXG4gICAgICAgICAgICB0aGlzLl9zdWJPbkVtaXROZXdSZWxhdGlvbnNoaXBSZXF1ZXN0ID0gdGhpcy50b29sYmFyU2VydmljZS5vbkVtaXROZXdSZWxhdGlvbnNoaXBSZXF1ZXN0KCkuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLmZyb20gJiYgZGF0YS50bykge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFVwZGF0ZSBsaXN0IG9mIHJlbGF0aW9uc2hpcCByZXF1ZXN0cyB3aXRoIHN0YXR1cyBcIm5ld1wiXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyID0gdGhpcy50b29sYmFyU2VydmljZS5nZXRVc2VyUmVsYXRpb25zaGlwUmVxdWVzdHNTdGF0dXNOZXcodGhpcy50b29sYmFyU2VydmljZS5jdXJyZW50VXNlci5pZCkuc3Vic2NyaWJlKChyUmVxdWVzdHMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b29sYmFyU2VydmljZS5yZWxhdGlvbnNoaXBSZXF1ZXN0cyA9IHJSZXF1ZXN0cy5wcmVwYXJlZFJSO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCZWZvcmUgZGVzdHJveSBjb21wb25lbnQgdW5zdWJzY3JpYmVkIGZyb20gZXZlbnRzXHJcbiAgICAgKi9cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9zdWJHZXRNc2cpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViR2V0TXNnLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJLZWVwQ29ycmVzcG9uZGVuY2UpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViS2VlcENvcnJlc3BvbmRlbmNlLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJPbkVtaXROZXdSZWxhdGlvbnNoaXBSZXF1ZXN0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1Yk9uRW1pdE5ld1JlbGF0aW9uc2hpcFJlcXVlc3QudW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX3N1YlJlbGF0aW9uc2hpcFJlcXVlc3RzVXNlcikge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJSZWxhdGlvbnNoaXBSZXF1ZXN0c1VzZXIudW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iXX0=
