import {Component, OnInit, OnDestroy} from '@angular/core';
import "rxjs/add/operator/map";
import {ConfigService} from "../sharedService/config.service";
import {MainService} from "./main.service";
import {SocketService} from "../sharedService/socket.service";
import {Subscription} from "rxjs/Subscription";
import {ChatService} from "../chat/chat.service";
import {DatePipe} from '@angular/common';
import {ToolbarService} from "../toolbar/toolbar.service";

@Component({
    selector: 'main',
    templateUrl: 'app/main/main.component.html',
    styleUrls: ['app/main/main.component.scss']
})

export class MainComponent implements OnInit, OnDestroy {
    private _subGetMsg: Subscription;
    private _subKeepCorrespondence: Subscription;
    private _subOnEmitNewRelationshipRequest: Subscription;
    private _subRelationshipRequestsUser: Subscription;

    public defaultDatePipeLocale: string = 'en';

    constructor(public configService: ConfigService,
                public mainService: MainService,
                public socketService: SocketService,
                public chatService: ChatService,
                public toolbarService: ToolbarService) {}

    ngOnInit() {
        // If log in was success and user exist, then create user's socket connection.
        // Set to chatService's variable "senderId" data (user id)
        if (this.configService.user) {
            this.socketService.connection(this.configService.user.id);
            this.chatService.senderId = this.configService.user.id;

        }

        // Emit event that user online in the system
        this.socketService.socket.emit("user_online", this.configService.user.id);

        if (this.socketService.socket) {
            // Create date pipe to manipulate date's format and use another date's services
            let datePipe = new DatePipe(this.defaultDatePipeLocale);

            // Subscribe to the event about receiving messages from another user via the socket connection
            this._subGetMsg = this.mainService.getMessage()
                .subscribe(data => {
                    // Message's object
                    let chatMessage:any = {};

                    // Check if current sender(user who send me message) not exist in "chat rooms" (we doesn't talked with him early),
                    // then initialize empty array where save chat messages to current sender
                    if (!!this.chatService.chatRooms[data.from] == false) {
                        this.chatService.chatRooms[data.from] = [];
                    }

                    chatMessage = {
                        from: data.from,
                        fromFirstName: data.fromFirstName,
                        fromSecondName: data.fromSecondName,
                        fromLastName: data.fromLastName,
                        fromUsername: data.fromUsername,
                        to:data.to,
                        toFirstName:data.toFirstName,
                        toSecondName:data.toSecondName,
                        toLastName:data.toLastName,
                        toUsername:data.toUsername,
                        message: data.msg,
                        isNew: data.isNew,
                        time: {
                            date: data.time.date
                        }
                    };

                    //Check if the chat window was not opened when the message came from the sender
                    if (!this.chatService.chatWindowsState[data.from] || this.chatService.chatWindowsState[data.from] == false) {
                        // We check if the counter of the number of new messages was not previously created for the
                        // sender (that is, no new messages from this user were received after I logged in), then initialize the counter with the value 0
                        if (!this.chatService.chatMessagesCounter[data.from]) {
                            this.chatService.chatMessagesCounter[data.from] = 0;
                        }

                        // and increase the counter to 1 new message from the current sender
                        this.chatService.chatMessagesCounter[data.from] += 1;
                    } else {
                        // If at the time the new message arrived the chat window with the sender was opened,
                        // then the counter is reset to the value 0 for the sender
                        this.chatService.chatMessagesCounter[data.from] = 0;
                        chatMessage.isNew = false;
                    }

                    // Put message's object to current sender's chat rooms
                    this.chatService.chatRooms[data.from].push(chatMessage);
                });

            // Subscribe to the event of the request to add to contacts from another user (the request event comes through the socket connection)
            this._subOnEmitNewRelationshipRequest = this.toolbarService.onEmitNewRelationshipRequest().subscribe((data: any) => {
                if (data.from && data.to) {
                    // Update list of relationship requests with status "new"
                    this._subRelationshipRequestsUser = this.toolbarService.getUserRelationshipRequestsStatusNew(this.toolbarService.currentUser.id).subscribe((rRequests) => {
                        this.toolbarService.relationshipRequests = rRequests.preparedRR;
                    });
                }
            });
        }
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subGetMsg) {
            this._subGetMsg.unsubscribe();
        }
        if (this._subKeepCorrespondence) {
            this._subKeepCorrespondence.unsubscribe();
        }
        if (this._subOnEmitNewRelationshipRequest) {
            this._subOnEmitNewRelationshipRequest.unsubscribe();
        }
        if (this._subRelationshipRequestsUser) {
            this._subRelationshipRequestsUser.unsubscribe();
        }
    }
}