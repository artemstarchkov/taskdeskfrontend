System.register(["@angular/core", "./main.component", "./main.route", "../leftSidebarMenu/leftSidebarMenu.component", "../leftSidebarMenu/leftSideBarMenu.service", "../userSearch/userSearch.component", "../userSearch/userSearch.service", "@angular/common", "../sharedService/config.service", "@angular/platform-browser", "./main.service", "../sharedService/socket.service", "../chat/chat.component", "../chat/chat.service", "../task/task.component", "../task/task.service", "@angular/forms", "../task/task.model", "../profileSetting/profileSetting.component", "../profileSetting/profileSetting.service", "../profileSetting/profileSetting.model", "../sharedModule/shared.module"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, main_component_1, main_route_1, leftSidebarMenu_component_1, leftSideBarMenu_service_1, userSearch_component_1, userSearch_service_1, common_1, config_service_1, platform_browser_1, main_service_1, socket_service_1, chat_component_1, chat_service_1, task_component_1, task_service_1, forms_1, task_model_1, profileSetting_component_1, profileSetting_service_1, profileSetting_model_1, shared_module_1, MainModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (main_component_1_1) {
                main_component_1 = main_component_1_1;
            },
            function (main_route_1_1) {
                main_route_1 = main_route_1_1;
            },
            function (leftSidebarMenu_component_1_1) {
                leftSidebarMenu_component_1 = leftSidebarMenu_component_1_1;
            },
            function (leftSideBarMenu_service_1_1) {
                leftSideBarMenu_service_1 = leftSideBarMenu_service_1_1;
            },
            function (userSearch_component_1_1) {
                userSearch_component_1 = userSearch_component_1_1;
            },
            function (userSearch_service_1_1) {
                userSearch_service_1 = userSearch_service_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (main_service_1_1) {
                main_service_1 = main_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            },
            function (chat_component_1_1) {
                chat_component_1 = chat_component_1_1;
            },
            function (chat_service_1_1) {
                chat_service_1 = chat_service_1_1;
            },
            function (task_component_1_1) {
                task_component_1 = task_component_1_1;
            },
            function (task_service_1_1) {
                task_service_1 = task_service_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (task_model_1_1) {
                task_model_1 = task_model_1_1;
            },
            function (profileSetting_component_1_1) {
                profileSetting_component_1 = profileSetting_component_1_1;
            },
            function (profileSetting_service_1_1) {
                profileSetting_service_1 = profileSetting_service_1_1;
            },
            function (profileSetting_model_1_1) {
                profileSetting_model_1 = profileSetting_model_1_1;
            },
            function (shared_module_1_1) {
                shared_module_1 = shared_module_1_1;
            }
        ],
        execute: function () {
            MainModule = class MainModule {
            };
            MainModule = __decorate([
                core_1.NgModule({
                    imports: [
                        main_route_1.MAIN_ROUTES,
                        common_1.CommonModule,
                        platform_browser_1.BrowserModule,
                        forms_1.FormsModule,
                        shared_module_1.SharedModule
                    ],
                    declarations: [
                        main_component_1.MainComponent,
                        leftSidebarMenu_component_1.LeftSideBarMenuComponent,
                        userSearch_component_1.UserSearchComponent,
                        chat_component_1.ChatComponent,
                        task_component_1.TaskComponent,
                        profileSetting_component_1.ProfileSettingComponent
                    ],
                    providers: [
                        leftSideBarMenu_service_1.LeftSideBarMenuService,
                        userSearch_service_1.UserSearchService,
                        config_service_1.ConfigService,
                        main_service_1.MainService,
                        socket_service_1.SocketService,
                        chat_service_1.ChatService,
                        task_service_1.TaskService,
                        task_model_1.TaskModel,
                        profileSetting_service_1.ProfileSettingService,
                        profileSetting_model_1.ProfileSettingModel
                    ]
                })
            ], MainModule);
            exports_1("MainModule", MainModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL21haW4vbWFpbi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFvRGEsVUFBVSxHQUF2QjthQUEyQixDQUFBO1lBQWQsVUFBVTtnQkE3QnRCLGVBQVEsQ0FBQztvQkFDTixPQUFPLEVBQU87d0JBQ1Ysd0JBQVc7d0JBQ1gscUJBQVk7d0JBQ1osZ0NBQWE7d0JBQ2IsbUJBQVc7d0JBQ1gsNEJBQVk7cUJBQ2Y7b0JBQ0QsWUFBWSxFQUFFO3dCQUNWLDhCQUFhO3dCQUNiLG9EQUF3Qjt3QkFDeEIsMENBQW1CO3dCQUNuQiw4QkFBYTt3QkFDYiw4QkFBYTt3QkFDYixrREFBdUI7cUJBQzFCO29CQUNELFNBQVMsRUFBRTt3QkFDUCxnREFBc0I7d0JBQ3RCLHNDQUFpQjt3QkFDakIsOEJBQWE7d0JBQ2IsMEJBQVc7d0JBQ1gsOEJBQWE7d0JBQ2IsMEJBQVc7d0JBQ1gsMEJBQVc7d0JBQ1gsc0JBQVM7d0JBQ1QsOENBQXFCO3dCQUNyQiwwQ0FBbUI7cUJBQ3RCO2lCQUNKLENBQUM7ZUFDVyxVQUFVLENBQUk7O1FBQUEsQ0FBQyIsImZpbGUiOiJtYWluL21haW4ubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7TWFpbkNvbXBvbmVudH0gZnJvbSBcIi4vbWFpbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTUFJTl9ST1VURVMgfSBmcm9tICcuL21haW4ucm91dGUnO1xyXG5pbXBvcnQge0xlZnRTaWRlQmFyTWVudUNvbXBvbmVudH0gZnJvbSBcIi4uL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZWJhck1lbnUuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7TGVmdFNpZGVCYXJNZW51U2VydmljZX0gZnJvbSBcIi4uL2xlZnRTaWRlYmFyTWVudS9sZWZ0U2lkZUJhck1lbnUuc2VydmljZVwiO1xyXG5pbXBvcnQge1VzZXJTZWFyY2hDb21wb25lbnR9IGZyb20gXCIuLi91c2VyU2VhcmNoL3VzZXJTZWFyY2guY29tcG9uZW50XCI7XHJcbmltcG9ydCB7VXNlclNlYXJjaFNlcnZpY2V9IGZyb20gXCIuLi91c2VyU2VhcmNoL3VzZXJTZWFyY2guc2VydmljZVwiO1xyXG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7QnJvd3Nlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXJcIjtcclxuaW1wb3J0IHtNYWluU2VydmljZX0gZnJvbSBcIi4vbWFpbi5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7U29ja2V0U2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZFNlcnZpY2Uvc29ja2V0LnNlcnZpY2VcIjtcclxuaW1wb3J0IHtDaGF0Q29tcG9uZW50fSBmcm9tIFwiLi4vY2hhdC9jaGF0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge0NoYXRTZXJ2aWNlfSBmcm9tIFwiLi4vY2hhdC9jaGF0LnNlcnZpY2VcIjtcclxuaW1wb3J0IHtUYXNrQ29tcG9uZW50fSBmcm9tIFwiLi4vdGFzay90YXNrLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge1Rhc2tTZXJ2aWNlfSBmcm9tIFwiLi4vdGFzay90YXNrLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtGb3Jtc01vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7VGFza01vZGVsfSBmcm9tIFwiLi4vdGFzay90YXNrLm1vZGVsXCI7XHJcbmltcG9ydCB7UHJvZmlsZVNldHRpbmdDb21wb25lbnR9IGZyb20gXCIuLi9wcm9maWxlU2V0dGluZy9wcm9maWxlU2V0dGluZy5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtQcm9maWxlU2V0dGluZ1NlcnZpY2V9IGZyb20gXCIuLi9wcm9maWxlU2V0dGluZy9wcm9maWxlU2V0dGluZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7UHJvZmlsZVNldHRpbmdNb2RlbH0gZnJvbSBcIi4uL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLm1vZGVsXCI7XHJcbmltcG9ydCB7U2hhcmVkTW9kdWxlfSBmcm9tIFwiLi4vc2hhcmVkTW9kdWxlL3NoYXJlZC5tb2R1bGVcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiAgICAgIFtcclxuICAgICAgICBNQUlOX1JPVVRFUyxcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgQnJvd3Nlck1vZHVsZSxcclxuICAgICAgICBGb3Jtc01vZHVsZSxcclxuICAgICAgICBTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBNYWluQ29tcG9uZW50LFxyXG4gICAgICAgIExlZnRTaWRlQmFyTWVudUNvbXBvbmVudCxcclxuICAgICAgICBVc2VyU2VhcmNoQ29tcG9uZW50LFxyXG4gICAgICAgIENoYXRDb21wb25lbnQsXHJcbiAgICAgICAgVGFza0NvbXBvbmVudCxcclxuICAgICAgICBQcm9maWxlU2V0dGluZ0NvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIExlZnRTaWRlQmFyTWVudVNlcnZpY2UsXHJcbiAgICAgICAgVXNlclNlYXJjaFNlcnZpY2UsXHJcbiAgICAgICAgQ29uZmlnU2VydmljZSxcclxuICAgICAgICBNYWluU2VydmljZSxcclxuICAgICAgICBTb2NrZXRTZXJ2aWNlLFxyXG4gICAgICAgIENoYXRTZXJ2aWNlLFxyXG4gICAgICAgIFRhc2tTZXJ2aWNlLFxyXG4gICAgICAgIFRhc2tNb2RlbCxcclxuICAgICAgICBQcm9maWxlU2V0dGluZ1NlcnZpY2UsXHJcbiAgICAgICAgUHJvZmlsZVNldHRpbmdNb2RlbFxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFpbk1vZHVsZSB7IH0iXX0=
