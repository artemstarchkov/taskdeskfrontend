import {NgModule} from '@angular/core';
import {MainComponent} from "./main.component";
import { MAIN_ROUTES } from './main.route';
import {LeftSideBarMenuComponent} from "../leftSidebarMenu/leftSidebarMenu.component";
import {LeftSideBarMenuService} from "../leftSidebarMenu/leftSideBarMenu.service";
import {UserSearchComponent} from "../userSearch/userSearch.component";
import {UserSearchService} from "../userSearch/userSearch.service";
import {CommonModule} from "@angular/common";
import {ConfigService} from "../sharedService/config.service";
import {BrowserModule} from "@angular/platform-browser";
import {MainService} from "./main.service";
import {SocketService} from "../sharedService/socket.service";
import {ChatComponent} from "../chat/chat.component";
import {ChatService} from "../chat/chat.service";
import {TaskComponent} from "../task/task.component";
import {TaskService} from "../task/task.service";
import {FormsModule} from "@angular/forms";
import {TaskModel} from "../task/task.model";
import {ProfileSettingComponent} from "../profileSetting/profileSetting.component";
import {ProfileSettingService} from "../profileSetting/profileSetting.service";
import {ProfileSettingModel} from "../profileSetting/profileSetting.model";
import {SharedModule} from "../sharedModule/shared.module";

@NgModule({
    imports:      [
        MAIN_ROUTES,
        CommonModule,
        BrowserModule,
        FormsModule,
        SharedModule
    ],
    declarations: [
        MainComponent,
        LeftSideBarMenuComponent,
        UserSearchComponent,
        ChatComponent,
        TaskComponent,
        ProfileSettingComponent
    ],
    providers: [
        LeftSideBarMenuService,
        UserSearchService,
        ConfigService,
        MainService,
        SocketService,
        ChatService,
        TaskService,
        TaskModel,
        ProfileSettingService,
        ProfileSettingModel
    ]
})
export class MainModule { }