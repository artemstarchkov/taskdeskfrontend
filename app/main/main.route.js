System.register(["@angular/router", "./main.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, main_component_1, mainRoutes, MAIN_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (main_component_1_1) {
                main_component_1 = main_component_1_1;
            }
        ],
        execute: function () {
            mainRoutes = [
                {
                    path: 'main-page',
                    component: main_component_1.MainComponent
                }
            ];
            exports_1("MAIN_ROUTES", MAIN_ROUTES = router_1.RouterModule.forChild(mainRoutes));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL21haW4vbWFpbi5yb3V0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztZQUlNLFVBQVUsR0FBVTtnQkFDdEI7b0JBQ0ksSUFBSSxFQUFFLFdBQVc7b0JBQ2pCLFNBQVMsRUFBRSw4QkFBYTtpQkFDM0I7YUFDSixDQUFDO1lBRUYseUJBQWEsV0FBVyxHQUF1QixxQkFBWSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBQztRQUFBLENBQUMiLCJmaWxlIjoibWFpbi9tYWluLnJvdXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSb3V0ZXMsIFJvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge01vZHVsZVdpdGhQcm92aWRlcnN9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7TWFpbkNvbXBvbmVudH0gZnJvbSBcIi4vbWFpbi5jb21wb25lbnRcIjtcclxuXHJcbmNvbnN0IG1haW5Sb3V0ZXM6Um91dGVzID0gW1xyXG4gICAge1xyXG4gICAgICAgIHBhdGg6ICdtYWluLXBhZ2UnLFxyXG4gICAgICAgIGNvbXBvbmVudDogTWFpbkNvbXBvbmVudFxyXG4gICAgfVxyXG5dO1xyXG5cclxuZXhwb3J0IGNvbnN0IE1BSU5fUk9VVEVTOk1vZHVsZVdpdGhQcm92aWRlcnMgPSBSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQobWFpblJvdXRlcyk7Il19
