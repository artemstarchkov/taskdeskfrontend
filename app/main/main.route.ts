import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {MainComponent} from "./main.component";

const mainRoutes:Routes = [
    {
        path: 'main-page',
        component: MainComponent
    }
];

export const MAIN_ROUTES:ModuleWithProviders = RouterModule.forChild(mainRoutes);