import { Observable } from "rxjs/Observable";
import { ConfigService } from "../sharedService/config.service";
import { SocketService } from "../sharedService/socket.service";
export declare class MainService {
    configService: ConfigService;
    socketService: SocketService;
    constructor(configService: ConfigService, socketService: SocketService);
    getMessage(): Observable<any>;
    sendMessage(data: any): void;
}
