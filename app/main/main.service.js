System.register(["@angular/core", "../sharedService/config.service", "../sharedService/socket.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, config_service_1, socket_service_1, MainService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            }
        ],
        execute: function () {
            MainService = class MainService {
                constructor(configService, socketService) {
                    this.configService = configService;
                    this.socketService = socketService;
                }
                getMessage() {
                    if (this.socketService.socket) {
                        return this.socketService
                            .fromEvent("received_msg")
                            .map(data => data);
                    }
                }
                sendMessage(data) {
                    this.socketService.socket.emit("send_msg", data);
                }
            };
            MainService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [config_service_1.ConfigService,
                    socket_service_1.SocketService])
            ], MainService);
            exports_1("MainService", MainService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL21haW4vbWFpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBTWEsV0FBVyxHQUF4QjtnQkFFSSxZQUFtQixhQUE0QixFQUM1QixhQUE0QjtvQkFENUIsa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBQzVCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO2dCQUFHLENBQUM7Z0JBTTVDLFVBQVU7b0JBQ2IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUM1QixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWE7NkJBQ3BCLFNBQVMsQ0FBTSxjQUFjLENBQUM7NkJBQzlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMzQixDQUFDO2dCQUNMLENBQUM7Z0JBTU0sV0FBVyxDQUFDLElBQUk7b0JBQ25CLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3JELENBQUM7YUFDSixDQUFBO1lBeEJZLFdBQVc7Z0JBRHZCLGlCQUFVLEVBQUU7aURBR3lCLDhCQUFhO29CQUNiLDhCQUFhO2VBSHRDLFdBQVcsQ0F3QnZCOztRQUFBLENBQUMiLCJmaWxlIjoibWFpbi9tYWluLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7U29ja2V0U2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZFNlcnZpY2Uvc29ja2V0LnNlcnZpY2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIE1haW5TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBzb2NrZXRTZXJ2aWNlOiBTb2NrZXRTZXJ2aWNlKSB7fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3Vic2NyaWJlIG9uIGdldCBuZXcgbWVzc2FnZSBmcm9tIGFub3RoZXIgdXNlciB2aWEgc29ja2V0IGNvbm5lY3Rpb25cclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRNZXNzYWdlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnNvY2tldFNlcnZpY2Uuc29ja2V0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNvY2tldFNlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5mcm9tRXZlbnQ8YW55PihcInJlY2VpdmVkX21zZ1wiKVxyXG4gICAgICAgICAgICAgICAgLm1hcChkYXRhID0+IGRhdGEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbmQgbWVzc2FnZSBvYmplY3QgdG8gcmVjZWl2ZXIgdmlhIHNvY2tldCBjb25uZWN0aW9uXHJcbiAgICAgKiBAcGFyYW0gZGF0YVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc2VuZE1lc3NhZ2UoZGF0YSkge1xyXG4gICAgICAgIHRoaXMuc29ja2V0U2VydmljZS5zb2NrZXQuZW1pdChcInNlbmRfbXNnXCIsIGRhdGEpO1xyXG4gICAgfVxyXG59Il19
