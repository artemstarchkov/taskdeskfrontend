import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ConfigService} from "../sharedService/config.service";
import {SocketService} from "../sharedService/socket.service";

@Injectable()
export class MainService {

    constructor(public configService: ConfigService,
                public socketService: SocketService) {}

    /**
     * Subscribe on get new message from another user via socket connection
     * @returns {Observable<any>}
     */
    public getMessage() {
        if (this.socketService.socket) {
            return this.socketService
                .fromEvent<any>("received_msg")
                .map(data => data);
        }
    }

    /**
     * Send message object to receiver via socket connection
     * @param data
     */
    public sendMessage(data) {
        this.socketService.socket.emit("send_msg", data);
    }
}