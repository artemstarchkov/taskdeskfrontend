import { OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ProfileSettingService } from "./profileSetting.service";
import { ProfileSettingModel } from "./profileSetting.model";
import { ConfigService } from "../sharedService/config.service";
import { DomSanitizer } from "@angular/platform-browser";
import { ChatService } from "../chat/chat.service";
import { TranslateService } from "ng2-translate";
import { Ng4LoadingSpinnerService } from "ng4-loading-spinner";
export declare class ProfileSettingComponent implements OnInit, OnDestroy {
    profileSettingService: ProfileSettingService;
    profileSettingModel: ProfileSettingModel;
    configService: ConfigService;
    domSanitizer: DomSanitizer;
    chatService: ChatService;
    translate: TranslateService;
    private spinnerService;
    avatarImageElement: ElementRef;
    private _subProfileSettingUserInfo;
    private _subProfileSettingUpdate;
    private userProfileSetting;
    constructor(profileSettingService: ProfileSettingService, profileSettingModel: ProfileSettingModel, configService: ConfigService, domSanitizer: DomSanitizer, chatService: ChatService, translate: TranslateService, spinnerService: Ng4LoadingSpinnerService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    updateProfileSetting(): void;
    onCloseProfileSettings(): void;
    onChangeAvatarImage(event: any): void;
}
