System.register(["@angular/core", "./profileSetting.service", "./profileSetting.model", "../sharedService/config.service", "@angular/platform-browser", "../chat/chat.service", "ng2-translate", "ng4-loading-spinner"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, profileSetting_service_1, profileSetting_model_1, config_service_1, platform_browser_1, chat_service_1, ng2_translate_1, ng4_loading_spinner_1, ProfileSettingComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (profileSetting_service_1_1) {
                profileSetting_service_1 = profileSetting_service_1_1;
            },
            function (profileSetting_model_1_1) {
                profileSetting_model_1 = profileSetting_model_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (chat_service_1_1) {
                chat_service_1 = chat_service_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (ng4_loading_spinner_1_1) {
                ng4_loading_spinner_1 = ng4_loading_spinner_1_1;
            }
        ],
        execute: function () {
            ProfileSettingComponent = class ProfileSettingComponent {
                constructor(profileSettingService, profileSettingModel, configService, domSanitizer, chatService, translate, spinnerService) {
                    this.profileSettingService = profileSettingService;
                    this.profileSettingModel = profileSettingModel;
                    this.configService = configService;
                    this.domSanitizer = domSanitizer;
                    this.chatService = chatService;
                    this.translate = translate;
                    this.spinnerService = spinnerService;
                }
                ngOnInit() {
                    if (this.configService.user.id) {
                        this._subProfileSettingUserInfo = this.profileSettingService.getUserProfile(this.configService.user.id).subscribe(userProfile => {
                            this.userProfileSetting = userProfile;
                            this.profileSettingModel.id = this.userProfileSetting.user.id;
                            this.profileSettingModel.firstName = this.userProfileSetting.user.first_name;
                            this.profileSettingModel.secondName = this.userProfileSetting.user.second_name;
                            this.profileSettingModel.lastName = this.userProfileSetting.user.last_name;
                            this.profileSettingModel.email = this.userProfileSetting.user.email;
                            this.profileSettingModel.username = this.userProfileSetting.user.username;
                        });
                    }
                }
                ngOnDestroy() {
                    if (this._subProfileSettingUserInfo) {
                        this._subProfileSettingUserInfo.unsubscribe();
                    }
                    if (this._subProfileSettingUpdate) {
                        this._subProfileSettingUpdate.unsubscribe();
                    }
                }
                updateProfileSetting() {
                    this.spinnerService.show();
                    this._subProfileSettingUpdate = this.profileSettingService.updateProfileSetting(this.profileSettingModel).subscribe(profileSetting => {
                        this.userProfileSetting = profileSetting;
                        this.profileSettingModel.id = this.userProfileSetting.user.id;
                        this.profileSettingModel.firstName = this.userProfileSetting.user.first_name;
                        this.profileSettingModel.secondName = this.userProfileSetting.user.second_name;
                        this.profileSettingModel.lastName = this.userProfileSetting.user.last_name;
                        this.profileSettingModel.email = this.userProfileSetting.user.email;
                        this.profileSettingModel.username = this.userProfileSetting.user.username;
                        this.configService.user.link_avatar_image = this.userProfileSetting.user.link_avatar_image;
                        this.profileSettingService.isChangedAvatarProfileSettings = false;
                        this.profileSettingService.changedAvatarProfileSettingsPath = '';
                        this.avatarImageElement.nativeElement.value = '';
                        this.spinnerService.hide();
                    }, (err) => {
                        this.spinnerService.hide();
                        console.info(err);
                    });
                }
                onCloseProfileSettings() {
                    this.profileSettingService.isShowProfileSettingsComponent = false;
                    this.profileSettingService.isChangedAvatarProfileSettings = false;
                    this.profileSettingService.changedAvatarProfileSettingsPath = '';
                    this.avatarImageElement.nativeElement.value = '';
                }
                onChangeAvatarImage(event) {
                    this.profileSettingModel.avatarImage = event.target.files[0];
                    this.profileSettingService.changedAvatarProfileSettingsPath = this.domSanitizer.bypassSecurityTrustUrl(event.srcElement.files[0].path);
                    this.profileSettingService.isChangedAvatarProfileSettings = true;
                }
            };
            __decorate([
                core_1.ViewChild('avatarImage'),
                __metadata("design:type", core_1.ElementRef)
            ], ProfileSettingComponent.prototype, "avatarImageElement", void 0);
            ProfileSettingComponent = __decorate([
                core_1.Component({
                    selector: 'profileSetting',
                    templateUrl: 'app/profileSetting/profileSetting.component.html',
                    styleUrls: ['app/profileSetting/profileSetting.component.scss']
                }),
                __metadata("design:paramtypes", [profileSetting_service_1.ProfileSettingService,
                    profileSetting_model_1.ProfileSettingModel,
                    config_service_1.ConfigService,
                    platform_browser_1.DomSanitizer,
                    chat_service_1.ChatService,
                    ng2_translate_1.TranslateService,
                    ng4_loading_spinner_1.Ng4LoadingSpinnerService])
            ], ProfileSettingComponent);
            exports_1("ProfileSettingComponent", ProfileSettingComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQWdCYSx1QkFBdUIsR0FBcEM7Z0JBT0ksWUFBbUIscUJBQTRDLEVBQzVDLG1CQUF3QyxFQUN4QyxhQUE0QixFQUM1QixZQUEwQixFQUMxQixXQUF3QixFQUN4QixTQUEyQixFQUMxQixjQUF3QztvQkFOekMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtvQkFDNUMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtvQkFDeEMsa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBQzVCLGlCQUFZLEdBQVosWUFBWSxDQUFjO29CQUMxQixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtvQkFDeEIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7b0JBQzFCLG1CQUFjLEdBQWQsY0FBYyxDQUEwQjtnQkFBRyxDQUFDO2dCQUVoRSxRQUFRO29CQUNKLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBRTdCLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsRUFBRTs0QkFDNUgsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFdBQVcsQ0FBQzs0QkFHdEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQzs0QkFDOUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQzs0QkFDN0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQzs0QkFDL0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzs0QkFDM0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzs0QkFDcEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQzt3QkFDOUUsQ0FBQyxDQUFDLENBQUE7b0JBQ04sQ0FBQztnQkFDTCxDQUFDO2dCQUtELFdBQVc7b0JBQ1AsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQzt3QkFDakMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNsRCxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7d0JBQ2hDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDaEQsQ0FBQztnQkFDTCxDQUFDO2dCQUtELG9CQUFvQjtvQkFDaEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDM0IsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEVBQUU7d0JBQ2pJLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxjQUFjLENBQUM7d0JBQ3pDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7d0JBQzlELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7d0JBQzdFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7d0JBQy9FLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7d0JBQzNFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7d0JBQ3BFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7d0JBQzFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7d0JBSTNGLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7d0JBSWxFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxnQ0FBZ0MsR0FBRyxFQUFFLENBQUM7d0JBR2pFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQzt3QkFDakQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDL0IsQ0FBQyxFQUNELENBQUMsR0FBUSxFQUFFLEVBQUU7d0JBQ1QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDM0IsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDdEIsQ0FBQyxDQUFDLENBQUE7Z0JBQ04sQ0FBQztnQkFNRCxzQkFBc0I7b0JBQ2xCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7b0JBQ2xFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyw4QkFBOEIsR0FBRyxLQUFLLENBQUM7b0JBQ2xFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxnQ0FBZ0MsR0FBRyxFQUFFLENBQUM7b0JBQ2pFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztnQkFDckQsQ0FBQztnQkFPRCxtQkFBbUIsQ0FBQyxLQUFLO29CQUNyQixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM3RCxJQUFJLENBQUMscUJBQXFCLENBQUMsZ0NBQWdDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdkksSUFBSSxDQUFDLHFCQUFxQixDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQztnQkFDckUsQ0FBQzthQUNKLENBQUE7WUFqRzZCO2dCQUF6QixnQkFBUyxDQUFDLGFBQWEsQ0FBQzswQ0FBcUIsaUJBQVU7K0VBQUM7WUFEaEQsdUJBQXVCO2dCQU5uQyxnQkFBUyxDQUFDO29CQUNQLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLFdBQVcsRUFBRSxrREFBa0Q7b0JBQy9ELFNBQVMsRUFBRSxDQUFDLGtEQUFrRCxDQUFDO2lCQUNsRSxDQUFDO2lEQVM0Qyw4Q0FBcUI7b0JBQ3ZCLDBDQUFtQjtvQkFDekIsOEJBQWE7b0JBQ2QsK0JBQVk7b0JBQ2IsMEJBQVc7b0JBQ2IsZ0NBQWdCO29CQUNWLDhDQUF3QjtlQWJuRCx1QkFBdUIsQ0FrR25DOztRQUFBLENBQUMiLCJmaWxlIjoicHJvZmlsZVNldHRpbmcvcHJvZmlsZVNldHRpbmcuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdCwgT25EZXN0cm95LCBFbGVtZW50UmVmLCBWaWV3Q2hpbGR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge1Byb2ZpbGVTZXR0aW5nU2VydmljZX0gZnJvbSBcIi4vcHJvZmlsZVNldHRpbmcuc2VydmljZVwiO1xyXG5pbXBvcnQge1Byb2ZpbGVTZXR0aW5nTW9kZWx9IGZyb20gXCIuL3Byb2ZpbGVTZXR0aW5nLm1vZGVsXCI7XHJcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqcy9TdWJzY3JpcHRpb25cIjtcclxuaW1wb3J0IHtDb25maWdTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9jb25maWcuc2VydmljZVwiO1xyXG5pbXBvcnQge0RvbVNhbml0aXplcn0gZnJvbSBcIkBhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXJcIjtcclxuaW1wb3J0IHtDaGF0U2VydmljZX0gZnJvbSBcIi4uL2NoYXQvY2hhdC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7VHJhbnNsYXRlU2VydmljZX0gZnJvbSBcIm5nMi10cmFuc2xhdGVcIjtcclxuaW1wb3J0IHtOZzRMb2FkaW5nU3Bpbm5lclNlcnZpY2V9IGZyb20gXCJuZzQtbG9hZGluZy1zcGlubmVyXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAncHJvZmlsZVNldHRpbmcnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhcHAvcHJvZmlsZVNldHRpbmcvcHJvZmlsZVNldHRpbmcuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJ2FwcC9wcm9maWxlU2V0dGluZy9wcm9maWxlU2V0dGluZy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUHJvZmlsZVNldHRpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBAVmlld0NoaWxkKCdhdmF0YXJJbWFnZScpIGF2YXRhckltYWdlRWxlbWVudDogRWxlbWVudFJlZjtcclxuXHJcbiAgICBwcml2YXRlIF9zdWJQcm9maWxlU2V0dGluZ1VzZXJJbmZvOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJQcm9maWxlU2V0dGluZ1VwZGF0ZTogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSB1c2VyUHJvZmlsZVNldHRpbmc6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgcHJvZmlsZVNldHRpbmdTZXJ2aWNlOiBQcm9maWxlU2V0dGluZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgcHJvZmlsZVNldHRpbmdNb2RlbDogUHJvZmlsZVNldHRpbmdNb2RlbCxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBjb25maWdTZXJ2aWNlOiBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGRvbVNhbml0aXplcjogRG9tU2FuaXRpemVyLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNoYXRTZXJ2aWNlOiBDaGF0U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHNwaW5uZXJTZXJ2aWNlOiBOZzRMb2FkaW5nU3Bpbm5lclNlcnZpY2UpIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29uZmlnU2VydmljZS51c2VyLmlkKSB7XHJcbiAgICAgICAgICAgIC8vIEdldCB1c2VyJ3MgcHJvZmlsZSBzZXR0aW5nc1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJQcm9maWxlU2V0dGluZ1VzZXJJbmZvID0gdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UuZ2V0VXNlclByb2ZpbGUodGhpcy5jb25maWdTZXJ2aWNlLnVzZXIuaWQpLnN1YnNjcmliZSh1c2VyUHJvZmlsZSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcm9maWxlU2V0dGluZyA9IHVzZXJQcm9maWxlO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIEZpbGwgb3V0IHByb2ZpbGVTZXR0aW5nTW9kZWwncyBmaWVsZHMgd2l0aCBkYXRhXHJcbiAgICAgICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nTW9kZWwuaWQgPSB0aGlzLnVzZXJQcm9maWxlU2V0dGluZy51c2VyLmlkO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ01vZGVsLmZpcnN0TmFtZSA9IHRoaXMudXNlclByb2ZpbGVTZXR0aW5nLnVzZXIuZmlyc3RfbmFtZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdNb2RlbC5zZWNvbmROYW1lID0gdGhpcy51c2VyUHJvZmlsZVNldHRpbmcudXNlci5zZWNvbmRfbmFtZTtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdNb2RlbC5sYXN0TmFtZSA9IHRoaXMudXNlclByb2ZpbGVTZXR0aW5nLnVzZXIubGFzdF9uYW1lO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ01vZGVsLmVtYWlsID0gdGhpcy51c2VyUHJvZmlsZVNldHRpbmcudXNlci5lbWFpbDtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdNb2RlbC51c2VybmFtZSA9IHRoaXMudXNlclByb2ZpbGVTZXR0aW5nLnVzZXIudXNlcm5hbWU7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQmVmb3JlIGRlc3Ryb3kgY29tcG9uZW50IHVuc3Vic2NyaWJlZCBmcm9tIGV2ZW50c1xyXG4gICAgICovXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZih0aGlzLl9zdWJQcm9maWxlU2V0dGluZ1VzZXJJbmZvKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YlByb2ZpbGVTZXR0aW5nVXNlckluZm8udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX3N1YlByb2ZpbGVTZXR0aW5nVXBkYXRlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YlByb2ZpbGVTZXR0aW5nVXBkYXRlLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlIHVzZXIncyBwcm9maWxlIHNldHRpbmcgYW5kIHVwZGF0ZSBwcm9maWxlU2V0dGluZ01vZGVsJ3MgZmllbGRzXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZVByb2ZpbGVTZXR0aW5nKCkge1xyXG4gICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2Uuc2hvdygpO1xyXG4gICAgICAgIHRoaXMuX3N1YlByb2ZpbGVTZXR0aW5nVXBkYXRlID0gdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UudXBkYXRlUHJvZmlsZVNldHRpbmcodGhpcy5wcm9maWxlU2V0dGluZ01vZGVsKS5zdWJzY3JpYmUocHJvZmlsZVNldHRpbmcgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJQcm9maWxlU2V0dGluZyA9IHByb2ZpbGVTZXR0aW5nO1xyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nTW9kZWwuaWQgPSB0aGlzLnVzZXJQcm9maWxlU2V0dGluZy51c2VyLmlkO1xyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nTW9kZWwuZmlyc3ROYW1lID0gdGhpcy51c2VyUHJvZmlsZVNldHRpbmcudXNlci5maXJzdF9uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nTW9kZWwuc2Vjb25kTmFtZSA9IHRoaXMudXNlclByb2ZpbGVTZXR0aW5nLnVzZXIuc2Vjb25kX25hbWU7XHJcbiAgICAgICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdNb2RlbC5sYXN0TmFtZSA9IHRoaXMudXNlclByb2ZpbGVTZXR0aW5nLnVzZXIubGFzdF9uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nTW9kZWwuZW1haWwgPSB0aGlzLnVzZXJQcm9maWxlU2V0dGluZy51c2VyLmVtYWlsO1xyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nTW9kZWwudXNlcm5hbWUgPSB0aGlzLnVzZXJQcm9maWxlU2V0dGluZy51c2VyLnVzZXJuYW1lO1xyXG4gICAgICAgICAgICB0aGlzLmNvbmZpZ1NlcnZpY2UudXNlci5saW5rX2F2YXRhcl9pbWFnZSA9IHRoaXMudXNlclByb2ZpbGVTZXR0aW5nLnVzZXIubGlua19hdmF0YXJfaW1hZ2U7XHJcblxyXG4gICAgICAgICAgICAvLyBJZiBwcm9maWxlJ3Mgc2V0dGluZyBzdWNjZXNzIHVwZGF0ZSwgdGhlbiBzZXQgZmxhZyBcImlzQ2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nc1wiIHRvIHZhbHVlIFwiZmFsc2VcIixcclxuICAgICAgICAgICAgLy8gdGhpcyBtZWFucyB0aGF0IHNvIGZhciB0aGUgYXZhdGFyIGhhcyBub3QgYmVlbiBjaGFuZ2VkXHJcbiAgICAgICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdTZXJ2aWNlLmlzQ2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5ncyA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgLy8gVGhlIGNoYW5nZWRBdmF0YXJQcm9maWxlU2V0dGluZ3NQYXRoIHN0b3JlcyB0aGUgcGF0aCB0byB0aGUgZmlsZSB0aGF0IHdlIGFyZSBnb2luZyB0byByZXBsYWNlIHRoZVxyXG4gICAgICAgICAgICAvLyBjdXJyZW50IGF2YXRhciBpbiB0aGUgcHJvZmlsZSBzZXR0aW5ncy4gU2V0IFwiY2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nc1BhdGhcIiB0byBlbXB0eVxyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nU2VydmljZS5jaGFuZ2VkQXZhdGFyUHJvZmlsZVNldHRpbmdzUGF0aCA9ICcnO1xyXG5cclxuICAgICAgICAgICAgLy8gXCJhdmF0YXJJbWFnZUVsZW1lbnRcIiBpcyB0aGUgPGlucHV0PiBhdmF0YXIgZWxlbWVudCwgdHlwZSBmaWxlLiBJdHMgbWVhbmluZyBpcyBvdmVyd3JpdHRlblxyXG4gICAgICAgICAgICB0aGlzLmF2YXRhckltYWdlRWxlbWVudC5uYXRpdmVFbGVtZW50LnZhbHVlID0gJyc7XHJcbiAgICAgICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgKGVycjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8oZXJyKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hhbmdlIHN0YXR1cyBmb3IgXCJQcm9maWxlU2V0dGluZ3NDb21wb25lbnRcIiAocmVtb3ZlIGZyb20gRE9NLCBiZWNhdXNlIHVzaW5nIFwibmdJZlwiKSxcclxuICAgICAqIGNoYW5nZSBzdGF0dXMgZm9yIGF2YXRhciBpbWFnZSAodXNlL25vdCB1c2UgY2hhbmdlZCBhdmF0YXIgaW1hZ2UgcGF0aCkgYW5kIHNldCBcImZhbHNlXCIsIGFuZCByZW1vdmUgdGVtcG9yYXJ5IGF2YXRhciBpbWFnZSBwYXRoXHJcbiAgICAgKi9cclxuICAgIG9uQ2xvc2VQcm9maWxlU2V0dGluZ3MoKSB7XHJcbiAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UuaXNTaG93UHJvZmlsZVNldHRpbmdzQ29tcG9uZW50ID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UuaXNDaGFuZ2VkQXZhdGFyUHJvZmlsZVNldHRpbmdzID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UuY2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nc1BhdGggPSAnJztcclxuICAgICAgICB0aGlzLmF2YXRhckltYWdlRWxlbWVudC5uYXRpdmVFbGVtZW50LnZhbHVlID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCeSBjaGFuZ2luZyBhdmF0YXIgaW1hZ2Ugc2V0IHRvIFwiaXNDaGFuZ2VkQXZhdGFyUHJvZmlsZVNldHRpbmdzXCIgKGZsYWcpIHZhbHVlIFwidHJ1ZVwiIGFuZCBjaGFuZ2UgdGVtcG9yYXJ5IGF2YXRhciBpbWFnZSBwYXRoXHJcbiAgICAgKiB0byBjdXJyZW50IG5ldyBhdmF0YXIgaW1hZ2UgcGF0aCB0byBzaG93IGhvdyBhdmF0YXIgaXMgbG9vayBub3coaW4gcmVhbCB0aW1lKS5cclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICovXHJcbiAgICBvbkNoYW5nZUF2YXRhckltYWdlKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ01vZGVsLmF2YXRhckltYWdlID0gZXZlbnQudGFyZ2V0LmZpbGVzWzBdO1xyXG4gICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdTZXJ2aWNlLmNoYW5nZWRBdmF0YXJQcm9maWxlU2V0dGluZ3NQYXRoID0gdGhpcy5kb21TYW5pdGl6ZXIuYnlwYXNzU2VjdXJpdHlUcnVzdFVybChldmVudC5zcmNFbGVtZW50LmZpbGVzWzBdLnBhdGgpO1xyXG4gICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdTZXJ2aWNlLmlzQ2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5ncyA9IHRydWU7XHJcbiAgICB9XHJcbn0iXX0=
