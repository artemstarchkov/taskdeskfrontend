import {Component, OnInit, OnDestroy, ElementRef, ViewChild} from '@angular/core';
import {ProfileSettingService} from "./profileSetting.service";
import {ProfileSettingModel} from "./profileSetting.model";
import {Subscription} from "rxjs/Subscription";
import {ConfigService} from "../sharedService/config.service";
import {DomSanitizer} from "@angular/platform-browser";
import {ChatService} from "../chat/chat.service";
import {TranslateService} from "ng2-translate";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
    selector: 'profileSetting',
    templateUrl: 'app/profileSetting/profileSetting.component.html',
    styleUrls: ['app/profileSetting/profileSetting.component.scss']
})

export class ProfileSettingComponent implements OnInit, OnDestroy {
    @ViewChild('avatarImage') avatarImageElement: ElementRef;

    private _subProfileSettingUserInfo: Subscription;
    private _subProfileSettingUpdate: Subscription;
    private userProfileSetting: any;

    constructor(public profileSettingService: ProfileSettingService,
                public profileSettingModel: ProfileSettingModel,
                public configService: ConfigService,
                public domSanitizer: DomSanitizer,
                public chatService: ChatService,
                public translate: TranslateService,
                private spinnerService: Ng4LoadingSpinnerService) {}

    ngOnInit() {
        if (this.configService.user.id) {
            // Get user's profile settings
            this._subProfileSettingUserInfo = this.profileSettingService.getUserProfile(this.configService.user.id).subscribe(userProfile => {
                this.userProfileSetting = userProfile;

                // Fill out profileSettingModel's fields with data
                this.profileSettingModel.id = this.userProfileSetting.user.id;
                this.profileSettingModel.firstName = this.userProfileSetting.user.first_name;
                this.profileSettingModel.secondName = this.userProfileSetting.user.second_name;
                this.profileSettingModel.lastName = this.userProfileSetting.user.last_name;
                this.profileSettingModel.email = this.userProfileSetting.user.email;
                this.profileSettingModel.username = this.userProfileSetting.user.username;
            })
        }
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if(this._subProfileSettingUserInfo) {
            this._subProfileSettingUserInfo.unsubscribe();
        }
        if (this._subProfileSettingUpdate) {
            this._subProfileSettingUpdate.unsubscribe();
        }
    }

    /**
     * Update user's profile setting and update profileSettingModel's fields
     */
    updateProfileSetting() {
        this.spinnerService.show();
        this._subProfileSettingUpdate = this.profileSettingService.updateProfileSetting(this.profileSettingModel).subscribe(profileSetting => {
            this.userProfileSetting = profileSetting;
            this.profileSettingModel.id = this.userProfileSetting.user.id;
            this.profileSettingModel.firstName = this.userProfileSetting.user.first_name;
            this.profileSettingModel.secondName = this.userProfileSetting.user.second_name;
            this.profileSettingModel.lastName = this.userProfileSetting.user.last_name;
            this.profileSettingModel.email = this.userProfileSetting.user.email;
            this.profileSettingModel.username = this.userProfileSetting.user.username;
            this.configService.user.link_avatar_image = this.userProfileSetting.user.link_avatar_image;

            // If profile's setting success update, then set flag "isChangedAvatarProfileSettings" to value "false",
            // this means that so far the avatar has not been changed
            this.profileSettingService.isChangedAvatarProfileSettings = false;

            // The changedAvatarProfileSettingsPath stores the path to the file that we are going to replace the
            // current avatar in the profile settings. Set "changedAvatarProfileSettingsPath" to empty
            this.profileSettingService.changedAvatarProfileSettingsPath = '';

            // "avatarImageElement" is the <input> avatar element, type file. Its meaning is overwritten
            this.avatarImageElement.nativeElement.value = '';
            this.spinnerService.hide();
        },
        (err: any) => {
            this.spinnerService.hide();
            console.info(err);
        })
    }

    /**
     * Change status for "ProfileSettingsComponent" (remove from DOM, because using "ngIf"),
     * change status for avatar image (use/not use changed avatar image path) and set "false", and remove temporary avatar image path
     */
    onCloseProfileSettings() {
        this.profileSettingService.isShowProfileSettingsComponent = false;
        this.profileSettingService.isChangedAvatarProfileSettings = false;
        this.profileSettingService.changedAvatarProfileSettingsPath = '';
        this.avatarImageElement.nativeElement.value = '';
    }

    /**
     * By changing avatar image set to "isChangedAvatarProfileSettings" (flag) value "true" and change temporary avatar image path
     * to current new avatar image path to show how avatar is look now(in real time).
     * @param event
     */
    onChangeAvatarImage(event) {
        this.profileSettingModel.avatarImage = event.target.files[0];
        this.profileSettingService.changedAvatarProfileSettingsPath = this.domSanitizer.bypassSecurityTrustUrl(event.srcElement.files[0].path);
        this.profileSettingService.isChangedAvatarProfileSettings = true;
    }
}