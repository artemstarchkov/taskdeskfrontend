export declare class ProfileSettingModel {
    id: number;
    firstName: string;
    secondName: string;
    lastName: string;
    email: string;
    password: string;
    passwordConfirmation: string;
    username: string;
    avatarImage: any;
}
