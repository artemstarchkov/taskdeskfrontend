System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var ProfileSettingModel;
    return {
        setters: [],
        execute: function () {
            ProfileSettingModel = class ProfileSettingModel {
                constructor() {
                    this.firstName = '';
                    this.secondName = '';
                    this.lastName = '';
                    this.email = '';
                    this.password = '';
                    this.passwordConfirmation = '';
                    this.username = '';
                    this.avatarImage = null;
                }
            };
            exports_1("ProfileSettingModel", ProfileSettingModel);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7WUFBQSxzQkFBQTtnQkFBQTtvQkFFSSxjQUFTLEdBQVcsRUFBRSxDQUFDO29CQUN2QixlQUFVLEdBQVcsRUFBRSxDQUFDO29CQUN4QixhQUFRLEdBQVcsRUFBRSxDQUFDO29CQUN0QixVQUFLLEdBQVcsRUFBRSxDQUFDO29CQUNuQixhQUFRLEdBQVcsRUFBRSxDQUFDO29CQUN0Qix5QkFBb0IsR0FBVyxFQUFFLENBQUM7b0JBQ2xDLGFBQVEsR0FBVyxFQUFFLENBQUM7b0JBQ3RCLGdCQUFXLEdBQVEsSUFBSSxDQUFDO2dCQUM1QixDQUFDO2FBQUEsQ0FBQTs7UUFBQSxDQUFDIiwiZmlsZSI6InByb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLm1vZGVsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFByb2ZpbGVTZXR0aW5nTW9kZWwge1xyXG4gICAgaWQ6IG51bWJlcjtcclxuICAgIGZpcnN0TmFtZTogc3RyaW5nID0gJyc7XHJcbiAgICBzZWNvbmROYW1lOiBzdHJpbmcgPSAnJztcclxuICAgIGxhc3ROYW1lOiBzdHJpbmcgPSAnJztcclxuICAgIGVtYWlsOiBzdHJpbmcgPSAnJztcclxuICAgIHBhc3N3b3JkOiBzdHJpbmcgPSAnJztcclxuICAgIHBhc3N3b3JkQ29uZmlybWF0aW9uOiBzdHJpbmcgPSAnJztcclxuICAgIHVzZXJuYW1lOiBzdHJpbmcgPSAnJztcclxuICAgIGF2YXRhckltYWdlOiBhbnkgPSBudWxsO1xyXG59Il19
