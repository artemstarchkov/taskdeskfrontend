System.register(["@angular/core", "@angular/http", "@angular/common", "./profileSetting.route", "@angular/router", "@angular/forms", "@angular/platform-browser", "./profileSetting.service", "./profileSetting.component", "./profileSetting.model"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, common_1, profileSetting_route_1, router_1, forms_1, platform_browser_1, profileSetting_service_1, profileSetting_component_1, profileSetting_model_1, ProfileSettingModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (profileSetting_route_1_1) {
                profileSetting_route_1 = profileSetting_route_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (profileSetting_service_1_1) {
                profileSetting_service_1 = profileSetting_service_1_1;
            },
            function (profileSetting_component_1_1) {
                profileSetting_component_1 = profileSetting_component_1_1;
            },
            function (profileSetting_model_1_1) {
                profileSetting_model_1 = profileSetting_model_1_1;
            }
        ],
        execute: function () {
            ProfileSettingModule = class ProfileSettingModule {
            };
            ProfileSettingModule = __decorate([
                core_1.NgModule({
                    imports: [
                        common_1.CommonModule,
                        http_1.HttpModule,
                        profileSetting_route_1.PROFILE_SETTING_ROUTES,
                        router_1.RouterModule,
                        forms_1.FormsModule,
                        platform_browser_1.BrowserModule
                    ],
                    providers: [
                        profileSetting_service_1.ProfileSettingService,
                        profileSetting_model_1.ProfileSettingModel
                    ],
                    declarations: [
                        profileSetting_component_1.ProfileSettingComponent
                    ]
                })
            ], ProfileSettingModule);
            exports_1("ProfileSettingModule", ProfileSettingModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQTRCYSxvQkFBb0IsR0FBakM7YUFBcUMsQ0FBQTtZQUF4QixvQkFBb0I7Z0JBakJoQyxlQUFRLENBQUM7b0JBQ04sT0FBTyxFQUFPO3dCQUNWLHFCQUFZO3dCQUNaLGlCQUFVO3dCQUNWLDZDQUFzQjt3QkFDdEIscUJBQVk7d0JBQ1osbUJBQVc7d0JBQ1gsZ0NBQWE7cUJBQ2hCO29CQUNELFNBQVMsRUFBRTt3QkFDUCw4Q0FBcUI7d0JBQ3JCLDBDQUFtQjtxQkFDdEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNWLGtEQUF1QjtxQkFDMUI7aUJBQ0osQ0FBQztlQUNXLG9CQUFvQixDQUFJOztRQUFBLENBQUMiLCJmaWxlIjoicHJvZmlsZVNldHRpbmcvcHJvZmlsZVNldHRpbmcubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SHR0cE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBQUk9GSUxFX1NFVFRJTkdfUk9VVEVTIH0gZnJvbSAnLi9wcm9maWxlU2V0dGluZy5yb3V0ZSc7XHJcbmltcG9ydCB7Um91dGVyTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7Rm9ybXNNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5pbXBvcnQge0Jyb3dzZXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQge1Byb2ZpbGVTZXR0aW5nU2VydmljZX0gZnJvbSBcIi4vcHJvZmlsZVNldHRpbmcuc2VydmljZVwiO1xyXG5pbXBvcnQge1Byb2ZpbGVTZXR0aW5nQ29tcG9uZW50fSBmcm9tIFwiLi9wcm9maWxlU2V0dGluZy5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtQcm9maWxlU2V0dGluZ01vZGVsfSBmcm9tIFwiLi9wcm9maWxlU2V0dGluZy5tb2RlbFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6ICAgICAgW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBIdHRwTW9kdWxlLFxyXG4gICAgICAgIFBST0ZJTEVfU0VUVElOR19ST1VURVMsXHJcbiAgICAgICAgUm91dGVyTW9kdWxlLFxyXG4gICAgICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgICAgIEJyb3dzZXJNb2R1bGVcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBQcm9maWxlU2V0dGluZ1NlcnZpY2UsXHJcbiAgICAgICAgUHJvZmlsZVNldHRpbmdNb2RlbFxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIFByb2ZpbGVTZXR0aW5nQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQcm9maWxlU2V0dGluZ01vZHVsZSB7IH0iXX0=
