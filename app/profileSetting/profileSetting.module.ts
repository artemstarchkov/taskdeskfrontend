import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import { PROFILE_SETTING_ROUTES } from './profileSetting.route';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser';
import {ProfileSettingService} from "./profileSetting.service";
import {ProfileSettingComponent} from "./profileSetting.component";
import {ProfileSettingModel} from "./profileSetting.model";

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        PROFILE_SETTING_ROUTES,
        RouterModule,
        FormsModule,
        BrowserModule
    ],
    providers: [
        ProfileSettingService,
        ProfileSettingModel
    ],
    declarations: [
        ProfileSettingComponent
    ]
})
export class ProfileSettingModule { }