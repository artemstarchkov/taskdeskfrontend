System.register(["@angular/router", "./profileSetting.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, profileSetting_component_1, profileSettingRoutes, PROFILE_SETTING_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (profileSetting_component_1_1) {
                profileSetting_component_1 = profileSetting_component_1_1;
            }
        ],
        execute: function () {
            profileSettingRoutes = [
                {
                    path: '',
                    component: profileSetting_component_1.ProfileSettingComponent
                }
            ];
            exports_1("PROFILE_SETTING_ROUTES", PROFILE_SETTING_ROUTES = router_1.RouterModule.forChild(profileSettingRoutes));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLnJvdXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O1lBSU0sb0JBQW9CLEdBQVU7Z0JBQ2hDO29CQUNJLElBQUksRUFBRSxFQUFFO29CQUNSLFNBQVMsRUFBRSxrREFBdUI7aUJBQ3JDO2FBQ0osQ0FBQztZQUVGLG9DQUFhLHNCQUFzQixHQUF1QixxQkFBWSxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFDO1FBQUEsQ0FBQyIsImZpbGUiOiJwcm9maWxlU2V0dGluZy9wcm9maWxlU2V0dGluZy5yb3V0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Um91dGVzLCBSb3V0ZXJNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtNb2R1bGVXaXRoUHJvdmlkZXJzfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1Byb2ZpbGVTZXR0aW5nQ29tcG9uZW50fSBmcm9tIFwiLi9wcm9maWxlU2V0dGluZy5jb21wb25lbnRcIjtcclxuXHJcbmNvbnN0IHByb2ZpbGVTZXR0aW5nUm91dGVzOlJvdXRlcyA9IFtcclxuICAgIHtcclxuICAgICAgICBwYXRoOiAnJyxcclxuICAgICAgICBjb21wb25lbnQ6IFByb2ZpbGVTZXR0aW5nQ29tcG9uZW50XHJcbiAgICB9XHJcbl07XHJcblxyXG5leHBvcnQgY29uc3QgUFJPRklMRV9TRVRUSU5HX1JPVVRFUzpNb2R1bGVXaXRoUHJvdmlkZXJzID0gUm91dGVyTW9kdWxlLmZvckNoaWxkKHByb2ZpbGVTZXR0aW5nUm91dGVzKTsiXX0=
