import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {ProfileSettingComponent} from "./profileSetting.component";

const profileSettingRoutes:Routes = [
    {
        path: '',
        component: ProfileSettingComponent
    }
];

export const PROFILE_SETTING_ROUTES:ModuleWithProviders = RouterModule.forChild(profileSettingRoutes);