import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/map";
import { ProfileSettingModel } from "./profileSetting.model";
import { ConfigService } from "../sharedService/config.service";
export declare class ProfileSettingService {
    router: Router;
    http: Http;
    configService: ConfigService;
    isShowProfileSettingsComponent: boolean;
    isChangedAvatarProfileSettings: boolean;
    changedAvatarProfileSettingsPath: any;
    constructor(router: Router, http: Http, configService: ConfigService);
    getUserProfile(id: number): Observable<Response>;
    updateProfileSetting(profileSettingModel: ProfileSettingModel): Observable<any>;
    resetData(): void;
}
