System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "../sharedService/config.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, config_service_1, ProfileSettingService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            }
        ],
        execute: function () {
            ProfileSettingService = class ProfileSettingService {
                constructor(router, http, configService) {
                    this.router = router;
                    this.http = http;
                    this.configService = configService;
                    this.isShowProfileSettingsComponent = false;
                    this.isChangedAvatarProfileSettings = false;
                    this.changedAvatarProfileSettingsPath = '';
                }
                getUserProfile(id) {
                    return this.http.post(this.configService.domen + 'user/getuserprofile', { id: id })
                        .map(res => res.json());
                }
                updateProfileSetting(profileSettingModel) {
                    let formData = new FormData();
                    let strProfileSettingModel = JSON.stringify(profileSettingModel);
                    formData.append('profileSettingModel', strProfileSettingModel);
                    formData.append('avatarImage', profileSettingModel.avatarImage);
                    return this.http.post(this.configService.domen + 'user/updateuserprofile', formData)
                        .map(res => res.json());
                }
                resetData() {
                    this.isShowProfileSettingsComponent = false;
                    this.isChangedAvatarProfileSettings = false;
                    this.changedAvatarProfileSettingsPath = '';
                }
            };
            ProfileSettingService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router,
                    http_1.Http,
                    config_service_1.ConfigService])
            ], ProfileSettingService);
            exports_1("ProfileSettingService", ProfileSettingService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQVVhLHFCQUFxQixHQUFsQztnQkFPSSxZQUFtQixNQUFjLEVBQ2QsSUFBVSxFQUNWLGFBQTRCO29CQUY1QixXQUFNLEdBQU4sTUFBTSxDQUFRO29CQUNkLFNBQUksR0FBSixJQUFJLENBQU07b0JBQ1Ysa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBUnhDLG1DQUE4QixHQUFZLEtBQUssQ0FBQztvQkFFaEQsbUNBQThCLEdBQVksS0FBSyxDQUFDO29CQUVoRCxxQ0FBZ0MsR0FBUSxFQUFFLENBQUM7Z0JBSUEsQ0FBQztnQkFPNUMsY0FBYyxDQUFDLEVBQVU7b0JBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxxQkFBcUIsRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQzt5QkFDNUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ2hDLENBQUM7Z0JBT00sb0JBQW9CLENBQUMsbUJBQXdDO29CQUdoRSxJQUFJLFFBQVEsR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO29CQUM5QixJQUFJLHNCQUFzQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFFakUsUUFBUSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO29CQUMvRCxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFaEUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLHdCQUF3QixFQUFFLFFBQVEsQ0FBQzt5QkFDL0UsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ2hDLENBQUM7Z0JBS00sU0FBUztvQkFDWixJQUFJLENBQUMsOEJBQThCLEdBQUcsS0FBSyxDQUFDO29CQUM1QyxJQUFJLENBQUMsOEJBQThCLEdBQUcsS0FBSyxDQUFDO29CQUM1QyxJQUFJLENBQUMsZ0NBQWdDLEdBQUcsRUFBRSxDQUFDO2dCQUMvQyxDQUFDO2FBQ0osQ0FBQTtZQS9DWSxxQkFBcUI7Z0JBRGpDLGlCQUFVLEVBQUU7aURBUWtCLGVBQU07b0JBQ1IsV0FBSTtvQkFDSyw4QkFBYTtlQVR0QyxxQkFBcUIsQ0ErQ2pDOztRQUFBLENBQUMiLCJmaWxlIjoicHJvZmlsZVNldHRpbmcvcHJvZmlsZVNldHRpbmcuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tIFwicnhqcy9PYnNlcnZhYmxlXCI7XHJcbmltcG9ydCB7SHR0cCwgSGVhZGVycywgUmVzcG9uc2V9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9tYXBcIjtcclxuaW1wb3J0IHtTdWJqZWN0fSBmcm9tIFwicnhqcy9TdWJqZWN0XCI7XHJcbmltcG9ydCB7UHJvZmlsZVNldHRpbmdNb2RlbH0gZnJvbSBcIi4vcHJvZmlsZVNldHRpbmcubW9kZWxcIjtcclxuaW1wb3J0IHtDb25maWdTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9jb25maWcuc2VydmljZVwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgUHJvZmlsZVNldHRpbmdTZXJ2aWNlIHtcclxuICAgIHB1YmxpYyBpc1Nob3dQcm9maWxlU2V0dGluZ3NDb21wb25lbnQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIC8vIEF2YXRhciBpbWFnZSBmbGFnIHRvIHN3aXRjaCBiZXR3ZWVuIHVzZS9ub3QgdXNlIHRlbXBvcmFyeSBhdmF0YXIgaW1hZ2UgcGF0aFxyXG4gICAgcHVibGljIGlzQ2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgLy8gQXZhdGFyIHRlbXBvcmFyeSBpbWFnZSBwYXRoXHJcbiAgICBwdWJsaWMgY2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nc1BhdGg6IGFueSA9ICcnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBodHRwOiBIdHRwLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2UpIHt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdXNlciBpbmZvIGJ5IGlkXHJcbiAgICAgKiBAcGFyYW0gaWQ6IE51bWJlclxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8Uj59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRVc2VyUHJvZmlsZShpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxSZXNwb25zZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZ1NlcnZpY2UuZG9tZW4gKyAndXNlci9nZXR1c2VycHJvZmlsZScsIHtpZDogaWR9KVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiByZXMuanNvbigpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFVwZGF0ZSB1c2VyJ3MgcHJvZmlsZSBzZXR0aW5nc1xyXG4gICAgICogQHBhcmFtIHByb2ZpbGVTZXR0aW5nTW9kZWxcclxuICAgICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPFI+fVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgdXBkYXRlUHJvZmlsZVNldHRpbmcocHJvZmlsZVNldHRpbmdNb2RlbDogUHJvZmlsZVNldHRpbmdNb2RlbCkge1xyXG4gICAgICAgIC8vIEluIHByb2ZpbGVTZXR0aW5nTW9kZWwuYXZhdGFySW1hZ2Ugd2UgaGF2ZSBkYXRhIG9mIFwiZm9ybURhdGFcIiB0eXBlIChpbWFnZSAtIFwiRmlsZSB0eXBlXCIpLlxyXG4gICAgICAgIC8vIFRvIHNlbmQgRmlsZSB0eXBlIGRhdGEgYW5kIGpzb24gZGF0YSAocHJvZmlsZVNldHRpbmdNb2RlbCkgd2UgbmVlZCBhZGQgXCJqc29uIGRhdGFcIiBsaWtlIFwianNvbiBzdHJpbmdcIiBpbiBkYXRhIG9mIGEgXCJmb3JtRGF0YVwiIHR5cGUuXHJcbiAgICAgICAgbGV0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgbGV0IHN0clByb2ZpbGVTZXR0aW5nTW9kZWwgPSBKU09OLnN0cmluZ2lmeShwcm9maWxlU2V0dGluZ01vZGVsKTtcclxuICAgICAgICAvL2Zvcm1EYXRhID0gcHJvZmlsZVNldHRpbmdNb2RlbC5hdmF0YXJJbWFnZTtcclxuICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ3Byb2ZpbGVTZXR0aW5nTW9kZWwnLCBzdHJQcm9maWxlU2V0dGluZ01vZGVsKTtcclxuICAgICAgICBmb3JtRGF0YS5hcHBlbmQoJ2F2YXRhckltYWdlJywgcHJvZmlsZVNldHRpbmdNb2RlbC5hdmF0YXJJbWFnZSk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZ1NlcnZpY2UuZG9tZW4gKyAndXNlci91cGRhdGV1c2VycHJvZmlsZScsIGZvcm1EYXRhKVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiByZXMuanNvbigpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlc2V0IGFsbCBkYXRhLiBUaGlzIGZ1bmN0aW9uIGNhbGwgd2hlbiB1c2VyIGV4aXQgZnJvbSBwcm9ncmFtIGFuZCBuZWVkIHRvIGNsZWFyIGRhdGEuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyByZXNldERhdGEoKSB7XHJcbiAgICAgICAgdGhpcy5pc1Nob3dQcm9maWxlU2V0dGluZ3NDb21wb25lbnQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmlzQ2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5ncyA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nc1BhdGggPSAnJztcclxuICAgIH1cclxufSJdfQ==
