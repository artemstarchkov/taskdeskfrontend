import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {Subject} from "rxjs/Subject";
import {ProfileSettingModel} from "./profileSetting.model";
import {ConfigService} from "../sharedService/config.service";

@Injectable()
export class ProfileSettingService {
    public isShowProfileSettingsComponent: boolean = false;
    // Avatar image flag to switch between use/not use temporary avatar image path
    public isChangedAvatarProfileSettings: boolean = false;
    // Avatar temporary image path
    public changedAvatarProfileSettingsPath: any = '';

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService) {}

    /**
     * Get user info by id
     * @param id: Number
     * @returns {Observable<R>}
     */
    public getUserProfile(id: number): Observable<Response> {
        return this.http.post(this.configService.domen + 'user/getuserprofile', {id: id})
            .map(res => res.json());
    }

    /**
     * Update user's profile settings
     * @param profileSettingModel
     * @returns {Observable<R>}
     */
    public updateProfileSetting(profileSettingModel: ProfileSettingModel) {
        // In profileSettingModel.avatarImage we have data of "formData" type (image - "File type").
        // To send File type data and json data (profileSettingModel) we need add "json data" like "json string" in data of a "formData" type.
        let formData = new FormData();
        let strProfileSettingModel = JSON.stringify(profileSettingModel);
        //formData = profileSettingModel.avatarImage;
        formData.append('profileSettingModel', strProfileSettingModel);
        formData.append('avatarImage', profileSettingModel.avatarImage);

        return this.http.post(this.configService.domen + 'user/updateuserprofile', formData)
            .map(res => res.json());
    }

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.isShowProfileSettingsComponent = false;
        this.isChangedAvatarProfileSettings = false;
        this.changedAvatarProfileSettingsPath = '';
    }
}