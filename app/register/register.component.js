System.register(["@angular/core", "@angular/router", "rxjs/add/operator/map", "./register.model", "./register.service", "../login/login.service", "../login/login.model", "../toolbar/toolbar.service", "../sharedService/config.service", "ng2-translate", "ng4-loading-spinner"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, register_model_1, register_service_1, login_service_1, login_model_1, toolbar_service_1, config_service_1, ng2_translate_1, ng4_loading_spinner_1, RegisterComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (_1) {
            },
            function (register_model_1_1) {
                register_model_1 = register_model_1_1;
            },
            function (register_service_1_1) {
                register_service_1 = register_service_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (login_model_1_1) {
                login_model_1 = login_model_1_1;
            },
            function (toolbar_service_1_1) {
                toolbar_service_1 = toolbar_service_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (ng4_loading_spinner_1_1) {
                ng4_loading_spinner_1 = ng4_loading_spinner_1_1;
            }
        ],
        execute: function () {
            RegisterComponent = class RegisterComponent {
                constructor(registerModel, registerService, router, loginService, loginModel, toolbarService, configService, translate, spinnerService) {
                    this.registerModel = registerModel;
                    this.registerService = registerService;
                    this.router = router;
                    this.loginService = loginService;
                    this.loginModel = loginModel;
                    this.toolbarService = toolbarService;
                    this.configService = configService;
                    this.translate = translate;
                    this.spinnerService = spinnerService;
                    this.isLogin = true;
                    this.usernameLength = 14;
                    translate.setDefaultLang(localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en');
                    translate.use(localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en');
                }
                ngOnInit() {
                    console.info('REGISTER COMPONENT');
                    this._subChangedLanguage = this.toolbarService.getChangedLanguage().subscribe(() => {
                        this.translate.setDefaultLang(localStorage.getItem('locale'));
                        this.translate.use(localStorage.getItem('locale'));
                    });
                }
                ngOnDestroy() {
                    if (this._subRegisterUser) {
                        this._subRegisterUser.unsubscribe();
                    }
                    if (this._subLoginUser) {
                        this._subLoginUser.unsubscribe();
                    }
                    if (this._subRelationshipRequestsUser) {
                        this._subRelationshipRequestsUser.unsubscribe();
                    }
                    if (this._subChangedLanguage) {
                        this._subChangedLanguage.unsubscribe();
                    }
                }
                onRegister() {
                    if (this.registerModel.firstName.length == 0) {
                        let regErr = this.translate.get('RegisterError.firstNameError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.secondName.length == 0) {
                        let regErr = this.translate.get('RegisterError.secondNameError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.lastName.length == 0) {
                        let regErr = this.translate.get('RegisterError.lastNameError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.email.length == 0) {
                        let regErr = this.translate.get('RegisterError.emailNameError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.nickName.length < 5) {
                        let regErr = this.translate.get('RegisterError.usernameLengthError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.nickName.length == 0) {
                        let regErr = this.translate.get('RegisterError.usernameEmptyError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.password.length == 0) {
                        let regErr = this.translate.get('RegisterError.passwordNameError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.passwordConfirmation.length == 0) {
                        let regErr = this.translate.get('RegisterError.confirmPasswordNameError');
                        alert(regErr.value);
                    }
                    else if (this.registerModel.passwordConfirmation != this.registerModel.password) {
                        let regErr = this.translate.get('RegisterError.equalPasswordsError');
                        alert(regErr.value);
                    }
                    else {
                        this.spinnerService.show();
                        this._subRegisterUser = this.registerService.registration(this.registerModel).subscribe(data => {
                            if (data.user && data.user.id) {
                                let user = null;
                                user = data.user;
                                this.isLogin = this.loginService.isExistUser(data);
                                if (data.status.isExistUser) {
                                    this.toolbarService.currentUser = user;
                                    this.toolbarService.username = this.toolbarService.currentUser.username.length >= this.usernameLength ? this.toolbarService.currentUser.username : this.toolbarService.currentUser.username.substr(0, this.usernameLength);
                                    this.toolbarService.userEmail = this.toolbarService.currentUser.email;
                                    this.configService.user = user;
                                    this._subRelationshipRequestsUser = this.toolbarService.getUserRelationshipRequestsStatusNew(this.toolbarService.currentUser.id).subscribe((rRequests) => {
                                        this.toolbarService.relationshipRequests = rRequests.preparedRR;
                                        this.spinnerService.hide();
                                        this.router.navigate(['/main-page']);
                                    });
                                }
                            }
                        }, (err) => {
                            this.spinnerService.hide();
                            console.info(err);
                        });
                    }
                }
                onSetAvatarImage(event) {
                    this.registerModel.avatarImage = event.target.files[0];
                }
            };
            RegisterComponent = __decorate([
                core_1.Component({
                    selector: 'main',
                    templateUrl: 'app/register/register.component.html',
                    styleUrls: ['app/register/register.component.scss']
                }),
                __metadata("design:paramtypes", [register_model_1.RegisterModel,
                    register_service_1.RegisterService,
                    router_1.Router,
                    login_service_1.LoginService,
                    login_model_1.LoginModel,
                    toolbar_service_1.ToolbarService,
                    config_service_1.ConfigService,
                    ng2_translate_1.TranslateService,
                    ng4_loading_spinner_1.Ng4LoadingSpinnerService])
            ], RegisterComponent);
            exports_1("RegisterComponent", RegisterComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBb0JhLGlCQUFpQixHQUE5QjtnQkFTSSxZQUFtQixhQUE0QixFQUM1QixlQUFnQyxFQUNoQyxNQUFjLEVBQ2QsWUFBMEIsRUFDMUIsVUFBc0IsRUFDdEIsY0FBOEIsRUFDOUIsYUFBNEIsRUFDNUIsU0FBMkIsRUFDMUIsY0FBd0M7b0JBUnpDLGtCQUFhLEdBQWIsYUFBYSxDQUFlO29CQUM1QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7b0JBQ2hDLFdBQU0sR0FBTixNQUFNLENBQVE7b0JBQ2QsaUJBQVksR0FBWixZQUFZLENBQWM7b0JBQzFCLGVBQVUsR0FBVixVQUFVLENBQVk7b0JBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtvQkFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBQzVCLGNBQVMsR0FBVCxTQUFTLENBQWtCO29CQUMxQixtQkFBYyxHQUFkLGNBQWMsQ0FBMEI7b0JBWHJELFlBQU8sR0FBWSxJQUFJLENBQUM7b0JBQ3ZCLG1CQUFjLEdBQVcsRUFBRSxDQUFDO29CQVloQyxTQUFTLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNoRyxTQUFTLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6RixDQUFDO2dCQUVELFFBQVE7b0JBQ0osT0FBTyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUNuQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7d0JBQy9FLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDOUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN2RCxDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDO2dCQUtELFdBQVc7b0JBQ1AsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQzt3QkFDeEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN4QyxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO3dCQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNyQyxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7d0JBQ3BDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDcEQsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO3dCQUMzQixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQzNDLENBQUM7Z0JBQ0wsQ0FBQztnQkFLRCxVQUFVO29CQUNOLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMzQyxJQUFJLE1BQU0sR0FBUSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO3dCQUNyRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN4QixDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkQsSUFBSSxNQUFNLEdBQVEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsK0JBQStCLENBQUMsQ0FBQzt3QkFDdEUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2pELElBQUksTUFBTSxHQUFRLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLDZCQUE2QixDQUFDLENBQUM7d0JBQ3BFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3hCLENBQUM7b0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM5QyxJQUFJLE1BQU0sR0FBUSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO3dCQUNyRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN4QixDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxNQUFNLEdBQVEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQzt3QkFDMUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2pELElBQUksTUFBTSxHQUFRLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxDQUFDLENBQUM7d0JBQ3pFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3hCLENBQUM7b0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNqRCxJQUFJLE1BQU0sR0FBUSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO3dCQUN4RSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN4QixDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM3RCxJQUFJLE1BQU0sR0FBUSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO3dCQUMvRSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN4QixDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDaEYsSUFBSSxNQUFNLEdBQVEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQzt3QkFDMUUsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO3dCQUMzQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FDbEYsSUFBSSxDQUFDLEVBQUU7NEJBQ0gsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0NBQzVCLElBQUksSUFBSSxHQUFRLElBQUksQ0FBQztnQ0FDckIsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0NBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7Z0NBR25ELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztvQ0FDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO29DQUd2QyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUMzTixJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7b0NBQ3RFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztvQ0FHL0IsSUFBSSxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsb0NBQW9DLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUU7d0NBQ3JKLElBQUksQ0FBQyxjQUFjLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQzt3Q0FDaEUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3Q0FDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29DQUN6QyxDQUFDLENBQUMsQ0FBQztnQ0FDUCxDQUFDOzRCQUNMLENBQUM7d0JBQ0wsQ0FBQyxFQUNGLENBQUMsR0FBUSxFQUFFLEVBQUU7NEJBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDM0IsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDdEIsQ0FBQyxDQUNKLENBQUM7b0JBQ04sQ0FBQztnQkFDTCxDQUFDO2dCQU1ELGdCQUFnQixDQUFDLEtBQUs7b0JBQ2xCLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxDQUFDO2FBQ0osQ0FBQTtZQTFIWSxpQkFBaUI7Z0JBTjdCLGdCQUFTLENBQUM7b0JBQ1AsUUFBUSxFQUFFLE1BQU07b0JBQ2hCLFdBQVcsRUFBRSxzQ0FBc0M7b0JBQ25ELFNBQVMsRUFBRSxDQUFDLHNDQUFzQyxDQUFDO2lCQUN0RCxDQUFDO2lEQVdvQyw4QkFBYTtvQkFDWCxrQ0FBZTtvQkFDeEIsZUFBTTtvQkFDQSw0QkFBWTtvQkFDZCx3QkFBVTtvQkFDTixnQ0FBYztvQkFDZiw4QkFBYTtvQkFDakIsZ0NBQWdCO29CQUNWLDhDQUF3QjtlQWpCbkQsaUJBQWlCLENBMEg3Qjs7UUFBQSxDQUFDIiwiZmlsZSI6InJlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7Um91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9tYXBcIjtcclxuaW1wb3J0IHsgUmVnaXN0ZXJNb2RlbCB9IGZyb20gJy4vcmVnaXN0ZXIubW9kZWwnO1xyXG5pbXBvcnQge1JlZ2lzdGVyU2VydmljZX0gZnJvbSBcIi4vcmVnaXN0ZXIuc2VydmljZVwiO1xyXG5pbXBvcnQge0xvZ2luU2VydmljZX0gZnJvbSBcIi4uL2xvZ2luL2xvZ2luLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtMb2dpbk1vZGVsfSBmcm9tIFwiLi4vbG9naW4vbG9naW4ubW9kZWxcIjtcclxuaW1wb3J0IHtTdWJzY3JpcHRpb259IGZyb20gXCJyeGpzL1N1YnNjcmlwdGlvblwiO1xyXG5pbXBvcnQge1Rvb2xiYXJTZXJ2aWNlfSBmcm9tIFwiLi4vdG9vbGJhci90b29sYmFyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtDb25maWdTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9jb25maWcuc2VydmljZVwiO1xyXG5pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9IGZyb20gXCJuZzItdHJhbnNsYXRlXCI7XHJcbmltcG9ydCB7Tmc0TG9hZGluZ1NwaW5uZXJTZXJ2aWNlfSBmcm9tIFwibmc0LWxvYWRpbmctc3Bpbm5lclwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ21haW4nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhcHAvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJ2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgUmVnaXN0ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBwcml2YXRlIF9zdWJSZWdpc3RlclVzZXI6IFN1YnNjcmlwdGlvbjtcclxuICAgIHByaXZhdGUgX3N1YkxvZ2luVXNlcjogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJDaGFuZ2VkTGFuZ3VhZ2U6IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgICBwdWJsaWMgaXNMb2dpbjogYm9vbGVhbiA9IHRydWU7XHJcbiAgICBwcml2YXRlIHVzZXJuYW1lTGVuZ3RoOiBudW1iZXIgPSAxNDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgcmVnaXN0ZXJNb2RlbDogUmVnaXN0ZXJNb2RlbCxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyByZWdpc3RlclNlcnZpY2U6IFJlZ2lzdGVyU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBsb2dpblNlcnZpY2U6IExvZ2luU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBsb2dpbk1vZGVsOiBMb2dpbk1vZGVsLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHRvb2xiYXJTZXJ2aWNlOiBUb29sYmFyU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBjb25maWdTZXJ2aWNlOiBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc3Bpbm5lclNlcnZpY2U6IE5nNExvYWRpbmdTcGlubmVyU2VydmljZSkge1xyXG5cclxuICAgICAgICB0cmFuc2xhdGUuc2V0RGVmYXVsdExhbmcobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xvY2FsZScpPyBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9jYWxlJykgOiAnZW4nKTtcclxuICAgICAgICB0cmFuc2xhdGUudXNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKT8gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xvY2FsZScpIDogJ2VuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgY29uc29sZS5pbmZvKCdSRUdJU1RFUiBDT01QT05FTlQnKTtcclxuICAgICAgICB0aGlzLl9zdWJDaGFuZ2VkTGFuZ3VhZ2UgPSB0aGlzLnRvb2xiYXJTZXJ2aWNlLmdldENoYW5nZWRMYW5ndWFnZSgpLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudHJhbnNsYXRlLnNldERlZmF1bHRMYW5nKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKSk7XHJcbiAgICAgICAgICAgIHRoaXMudHJhbnNsYXRlLnVzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9jYWxlJykpO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCZWZvcmUgZGVzdHJveSBjb21wb25lbnQgdW5zdWJzY3JpYmVkIGZyb20gZXZlbnRzXHJcbiAgICAgKi9cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9zdWJSZWdpc3RlclVzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViUmVnaXN0ZXJVc2VyLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJMb2dpblVzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViTG9naW5Vc2VyLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJSZWxhdGlvbnNoaXBSZXF1ZXN0c1VzZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJDaGFuZ2VkTGFuZ3VhZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViQ2hhbmdlZExhbmd1YWdlLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVnaXN0ZXIgbmV3IHVzZXJcclxuICAgICAqL1xyXG4gICAgb25SZWdpc3RlcigpIHtcclxuICAgICAgICBpZiAodGhpcy5yZWdpc3Rlck1vZGVsLmZpcnN0TmFtZS5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICBsZXQgcmVnRXJyOiBhbnkgPSB0aGlzLnRyYW5zbGF0ZS5nZXQoJ1JlZ2lzdGVyRXJyb3IuZmlyc3ROYW1lRXJyb3InKTtcclxuICAgICAgICAgICAgYWxlcnQocmVnRXJyLnZhbHVlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmVnaXN0ZXJNb2RlbC5zZWNvbmROYW1lLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgICAgIGxldCByZWdFcnI6IGFueSA9IHRoaXMudHJhbnNsYXRlLmdldCgnUmVnaXN0ZXJFcnJvci5zZWNvbmROYW1lRXJyb3InKTtcclxuICAgICAgICAgICAgYWxlcnQocmVnRXJyLnZhbHVlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmVnaXN0ZXJNb2RlbC5sYXN0TmFtZS5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICBsZXQgcmVnRXJyOiBhbnkgPSB0aGlzLnRyYW5zbGF0ZS5nZXQoJ1JlZ2lzdGVyRXJyb3IubGFzdE5hbWVFcnJvcicpO1xyXG4gICAgICAgICAgICBhbGVydChyZWdFcnIudmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5yZWdpc3Rlck1vZGVsLmVtYWlsLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgICAgIGxldCByZWdFcnI6IGFueSA9IHRoaXMudHJhbnNsYXRlLmdldCgnUmVnaXN0ZXJFcnJvci5lbWFpbE5hbWVFcnJvcicpO1xyXG4gICAgICAgICAgICBhbGVydChyZWdFcnIudmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5yZWdpc3Rlck1vZGVsLm5pY2tOYW1lLmxlbmd0aCA8IDUpIHtcclxuICAgICAgICAgICAgbGV0IHJlZ0VycjogYW55ID0gdGhpcy50cmFuc2xhdGUuZ2V0KCdSZWdpc3RlckVycm9yLnVzZXJuYW1lTGVuZ3RoRXJyb3InKTtcclxuICAgICAgICAgICAgYWxlcnQocmVnRXJyLnZhbHVlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmVnaXN0ZXJNb2RlbC5uaWNrTmFtZS5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICBsZXQgcmVnRXJyOiBhbnkgPSB0aGlzLnRyYW5zbGF0ZS5nZXQoJ1JlZ2lzdGVyRXJyb3IudXNlcm5hbWVFbXB0eUVycm9yJyk7XHJcbiAgICAgICAgICAgIGFsZXJ0KHJlZ0Vyci52YWx1ZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnJlZ2lzdGVyTW9kZWwucGFzc3dvcmQubGVuZ3RoID09IDApIHtcclxuICAgICAgICAgICAgbGV0IHJlZ0VycjogYW55ID0gdGhpcy50cmFuc2xhdGUuZ2V0KCdSZWdpc3RlckVycm9yLnBhc3N3b3JkTmFtZUVycm9yJyk7XHJcbiAgICAgICAgICAgIGFsZXJ0KHJlZ0Vyci52YWx1ZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnJlZ2lzdGVyTW9kZWwucGFzc3dvcmRDb25maXJtYXRpb24ubGVuZ3RoID09IDApIHtcclxuICAgICAgICAgICAgbGV0IHJlZ0VycjogYW55ID0gdGhpcy50cmFuc2xhdGUuZ2V0KCdSZWdpc3RlckVycm9yLmNvbmZpcm1QYXNzd29yZE5hbWVFcnJvcicpO1xyXG4gICAgICAgICAgICBhbGVydChyZWdFcnIudmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5yZWdpc3Rlck1vZGVsLnBhc3N3b3JkQ29uZmlybWF0aW9uICE9IHRoaXMucmVnaXN0ZXJNb2RlbC5wYXNzd29yZCkge1xyXG4gICAgICAgICAgICBsZXQgcmVnRXJyOiBhbnkgPSB0aGlzLnRyYW5zbGF0ZS5nZXQoJ1JlZ2lzdGVyRXJyb3IuZXF1YWxQYXNzd29yZHNFcnJvcicpO1xyXG4gICAgICAgICAgICBhbGVydChyZWdFcnIudmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgdGhpcy5zcGlubmVyU2VydmljZS5zaG93KCk7XHJcbiAgICAgICAgICAgdGhpcy5fc3ViUmVnaXN0ZXJVc2VyID0gdGhpcy5yZWdpc3RlclNlcnZpY2UucmVnaXN0cmF0aW9uKHRoaXMucmVnaXN0ZXJNb2RlbCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgZGF0YSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEudXNlciAmJiBkYXRhLnVzZXIuaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHVzZXI6IGFueSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVzZXIgPSBkYXRhLnVzZXI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNMb2dpbiA9IHRoaXMubG9naW5TZXJ2aWNlLmlzRXhpc3RVc2VyKGRhdGEpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQ2hlY2sgaWYgZXhpc3QgdXNlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMuaXNFeGlzdFVzZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIgPSB1c2VyO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEdldCBhbmQgY3V0IHVzZXJuYW1lIGJ5IGxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b29sYmFyU2VydmljZS51c2VybmFtZSA9IHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIudXNlcm5hbWUubGVuZ3RoID49IHRoaXMudXNlcm5hbWVMZW5ndGggPyB0aGlzLnRvb2xiYXJTZXJ2aWNlLmN1cnJlbnRVc2VyLnVzZXJuYW1lIDogdGhpcy50b29sYmFyU2VydmljZS5jdXJyZW50VXNlci51c2VybmFtZS5zdWJzdHIoMCwgdGhpcy51c2VybmFtZUxlbmd0aCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRvb2xiYXJTZXJ2aWNlLnVzZXJFbWFpbCA9IHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIuZW1haWw7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbmZpZ1NlcnZpY2UudXNlciA9IHVzZXI7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gR2V0IG5ldyB1c2VyJ3MgcmVsYXRpb25zaGlwIHJlcXVlc3RzIGFuZCByZWRpcmVjdCB0byBcIm1haW4tcGFnZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9zdWJSZWxhdGlvbnNoaXBSZXF1ZXN0c1VzZXIgPSB0aGlzLnRvb2xiYXJTZXJ2aWNlLmdldFVzZXJSZWxhdGlvbnNoaXBSZXF1ZXN0c1N0YXR1c05ldyh0aGlzLnRvb2xiYXJTZXJ2aWNlLmN1cnJlbnRVc2VyLmlkKS5zdWJzY3JpYmUoKHJSZXF1ZXN0cykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UucmVsYXRpb25zaGlwUmVxdWVzdHMgPSByUmVxdWVzdHMucHJlcGFyZWRSUjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNwaW5uZXJTZXJ2aWNlLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9tYWluLXBhZ2UnXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgIChlcnI6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3Bpbm5lclNlcnZpY2UuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhlcnIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldCBhdmF0YXIgaW1hZ2UgZmlsZSB0byByZWdpc3RlciBtb2RlbFxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKi9cclxuICAgIG9uU2V0QXZhdGFySW1hZ2UoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLnJlZ2lzdGVyTW9kZWwuYXZhdGFySW1hZ2UgPSBldmVudC50YXJnZXQuZmlsZXNbMF07XHJcbiAgICB9XHJcbn0iXX0=
