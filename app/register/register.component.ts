import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import { RegisterModel } from './register.model';
import {RegisterService} from "./register.service";
import {LoginService} from "../login/login.service";
import {LoginModel} from "../login/login.model";
import {Subscription} from "rxjs/Subscription";
import {ToolbarService} from "../toolbar/toolbar.service";
import {ConfigService} from "../sharedService/config.service";
import {TranslateService} from "ng2-translate";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
    selector: 'main',
    templateUrl: 'app/register/register.component.html',
    styleUrls: ['app/register/register.component.scss']
})

export class RegisterComponent implements OnInit, OnDestroy {
    private _subRegisterUser: Subscription;
    private _subLoginUser: Subscription;
    private _subRelationshipRequestsUser: Subscription;
    private _subChangedLanguage: Subscription;

    public isLogin: boolean = true;
    private usernameLength: number = 14;

    constructor(public registerModel: RegisterModel,
                public registerService: RegisterService,
                public router: Router,
                public loginService: LoginService,
                public loginModel: LoginModel,
                public toolbarService: ToolbarService,
                public configService: ConfigService,
                public translate: TranslateService,
                private spinnerService: Ng4LoadingSpinnerService) {

        translate.setDefaultLang(localStorage.getItem('locale')? localStorage.getItem('locale') : 'en');
        translate.use(localStorage.getItem('locale')? localStorage.getItem('locale') : 'en');
    }

    ngOnInit() {
        console.info('REGISTER COMPONENT');
        this._subChangedLanguage = this.toolbarService.getChangedLanguage().subscribe(() => {
            this.translate.setDefaultLang(localStorage.getItem('locale'));
            this.translate.use(localStorage.getItem('locale'));
        })
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subRegisterUser) {
            this._subRegisterUser.unsubscribe();
        }
        if (this._subLoginUser) {
            this._subLoginUser.unsubscribe();
        }
        if (this._subRelationshipRequestsUser) {
            this._subRelationshipRequestsUser.unsubscribe();
        }
        if (this._subChangedLanguage) {
            this._subChangedLanguage.unsubscribe();
        }
    }

    /**
     * Register new user
     */
    onRegister() {
        if (this.registerModel.firstName.length == 0) {
            let regErr: any = this.translate.get('RegisterError.firstNameError');
            alert(regErr.value);
        } else if (this.registerModel.secondName.length == 0) {
            let regErr: any = this.translate.get('RegisterError.secondNameError');
            alert(regErr.value);
        } else if (this.registerModel.lastName.length == 0) {
            let regErr: any = this.translate.get('RegisterError.lastNameError');
            alert(regErr.value);
        } else if (this.registerModel.email.length == 0) {
            let regErr: any = this.translate.get('RegisterError.emailNameError');
            alert(regErr.value);
        } else if (this.registerModel.nickName.length < 5) {
            let regErr: any = this.translate.get('RegisterError.usernameLengthError');
            alert(regErr.value);
        } else if (this.registerModel.nickName.length == 0) {
            let regErr: any = this.translate.get('RegisterError.usernameEmptyError');
            alert(regErr.value);
        } else if (this.registerModel.password.length == 0) {
            let regErr: any = this.translate.get('RegisterError.passwordNameError');
            alert(regErr.value);
        } else if (this.registerModel.passwordConfirmation.length == 0) {
            let regErr: any = this.translate.get('RegisterError.confirmPasswordNameError');
            alert(regErr.value);
        } else if (this.registerModel.passwordConfirmation != this.registerModel.password) {
            let regErr: any = this.translate.get('RegisterError.equalPasswordsError');
            alert(regErr.value);
        } else {
           this.spinnerService.show();
           this._subRegisterUser = this.registerService.registration(this.registerModel).subscribe(
                data => {
                    if (data.user && data.user.id) {
                        let user: any = null;
                        user = data.user;
                        this.isLogin = this.loginService.isExistUser(data);

                        // Check if exist user
                        if (data.status.isExistUser) {
                            this.toolbarService.currentUser = user;

                            // Get and cut username by length
                            this.toolbarService.username = this.toolbarService.currentUser.username.length >= this.usernameLength ? this.toolbarService.currentUser.username : this.toolbarService.currentUser.username.substr(0, this.usernameLength);
                            this.toolbarService.userEmail = this.toolbarService.currentUser.email;
                            this.configService.user = user;

                            // Get new user's relationship requests and redirect to "main-page"
                            this._subRelationshipRequestsUser = this.toolbarService.getUserRelationshipRequestsStatusNew(this.toolbarService.currentUser.id).subscribe((rRequests) => {
                                this.toolbarService.relationshipRequests = rRequests.preparedRR;
                                this.spinnerService.hide();
                                this.router.navigate(['/main-page']);
                            });
                        }
                    }
                },
               (err: any) => {
                    this.spinnerService.hide();
                    console.info(err);
                }
            );
        }
    }

    /**
     * Set avatar image file to register model
     * @param event
     */
    onSetAvatarImage(event) {
        this.registerModel.avatarImage = event.target.files[0];
    }
}