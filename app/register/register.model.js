System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var RegisterModel;
    return {
        setters: [],
        execute: function () {
            RegisterModel = class RegisterModel {
                constructor() {
                    this.firstName = '';
                    this.secondName = '';
                    this.lastName = '';
                    this.email = '';
                    this.password = '';
                    this.passwordConfirmation = '';
                    this.nickName = '';
                    this.avatarImage = null;
                }
            };
            exports_1("RegisterModel", RegisterModel);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7WUFBQSxnQkFBQTtnQkFBQTtvQkFDSSxjQUFTLEdBQVcsRUFBRSxDQUFDO29CQUN2QixlQUFVLEdBQVcsRUFBRSxDQUFDO29CQUN4QixhQUFRLEdBQVcsRUFBRSxDQUFDO29CQUN0QixVQUFLLEdBQVcsRUFBRSxDQUFDO29CQUNuQixhQUFRLEdBQVcsRUFBRSxDQUFDO29CQUN0Qix5QkFBb0IsR0FBVyxFQUFFLENBQUM7b0JBQ2xDLGFBQVEsR0FBVyxFQUFFLENBQUM7b0JBQ3RCLGdCQUFXLEdBQVEsSUFBSSxDQUFDO2dCQUM1QixDQUFDO2FBQUEsQ0FBQTs7UUFBQSxDQUFDIiwiZmlsZSI6InJlZ2lzdGVyL3JlZ2lzdGVyLm1vZGVsLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIFJlZ2lzdGVyTW9kZWwge1xyXG4gICAgZmlyc3ROYW1lOiBzdHJpbmcgPSAnJztcclxuICAgIHNlY29uZE5hbWU6IHN0cmluZyA9ICcnO1xyXG4gICAgbGFzdE5hbWU6IHN0cmluZyA9ICcnO1xyXG4gICAgZW1haWw6IHN0cmluZyA9ICcnO1xyXG4gICAgcGFzc3dvcmQ6IHN0cmluZyA9ICcnO1xyXG4gICAgcGFzc3dvcmRDb25maXJtYXRpb246IHN0cmluZyA9ICcnO1xyXG4gICAgbmlja05hbWU6IHN0cmluZyA9ICcnO1xyXG4gICAgYXZhdGFySW1hZ2U6IGFueSA9IG51bGw7XHJcbn0iXX0=
