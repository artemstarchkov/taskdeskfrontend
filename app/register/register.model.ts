export class RegisterModel {
    firstName: string = '';
    secondName: string = '';
    lastName: string = '';
    email: string = '';
    password: string = '';
    passwordConfirmation: string = '';
    nickName: string = '';
    avatarImage: any = null;
}