System.register(["@angular/core", "@angular/common", "@angular/forms", "@angular/http", "@angular/router", "./register.route", "./register.component", "./register.model", "./register.service", "ng2-translate"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, common_1, forms_1, http_1, router_1, register_route_1, register_component_1, register_model_1, register_service_1, ng2_translate_1, RegisterModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (register_route_1_1) {
                register_route_1 = register_route_1_1;
            },
            function (register_component_1_1) {
                register_component_1 = register_component_1_1;
            },
            function (register_model_1_1) {
                register_model_1 = register_model_1_1;
            },
            function (register_service_1_1) {
                register_service_1 = register_service_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            }
        ],
        execute: function () {
            RegisterModule = class RegisterModule {
            };
            RegisterModule = __decorate([
                core_1.NgModule({
                    imports: [
                        common_1.CommonModule,
                        forms_1.FormsModule,
                        http_1.HttpModule,
                        router_1.RouterModule,
                        register_route_1.REGISTER_ROUTES,
                        ng2_translate_1.TranslateModule.forRoot({
                            provide: ng2_translate_1.TranslateLoader,
                            useFactory: (http) => new ng2_translate_1.TranslateStaticLoader(http, './assets/i18n', '.json'),
                            deps: [http_1.Http]
                        })
                    ],
                    providers: [
                        register_model_1.RegisterModel,
                        register_service_1.RegisterService
                    ],
                    declarations: [
                        register_component_1.RegisterComponent
                    ]
                })
            ], RegisterModule);
            exports_1("RegisterModule", RegisterModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQWdDYSxjQUFjLEdBQTNCO2FBQStCLENBQUE7WUFBbEIsY0FBYztnQkFyQjFCLGVBQVEsQ0FBQztvQkFDTixPQUFPLEVBQUU7d0JBQ0wscUJBQVk7d0JBQ1osbUJBQVc7d0JBQ1gsaUJBQVU7d0JBQ1YscUJBQVk7d0JBQ1osZ0NBQWU7d0JBQ2YsK0JBQWUsQ0FBQyxPQUFPLENBQUM7NEJBQ3BCLE9BQU8sRUFBRSwrQkFBZTs0QkFDeEIsVUFBVSxFQUFFLENBQUMsSUFBVSxFQUFFLEVBQUUsQ0FBQyxJQUFJLHFDQUFxQixDQUFDLElBQUksRUFBRSxlQUFlLEVBQUUsT0FBTyxDQUFDOzRCQUNyRixJQUFJLEVBQUUsQ0FBQyxXQUFJLENBQUM7eUJBQ2YsQ0FBQztxQkFDTDtvQkFDRCxTQUFTLEVBQUU7d0JBQ1AsOEJBQWE7d0JBQ2Isa0NBQWU7cUJBQ2xCO29CQUNELFlBQVksRUFBRTt3QkFDVixzQ0FBaUI7cUJBQ3BCO2lCQUNKLENBQUM7ZUFDVyxjQUFjLENBQUk7O1FBQUEsQ0FBQyIsImZpbGUiOiJyZWdpc3Rlci9yZWdpc3Rlci5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7Rm9ybXNNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5pbXBvcnQge0h0dHAsIEh0dHBNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBSRUdJU1RFUl9ST1VURVMgfSBmcm9tICcuL3JlZ2lzdGVyLnJvdXRlJztcclxuaW1wb3J0IHtSZWdpc3RlckNvbXBvbmVudH0gZnJvbSBcIi4vcmVnaXN0ZXIuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7UmVnaXN0ZXJNb2RlbH0gZnJvbSBcIi4vcmVnaXN0ZXIubW9kZWxcIjtcclxuaW1wb3J0IHtSZWdpc3RlclNlcnZpY2V9IGZyb20gXCIuL3JlZ2lzdGVyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtUcmFuc2xhdGVMb2FkZXIsIFRyYW5zbGF0ZU1vZHVsZSwgVHJhbnNsYXRlU3RhdGljTG9hZGVyfSBmcm9tIFwibmcyLXRyYW5zbGF0ZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgSHR0cE1vZHVsZSxcclxuICAgICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgICAgUkVHSVNURVJfUk9VVEVTLFxyXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KHtcclxuICAgICAgICAgICAgcHJvdmlkZTogVHJhbnNsYXRlTG9hZGVyLFxyXG4gICAgICAgICAgICB1c2VGYWN0b3J5OiAoaHR0cDogSHR0cCkgPT4gbmV3IFRyYW5zbGF0ZVN0YXRpY0xvYWRlcihodHRwLCAnLi9hc3NldHMvaTE4bicsICcuanNvbicpLFxyXG4gICAgICAgICAgICBkZXBzOiBbSHR0cF1cclxuICAgICAgICB9KVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIFJlZ2lzdGVyTW9kZWwsXHJcbiAgICAgICAgUmVnaXN0ZXJTZXJ2aWNlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgUmVnaXN0ZXJDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFJlZ2lzdGVyTW9kdWxlIHsgfSJdfQ==
