import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {Http, HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";
import { REGISTER_ROUTES } from './register.route';
import {RegisterComponent} from "./register.component";
import {RegisterModel} from "./register.model";
import {RegisterService} from "./register.service";
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from "ng2-translate";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule,
        REGISTER_ROUTES,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, './assets/i18n', '.json'),
            deps: [Http]
        })
    ],
    providers: [
        RegisterModel,
        RegisterService
    ],
    declarations: [
        RegisterComponent
    ]
})
export class RegisterModule { }