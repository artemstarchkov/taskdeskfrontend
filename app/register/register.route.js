System.register(["@angular/router", "./register.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, register_component_1, registerRoutes, REGISTER_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (register_component_1_1) {
                register_component_1 = register_component_1_1;
            }
        ],
        execute: function () {
            registerRoutes = [
                {
                    path: '',
                    component: register_component_1.RegisterComponent
                }
            ];
            exports_1("REGISTER_ROUTES", REGISTER_ROUTES = router_1.RouterModule.forChild(registerRoutes));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLnJvdXRlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O1lBSU0sY0FBYyxHQUFVO2dCQUMxQjtvQkFDSSxJQUFJLEVBQUUsRUFBRTtvQkFDUixTQUFTLEVBQUUsc0NBQWlCO2lCQUMvQjthQUNKLENBQUM7WUFFRiw2QkFBYSxlQUFlLEdBQXVCLHFCQUFZLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFDO1FBQUEsQ0FBQyIsImZpbGUiOiJyZWdpc3Rlci9yZWdpc3Rlci5yb3V0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Um91dGVzLCBSb3V0ZXJNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtNb2R1bGVXaXRoUHJvdmlkZXJzfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1JlZ2lzdGVyQ29tcG9uZW50fSBmcm9tIFwiLi9yZWdpc3Rlci5jb21wb25lbnRcIjtcclxuXHJcbmNvbnN0IHJlZ2lzdGVyUm91dGVzOlJvdXRlcyA9IFtcclxuICAgIHtcclxuICAgICAgICBwYXRoOiAnJyxcclxuICAgICAgICBjb21wb25lbnQ6IFJlZ2lzdGVyQ29tcG9uZW50XHJcbiAgICB9XHJcbl07XHJcblxyXG5leHBvcnQgY29uc3QgUkVHSVNURVJfUk9VVEVTOk1vZHVsZVdpdGhQcm92aWRlcnMgPSBSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocmVnaXN0ZXJSb3V0ZXMpOyJdfQ==
