import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {RegisterComponent} from "./register.component";

const registerRoutes:Routes = [
    {
        path: '',
        component: RegisterComponent
    }
];

export const REGISTER_ROUTES:ModuleWithProviders = RouterModule.forChild(registerRoutes);