import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Http } from '@angular/http';
import "rxjs/add/operator/map";
import { RegisterModel } from "./register.model";
import { ConfigService } from "../sharedService/config.service";
export declare class RegisterService {
    private router;
    http: Http;
    configService: ConfigService;
    constructor(router: Router, http: Http, configService: ConfigService);
    registration(registerModel: RegisterModel): Observable<any>;
}
