System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "../sharedService/config.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, config_service_1, RegisterService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            }
        ],
        execute: function () {
            RegisterService = class RegisterService {
                constructor(router, http, configService) {
                    this.router = router;
                    this.http = http;
                    this.configService = configService;
                }
                registration(registerModel) {
                    let formData = new FormData();
                    let strRegisterModel = JSON.stringify(registerModel);
                    formData.append('avatarImage', registerModel.avatarImage);
                    formData.append('registerModel', strRegisterModel);
                    return this.http.post(this.configService.domen + 'user/new', formData)
                        .map(res => res.json());
                }
            };
            RegisterService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router,
                    http_1.Http,
                    config_service_1.ConfigService])
            ], RegisterService);
            exports_1("RegisterService", RegisterService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQVNhLGVBQWUsR0FBNUI7Z0JBRUksWUFBb0IsTUFBYyxFQUNmLElBQVUsRUFDVixhQUE0QjtvQkFGM0IsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFDZixTQUFJLEdBQUosSUFBSSxDQUFNO29CQUNWLGtCQUFhLEdBQWIsYUFBYSxDQUFlO2dCQUFHLENBQUM7Z0JBTTVDLFlBQVksQ0FBQyxhQUE0QjtvQkFHNUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztvQkFDOUIsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUVyRCxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzFELFFBQVEsQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLGdCQUFnQixDQUFDLENBQUM7b0JBRW5ELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxVQUFVLEVBQUUsUUFBUSxDQUFDO3lCQUNqRSxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQzthQUNKLENBQUE7WUF0QlksZUFBZTtnQkFEM0IsaUJBQVUsRUFBRTtpREFHbUIsZUFBTTtvQkFDVCxXQUFJO29CQUNLLDhCQUFhO2VBSnRDLGVBQWUsQ0FzQjNCOztRQUFBLENBQUMiLCJmaWxlIjoicmVnaXN0ZXIvcmVnaXN0ZXIuc2VydmljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SW5qZWN0YWJsZX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtSb3V0ZXJ9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtPYnNlcnZhYmxlfSBmcm9tIFwicnhqcy9PYnNlcnZhYmxlXCI7XHJcbmltcG9ydCB7SHR0cCwgSGVhZGVyc30gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCBcInJ4anMvYWRkL29wZXJhdG9yL21hcFwiO1xyXG5pbXBvcnQge1JlZ2lzdGVyTW9kZWx9IGZyb20gXCIuL3JlZ2lzdGVyLm1vZGVsXCI7XHJcbmltcG9ydCB7Q29uZmlnU2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZFNlcnZpY2UvY29uZmlnLnNlcnZpY2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFJlZ2lzdGVyU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBodHRwOiBIdHRwLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2UpIHt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZW5kIHBvc3QgcmVxdWVzdCB0byByZWdpc3RlciBuZXcgdXNlciBhbmQgZ2V0IHJlc3BvbnNlOiByZWdpc3RlcmVkIFwidXNlclwiIGFuZCBcInN0YXR1c1wiXHJcbiAgICAgKiBAcGFyYW0gcmVnaXN0ZXJNb2RlbDogUmVnaXN0ZXJNb2RlbFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcmVnaXN0cmF0aW9uKHJlZ2lzdGVyTW9kZWw6IFJlZ2lzdGVyTW9kZWwpIHtcclxuICAgICAgICAvLyBJbiByZWdpc3Rlck1vZGVsLmF2YXRhckltYWdlIHdlIGhhdmUgZGF0YSBvZiBcImZvcm1EYXRhXCIgdHlwZSAoaW1hZ2UgLSBcIkZpbGUgdHlwZVwiKS5cclxuICAgICAgICAvLyBUbyBzZW5kIEZpbGUgdHlwZSBkYXRhIGFuZCBqc29uIGRhdGEgKHJlZ2lzdGVyTW9kZWwpIHdlIG5lZWQgYWRkIFwianNvbiBkYXRhXCIgbGlrZSBcImpzb24gc3RyaW5nXCIgaW4gZGF0YSBvZiBhIFwiZm9ybURhdGFcIiB0eXBlLlxyXG4gICAgICAgIGxldCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICAgIGxldCBzdHJSZWdpc3Rlck1vZGVsID0gSlNPTi5zdHJpbmdpZnkocmVnaXN0ZXJNb2RlbCk7XHJcbiAgICAgICAgLy9mb3JtRGF0YSA9IHJlZ2lzdGVyTW9kZWwuYXZhdGFySW1hZ2U7XHJcbiAgICAgICAgZm9ybURhdGEuYXBwZW5kKCdhdmF0YXJJbWFnZScsIHJlZ2lzdGVyTW9kZWwuYXZhdGFySW1hZ2UpO1xyXG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZCgncmVnaXN0ZXJNb2RlbCcsIHN0clJlZ2lzdGVyTW9kZWwpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWdTZXJ2aWNlLmRvbWVuICsgJ3VzZXIvbmV3JywgZm9ybURhdGEpXHJcbiAgICAgICAgICAgIC5tYXAocmVzID0+IHJlcy5qc29uKCkpO1xyXG4gICAgfVxyXG59Il19
