import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers} from '@angular/http';
import "rxjs/add/operator/map";
import {RegisterModel} from "./register.model";
import {ConfigService} from "../sharedService/config.service";

@Injectable()
export class RegisterService {

    constructor(private router: Router,
                public http: Http,
                public configService: ConfigService) {}

    /**
     * Send post request to register new user and get response: registered "user" and "status"
     * @param registerModel: RegisterModel
     */
    public registration(registerModel: RegisterModel) {
        // In registerModel.avatarImage we have data of "formData" type (image - "File type").
        // To send File type data and json data (registerModel) we need add "json data" like "json string" in data of a "formData" type.
        let formData = new FormData();
        let strRegisterModel = JSON.stringify(registerModel);
        //formData = registerModel.avatarImage;
        formData.append('avatarImage', registerModel.avatarImage);
        formData.append('registerModel', strRegisterModel);

        return this.http.post(this.configService.domen + 'user/new', formData)
            .map(res => res.json());
    }
}