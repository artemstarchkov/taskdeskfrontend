System.register(["@angular/core", "@angular/common", "ng2-translate", "@angular/http"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, common_1, ng2_translate_1, http_1, SharedModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }
        ],
        execute: function () {
            SharedModule = class SharedModule {
            };
            SharedModule = __decorate([
                core_1.NgModule({
                    imports: [
                        common_1.CommonModule,
                        ng2_translate_1.TranslateModule.forRoot({
                            provide: ng2_translate_1.TranslateLoader,
                            useFactory: (http) => new ng2_translate_1.TranslateStaticLoader(http, './assets/i18n', '.json'),
                            deps: [http_1.Http]
                        })
                    ],
                    providers: [],
                    declarations: [],
                    exports: [
                        common_1.CommonModule,
                        ng2_translate_1.TranslateModule
                    ]
                })
            ], SharedModule);
            exports_1("SharedModule", SharedModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3NoYXJlZE1vZHVsZS9zaGFyZWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBZ0NhLFlBQVksR0FBekI7YUFBNkIsQ0FBQTtZQUFoQixZQUFZO2dCQXZCeEIsZUFBUSxDQUFDO29CQUNOLE9BQU8sRUFBRTt3QkFFTCxxQkFBWTt3QkFDWiwrQkFBZSxDQUFDLE9BQU8sQ0FBQzs0QkFDcEIsT0FBTyxFQUFFLCtCQUFlOzRCQUN4QixVQUFVLEVBQUUsQ0FBQyxJQUFVLEVBQUUsRUFBRSxDQUFDLElBQUkscUNBQXFCLENBQUMsSUFBSSxFQUFFLGVBQWUsRUFBRSxPQUFPLENBQUM7NEJBQ3JGLElBQUksRUFBRSxDQUFDLFdBQUksQ0FBQzt5QkFDZixDQUFDO3FCQUNMO29CQUNELFNBQVMsRUFBRSxFQUdWO29CQUNELFlBQVksRUFBRSxFQUViO29CQUNELE9BQU8sRUFBRTt3QkFDTCxxQkFBWTt3QkFFWiwrQkFBZTtxQkFDbEI7aUJBQ0osQ0FBQztlQUNXLFlBQVksQ0FBSTs7UUFBQSxDQUFDIiwiZmlsZSI6InNoYXJlZE1vZHVsZS9zaGFyZWQubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbi8vaW1wb3J0IHtUUkFOU0xBVElPTl9QUk9WSURFUlN9ICAgZnJvbSAnLi4vdHJhbnNsYXRlL3RyYW5zbGF0aW9uJztcclxuLy9pbXBvcnQge1RyYW5zbGF0ZVBpcGV9ICAgZnJvbSAnLi4vdHJhbnNsYXRlL3RyYW5zbGF0ZS5waXBlJztcclxuLy9pbXBvcnQge1RyYW5zbGF0ZVNlcnZpY2V9ICAgZnJvbSAnLi4vdHJhbnNsYXRlL3RyYW5zbGF0ZS5zZXJ2aWNlJztcclxuLy9pbXBvcnQgeyBTb2NrZXRJb01vZHVsZSwgU29ja2V0SW9Db25maWcgfSBmcm9tICduZy1zb2NrZXQtaW8nO1xyXG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQge1RyYW5zbGF0ZUxvYWRlciwgVHJhbnNsYXRlTW9kdWxlLCBUcmFuc2xhdGVTdGF0aWNMb2FkZXJ9IGZyb20gXCJuZzItdHJhbnNsYXRlXCI7XHJcbmltcG9ydCB7SHR0cH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbi8vICAgICAgICBTb2NrZXRJb01vZHVsZS5mb3JSb290KGNvbmZpZylcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvclJvb3Qoe1xyXG4gICAgICAgICAgICBwcm92aWRlOiBUcmFuc2xhdGVMb2FkZXIsXHJcbiAgICAgICAgICAgIHVzZUZhY3Rvcnk6IChodHRwOiBIdHRwKSA9PiBuZXcgVHJhbnNsYXRlU3RhdGljTG9hZGVyKGh0dHAsICcuL2Fzc2V0cy9pMThuJywgJy5qc29uJyksXHJcbiAgICAgICAgICAgIGRlcHM6IFtIdHRwXVxyXG4gICAgICAgIH0pXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgLy9UUkFOU0xBVElPTl9QUk9WSURFUlMsXHJcbiAgICAgICAgLy9UcmFuc2xhdGVTZXJ2aWNlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgLy9UcmFuc2xhdGVQaXBlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICAvL1RyYW5zbGF0ZVBpcGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGUgeyB9Il19
