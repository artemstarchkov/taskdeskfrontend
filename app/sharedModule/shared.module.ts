import {NgModule} from '@angular/core';
//import {TRANSLATION_PROVIDERS}   from '../translate/translation';
//import {TranslatePipe}   from '../translate/translate.pipe';
//import {TranslateService}   from '../translate/translate.service';
//import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import {CommonModule} from "@angular/common";
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from "ng2-translate";
import {Http} from "@angular/http";

@NgModule({
    imports: [
//        SocketIoModule.forRoot(config)
        CommonModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, './assets/i18n', '.json'),
            deps: [Http]
        })
    ],
    providers: [
        //TRANSLATION_PROVIDERS,
        //TranslateService
    ],
    declarations: [
        //TranslatePipe
    ],
    exports: [
        CommonModule,
        //TranslatePipe,
        TranslateModule
    ]
})
export class SharedModule { }