import { Pipe, PipeTransform } from '@angular/core';

// Pipe on get keys from object
@Pipe({
    name: 'objectKeys',
    pure: true // add in this line, handle every time when value was changed
})
export class ObjectKeysPipe implements PipeTransform {
    transform(object: any, args: any[]): any {
        if (!object || Object.keys(object).length <= 0) {
            return false;
        }

        return Object.keys(object);
    }
}