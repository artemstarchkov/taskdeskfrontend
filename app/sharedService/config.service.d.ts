import { Router } from "@angular/router";
import { Http } from '@angular/http';
import "rxjs/add/operator/map";
import "rxjs/add/operator/share";
import { Subject } from "rxjs/Subject";
export declare class ConfigService {
    router: Router;
    http: Http;
    sub: Subject<string>;
    user: any;
    currentLang: string;
    domen: string;
    constructor(router: Router, http: Http);
    resetData(): void;
}
