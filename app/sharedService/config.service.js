System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "rxjs/add/operator/share", "rxjs/Subject"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, Subject_1, ConfigService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (_2) {
            },
            function (Subject_1_1) {
                Subject_1 = Subject_1_1;
            }
        ],
        execute: function () {
            ConfigService = class ConfigService {
                constructor(router, http) {
                    this.router = router;
                    this.http = http;
                    this.sub = new Subject_1.Subject();
                    this.user = null;
                    this.currentLang = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en';
                    this.domen = 'http://onetwo.on.kg/en/';
                }
                resetData() {
                    this.user = null;
                }
            };
            ConfigService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router, http_1.Http])
            ], ConfigService);
            exports_1("ConfigService", ConfigService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3NoYXJlZFNlcnZpY2UvY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBVWEsYUFBYSxHQUExQjtnQkFjSSxZQUFtQixNQUFjLEVBQVMsSUFBVTtvQkFBakMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFBUyxTQUFJLEdBQUosSUFBSSxDQUFNO29CQVpwRCxRQUFHLEdBQW9CLElBQUksaUJBQU8sRUFBVSxDQUFDO29CQUM3QyxTQUFJLEdBQVEsSUFBSSxDQUFDO29CQUNqQixnQkFBVyxHQUFXLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFJNUYsVUFBSyxHQUFXLHlCQUF5QixDQUFDO2dCQU1hLENBQUM7Z0JBeUJqRCxTQUFTO29CQUNaLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNyQixDQUFDO2FBQ0osQ0FBQTtZQTFDWSxhQUFhO2dCQUR6QixpQkFBVSxFQUFFO2lEQWVrQixlQUFNLEVBQWUsV0FBSTtlQWQzQyxhQUFhLENBMEN6Qjs7UUFBQSxDQUFDIiwiZmlsZSI6InNoYXJlZFNlcnZpY2UvY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGUsIE91dHB1dCwgRXZlbnRFbWl0dGVyfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzL09ic2VydmFibGVcIjtcclxuaW1wb3J0IHtIdHRwLCBIZWFkZXJzLCBSZXNwb25zZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCBcInJ4anMvYWRkL29wZXJhdG9yL21hcFwiO1xyXG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9zaGFyZVwiO1xyXG5pbXBvcnQge1N1YmplY3R9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0ICogYXMgaW8gZnJvbSAnc29ja2V0LmlvLWNsaWVudCc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBDb25maWdTZXJ2aWNlIHtcclxuXHJcbiAgICBzdWI6IFN1YmplY3Q8c3RyaW5nPiA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuICAgIHVzZXI6IGFueSA9IG51bGw7XHJcbiAgICBjdXJyZW50TGFuZzogc3RyaW5nID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2xvY2FsZScpPyBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9jYWxlJykgOiAnZW4nO1xyXG5cclxuICAgIC8vcHJvZHVjdGlvbiBkb21lblxyXG4gICAgLy9kb21lbjogc3RyaW5nID0gJ2h0dHA6Ly9hcnRlbXN0YXJjaGtvdi5vbi5rZy9lbi8nO1xyXG4gICAgZG9tZW46IHN0cmluZyA9ICdodHRwOi8vb25ldHdvLm9uLmtnL2VuLyc7XHJcblxyXG4gICAgLy8gZGV2ZWxvcGVyIGRvbWVuXHJcbiAgICAvL2RvbWVuOiBzdHJpbmcgPSAnaHR0cDovL2xvY2FsaG9zdC90YXNrZGVza2JhY2tlbmQvd2ViL2FwcF9kZXYucGhwL2VuLyc7XHJcbiAgICAvL3N1YnNjcmliZXJzQ291bnRlciA9IDA7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIHJvdXRlcjogUm91dGVyLCBwdWJsaWMgaHR0cDogSHR0cCkge31cclxuXHJcblxyXG5cclxuLy9jb25zdCBjb25maWc6IFNvY2tldElvQ29uZmlnID0geyB1cmw6ICdodHRwczovL2FnaWxlLXR1bmRyYS00NjcyNy5oZXJva3VhcHAuY29tLycsIG9wdGlvbnM6IHt9IH07XHJcblxyXG4gICAgLy9zb2NrZXQgPSBpby5jb25uZWN0KCdodHRwczovL2FnaWxlLXR1bmRyYS00NjcyNy5oZXJva3VhcHAuY29tLycsIHt9KTtcclxuXHJcbiAgICAvKiogY3JlYXRlIGFuIE9ic2VydmFibGUgZnJvbSBhbiBldmVudCAqL1xyXG4gICAgLypmcm9tRXZlbnQ8VD4oZXZlbnROYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFQ+IHtcclxuICAgICAgICB0aGlzLnN1YnNjcmliZXJzQ291bnRlcisrO1xyXG4gICAgICAgIHJldHVybiBPYnNlcnZhYmxlLmNyZWF0ZSggKG9ic2VydmVyOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zb2NrZXQub24oZXZlbnROYW1lLCAoZGF0YTogVCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkYXRhKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdWJzY3JpYmVyc0NvdW50ZXIgPT09IDEpXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zb2NrZXQucmVtb3ZlTGlzdGVuZXIoZXZlbnROYW1lKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9KS5zaGFyZSgpO1xyXG4gICAgfSovXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXNldCBhbGwgZGF0YS4gVGhpcyBmdW5jdGlvbiBjYWxsIHdoZW4gdXNlciBleGl0IGZyb20gcHJvZ3JhbSBhbmQgbmVlZCB0byBjbGVhciBkYXRhLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcmVzZXREYXRhKCkge1xyXG4gICAgICAgIHRoaXMudXNlciA9IG51bGw7XHJcbiAgICB9XHJcbn0iXX0=
