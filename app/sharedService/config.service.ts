import {Injectable, Output, EventEmitter} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import "rxjs/add/operator/share";
import {Subject} from "rxjs/Subject";
import * as io from 'socket.io-client';

@Injectable()
export class ConfigService {

    sub: Subject<string> = new Subject<string>();
    user: any = null;
    currentLang: string = localStorage.getItem('locale')? localStorage.getItem('locale') : 'en';

    //production domen
    //domen: string = 'http://artemstarchkov.on.kg/en/';
    domen: string = 'http://onetwo.on.kg/en/';

    // developer domen
    //domen: string = 'http://localhost/taskdeskbackend/web/app_dev.php/en/';
    //subscribersCounter = 0;

    constructor(public router: Router, public http: Http) {}



//const config: SocketIoConfig = { url: 'https://agile-tundra-46727.herokuapp.com/', options: {} };

    //socket = io.connect('https://agile-tundra-46727.herokuapp.com/', {});

    /** create an Observable from an event */
    /*fromEvent<T>(eventName: string): Observable<T> {
        this.subscribersCounter++;
        return Observable.create( (observer: any) => {
            this.socket.on(eventName, (data: T) => {
                observer.next(data);
            });
            return () => {
                if (this.subscribersCounter === 1)
                    this.socket.removeListener(eventName);
            };
        }).share();
    }*/

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.user = null;
    }
}