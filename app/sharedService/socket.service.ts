import {Injectable, Output, EventEmitter} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import "rxjs/add/operator/share";
import {Subject} from "rxjs/Subject";
import * as io from 'socket.io-client';
import {ConfigService} from "./config.service";

@Injectable()
export class SocketService {

    subscribersCounter: number = 0;

    constructor(public router: Router, public http: Http, public configService: ConfigService) {}

    socket : any = null;

    connection (user) {
        this.socket = io.connect('https://agile-tundra-46727.herokuapp.com/', {query: {user: user}});
    }

    /** create an Observable from an event */
    fromEvent<T>(eventName: string): Observable<T> {
        this.subscribersCounter++;
        return Observable.create( (observer: any) => {
            this.socket.on(eventName, (data: T) => {
                observer.next(data);
            });
            return () => {
                if (this.subscribersCounter === 1)
                    this.socket.removeListener(eventName);
            };
        }).share();
    }

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.subscribersCounter = 0;
        this.socket = null;
    }
}