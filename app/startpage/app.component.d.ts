import { OnInit, OnDestroy } from '@angular/core';
import { MainService } from "../main/main.service";
import { SocketService } from "../sharedService/socket.service";
export declare class AppComponent implements OnInit, OnDestroy {
    mainService: MainService;
    socketService: SocketService;
    constructor(mainService: MainService, socketService: SocketService);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
