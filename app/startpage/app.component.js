System.register(["@angular/core", "../main/main.service", "../sharedService/socket.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, main_service_1, socket_service_1, AppComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (main_service_1_1) {
                main_service_1 = main_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            }
        ],
        execute: function () {
            AppComponent = class AppComponent {
                constructor(mainService, socketService) {
                    this.mainService = mainService;
                    this.socketService = socketService;
                }
                ngOnInit() {
                    let locale = localStorage.getItem('locale') ? localStorage.getItem('locale') : 'en';
                    localStorage.setItem('locale', locale);
                }
                ngOnDestroy() { }
            };
            AppComponent = __decorate([
                core_1.Component({
                    selector: 'my-app',
                    templateUrl: 'app/startpage/app.component.html',
                    styleUrls: ['app/startpage/app.component.scss']
                }),
                __metadata("design:paramtypes", [main_service_1.MainService,
                    socket_service_1.SocketService])
            ], AppComponent);
            exports_1("AppComponent", AppComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3N0YXJ0cGFnZS9hcHAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBVWEsWUFBWSxHQUF6QjtnQkFHRSxZQUFtQixXQUF3QixFQUN4QixhQUE0QjtvQkFENUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7b0JBQ3hCLGtCQUFhLEdBQWIsYUFBYSxDQUFlO2dCQUFHLENBQUM7Z0JBRW5ELFFBQVE7b0JBQ04sSUFBSSxNQUFNLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUNuRixZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDekMsQ0FBQztnQkFFRCxXQUFXLEtBQUksQ0FBQzthQUNqQixDQUFBO1lBWlksWUFBWTtnQkFOeEIsZ0JBQVMsQ0FBQztvQkFDVCxRQUFRLEVBQUUsUUFBUTtvQkFDbEIsV0FBVyxFQUFFLGtDQUFrQztvQkFDL0MsU0FBUyxFQUFFLENBQUMsa0NBQWtDLENBQUM7aUJBQ2hELENBQUM7aURBS2dDLDBCQUFXO29CQUNULDhCQUFhO2VBSnBDLFlBQVksQ0FZeEI7O1FBQUEsQ0FBQyIsImZpbGUiOiJzdGFydHBhZ2UvYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7TWFpblNlcnZpY2V9IGZyb20gXCIuLi9tYWluL21haW4uc2VydmljZVwiO1xyXG5pbXBvcnQge1NvY2tldFNlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL3NvY2tldC5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ215LWFwcCcsXHJcbiAgdGVtcGxhdGVVcmw6ICdhcHAvc3RhcnRwYWdlL2FwcC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJ2FwcC9zdGFydHBhZ2UvYXBwLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG5cclxuICBjb25zdHJ1Y3RvcihwdWJsaWMgbWFpblNlcnZpY2U6IE1haW5TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgIHB1YmxpYyBzb2NrZXRTZXJ2aWNlOiBTb2NrZXRTZXJ2aWNlKSB7fVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGxldCBsb2NhbGUgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9jYWxlJyk/IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKSA6ICdlbic7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbG9jYWxlJywgbG9jYWxlKTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge31cclxufSJdfQ==
