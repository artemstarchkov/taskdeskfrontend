import {Component, OnInit, OnDestroy} from '@angular/core';
import {MainService} from "../main/main.service";
import {SocketService} from "../sharedService/socket.service";

@Component({
  selector: 'my-app',
  templateUrl: 'app/startpage/app.component.html',
  styleUrls: ['app/startpage/app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {


  constructor(public mainService: MainService,
              public socketService: SocketService) {}

  ngOnInit() {
    let locale = localStorage.getItem('locale')? localStorage.getItem('locale') : 'en';
    localStorage.setItem('locale', locale);
  }

  ngOnDestroy() {}
}