System.register(["@angular/platform-browser", "@angular/core", "@angular/http", "@angular/common", "./app.component", "../toolbar/toolbar.module", "../toolbar/toolbar.component", "@angular/forms", "@angular/router", "./app.route", "../login/login.route", "./app.service", "../login/login.module", "../sharedModule/shared.module", "ng2-translate", "ng4-loading-spinner"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var platform_browser_1, core_1, http_1, common_1, app_component_1, toolbar_module_1, toolbar_component_1, forms_1, router_1, app_route_1, login_route_1, app_service_1, login_module_1, shared_module_1, ng2_translate_1, ng4_loading_spinner_1, AppModule;
    return {
        setters: [
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (app_component_1_1) {
                app_component_1 = app_component_1_1;
            },
            function (toolbar_module_1_1) {
                toolbar_module_1 = toolbar_module_1_1;
            },
            function (toolbar_component_1_1) {
                toolbar_component_1 = toolbar_component_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (app_route_1_1) {
                app_route_1 = app_route_1_1;
            },
            function (login_route_1_1) {
                login_route_1 = login_route_1_1;
            },
            function (app_service_1_1) {
                app_service_1 = app_service_1_1;
            },
            function (login_module_1_1) {
                login_module_1 = login_module_1_1;
            },
            function (shared_module_1_1) {
                shared_module_1 = shared_module_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (ng4_loading_spinner_1_1) {
                ng4_loading_spinner_1 = ng4_loading_spinner_1_1;
            }
        ],
        execute: function () {
            AppModule = class AppModule {
            };
            AppModule = __decorate([
                core_1.NgModule({
                    imports: [
                        app_route_1.APP_ROUTES,
                        login_route_1.LOGIN_ROUTES,
                        platform_browser_1.BrowserModule,
                        forms_1.FormsModule,
                        common_1.CommonModule,
                        http_1.HttpModule,
                        router_1.RouterModule,
                        shared_module_1.SharedModule,
                        toolbar_module_1.ToolbarModule,
                        login_module_1.LoginModule,
                        ng2_translate_1.TranslateModule.forRoot({
                            provide: ng2_translate_1.TranslateLoader,
                            useFactory: (http) => new ng2_translate_1.TranslateStaticLoader(http, './assets/i18n', '.json'),
                            deps: [http_1.Http]
                        }),
                        ng4_loading_spinner_1.Ng4LoadingSpinnerModule.forRoot()
                    ],
                    declarations: [
                        app_component_1.AppComponent,
                        toolbar_component_1.ToolbarComponent
                    ],
                    providers: [
                        app_service_1.AppService
                    ],
                    bootstrap: [app_component_1.AppComponent]
                })
            ], AppModule);
            exports_1("AppModule", AppModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3N0YXJ0cGFnZS9hcHAubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lBNkNhLFNBQVMsR0FBdEI7YUFBMEIsQ0FBQTtZQUFiLFNBQVM7Z0JBNUJyQixlQUFRLENBQUM7b0JBQ1IsT0FBTyxFQUFPO3dCQUNWLHNCQUFVO3dCQUNWLDBCQUFZO3dCQUNaLGdDQUFhO3dCQUNiLG1CQUFXO3dCQUNYLHFCQUFZO3dCQUNaLGlCQUFVO3dCQUNWLHFCQUFZO3dCQUNaLDRCQUFZO3dCQUNaLDhCQUFhO3dCQUNiLDBCQUFXO3dCQUNYLCtCQUFlLENBQUMsT0FBTyxDQUFDOzRCQUNwQixPQUFPLEVBQUUsK0JBQWU7NEJBQ3hCLFVBQVUsRUFBRSxDQUFDLElBQVUsRUFBRSxFQUFFLENBQUMsSUFBSSxxQ0FBcUIsQ0FBQyxJQUFJLEVBQUUsZUFBZSxFQUFFLE9BQU8sQ0FBQzs0QkFDckYsSUFBSSxFQUFFLENBQUMsV0FBSSxDQUFDO3lCQUNmLENBQUM7d0JBQ0YsNkNBQXVCLENBQUMsT0FBTyxFQUFFO3FCQUNwQztvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsNEJBQVk7d0JBQ1osb0NBQWdCO3FCQUNuQjtvQkFDRCxTQUFTLEVBQUU7d0JBQ1Asd0JBQVU7cUJBQ2I7b0JBQ0QsU0FBUyxFQUFLLENBQUUsNEJBQVksQ0FBRTtpQkFDL0IsQ0FBQztlQUNXLFNBQVMsQ0FBSTs7UUFBQSxDQUFDIiwiZmlsZSI6InN0YXJ0cGFnZS9hcHAubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtCcm93c2VyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SHR0cCwgSHR0cE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge0FwcENvbXBvbmVudH0gZnJvbSBcIi4vYXBwLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge1Rvb2xiYXJNb2R1bGV9IGZyb20gXCIuLi90b29sYmFyL3Rvb2xiYXIubW9kdWxlXCI7XHJcbmltcG9ydCB7VG9vbGJhckNvbXBvbmVudH0gZnJvbSBcIi4uL3Rvb2xiYXIvdG9vbGJhci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtGb3Jtc01vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7Um91dGVyTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7QVBQX1JPVVRFU30gZnJvbSBcIi4vYXBwLnJvdXRlXCI7XHJcbmltcG9ydCB7TE9HSU5fUk9VVEVTfSBmcm9tIFwiLi4vbG9naW4vbG9naW4ucm91dGVcIjtcclxuaW1wb3J0IHtBcHBTZXJ2aWNlfSBmcm9tIFwiLi9hcHAuc2VydmljZVwiO1xyXG5pbXBvcnQge0xvZ2luTW9kdWxlfSBmcm9tIFwiLi4vbG9naW4vbG9naW4ubW9kdWxlXCI7XHJcbmltcG9ydCB7U2hhcmVkTW9kdWxlfSBmcm9tIFwiLi4vc2hhcmVkTW9kdWxlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHtUcmFuc2xhdGVMb2FkZXIsIFRyYW5zbGF0ZU1vZHVsZSwgVHJhbnNsYXRlU3RhdGljTG9hZGVyfSBmcm9tIFwibmcyLXRyYW5zbGF0ZVwiO1xyXG5pbXBvcnQge05nNExvYWRpbmdTcGlubmVyTW9kdWxlfSBmcm9tIFwibmc0LWxvYWRpbmctc3Bpbm5lclwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiAgICAgIFtcclxuICAgICAgQVBQX1JPVVRFUyxcclxuICAgICAgTE9HSU5fUk9VVEVTLFxyXG4gICAgICBCcm93c2VyTW9kdWxlLFxyXG4gICAgICBGb3Jtc01vZHVsZSxcclxuICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICBIdHRwTW9kdWxlLFxyXG4gICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgIFNoYXJlZE1vZHVsZSxcclxuICAgICAgVG9vbGJhck1vZHVsZSxcclxuICAgICAgTG9naW5Nb2R1bGUsXHJcbiAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KHtcclxuICAgICAgICAgIHByb3ZpZGU6IFRyYW5zbGF0ZUxvYWRlcixcclxuICAgICAgICAgIHVzZUZhY3Rvcnk6IChodHRwOiBIdHRwKSA9PiBuZXcgVHJhbnNsYXRlU3RhdGljTG9hZGVyKGh0dHAsICcuL2Fzc2V0cy9pMThuJywgJy5qc29uJyksXHJcbiAgICAgICAgICBkZXBzOiBbSHR0cF1cclxuICAgICAgfSksXHJcbiAgICAgIE5nNExvYWRpbmdTcGlubmVyTW9kdWxlLmZvclJvb3QoKVxyXG4gIF0sXHJcbiAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgIEFwcENvbXBvbmVudCxcclxuICAgICAgVG9vbGJhckNvbXBvbmVudFxyXG4gIF0sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICAgIEFwcFNlcnZpY2VcclxuICBdLFxyXG4gIGJvb3RzdHJhcDogICAgWyBBcHBDb21wb25lbnQgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfSJdfQ==
