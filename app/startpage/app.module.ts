import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {Http, HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import {AppComponent} from "./app.component";
import {ToolbarModule} from "../toolbar/toolbar.module";
import {ToolbarComponent} from "../toolbar/toolbar.component";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {APP_ROUTES} from "./app.route";
import {LOGIN_ROUTES} from "../login/login.route";
import {AppService} from "./app.service";
import {LoginModule} from "../login/login.module";
import {SharedModule} from "../sharedModule/shared.module";
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from "ng2-translate";
import {Ng4LoadingSpinnerModule} from "ng4-loading-spinner";

@NgModule({
  imports:      [
      APP_ROUTES,
      LOGIN_ROUTES,
      BrowserModule,
      FormsModule,
      CommonModule,
      HttpModule,
      RouterModule,
      SharedModule,
      ToolbarModule,
      LoginModule,
      TranslateModule.forRoot({
          provide: TranslateLoader,
          useFactory: (http: Http) => new TranslateStaticLoader(http, './assets/i18n', '.json'),
          deps: [Http]
      }),
      Ng4LoadingSpinnerModule.forRoot()
  ],
  declarations: [
      AppComponent,
      ToolbarComponent
  ],
  providers: [
      AppService
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }