System.register(["@angular/router"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, mainRoutes, APP_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            }
        ],
        execute: function () {
            mainRoutes = [
                {
                    path: '',
                    redirectTo: 'start-page',
                    pathMatch: 'full'
                },
                {
                    path: 'start-page',
                    loadChildren: 'app/login/login.module#LoginModule',
                    pathMatch: 'full'
                },
                {
                    path: 'main-page',
                    loadChildren: 'app/main/main.module#MainModule',
                    pathMatch: 'full'
                },
                {
                    path: 'register',
                    loadChildren: 'app/register/register.module#RegisterModule',
                    pathMatch: 'full'
                }
            ];
            exports_1("APP_ROUTES", APP_ROUTES = router_1.RouterModule.forRoot(mainRoutes, { useHash: false }));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3N0YXJ0cGFnZS9hcHAucm91dGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7WUFHTSxVQUFVLEdBQVU7Z0JBQ3RCO29CQUNJLElBQUksRUFBRSxFQUFFO29CQUNSLFVBQVUsRUFBRSxZQUFZO29CQUN4QixTQUFTLEVBQUUsTUFBTTtpQkFDcEI7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLFlBQVk7b0JBQ2xCLFlBQVksRUFBRSxvQ0FBb0M7b0JBQ2xELFNBQVMsRUFBRSxNQUFNO2lCQUNwQjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsV0FBVztvQkFDakIsWUFBWSxFQUFFLGlDQUFpQztvQkFDL0MsU0FBUyxFQUFFLE1BQU07aUJBQ3BCO2dCQUNEO29CQUNJLElBQUksRUFBRSxVQUFVO29CQUNoQixZQUFZLEVBQUUsNkNBQTZDO29CQUMzRCxTQUFTLEVBQUUsTUFBTTtpQkFDcEI7YUFDSixDQUFDO1lBRUYsd0JBQWEsVUFBVSxHQUF1QixxQkFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFDLENBQUMsRUFBQztRQUFBLENBQUMiLCJmaWxlIjoic3RhcnRwYWdlL2FwcC5yb3V0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Um91dGVzLCBSb3V0ZXJNb2R1bGV9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtNb2R1bGVXaXRoUHJvdmlkZXJzfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuY29uc3QgbWFpblJvdXRlczpSb3V0ZXMgPSBbXHJcbiAgICB7XHJcbiAgICAgICAgcGF0aDogJycsXHJcbiAgICAgICAgcmVkaXJlY3RUbzogJ3N0YXJ0LXBhZ2UnLFxyXG4gICAgICAgIHBhdGhNYXRjaDogJ2Z1bGwnXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICAgIHBhdGg6ICdzdGFydC1wYWdlJyxcclxuICAgICAgICBsb2FkQ2hpbGRyZW46ICdhcHAvbG9naW4vbG9naW4ubW9kdWxlI0xvZ2luTW9kdWxlJyxcclxuICAgICAgICBwYXRoTWF0Y2g6ICdmdWxsJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBwYXRoOiAnbWFpbi1wYWdlJyxcclxuICAgICAgICBsb2FkQ2hpbGRyZW46ICdhcHAvbWFpbi9tYWluLm1vZHVsZSNNYWluTW9kdWxlJyxcclxuICAgICAgICBwYXRoTWF0Y2g6ICdmdWxsJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgICBwYXRoOiAncmVnaXN0ZXInLFxyXG4gICAgICAgIGxvYWRDaGlsZHJlbjogJ2FwcC9yZWdpc3Rlci9yZWdpc3Rlci5tb2R1bGUjUmVnaXN0ZXJNb2R1bGUnLFxyXG4gICAgICAgIHBhdGhNYXRjaDogJ2Z1bGwnXHJcbiAgICB9XHJcbl07XHJcblxyXG5leHBvcnQgY29uc3QgQVBQX1JPVVRFUzpNb2R1bGVXaXRoUHJvdmlkZXJzID0gUm91dGVyTW9kdWxlLmZvclJvb3QobWFpblJvdXRlcywge3VzZUhhc2g6IGZhbHNlfSk7Il19
