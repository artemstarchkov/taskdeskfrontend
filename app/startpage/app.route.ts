import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";

const mainRoutes:Routes = [
    {
        path: '',
        redirectTo: 'start-page',
        pathMatch: 'full'
    },
    {
        path: 'start-page',
        loadChildren: 'app/login/login.module#LoginModule',
        pathMatch: 'full'
    },
    {
        path: 'main-page',
        loadChildren: 'app/main/main.module#MainModule',
        pathMatch: 'full'
    },
    {
        path: 'register',
        loadChildren: 'app/register/register.module#RegisterModule',
        pathMatch: 'full'
    }
];

export const APP_ROUTES:ModuleWithProviders = RouterModule.forRoot(mainRoutes, {useHash: false});