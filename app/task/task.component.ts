import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {TaskService} from "./task.service";
import {TaskModel} from "./task.model";
import {TranslateService} from "ng2-translate";
import {DatePipe} from "@angular/common";
import {LeftSideBarMenuService} from "../leftSidebarMenu/leftSideBarMenu.service";
import {ConfigService} from "../sharedService/config.service";

@Component({
    selector: 'task',
    templateUrl: 'app/task/task.component.html',
    styleUrls: ['app/task/task.component.scss']
})

export class TaskComponent implements OnInit, OnDestroy {
    public _subAddNewTask: Subscription;
    public _subGetTasks: Subscription;
    public _subRemoveDoneTask: Subscription;
    public _subDeleteTask: Subscription;

    // List of task's priorities
    public listOfPriorities = ['simple', 'middle', 'senior'];

    // List of task's statuses
    public listOfStatuses = ['new', 'in progress', 'pause', 'done'];

    // Date time format by current selected locale
    public dateFormatByLocale: any = {
        en: 'MM-dd-y H:mm:ss',
        es: 'MM-dd-y H:mm:ss',
        ru: 'dd-MM-y H:mm:ss',
        ch: 'dd-MM-y H:mm:ss'
    };

    public datePipe: any;

    constructor(public taskService: TaskService,
                public taskModel: TaskModel,
                public translate: TranslateService,
                public leftSideBarMenuService: LeftSideBarMenuService,
                public configService: ConfigService) {}

    ngOnInit() {
        this.datePipe = new DatePipe(this.configService.currentLang);
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subAddNewTask) {
            this._subAddNewTask.unsubscribe();
        }
        if (this._subGetTasks) {
            this._subGetTasks.unsubscribe();
        }
        if (this._subRemoveDoneTask) {
            this._subRemoveDoneTask.unsubscribe();
        }
        if (this._subDeleteTask) {
            this._subDeleteTask.unsubscribe();
        }
    }

    /**
     * Close task window (change window state)
     */
    onCloseTask() {
        this.taskService.taskWindowsState[this.taskModel.to] = false;
    }

    /**
     * Add task to another user (receiver), save task in database and emit task object to receiver via socket connection
     */
    onAddTask() {
        this.taskModel.createdAt = this.datePipe.transform(new Date(), 'yyyy-M-d H:m:s');
        this.taskModel.updatedAt = this.datePipe.transform(new Date(), 'yyyy-M-d H:m:s');

        this._subAddNewTask = this.taskService.addNewTask(this.taskModel).subscribe((data: any) => {
            if (data.status.error == false) {
                this.taskService.emitAddNewTask(this.taskModel);

                // If task successfully added, then close task windows and reset task model
                this.taskService.taskWindowsState[this.taskModel.to] = false;
            } else {
                console.info(data.status.error);
                console.info(data.status.message);
            }
        })
    }

    /**
     * Close task information window
     */
    onCloseTaskInfoShow() {
        this.taskService.currentTaskId = '';
        this.taskModel.resetModel();
    }

    /**
     * Change task's progress status
     */
    onChangeTaskStatus() {
        // Prepared update date to set task
        let updateAt = this.datePipe.transform(new Date(), 'yyyy-M-d H:m:s');

        this.taskService.changeTaskProgressStatus(this.taskModel.status, this.taskService.tasksList[this.taskService.currentTaskId].id, updateAt).subscribe((data: any) => {
            // Check if task that changed status was exist (it's means that status was changed successful)
            if (data.isExistTask) {
                // Check if changed task's status not "done", then update task from taskList with task that status was changed
                if (data.task.status != 'done') {
                    this.taskService.tasksList[this.taskService.currentTaskId] = data.task;
                } else {
                    // If task after changed status is "done", then close task window (change task window state)
                    this.taskService.taskWindowsState[data.task.from.id] = false;

                    // Delete task from task list ("this.taskService.tasksList" is an object that contains properties that are objects. P.S. It's not array of objects)
                    delete this.taskService.tasksList[this.taskService.currentTaskId];
                    this.taskService.currentTaskId = '';
                    this.taskModel.resetModel();

                    // Check the number of tasks for the owner after reducing them by 1
                    if ((this.taskService.tasksCounter[data.task.from.id] - 1) >= 0) {
                        // Reduce the total number of tasks of the owner by 1
                        this.taskService.tasksCounter[data.task.from.id] = this.taskService.tasksCounter[data.task.from.id] - 1;
                    }

                    // Update task list for leftSideBarMenuComponent
                    this._subGetTasks = this.leftSideBarMenuService.getTasks(this.configService.user.id).subscribe((data: any) => {
                        this.leftSideBarMenuService.taskKeys = Object.keys(data.groupedByFromUserTasks);
                        this.leftSideBarMenuService.tasks = data.groupedByFromUserTasks;
                        this.taskService.tasksList = data.tasksList;
                    })
                }
            }
        });
    }

    /**
     * Close window tasks list with status "done"
     */
    onCloseDoneTasksWindow() {
        this.taskService.isShowDoneTasksWindow = false;
    }

    /**
     * Set to task's status value "remove"
     */
    onRemoveDoneTask(doneTaskId: number, index: number) {
        this._subRemoveDoneTask = this.taskService.removeDoneTask(doneTaskId).subscribe((data: any) => {
           this.taskService.doneTasks.splice(index, 1);
        });
    }

    /**
     * Delete task from database
     */
    onDeleteTask(appointedByMeTaskId: number, index: number) {
        this._subDeleteTask = this.taskService.deleteTask(appointedByMeTaskId).subscribe((data: any) => {
            console.info(data);
            this.taskService.appointedByMeTasks.splice(index, 1);
        });
    }

    /**
     * Close window tasks list appointed by med
     */
    onCloseAppointedByMeTasksWindow() {
        this.taskService.isShowAppointedByMeTasksWindow = false;
    }

    /**
     * Return class name by task's status
     */
    onGetClassColorByTaskStatus(taskStatus: string) {
        let statusClasses: any = {
            'new': 'list-group-item-info',
            'in progress': 'list-group-item-warning',
            'pause': 'list-group-item-text',
            'done': 'list-group-item-success',
            'remove': 'list-group-item-danger'
        };

        return statusClasses[taskStatus];
    }
}