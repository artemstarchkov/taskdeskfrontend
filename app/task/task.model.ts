export class TaskModel {
    title: string;
    description: string;
    priority: string;
    status: string;
    from: number;
    to: number;
    createdAt: any;
    updatedAt: any;

    resetModel() {
        this.title = '';
        this.description = '';
        this.priority = '';
        this.status = '';
        this.from = null;
        this.to = null;
        this.createdAt = null;
        this.updatedAt = null;
    }
}