import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import { TASK_ROUTES } from "./task.route";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser';
import {TaskService} from "./task.service";
import {TaskComponent} from "./task.component";
import {TaskModel} from "./task.model";

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        FormsModule,
        TASK_ROUTES,
        RouterModule,
        BrowserModule
    ],
    providers: [
        TaskService,
        TaskModel
    ],
    declarations: [
        TaskComponent
    ]
})
export class TaskModule { }