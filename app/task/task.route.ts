import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {TaskComponent} from "./task.component";

const taskRoutes:Routes = [
    {
        path: '',
        component: TaskComponent
    }
];

export const TASK_ROUTES:ModuleWithProviders = RouterModule.forChild(taskRoutes);