import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {Subject} from "rxjs/Subject";
import {TaskModel} from "./task.model";
import {ConfigService} from "../sharedService/config.service";
import {SocketService} from "../sharedService/socket.service";

@Injectable()
export class TaskService {
    public taskWindowsState: any = [];
    public tasksCounter: any = [];
    public tasksList: any = [];
    public currentTaskId: any = '';
    public doneTasks: any = [];
    public appointedByMeTasks: any = [];
    public isShowDoneTasksWindow: boolean = false;
    public isShowAppointedByMeTasksWindow: boolean = false;

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService,
                public socketService: SocketService) {}

    /**
     * Add new task from one user to another user
     * @param {TaskModel} task
     */
    addNewTask(task: TaskModel) {
       return this.http.post(this.configService.domen + 'task/new', task)
           .map(res => res.json());
    }

    /**
     * Change task's progress status
     * @param {string} taskStatus
     * @param {string} taskId
     * @param updateAt
     * @returns {Observable<any>}
     */
    changeTaskProgressStatus(taskStatus: string, taskId: string, updateAt: any) {
        return this.http.post(this.configService.domen + 'task/change_task_progress_status',
            {
            taskStatus: taskStatus,
            taskId: taskId,
            updateAt: updateAt
            }).map(res => res.json());
    }

    /**
     * Set to task's status value "remove"
     * @param {number} doneTaskId
     * @returns {Observable<any>}
     */
    removeDoneTask(doneTaskId: number) {
        return this.http.post(this.configService.domen + 'task/remove_done_task', {doneTaskId: doneTaskId})
            .map(res => res.json());
    }

    /**
     * Delete task from database
     * @param {number} appointedByMeTaskId
     * @returns {Observable<any>}
     */
    deleteTask(appointedByMeTaskId: number) {
        return this.http.post(this.configService.domen + 'task/delete_task', {appointedByMeTaskId: appointedByMeTaskId})
            .map(res => res.json());
    }

    /**
     * Emit event on added new task to user
     * @param {TaskModel} task
     */
    public emitAddNewTask(task: TaskModel) {
        this.socketService.socket.emit("add_new_task_to_user", task);
    }

    /**
     * Subscribe on get new task from another user via socket connection
     * @returns {Observable<any>}
     */
    public socketOnGetNewTask() {
        if (this.socketService.socket) {
            return this.socketService
                .fromEvent<any>("get_new_task_from_user")
                .map(data => data);
        }
    }

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.taskWindowsState = [];
        this.tasksCounter = [];
        this.tasksList = [];
        this.currentTaskId = '';
        this.doneTasks = [];
        this.appointedByMeTasks = [];
        this.isShowDoneTasksWindow = false;
        this.isShowAppointedByMeTasksWindow = false;
    }

}