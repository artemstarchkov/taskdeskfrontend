System.register(["@angular/core", "ng2-translate", "../sharedService/config.service", "./toolbar.service", "@angular/router", "../leftSidebarMenu/leftSideBarMenu.service", "../profileSetting/profileSetting.service", "../chat/chat.service", "../sharedService/socket.service", "../task/task.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ng2_translate_1, config_service_1, toolbar_service_1, router_1, leftSideBarMenu_service_1, profileSetting_service_1, chat_service_1, socket_service_1, task_service_1, ToolbarComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (toolbar_service_1_1) {
                toolbar_service_1 = toolbar_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (leftSideBarMenu_service_1_1) {
                leftSideBarMenu_service_1 = leftSideBarMenu_service_1_1;
            },
            function (profileSetting_service_1_1) {
                profileSetting_service_1 = profileSetting_service_1_1;
            },
            function (chat_service_1_1) {
                chat_service_1 = chat_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            },
            function (task_service_1_1) {
                task_service_1 = task_service_1_1;
            }
        ],
        execute: function () {
            ToolbarComponent = class ToolbarComponent {
                constructor(configService, toolbarService, leftSideBarMenuService, router, profileSettingService, _eref, translate, chatService, socketService, taskService) {
                    this.configService = configService;
                    this.toolbarService = toolbarService;
                    this.leftSideBarMenuService = leftSideBarMenuService;
                    this.router = router;
                    this.profileSettingService = profileSettingService;
                    this._eref = _eref;
                    this.translate = translate;
                    this.chatService = chatService;
                    this.socketService = socketService;
                    this.taskService = taskService;
                    this.usernameLength = 14;
                    this.relationshipRequests = [];
                    this.isShowRRequests = true;
                    this.isShowOptionsMenu = false;
                    this.nativeWin = require('electron').remote.getCurrentWindow();
                }
                ngOnInit() {
                    this.supportedLanguages = [
                        { display: 'English', value: 'en' },
                        { display: 'Español', value: 'es' },
                        { display: '华语', value: 'ch' },
                        { display: 'Русский', value: 'ru' },
                    ];
                }
                ngOnDestroy() {
                    this.nativeWin = null;
                    if (this._subscriptionLoginUser) {
                        this._subscriptionLoginUser.unsubscribe();
                    }
                    if (this._subRelationshipRequestsUser) {
                        this._subRelationshipRequestsUser.unsubscribe();
                    }
                    if (this._subAcceptRelationshipRequest) {
                        this._subAcceptRelationshipRequest.unsubscribe();
                    }
                    if (this._subGetContactsList) {
                        this._subGetContactsList.unsubscribe();
                    }
                }
                onSwitchShowHideUserSettings(event) {
                    let toolBarProfileElement = event.target.closest('.tool-bar-profile');
                    if (!!toolBarProfileElement == true) {
                        this.profileSettingService.isShowProfileSettingsComponent = !this.profileSettingService.isShowProfileSettingsComponent;
                        if (!this.profileSettingService.isShowProfileSettingsComponent == false) {
                            this.profileSettingService.isChangedAvatarProfileSettings = false;
                            this.profileSettingService.changedAvatarProfileSettingsPath = '';
                        }
                    }
                }
                onSwitchToHideConfigurationMenu(event) {
                    let configMenuElement = event.target.closest('#configMenu');
                    let configOptionsIconElement = event.target.closest('.options');
                    if (!!configMenuElement != true && !!configOptionsIconElement != true) {
                        this.isShowOptionsMenu = false;
                    }
                }
                isCurrentLang(lang) {
                    return lang === localStorage.getItem('locale');
                }
                selectLang(lang) {
                    if (lang != localStorage.getItem('locale')) {
                        localStorage.setItem('locale', lang);
                        this.configService.currentLang = lang;
                        this.translate.use(lang);
                        this.toolbarService.changedLanguage();
                    }
                }
                onClose() {
                    this.configService.resetData();
                    this.chatService.resetData();
                    this.leftSideBarMenuService.resetData();
                    this.profileSettingService.resetData();
                    this.socketService.resetData();
                    this.taskService.resetData();
                    this.toolbarService.resetData();
                    this.isShowOptionsMenu = false;
                    localStorage.removeItem('user');
                    localStorage.clear();
                    this.nativeWin.close();
                    window.close();
                }
                onCollapse() {
                    this.nativeWin.minimize();
                }
                onIsShowRRequests() {
                    this.isShowRRequests = !this.isShowRRequests;
                }
                onExitUser() {
                    this.configService.resetData();
                    this.chatService.resetData();
                    this.leftSideBarMenuService.resetData();
                    this.profileSettingService.resetData();
                    this.socketService.resetData();
                    this.taskService.resetData();
                    this.toolbarService.resetData();
                    localStorage.removeItem('user');
                    localStorage.clear();
                    this.isShowOptionsMenu = false;
                    this.router.navigate(['/start-page']);
                }
                onAcceptRelationshipRequest(currentUserId, from, relationshipRequest) {
                    this._subAcceptRelationshipRequest = this.toolbarService.acceptRelationshipRequest(currentUserId, from, relationshipRequest).subscribe(data => {
                        console.info(data);
                        this.toolbarService.relationshipRequests = data.preparedRR;
                        this.onGetContactList();
                    });
                }
                onDeclineRelationshipRequest(fromUserId, toUserId, rRequestId) {
                    this._subDeclineRelationshipRequest = this.toolbarService.declineRelationshipRequest(fromUserId, toUserId, rRequestId).subscribe(data => {
                        console.info(data);
                        this.toolbarService.relationshipRequests = data.preparedRR;
                    });
                }
                onGetContactList() {
                    this._subGetContactsList = this.leftSideBarMenuService.getContactsList(this.configService.user.id).subscribe(data => {
                        console.info(data);
                        this.leftSideBarMenuService.contacts = data.contacts;
                    });
                }
                onShowHideOptionsMenu() {
                    this.isShowOptionsMenu = !this.isShowOptionsMenu;
                    console.info(this.isShowOptionsMenu);
                }
                onCutStringByLength(str, length) {
                    return str.length <= length ? str : str.substring(0, length);
                }
            };
            ToolbarComponent = __decorate([
                core_1.Component({
                    selector: 'toolbar',
                    templateUrl: 'app/toolbar/toolbar.component.html',
                    styleUrls: ['app/toolbar/toolbar.component.scss'],
                    host: {
                        '(document:click)': 'onSwitchShowHideUserSettings($event); onSwitchToHideConfigurationMenu($event)',
                    }
                }),
                __metadata("design:paramtypes", [config_service_1.ConfigService,
                    toolbar_service_1.ToolbarService,
                    leftSideBarMenu_service_1.LeftSideBarMenuService,
                    router_1.Router,
                    profileSetting_service_1.ProfileSettingService,
                    core_1.ElementRef,
                    ng2_translate_1.TranslateService,
                    chat_service_1.ChatService,
                    socket_service_1.SocketService,
                    task_service_1.TaskService])
            ], ToolbarComponent);
            exports_1("ToolbarComponent", ToolbarComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Rvb2xiYXIvdG9vbGJhci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFxQmEsZ0JBQWdCLEdBQTdCO2dCQWNJLFlBQW1CLGFBQTRCLEVBQzVCLGNBQThCLEVBQzlCLHNCQUE4QyxFQUM5QyxNQUFjLEVBQ2QscUJBQTRDLEVBQzNDLEtBQWlCLEVBQ2xCLFNBQTJCLEVBQzNCLFdBQXdCLEVBQ3hCLGFBQTRCLEVBQzVCLFdBQXdCO29CQVR4QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFDNUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO29CQUM5QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO29CQUM5QyxXQUFNLEdBQU4sTUFBTSxDQUFRO29CQUNkLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7b0JBQzNDLFVBQUssR0FBTCxLQUFLLENBQVk7b0JBQ2xCLGNBQVMsR0FBVCxTQUFTLENBQWtCO29CQUMzQixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtvQkFDeEIsa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBQzVCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO29CQXJCbkMsbUJBQWMsR0FBVyxFQUFFLENBQUM7b0JBUTdCLHlCQUFvQixHQUFVLEVBQUUsQ0FBQztvQkFDakMsb0JBQWUsR0FBWSxJQUFJLENBQUM7b0JBQ2hDLHNCQUFpQixHQUFZLEtBQUssQ0FBQztvQkFZdEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUM7Z0JBQ25FLENBQUM7Z0JBRUQsUUFBUTtvQkFXSixJQUFJLENBQUMsa0JBQWtCLEdBQUc7d0JBQ3RCLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFO3dCQUNuQyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTt3QkFDbkMsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7d0JBQzlCLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFO3FCQUN0QyxDQUFDO2dCQUVOLENBQUM7Z0JBS0QsV0FBVztvQkFDUCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFFdEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQzt3QkFDOUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUM5QyxDQUFDO29CQUNELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxDQUFDLENBQUM7d0JBQ3BDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDcEQsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO3dCQUNyQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3JELENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUMzQyxDQUFDO2dCQUNMLENBQUM7Z0JBS0QsNEJBQTRCLENBQUMsS0FBSztvQkFDOUIsSUFBSSxxQkFBcUIsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO29CQUV0RSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDbEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLDhCQUE4QixHQUFHLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLDhCQUE4QixDQUFDO3dCQUd2SCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyw4QkFBOEIsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUN0RSxJQUFJLENBQUMscUJBQXFCLENBQUMsOEJBQThCLEdBQUcsS0FBSyxDQUFDOzRCQUNsRSxJQUFJLENBQUMscUJBQXFCLENBQUMsZ0NBQWdDLEdBQUcsRUFBRSxDQUFDO3dCQUNyRSxDQUFDO29CQUNMLENBQUM7Z0JBQ0wsQ0FBQztnQkFNRCwrQkFBK0IsQ0FBQyxLQUFLO29CQUNqQyxJQUFJLGlCQUFpQixHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUM1RCxJQUFJLHdCQUF3QixHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUVoRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyx3QkFBd0IsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNwRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO29CQUNuQyxDQUFDO2dCQUNMLENBQUM7Z0JBT0QsYUFBYSxDQUFDLElBQVk7b0JBRXRCLE1BQU0sQ0FBQyxJQUFJLEtBQUssWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDbkQsQ0FBQztnQkFNRCxVQUFVLENBQUMsSUFBWTtvQkFDbkIsRUFBRSxDQUFDLENBQUMsSUFBSSxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN6QyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO3dCQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztvQkFDMUMsQ0FBQztnQkFFTCxDQUFDO2dCQUtELE9BQU87b0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDN0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUN4QyxJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQzdCLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQ2hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7b0JBRS9CLFlBQVksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ2hDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDdkIsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUNuQixDQUFDO2dCQUtELFVBQVU7b0JBQ04sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDOUIsQ0FBQztnQkFLRCxpQkFBaUI7b0JBQ2IsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7Z0JBQ2pELENBQUM7Z0JBS0QsVUFBVTtvQkFDTixJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUM3QixJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQ3hDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFDN0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFFaEMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDaEMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNyQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO29CQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLENBQUM7Z0JBUUQsMkJBQTJCLENBQUMsYUFBa0IsRUFBRSxJQUFZLEVBQUUsbUJBQTJCO29CQUNyRixJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyx5QkFBeUIsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLG1CQUFtQixDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUMxSSxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7d0JBQzNELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUM1QixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQVFELDRCQUE0QixDQUFDLFVBQWtCLEVBQUUsUUFBZSxFQUFFLFVBQWtCO29CQUNoRixJQUFJLENBQUMsOEJBQThCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxVQUFVLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDcEksT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO29CQUMvRCxDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDO2dCQUtELGdCQUFnQjtvQkFDWixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQ2hILE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ25CLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztvQkFDekQsQ0FBQyxDQUFDLENBQUE7Z0JBQ04sQ0FBQztnQkFLRCxxQkFBcUI7b0JBQ2pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztvQkFDakQsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztnQkFDekMsQ0FBQztnQkFRRCxtQkFBbUIsQ0FBQyxHQUFXLEVBQUUsTUFBYztvQkFDM0MsTUFBTSxDQUFDLEdBQUcsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFBLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNoRSxDQUFDO2FBQ0osQ0FBQTtZQWxPWSxnQkFBZ0I7Z0JBVDVCLGdCQUFTLENBQUM7b0JBQ1AsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFdBQVcsRUFBRSxvQ0FBb0M7b0JBQ2pELFNBQVMsRUFBRSxDQUFDLG9DQUFvQyxDQUFDO29CQUNqRCxJQUFJLEVBQUU7d0JBQ0Ysa0JBQWtCLEVBQUUsK0VBQStFO3FCQUN0RztpQkFDSixDQUFDO2lEQWdCb0MsOEJBQWE7b0JBQ1osZ0NBQWM7b0JBQ04sZ0RBQXNCO29CQUN0QyxlQUFNO29CQUNTLDhDQUFxQjtvQkFDcEMsaUJBQVU7b0JBQ1AsZ0NBQWdCO29CQUNkLDBCQUFXO29CQUNULDhCQUFhO29CQUNmLDBCQUFXO2VBdkJsQyxnQkFBZ0IsQ0FrTzVCOztRQUFBLENBQUMiLCJmaWxlIjoidG9vbGJhci90b29sYmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgRWxlbWVudFJlZn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqcy9TdWJzY3JpcHRpb25cIjtcclxuaW1wb3J0IHtUcmFuc2xhdGVTZXJ2aWNlfSBmcm9tIFwibmcyLXRyYW5zbGF0ZVwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7VG9vbGJhclNlcnZpY2V9IGZyb20gXCIuL3Rvb2xiYXIuc2VydmljZVwiO1xyXG5pbXBvcnQge1JvdXRlcn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge0xlZnRTaWRlQmFyTWVudVNlcnZpY2V9IGZyb20gXCIuLi9sZWZ0U2lkZWJhck1lbnUvbGVmdFNpZGVCYXJNZW51LnNlcnZpY2VcIjtcclxuaW1wb3J0IHtQcm9maWxlU2V0dGluZ1NlcnZpY2V9IGZyb20gXCIuLi9wcm9maWxlU2V0dGluZy9wcm9maWxlU2V0dGluZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7Q2hhdFNlcnZpY2V9IGZyb20gXCIuLi9jaGF0L2NoYXQuc2VydmljZVwiO1xyXG5pbXBvcnQge1NvY2tldFNlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL3NvY2tldC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7VGFza1NlcnZpY2V9IGZyb20gXCIuLi90YXNrL3Rhc2suc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3Rvb2xiYXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhcHAvdG9vbGJhci90b29sYmFyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWydhcHAvdG9vbGJhci90b29sYmFyLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgJyhkb2N1bWVudDpjbGljayknOiAnb25Td2l0Y2hTaG93SGlkZVVzZXJTZXR0aW5ncygkZXZlbnQpOyBvblN3aXRjaFRvSGlkZUNvbmZpZ3VyYXRpb25NZW51KCRldmVudCknLFxyXG4gICAgfVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFRvb2xiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBwcml2YXRlIG5hdGl2ZVdpbjphbnk7XHJcbiAgICBwcml2YXRlIHVzZXJuYW1lTGVuZ3RoOiBudW1iZXIgPSAxNDtcclxuICAgIHByaXZhdGUgX3N1YnNjcmlwdGlvbkxvZ2luVXNlcjogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJBY2NlcHRSZWxhdGlvbnNoaXBSZXF1ZXN0OiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9zdWJEZWNsaW5lUmVsYXRpb25zaGlwUmVxdWVzdDogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc3ViR2V0Q29udGFjdHNMaXN0OiBTdWJzY3JpcHRpb247XHJcbiAgICBwdWJsaWMgdHJhbnNsYXRlZFRleHQ6IHN0cmluZztcclxuICAgIHB1YmxpYyBzdXBwb3J0ZWRMYW5ndWFnZXM6IGFueVtdO1xyXG4gICAgcHVibGljIHJlbGF0aW9uc2hpcFJlcXVlc3RzOiBhbnlbXSA9IFtdO1xyXG4gICAgcHVibGljIGlzU2hvd1JSZXF1ZXN0czogYm9vbGVhbiA9IHRydWU7XHJcbiAgICBwdWJsaWMgaXNTaG93T3B0aW9uc01lbnU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgY29uZmlnU2VydmljZTogQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0b29sYmFyU2VydmljZTogVG9vbGJhclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgbGVmdFNpZGVCYXJNZW51U2VydmljZTogTGVmdFNpZGVCYXJNZW51U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBwcm9maWxlU2V0dGluZ1NlcnZpY2U6IFByb2ZpbGVTZXR0aW5nU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2VyZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgdHJhbnNsYXRlOiBUcmFuc2xhdGVTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGNoYXRTZXJ2aWNlOiBDaGF0U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBzb2NrZXRTZXJ2aWNlOiBTb2NrZXRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHRhc2tTZXJ2aWNlOiBUYXNrU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMubmF0aXZlV2luID0gcmVxdWlyZSgnZWxlY3Ryb24nKS5yZW1vdGUuZ2V0Q3VycmVudFdpbmRvdygpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIC8vIENoZWNrIGlmIGhhdmUgXCJ1c2VyXCIgb2JqZWN0IGluIGxvY2FsU3RvcmFnZSwgdGhlbiBnZXQgaXQgb3V0IG9mIHRoZXJlXHJcbiAgICAgICAgLy8gRm9yIGV4YW1wbGUsIGFuIHVuZXhwZWN0ZWQgY2xvc2luZyBvZiBhIHByb2dyYW1cclxuICAgICAgICAvKmlmIChKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyJykpKSB7IC8vIFRPRE8gY2hlY2sgd2h5IGNhbid0IHJlbW92ZSBrZXkgXCJ1c2VyXCIgZnJvbSBsb2NhbFN0b3JhZ2UgYWZ0ZXIgdXNlciBleGl0IGZyb20gYXBwLiAobWV0aG9kIFwib25FeGl0VXNlclwiKVxyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8oSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcicpKSk7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VyJykpO1xyXG4gICAgICAgICAgICB0aGlzLnRvb2xiYXJTZXJ2aWNlLnVzZXJuYW1lID0gdGhpcy50b29sYmFyU2VydmljZS5jdXJyZW50VXNlci51c2VybmFtZS5sZW5ndGggPj0gdGhpcy51c2VybmFtZUxlbmd0aCA/IHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXIudXNlcm5hbWUgOiB0aGlzLnRvb2xiYXJTZXJ2aWNlLmN1cnJlbnRVc2VyLnVzZXJuYW1lLnN1YnN0cigwLCB0aGlzLnVzZXJuYW1lTGVuZ3RoKTtcclxuICAgICAgICAgICAgdGhpcy50b29sYmFyU2VydmljZS51c2VyRW1haWwgPSB0aGlzLnRvb2xiYXJTZXJ2aWNlLmN1cnJlbnRVc2VyLmVtYWlsO1xyXG4gICAgICAgICAgICB0aGlzLmNvbmZpZ1NlcnZpY2UudXNlciA9IHRoaXMudG9vbGJhclNlcnZpY2UuY3VycmVudFVzZXI7XHJcbiAgICAgICAgfSovXHJcblxyXG4gICAgICAgIHRoaXMuc3VwcG9ydGVkTGFuZ3VhZ2VzID0gW1xyXG4gICAgICAgICAgICB7IGRpc3BsYXk6ICdFbmdsaXNoJywgdmFsdWU6ICdlbicgfSxcclxuICAgICAgICAgICAgeyBkaXNwbGF5OiAnRXNwYcOxb2wnLCB2YWx1ZTogJ2VzJyB9LFxyXG4gICAgICAgICAgICB7IGRpc3BsYXk6ICfljY7or60nLCB2YWx1ZTogJ2NoJyB9LFxyXG4gICAgICAgICAgICB7IGRpc3BsYXk6ICfQoNGD0YHRgdC60LjQuScsIHZhbHVlOiAncnUnIH0sXHJcbiAgICAgICAgXTtcclxuICAgICAgICAvL3RoaXMuc2VsZWN0TGFuZygnZW4nKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEJlZm9yZSBkZXN0cm95IGNvbXBvbmVudCB1bnN1YnNjcmliZWQgZnJvbSBldmVudHNcclxuICAgICAqL1xyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5uYXRpdmVXaW4gPSBudWxsO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fc3Vic2NyaXB0aW9uTG9naW5Vc2VyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YnNjcmlwdGlvbkxvZ2luVXNlci51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fc3ViUmVsYXRpb25zaGlwUmVxdWVzdHNVc2VyKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3N1YlJlbGF0aW9uc2hpcFJlcXVlc3RzVXNlci51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fc3ViQWNjZXB0UmVsYXRpb25zaGlwUmVxdWVzdCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJBY2NlcHRSZWxhdGlvbnNoaXBSZXF1ZXN0LnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJHZXRDb250YWN0c0xpc3QpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViR2V0Q29udGFjdHNMaXN0LnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgb24gY2xpY2sgYnkgcGFydHMgb2YgZG9jdW1lbnQgYXJlYSBhbmQgc2hvdy9oaWRlIHVzZXIgaW5mbyBkcm9wZG93blxyXG4gICAgICovXHJcbiAgICBvblN3aXRjaFNob3dIaWRlVXNlclNldHRpbmdzKGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IHRvb2xCYXJQcm9maWxlRWxlbWVudCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcudG9vbC1iYXItcHJvZmlsZScpO1xyXG5cclxuICAgICAgICBpZiAoISF0b29sQmFyUHJvZmlsZUVsZW1lbnQgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2ZpbGVTZXR0aW5nU2VydmljZS5pc1Nob3dQcm9maWxlU2V0dGluZ3NDb21wb25lbnQgPSAhdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UuaXNTaG93UHJvZmlsZVNldHRpbmdzQ29tcG9uZW50O1xyXG5cclxuICAgICAgICAgICAgLy8gQ2hlY2sgaWYgYnkgY2xpY2tpbmcgb24gXCJ1c2VybmFtZVwiIHRvIGNsb3NpbmcgXCJwcm9maWxlU2V0dGluZ3NDb21wb25lbnRcIiB0aGVuIGNoYW5nZSBmbGFnIGFuZCB0ZW1wb3JhcnkgYXZhdGFyIGltYWdlIHBhdGhcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnByb2ZpbGVTZXR0aW5nU2VydmljZS5pc1Nob3dQcm9maWxlU2V0dGluZ3NDb21wb25lbnQgPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdTZXJ2aWNlLmlzQ2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5ncyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcm9maWxlU2V0dGluZ1NlcnZpY2UuY2hhbmdlZEF2YXRhclByb2ZpbGVTZXR0aW5nc1BhdGggPSAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIG9uIGNsaWNrIGJ5IHBhcnRzIG9mIGRvY3VtZW50IGFyZWEgYW5kIGlmIGl0J3Mgbm90IG9wdGlvbnMgY29udGFpbmVyIGFuZCBub3Qgb3B0aW9ucydzIGljb24sIHRoZW4gaGlkZSB1c2VyIG9wdGlvbnMgbWVudVxyXG4gICAgICogTm90IHdvcmsgd2l0aCBkcmFnZ2FibGUgcGFydHMgb2YgZG9jdW1lbnQgYXJlYSBsaWtlIHRvb2xiYXIgKGZ1dHVyZSB3aWxsIGJlIGZpeCBpdClcclxuICAgICAqL1xyXG4gICAgb25Td2l0Y2hUb0hpZGVDb25maWd1cmF0aW9uTWVudShldmVudCkge1xyXG4gICAgICAgIGxldCBjb25maWdNZW51RWxlbWVudCA9IGV2ZW50LnRhcmdldC5jbG9zZXN0KCcjY29uZmlnTWVudScpO1xyXG4gICAgICAgIGxldCBjb25maWdPcHRpb25zSWNvbkVsZW1lbnQgPSBldmVudC50YXJnZXQuY2xvc2VzdCgnLm9wdGlvbnMnKTtcclxuXHJcbiAgICAgICAgaWYgKCEhY29uZmlnTWVudUVsZW1lbnQgIT0gdHJ1ZSAmJiAhIWNvbmZpZ09wdGlvbnNJY29uRWxlbWVudCAhPSB0cnVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNTaG93T3B0aW9uc01lbnUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiB0aGUgc2VsZWN0ZWQgbGFuZyBpcyBjdXJyZW50IGxhbmdcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBsYW5nXHJcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuICAgICAqL1xyXG4gICAgaXNDdXJyZW50TGFuZyhsYW5nOiBzdHJpbmcpIHtcclxuICAgICAgICAvL3JldHVybiBsYW5nID09PSB0aGlzLnRyYW5zbGF0ZVNlcnZpY2UuX2N1cnJlbnRMYW5nO1xyXG4gICAgICAgIHJldHVybiBsYW5nID09PSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbG9jYWxlJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXQgY3VycmVudCBsYW5nXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbGFuZ1xyXG4gICAgICovXHJcbiAgICBzZWxlY3RMYW5nKGxhbmc6IHN0cmluZykge1xyXG4gICAgICAgIGlmIChsYW5nICE9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsb2NhbGUnKSkge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbG9jYWxlJywgbGFuZyk7XHJcbiAgICAgICAgICAgIHRoaXMuY29uZmlnU2VydmljZS5jdXJyZW50TGFuZyA9IGxhbmc7XHJcbiAgICAgICAgICAgIHRoaXMudHJhbnNsYXRlLnVzZShsYW5nKTtcclxuICAgICAgICAgICAgdGhpcy50b29sYmFyU2VydmljZS5jaGFuZ2VkTGFuZ3VhZ2UoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy90aGlzLnJlZnJlc2hUZXh0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbGVhciBsb2NhbFN0b3JhZ2Uoc2Vzc2lvbiksIGNsb3NlIG1haW4gd2luZG93XHJcbiAgICAgKi9cclxuICAgIG9uQ2xvc2UoKSB7XHJcbiAgICAgICAgdGhpcy5jb25maWdTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMuY2hhdFNlcnZpY2UucmVzZXREYXRhKCk7XHJcbiAgICAgICAgdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMuc29ja2V0U2VydmljZS5yZXNldERhdGEoKTtcclxuICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UucmVzZXREYXRhKCk7XHJcbiAgICAgICAgdGhpcy5pc1Nob3dPcHRpb25zTWVudSA9IGZhbHNlO1xyXG5cclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgndXNlcicpO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgICAgIHRoaXMubmF0aXZlV2luLmNsb3NlKCk7XHJcbiAgICAgICAgd2luZG93LmNsb3NlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb2xsYXBzZSBtYWluIHdpbmRvd1xyXG4gICAgICovXHJcbiAgICBvbkNvbGxhcHNlKCkge1xyXG4gICAgICAgIHRoaXMubmF0aXZlV2luLm1pbmltaXplKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTd2l0Y2ggYmV0d2VlbiBzaG93L2hpZGUgc3RhdGUgb2YgcmVsYXRpb25zaGlwIHJlcXVlc3RzIGRyb3Bkb3duXHJcbiAgICAgKi9cclxuICAgIG9uSXNTaG93UlJlcXVlc3RzKCkge1xyXG4gICAgICAgIHRoaXMuaXNTaG93UlJlcXVlc3RzID0gIXRoaXMuaXNTaG93UlJlcXVlc3RzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRXhpdCB1c2VyIGZyb20gbWFpbiBwYWdlIGFuZCByZWRpcmVjdCB0byBsb2dpbiBwYWdlXHJcbiAgICAgKi9cclxuICAgIG9uRXhpdFVzZXIoKSB7XHJcbiAgICAgICAgdGhpcy5jb25maWdTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMuY2hhdFNlcnZpY2UucmVzZXREYXRhKCk7XHJcbiAgICAgICAgdGhpcy5sZWZ0U2lkZUJhck1lbnVTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMucHJvZmlsZVNldHRpbmdTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMuc29ja2V0U2VydmljZS5yZXNldERhdGEoKTtcclxuICAgICAgICB0aGlzLnRhc2tTZXJ2aWNlLnJlc2V0RGF0YSgpO1xyXG4gICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UucmVzZXREYXRhKCk7XHJcblxyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCd1c2VyJyk7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLmNsZWFyKCk7XHJcbiAgICAgICAgdGhpcy5pc1Nob3dPcHRpb25zTWVudSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL3N0YXJ0LXBhZ2UnXSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBY2NlcHQgcmVxdWVzdCBvbiByZWxhdGlvbnNoaXAgZnJvbSBhbm90aGVyIHVzZXJcclxuICAgICAqIEBwYXJhbSBjdXJyZW50VXNlcklkXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gZnJvbVxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHJlbGF0aW9uc2hpcFJlcXVlc3RcclxuICAgICAqL1xyXG4gICAgb25BY2NlcHRSZWxhdGlvbnNoaXBSZXF1ZXN0KGN1cnJlbnRVc2VySWQ6IGFueSwgZnJvbTogbnVtYmVyLCByZWxhdGlvbnNoaXBSZXF1ZXN0OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9zdWJBY2NlcHRSZWxhdGlvbnNoaXBSZXF1ZXN0ID0gdGhpcy50b29sYmFyU2VydmljZS5hY2NlcHRSZWxhdGlvbnNoaXBSZXF1ZXN0KGN1cnJlbnRVc2VySWQsIGZyb20sIHJlbGF0aW9uc2hpcFJlcXVlc3QpLnN1YnNjcmliZShkYXRhID0+e1xyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8oZGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbGJhclNlcnZpY2UucmVsYXRpb25zaGlwUmVxdWVzdHMgPSBkYXRhLnByZXBhcmVkUlI7XHJcbiAgICAgICAgICAgIHRoaXMub25HZXRDb250YWN0TGlzdCgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGVjbGluZSByZXF1ZXN0IG9uIHJlbGF0aW9uc2hpcCBmcm9tIGFub3RoZXIgdXNlclxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGZyb21Vc2VySWRcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB0b1VzZXJJZFxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHJSZXF1ZXN0SWRcclxuICAgICAqL1xyXG4gICAgb25EZWNsaW5lUmVsYXRpb25zaGlwUmVxdWVzdChmcm9tVXNlcklkOiBudW1iZXIsIHRvVXNlcklkOm51bWJlciwgclJlcXVlc3RJZDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fc3ViRGVjbGluZVJlbGF0aW9uc2hpcFJlcXVlc3QgPSB0aGlzLnRvb2xiYXJTZXJ2aWNlLmRlY2xpbmVSZWxhdGlvbnNoaXBSZXF1ZXN0KGZyb21Vc2VySWQsIHRvVXNlcklkLCByUmVxdWVzdElkKS5zdWJzY3JpYmUoZGF0YSA9PntcclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKGRhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLnRvb2xiYXJTZXJ2aWNlLnJlbGF0aW9uc2hpcFJlcXVlc3RzID0gZGF0YS5wcmVwYXJlZFJSO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdXNlcidzIGNvbnRhY3QgbGlzdFxyXG4gICAgICovXHJcbiAgICBvbkdldENvbnRhY3RMaXN0KCkge1xyXG4gICAgICAgIHRoaXMuX3N1YkdldENvbnRhY3RzTGlzdCA9IHRoaXMubGVmdFNpZGVCYXJNZW51U2VydmljZS5nZXRDb250YWN0c0xpc3QodGhpcy5jb25maWdTZXJ2aWNlLnVzZXIuaWQpLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKGRhdGEpO1xyXG4gICAgICAgICAgICB0aGlzLmxlZnRTaWRlQmFyTWVudVNlcnZpY2UuY29udGFjdHMgPSBkYXRhLmNvbnRhY3RzO1xyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTaG93L0hpZGUgb3B0aW9ucyBtZW51XHJcbiAgICAgKi9cclxuICAgIG9uU2hvd0hpZGVPcHRpb25zTWVudSgpIHtcclxuICAgICAgICB0aGlzLmlzU2hvd09wdGlvbnNNZW51ID0gIXRoaXMuaXNTaG93T3B0aW9uc01lbnU7XHJcbiAgICAgICAgY29uc29sZS5pbmZvKHRoaXMuaXNTaG93T3B0aW9uc01lbnUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3V0IHN0cmluZyBieSBsZW5ndGgsIGlmIHN0cmluZyBtb3JlIHRoZW4gbGVuZ3RoXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc3RyXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gbGVuZ3RoXHJcbiAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxyXG4gICAgICovXHJcbiAgICBvbkN1dFN0cmluZ0J5TGVuZ3RoKHN0cjogc3RyaW5nLCBsZW5ndGg6IG51bWJlcikge1xyXG4gICAgICAgIHJldHVybiBzdHIubGVuZ3RoIDw9IGxlbmd0aD8gc3RyIDogc3RyLnN1YnN0cmluZygwLCBsZW5ndGgpO1xyXG4gICAgfVxyXG59Il19
