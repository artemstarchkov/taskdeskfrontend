import {Component, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {TranslateService} from "ng2-translate";
import {ConfigService} from "../sharedService/config.service";
import {ToolbarService} from "./toolbar.service";
import {Router} from "@angular/router";
import {LeftSideBarMenuService} from "../leftSidebarMenu/leftSideBarMenu.service";
import {ProfileSettingService} from "../profileSetting/profileSetting.service";
import {ChatService} from "../chat/chat.service";
import {SocketService} from "../sharedService/socket.service";
import {TaskService} from "../task/task.service";

@Component({
    selector: 'toolbar',
    templateUrl: 'app/toolbar/toolbar.component.html',
    styleUrls: ['app/toolbar/toolbar.component.scss'],
    host: {
        '(document:click)': 'onSwitchShowHideUserSettings($event); onSwitchToHideConfigurationMenu($event)',
    }
})

export class ToolbarComponent implements OnInit, OnDestroy {
    private nativeWin:any;
    private usernameLength: number = 14;
    private _subscriptionLoginUser: Subscription;
    private _subRelationshipRequestsUser: Subscription;
    private _subAcceptRelationshipRequest: Subscription;
    private _subDeclineRelationshipRequest: Subscription;
    private _subGetContactsList: Subscription;
    public translatedText: string;
    public supportedLanguages: any[];
    public relationshipRequests: any[] = [];
    public isShowRRequests: boolean = true;
    public isShowOptionsMenu: boolean = false;

    constructor(public configService: ConfigService,
                public toolbarService: ToolbarService,
                public leftSideBarMenuService: LeftSideBarMenuService,
                public router: Router,
                public profileSettingService: ProfileSettingService,
                private _eref: ElementRef,
                public translate: TranslateService,
                public chatService: ChatService,
                public socketService: SocketService,
                public taskService: TaskService) {
        this.nativeWin = require('electron').remote.getCurrentWindow();
    }

    ngOnInit() {
        // Check if have "user" object in localStorage, then get it out of there
        // For example, an unexpected closing of a program
        /*if (JSON.parse(localStorage.getItem('user'))) { // TODO check why can't remove key "user" from localStorage after user exit from app. (method "onExitUser")
            console.info(JSON.parse(localStorage.getItem('user')));
            this.toolbarService.currentUser = JSON.parse(localStorage.getItem('user'));
            this.toolbarService.username = this.toolbarService.currentUser.username.length >= this.usernameLength ? this.toolbarService.currentUser.username : this.toolbarService.currentUser.username.substr(0, this.usernameLength);
            this.toolbarService.userEmail = this.toolbarService.currentUser.email;
            this.configService.user = this.toolbarService.currentUser;
        }*/

        this.supportedLanguages = [
            { display: 'English', value: 'en' },
            { display: 'Español', value: 'es' },
            { display: '华语', value: 'ch' },
            { display: 'Русский', value: 'ru' },
        ];
        //this.selectLang('en');
    }

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        this.nativeWin = null;

        if (this._subscriptionLoginUser) {
            this._subscriptionLoginUser.unsubscribe();
        }
        if (this._subRelationshipRequestsUser) {
            this._subRelationshipRequestsUser.unsubscribe();
        }
        if (this._subAcceptRelationshipRequest) {
            this._subAcceptRelationshipRequest.unsubscribe();
        }
        if (this._subGetContactsList) {
            this._subGetContactsList.unsubscribe();
        }
    }

    /**
     * Check on click by parts of document area and show/hide user info dropdown
     */
    onSwitchShowHideUserSettings(event) {
        let toolBarProfileElement = event.target.closest('.tool-bar-profile');

        if (!!toolBarProfileElement == true) {
            this.profileSettingService.isShowProfileSettingsComponent = !this.profileSettingService.isShowProfileSettingsComponent;

            // Check if by clicking on "username" to closing "profileSettingsComponent" then change flag and temporary avatar image path
            if (!this.profileSettingService.isShowProfileSettingsComponent == false) {
                this.profileSettingService.isChangedAvatarProfileSettings = false;
                this.profileSettingService.changedAvatarProfileSettingsPath = '';
            }
        }
    }

    /**
     * Check on click by parts of document area and if it's not options container and not options's icon, then hide user options menu
     * Not work with draggable parts of document area like toolbar (future will be fix it)
     */
    onSwitchToHideConfigurationMenu(event) {
        let configMenuElement = event.target.closest('#configMenu');
        let configOptionsIconElement = event.target.closest('.options');

        if (!!configMenuElement != true && !!configOptionsIconElement != true) {
            this.isShowOptionsMenu = false;
        }
    }

    /**
     * Check if the selected lang is current lang
     * @param {string} lang
     * @returns {boolean}
     */
    isCurrentLang(lang: string) {
        //return lang === this.translateService._currentLang;
        return lang === localStorage.getItem('locale');
    }

    /**
     * Set current lang
     * @param {string} lang
     */
    selectLang(lang: string) {
        if (lang != localStorage.getItem('locale')) {
            localStorage.setItem('locale', lang);
            this.configService.currentLang = lang;
            this.translate.use(lang);
            this.toolbarService.changedLanguage();
        }
        //this.refreshText();
    }

    /**
     * Clear localStorage(session), close main window
     */
    onClose() {
        this.configService.resetData();
        this.chatService.resetData();
        this.leftSideBarMenuService.resetData();
        this.profileSettingService.resetData();
        this.socketService.resetData();
        this.taskService.resetData();
        this.toolbarService.resetData();
        this.isShowOptionsMenu = false;

        localStorage.removeItem('user');
        localStorage.clear();
        this.nativeWin.close();
        window.close();
    }

    /**
     * Collapse main window
     */
    onCollapse() {
        this.nativeWin.minimize();
    }

    /**
     * Switch between show/hide state of relationship requests dropdown
     */
    onIsShowRRequests() {
        this.isShowRRequests = !this.isShowRRequests;
    }

    /**
     * Exit user from main page and redirect to login page
     */
    onExitUser() {
        this.configService.resetData();
        this.chatService.resetData();
        this.leftSideBarMenuService.resetData();
        this.profileSettingService.resetData();
        this.socketService.resetData();
        this.taskService.resetData();
        this.toolbarService.resetData();

        localStorage.removeItem('user');
        localStorage.clear();
        this.isShowOptionsMenu = false;
        this.router.navigate(['/start-page']);
    }

    /**
     * Accept request on relationship from another user
     * @param currentUserId
     * @param {number} from
     * @param {number} relationshipRequest
     */
    onAcceptRelationshipRequest(currentUserId: any, from: number, relationshipRequest: number) {
        this._subAcceptRelationshipRequest = this.toolbarService.acceptRelationshipRequest(currentUserId, from, relationshipRequest).subscribe(data =>{
            console.info(data);
            this.toolbarService.relationshipRequests = data.preparedRR;
            this.onGetContactList();
        });
    }

    /**
     * Decline request on relationship from another user
     * @param {number} fromUserId
     * @param {number} toUserId
     * @param {number} rRequestId
     */
    onDeclineRelationshipRequest(fromUserId: number, toUserId:number, rRequestId: number) {
        this._subDeclineRelationshipRequest = this.toolbarService.declineRelationshipRequest(fromUserId, toUserId, rRequestId).subscribe(data =>{
            console.info(data);
            this.toolbarService.relationshipRequests = data.preparedRR;
        })
    }

    /**
     * Get user's contact list
     */
    onGetContactList() {
        this._subGetContactsList = this.leftSideBarMenuService.getContactsList(this.configService.user.id).subscribe(data => {
            console.info(data);
            this.leftSideBarMenuService.contacts = data.contacts;
        })
    }

    /**
     * Show/Hide options menu
     */
    onShowHideOptionsMenu() {
        this.isShowOptionsMenu = !this.isShowOptionsMenu;
        console.info(this.isShowOptionsMenu);
    }

    /**
     * Cut string by length, if string more then length
     * @param {string} str
     * @param {number} length
     * @returns {string}
     */
    onCutStringByLength(str: string, length: number) {
        return str.length <= length? str : str.substring(0, length);
    }
}