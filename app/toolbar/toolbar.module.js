System.register(["@angular/platform-browser", "@angular/core", "@angular/http", "@angular/common", "./toolbar.route", "./toolbar.service", "../main/main.module", "@angular/forms"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var platform_browser_1, core_1, http_1, common_1, toolbar_route_1, toolbar_service_1, main_module_1, forms_1, ToolbarModule;
    return {
        setters: [
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (toolbar_route_1_1) {
                toolbar_route_1 = toolbar_route_1_1;
            },
            function (toolbar_service_1_1) {
                toolbar_service_1 = toolbar_service_1_1;
            },
            function (main_module_1_1) {
                main_module_1 = main_module_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            }
        ],
        execute: function () {
            ToolbarModule = class ToolbarModule {
            };
            ToolbarModule = __decorate([
                core_1.NgModule({
                    imports: [
                        platform_browser_1.BrowserModule,
                        common_1.CommonModule,
                        forms_1.FormsModule,
                        http_1.HttpModule,
                        toolbar_route_1.TOOLBAR_ROUTES,
                        main_module_1.MainModule
                    ],
                    providers: [
                        toolbar_service_1.ToolbarService,
                    ],
                    declarations: []
                })
            ], ToolbarModule);
            exports_1("ToolbarModule", ToolbarModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Rvb2xiYXIvdG9vbGJhci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUE4QmEsYUFBYSxHQUExQjthQUE4QixDQUFBO1lBQWpCLGFBQWE7Z0JBbEJ6QixlQUFRLENBQUM7b0JBQ04sT0FBTyxFQUFPO3dCQUNWLGdDQUFhO3dCQUNiLHFCQUFZO3dCQUNaLG1CQUFXO3dCQUNYLGlCQUFVO3dCQUNWLDhCQUFjO3dCQUNkLHdCQUFVO3FCQUNiO29CQUNELFNBQVMsRUFBRTt3QkFDUCxnQ0FBYztxQkFHakI7b0JBQ0QsWUFBWSxFQUFFLEVBRWI7aUJBQ0osQ0FBQztlQUNXLGFBQWEsQ0FBSTs7UUFBQSxDQUFDIiwiZmlsZSI6InRvb2xiYXIvdG9vbGJhci5tb2R1bGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0Jyb3dzZXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQge05nTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtIdHRwTW9kdWxlfSBmcm9tICdAYW5ndWxhci9odHRwJztcclxuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFRPT0xCQVJfUk9VVEVTIH0gZnJvbSAnLi90b29sYmFyLnJvdXRlJztcclxuaW1wb3J0IHtUb29sYmFyU2VydmljZX0gZnJvbSBcIi4vdG9vbGJhci5zZXJ2aWNlXCI7XHJcbi8vaW1wb3J0IHtQcm9maWxlU2V0dGluZ0NvbXBvbmVudH0gZnJvbSBcIi4uL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLmNvbXBvbmVudFwiO1xyXG4vL2ltcG9ydCB7UHJvZmlsZVNldHRpbmdTZXJ2aWNlfSBmcm9tIFwiLi4vcHJvZmlsZVNldHRpbmcvcHJvZmlsZVNldHRpbmcuc2VydmljZVwiO1xyXG4vL2ltcG9ydCB7UHJvZmlsZVNldHRpbmdNb2RlbH0gZnJvbSBcIi4uL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLm1vZGVsXCI7XHJcbmltcG9ydCB7TWFpbk1vZHVsZX0gZnJvbSBcIi4uL21haW4vbWFpbi5tb2R1bGVcIjtcclxuaW1wb3J0IHtGb3Jtc01vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogICAgICBbXHJcbiAgICAgICAgQnJvd3Nlck1vZHVsZSxcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgSHR0cE1vZHVsZSxcclxuICAgICAgICBUT09MQkFSX1JPVVRFUyxcclxuICAgICAgICBNYWluTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgVG9vbGJhclNlcnZpY2UsXHJcbiAgICAgICAgLy9Qcm9maWxlU2V0dGluZ1NlcnZpY2UsXHJcbiAgICAgICAgLy9Qcm9maWxlU2V0dGluZ01vZGVsXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgLy9Qcm9maWxlU2V0dGluZ0NvbXBvbmVudFxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9vbGJhck1vZHVsZSB7IH0iXX0=
