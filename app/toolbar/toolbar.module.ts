import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import { TOOLBAR_ROUTES } from './toolbar.route';
import {ToolbarService} from "./toolbar.service";
//import {ProfileSettingComponent} from "../profileSetting/profileSetting.component";
//import {ProfileSettingService} from "../profileSetting/profileSetting.service";
//import {ProfileSettingModel} from "../profileSetting/profileSetting.model";
import {MainModule} from "../main/main.module";
import {FormsModule} from "@angular/forms";

@NgModule({
    imports:      [
        BrowserModule,
        CommonModule,
        FormsModule,
        HttpModule,
        TOOLBAR_ROUTES,
        MainModule
    ],
    providers: [
        ToolbarService,
        //ProfileSettingService,
        //ProfileSettingModel
    ],
    declarations: [
        //ProfileSettingComponent
    ]
})
export class ToolbarModule { }