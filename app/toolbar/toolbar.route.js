System.register(["@angular/router", "../profileSetting/profileSetting.component", "../main/main.component"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var router_1, profileSetting_component_1, main_component_1, ToolbarRoutes, TOOLBAR_ROUTES;
    return {
        setters: [
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (profileSetting_component_1_1) {
                profileSetting_component_1 = profileSetting_component_1_1;
            },
            function (main_component_1_1) {
                main_component_1 = main_component_1_1;
            }
        ],
        execute: function () {
            ToolbarRoutes = [
                {
                    path: 'profile/:id',
                    component: profileSetting_component_1.ProfileSettingComponent
                },
                {
                    path: 'home',
                    component: main_component_1.MainComponent
                }
            ];
            exports_1("TOOLBAR_ROUTES", TOOLBAR_ROUTES = router_1.RouterModule.forChild(ToolbarRoutes));
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Rvb2xiYXIvdG9vbGJhci5yb3V0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztZQU1NLGFBQWEsR0FBVTtnQkFDekI7b0JBQ0ksSUFBSSxFQUFFLGFBQWE7b0JBQ25CLFNBQVMsRUFBRSxrREFBdUI7aUJBQ3JDO2dCQUNEO29CQUNJLElBQUksRUFBRSxNQUFNO29CQUNaLFNBQVMsRUFBRSw4QkFBYTtpQkFDM0I7YUFDSixDQUFDO1lBRUYsNEJBQWEsY0FBYyxHQUF1QixxQkFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsRUFBQztRQUFBLENBQUMiLCJmaWxlIjoidG9vbGJhci90b29sYmFyLnJvdXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtSb3V0ZXMsIFJvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge01vZHVsZVdpdGhQcm92aWRlcnN9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7VG9vbGJhckNvbXBvbmVudH0gZnJvbSBcIi4vdG9vbGJhci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtQcm9maWxlU2V0dGluZ0NvbXBvbmVudH0gZnJvbSBcIi4uL3Byb2ZpbGVTZXR0aW5nL3Byb2ZpbGVTZXR0aW5nLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge01haW5Db21wb25lbnR9IGZyb20gXCIuLi9tYWluL21haW4uY29tcG9uZW50XCI7XHJcblxyXG5jb25zdCBUb29sYmFyUm91dGVzOlJvdXRlcyA9IFtcclxuICAgIHtcclxuICAgICAgICBwYXRoOiAncHJvZmlsZS86aWQnLFxyXG4gICAgICAgIGNvbXBvbmVudDogUHJvZmlsZVNldHRpbmdDb21wb25lbnRcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgcGF0aDogJ2hvbWUnLFxyXG4gICAgICAgIGNvbXBvbmVudDogTWFpbkNvbXBvbmVudFxyXG4gICAgfVxyXG5dO1xyXG5cclxuZXhwb3J0IGNvbnN0IFRPT0xCQVJfUk9VVEVTOk1vZHVsZVdpdGhQcm92aWRlcnMgPSBSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQoVG9vbGJhclJvdXRlcyk7Il19
