import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";
import {ToolbarComponent} from "./toolbar.component";
import {ProfileSettingComponent} from "../profileSetting/profileSetting.component";
import {MainComponent} from "../main/main.component";

const ToolbarRoutes:Routes = [
    {
        path: 'profile/:id',
        component: ProfileSettingComponent
    },
    {
        path: 'home',
        component: MainComponent
    }
];

export const TOOLBAR_ROUTES:ModuleWithProviders = RouterModule.forChild(ToolbarRoutes);