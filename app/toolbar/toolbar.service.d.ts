import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/map";
import { ConfigService } from "../sharedService/config.service";
import { SocketService } from "../sharedService/socket.service";
export declare class ToolbarService {
    router: Router;
    http: Http;
    configService: ConfigService;
    socketService: SocketService;
    constructor(router: Router, http: Http, configService: ConfigService, socketService: SocketService);
    private subjectChangedLanguage;
    currentUser: any;
    userEmail: string;
    username: string;
    relationshipRequests: any;
    changedLanguage(): void;
    getChangedLanguage(): Observable<any>;
    addUserToMyList(from: number, to: number): Observable<Response>;
    getUserRelationshipRequestsStatusNew(currentUserId: any): Observable<any>;
    acceptRelationshipRequest(currentUserId: any, from: number, relationshipRequest: number): Observable<any>;
    declineRelationshipRequest(fromUserId: number, toUserId: number, rRequestId: number): Observable<any>;
    onEmitNewRelationshipRequest(): Observable<any>;
    resetData(): void;
}
