System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "rxjs/Subject", "../sharedService/config.service", "../sharedService/socket.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, Subject_1, config_service_1, socket_service_1, ToolbarService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (Subject_1_1) {
                Subject_1 = Subject_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            }
        ],
        execute: function () {
            ToolbarService = class ToolbarService {
                constructor(router, http, configService, socketService) {
                    this.router = router;
                    this.http = http;
                    this.configService = configService;
                    this.socketService = socketService;
                    this.subjectChangedLanguage = new Subject_1.Subject();
                    this.currentUser = null;
                    this.userEmail = '';
                    this.username = '';
                    this.relationshipRequests = [];
                }
                changedLanguage() {
                    this.subjectChangedLanguage.next();
                }
                getChangedLanguage() {
                    return this.subjectChangedLanguage.asObservable();
                }
                addUserToMyList(from, to) {
                    return this.http.post(this.configService.domen + 'relationshiprequest/addusertomylist', { from: from, to: to })
                        .map(res => res.json());
                }
                getUserRelationshipRequestsStatusNew(currentUserId) {
                    return this.http.post(this.configService.domen + 'relationshiprequest/relationshiprequestsstatusnew', { currentUserId: currentUserId })
                        .map(res => res.json());
                }
                acceptRelationshipRequest(currentUserId, from, relationshipRequest) {
                    return this.http.post(this.configService.domen + 'relationshiprequest/acceptrelationshiprequest', { currentUserId: currentUserId, from: from, relationshipRequest: relationshipRequest })
                        .map(res => res.json());
                }
                declineRelationshipRequest(fromUserId, toUserId, rRequestId) {
                    return this.http.post(this.configService.domen + 'relationshiprequest/declinerelationshiprequest', { fromUserId: fromUserId, toUserId: toUserId, rRequestId: rRequestId })
                        .map(res => res.json());
                }
                onEmitNewRelationshipRequest() {
                    if (this.socketService.socket) {
                        return this.socketService
                            .fromEvent("new_relationship_request")
                            .map(data => data);
                    }
                }
                resetData() {
                    this.currentUser = null;
                    this.userEmail = '';
                    this.username = '';
                    this.relationshipRequests = [];
                }
            };
            ToolbarService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router,
                    http_1.Http,
                    config_service_1.ConfigService,
                    socket_service_1.SocketService])
            ], ToolbarService);
            exports_1("ToolbarService", ToolbarService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3Rvb2xiYXIvdG9vbGJhci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFVYSxjQUFjLEdBQTNCO2dCQUVJLFlBQW1CLE1BQWMsRUFDZCxJQUFVLEVBQ1YsYUFBNEIsRUFDNUIsYUFBNEI7b0JBSDVCLFdBQU0sR0FBTixNQUFNLENBQVE7b0JBQ2QsU0FBSSxHQUFKLElBQUksQ0FBTTtvQkFDVixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtvQkFDNUIsa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBRXZDLDJCQUFzQixHQUFHLElBQUksaUJBQU8sRUFBTyxDQUFDO29CQUU3QyxnQkFBVyxHQUFRLElBQUksQ0FBQztvQkFDeEIsY0FBUyxHQUFXLEVBQUUsQ0FBQztvQkFDdkIsYUFBUSxHQUFXLEVBQUUsQ0FBQztvQkFDN0IseUJBQW9CLEdBQVEsRUFBRSxDQUFDO2dCQVBtQixDQUFDO2dCQVluRCxlQUFlO29CQUNYLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDdkMsQ0FBQztnQkFNRCxrQkFBa0I7b0JBQ2QsTUFBTSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDdEQsQ0FBQztnQkFRTSxlQUFlLENBQUMsSUFBWSxFQUFFLEVBQVU7b0JBQzNDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxxQ0FBcUMsRUFBRSxFQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBQyxDQUFDO3lCQUN4RyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFPTSxvQ0FBb0MsQ0FBQyxhQUFrQjtvQkFDMUQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLG1EQUFtRCxFQUFFLEVBQUMsYUFBYSxFQUFFLGFBQWEsRUFBQyxDQUFDO3lCQUNoSSxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFTTSx5QkFBeUIsQ0FBQyxhQUFrQixFQUFFLElBQVksRUFBRSxtQkFBMkI7b0JBQzFGLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRywrQ0FBK0MsRUFBRSxFQUFDLGFBQWEsRUFBRSxhQUFhLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxtQkFBbUIsRUFBRSxtQkFBbUIsRUFBQyxDQUFDO3lCQUNsTCxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDaEMsQ0FBQztnQkFTTSwwQkFBMEIsQ0FBQyxVQUFrQixFQUFFLFFBQWUsRUFBRSxVQUFrQjtvQkFDckYsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLGdEQUFnRCxFQUFFLEVBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUMsQ0FBQzt5QkFDbkssR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ2hDLENBQUM7Z0JBTU0sNEJBQTRCO29CQUMvQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7d0JBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYTs2QkFDcEIsU0FBUyxDQUFNLDBCQUEwQixDQUFDOzZCQUMxQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDM0IsQ0FBQztnQkFDTCxDQUFDO2dCQUtNLFNBQVM7b0JBQ1osSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO29CQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztvQkFDbkIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztnQkFDbkMsQ0FBQzthQUNKLENBQUE7WUEvRlksY0FBYztnQkFEMUIsaUJBQVUsRUFBRTtpREFHa0IsZUFBTTtvQkFDUixXQUFJO29CQUNLLDhCQUFhO29CQUNiLDhCQUFhO2VBTHRDLGNBQWMsQ0ErRjFCOztRQUFBLENBQUMiLCJmaWxlIjoidG9vbGJhci90b29sYmFyLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7Um91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQge0h0dHAsIEhlYWRlcnMsIFJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9odHRwJztcclxuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCI7XHJcbmltcG9ydCB7U3ViamVjdH0gZnJvbSBcInJ4anMvU3ViamVjdFwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7U29ja2V0U2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZFNlcnZpY2Uvc29ja2V0LnNlcnZpY2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFRvb2xiYXJTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgaHR0cDogSHR0cCxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBjb25maWdTZXJ2aWNlOiBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHNvY2tldFNlcnZpY2U6IFNvY2tldFNlcnZpY2UpIHt9XHJcblxyXG4gICAgcHJpdmF0ZSBzdWJqZWN0Q2hhbmdlZExhbmd1YWdlID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG5cclxuICAgIHB1YmxpYyBjdXJyZW50VXNlcjogYW55ID0gbnVsbDtcclxuICAgIHB1YmxpYyB1c2VyRW1haWw6IHN0cmluZyA9ICcnO1xyXG4gICAgcHVibGljIHVzZXJuYW1lOiBzdHJpbmcgPSAnJztcclxuICAgIHJlbGF0aW9uc2hpcFJlcXVlc3RzOiBhbnkgPSBbXTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGwgZmFrZSBlbXB0eSBmdW5jdGlvbiB0byBkZXRlY3QgaWYgbGFndWFnZSB3YXMgY2hhbmdlZFxyXG4gICAgICovXHJcbiAgICBjaGFuZ2VkTGFuZ3VhZ2UoKSB7XHJcbiAgICAgICAgdGhpcy5zdWJqZWN0Q2hhbmdlZExhbmd1YWdlLm5leHQoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN1YnNjcmliZSBvbiB0aGlzLCB3aGVuIGxhbmd1YWdlIHdhcyBjaGFuZ2VkIGFuZCBcImNoYW5nZWRMYW5ndWFnZVwiIGZ1bmN0aW9uIGlzIGZpcmVkXHJcbiAgICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAgICovXHJcbiAgICBnZXRDaGFuZ2VkTGFuZ3VhZ2UoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdWJqZWN0Q2hhbmdlZExhbmd1YWdlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIHVzZXIgdG8gbXkgbGlzdFxyXG4gICAgICogQHBhcmFtIGZyb21cclxuICAgICAqIEBwYXJhbSB0b1xyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8Uj59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhZGRVc2VyVG9NeUxpc3QoZnJvbTogbnVtYmVyLCB0bzogbnVtYmVyKTogT2JzZXJ2YWJsZTxSZXNwb25zZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZ1NlcnZpY2UuZG9tZW4gKyAncmVsYXRpb25zaGlwcmVxdWVzdC9hZGR1c2VydG9teWxpc3QnLCB7ZnJvbTogZnJvbSwgdG86IHRvfSlcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgcmVsYXRpb25zaGlwIHJlcXVlc3RzIGZvciBjdXJyZW50IHVzZXIgd2l0aCBzdGF0dXMgbmV3XHJcbiAgICAgKiBAcGFyYW0gY3VycmVudFVzZXJJZFxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8Uj59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRVc2VyUmVsYXRpb25zaGlwUmVxdWVzdHNTdGF0dXNOZXcoY3VycmVudFVzZXJJZDogYW55KSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KHRoaXMuY29uZmlnU2VydmljZS5kb21lbiArICdyZWxhdGlvbnNoaXByZXF1ZXN0L3JlbGF0aW9uc2hpcHJlcXVlc3Rzc3RhdHVzbmV3Jywge2N1cnJlbnRVc2VySWQ6IGN1cnJlbnRVc2VySWR9KVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiByZXMuanNvbigpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFjY2VwdCByZWxhdGlvbnNoaXAgcmVxdWVzdFxyXG4gICAgICogQHBhcmFtIGN1cnJlbnRVc2VySWRcclxuICAgICAqIEBwYXJhbSBmcm9tXHJcbiAgICAgKiBAcGFyYW0gcmVsYXRpb25zaGlwUmVxdWVzdFxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8Uj59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBhY2NlcHRSZWxhdGlvbnNoaXBSZXF1ZXN0KGN1cnJlbnRVc2VySWQ6IGFueSwgZnJvbTogbnVtYmVyLCByZWxhdGlvbnNoaXBSZXF1ZXN0OiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWdTZXJ2aWNlLmRvbWVuICsgJ3JlbGF0aW9uc2hpcHJlcXVlc3QvYWNjZXB0cmVsYXRpb25zaGlwcmVxdWVzdCcsIHtjdXJyZW50VXNlcklkOiBjdXJyZW50VXNlcklkLCBmcm9tOiBmcm9tLCByZWxhdGlvbnNoaXBSZXF1ZXN0OiByZWxhdGlvbnNoaXBSZXF1ZXN0fSlcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWNsaW5lIHJlbGF0aW9uc2hpcCByZXF1ZXN0XHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gZnJvbVVzZXJJZFxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHRvVXNlcklkXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gclJlcXVlc3RJZFxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGRlY2xpbmVSZWxhdGlvbnNoaXBSZXF1ZXN0KGZyb21Vc2VySWQ6IG51bWJlciwgdG9Vc2VySWQ6bnVtYmVyLCByUmVxdWVzdElkOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLnBvc3QodGhpcy5jb25maWdTZXJ2aWNlLmRvbWVuICsgJ3JlbGF0aW9uc2hpcHJlcXVlc3QvZGVjbGluZXJlbGF0aW9uc2hpcHJlcXVlc3QnLCB7ZnJvbVVzZXJJZDogZnJvbVVzZXJJZCwgdG9Vc2VySWQ6IHRvVXNlcklkLCByUmVxdWVzdElkOiByUmVxdWVzdElkfSlcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMaXN0ZW4gZXZlbnQgb2YgdGhlIHJlbGF0aW9uc2hpcCByZXF1ZXN0IHdpdGggc3RhdHVzIFwibmV3XCIgdGhhdCB3aWxsIGJlIHNlbmQgZnJvbSBhbm90aGVyIHVzZXIgdmlhIHNvY2tldCBjb25uZWN0aW9uXHJcbiAgICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgb25FbWl0TmV3UmVsYXRpb25zaGlwUmVxdWVzdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5zb2NrZXRTZXJ2aWNlLnNvY2tldCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zb2NrZXRTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAuZnJvbUV2ZW50PGFueT4oXCJuZXdfcmVsYXRpb25zaGlwX3JlcXVlc3RcIilcclxuICAgICAgICAgICAgICAgIC5tYXAoZGF0YSA9PiBkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXNldCBhbGwgZGF0YS4gVGhpcyBmdW5jdGlvbiBjYWxsIHdoZW4gdXNlciBleGl0IGZyb20gcHJvZ3JhbSBhbmQgbmVlZCB0byBjbGVhciBkYXRhLlxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcmVzZXREYXRhKCkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFVzZXIgPSBudWxsO1xyXG4gICAgICAgIHRoaXMudXNlckVtYWlsID0gJyc7XHJcbiAgICAgICAgdGhpcy51c2VybmFtZSA9ICcnO1xyXG4gICAgICAgIHRoaXMucmVsYXRpb25zaGlwUmVxdWVzdHMgPSBbXTtcclxuICAgIH1cclxufSJdfQ==
