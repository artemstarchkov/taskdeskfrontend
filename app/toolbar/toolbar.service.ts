import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {Subject} from "rxjs/Subject";
import {ConfigService} from "../sharedService/config.service";
import {SocketService} from "../sharedService/socket.service";

@Injectable()
export class ToolbarService {

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService,
                public socketService: SocketService) {}

    private subjectChangedLanguage = new Subject<any>();

    public currentUser: any = null;
    public userEmail: string = '';
    public username: string = '';
    relationshipRequests: any = [];

    /**
     * Call fake empty function to detect if laguage was changed
     */
    changedLanguage() {
        this.subjectChangedLanguage.next();
    }

    /**
     * Subscribe on this, when language was changed and "changedLanguage" function is fired
     * @returns {Observable<any>}
     */
    getChangedLanguage(): Observable<any> {
        return this.subjectChangedLanguage.asObservable();
    }

    /**
     * Add user to my list
     * @param from
     * @param to
     * @returns {Observable<R>}
     */
    public addUserToMyList(from: number, to: number): Observable<Response> {
        return this.http.post(this.configService.domen + 'relationshiprequest/addusertomylist', {from: from, to: to})
            .map(res => res.json());
    }

    /**
     * Get relationship requests for current user with status new
     * @param currentUserId
     * @returns {Observable<R>}
     */
    public getUserRelationshipRequestsStatusNew(currentUserId: any) {
        return this.http.post(this.configService.domen + 'relationshiprequest/relationshiprequestsstatusnew', {currentUserId: currentUserId})
            .map(res => res.json());
    }

    /**
     * Accept relationship request
     * @param currentUserId
     * @param from
     * @param relationshipRequest
     * @returns {Observable<R>}
     */
    public acceptRelationshipRequest(currentUserId: any, from: number, relationshipRequest: number) {
        return this.http.post(this.configService.domen + 'relationshiprequest/acceptrelationshiprequest', {currentUserId: currentUserId, from: from, relationshipRequest: relationshipRequest})
            .map(res => res.json());
    }

    /**
     * Decline relationship request
     * @param {number} fromUserId
     * @param {number} toUserId
     * @param {number} rRequestId
     * @returns {Observable<any>}
     */
    public declineRelationshipRequest(fromUserId: number, toUserId:number, rRequestId: number) {
        return this.http.post(this.configService.domen + 'relationshiprequest/declinerelationshiprequest', {fromUserId: fromUserId, toUserId: toUserId, rRequestId: rRequestId})
            .map(res => res.json());
    }

    /**
     * Listen event of the relationship request with status "new" that will be send from another user via socket connection
     * @returns {Observable<any>}
     */
    public onEmitNewRelationshipRequest() {
        if (this.socketService.socket) {
            return this.socketService
                .fromEvent<any>("new_relationship_request")
                .map(data => data);
        }
    }

    /**
     * Reset all data. This function call when user exit from program and need to clear data.
     */
    public resetData() {
        this.currentUser = null;
        this.userEmail = '';
        this.username = '';
        this.relationshipRequests = [];
    }
}