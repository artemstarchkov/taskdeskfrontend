import { OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from "ng2-translate";
import { ConfigService } from "../sharedService/config.service";
import { UserSearchService } from "./userSearch.service";
import { ToolbarService } from "../toolbar/toolbar.service";
export declare class UserSearchComponent implements OnInit, OnDestroy {
    configService: ConfigService;
    userSearchService: UserSearchService;
    toolbarService: ToolbarService;
    translate: TranslateService;
    private _subUserSearch;
    private _subAddUserToMyList;
    searchResult: any;
    users: any;
    relationshipRequests: any;
    rRequestStatusNew: any;
    rRequestStatusDeclined: any;
    rRequestStatusAccepted: any;
    isHideUserSearchBody: boolean;
    searchString: string;
    constructor(configService: ConfigService, userSearchService: UserSearchService, toolbarService: ToolbarService, translate: TranslateService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    searchUser(searchStr: any): void;
    addUserToMyList(from: number, to: number): void;
    onShowHideUserSearchComponent(event: any): void;
}
