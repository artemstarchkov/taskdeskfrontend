System.register(["@angular/core", "ng2-translate", "../sharedService/config.service", "./userSearch.service", "../toolbar/toolbar.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ng2_translate_1, config_service_1, userSearch_service_1, toolbar_service_1, UserSearchComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ng2_translate_1_1) {
                ng2_translate_1 = ng2_translate_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (userSearch_service_1_1) {
                userSearch_service_1 = userSearch_service_1_1;
            },
            function (toolbar_service_1_1) {
                toolbar_service_1 = toolbar_service_1_1;
            }
        ],
        execute: function () {
            UserSearchComponent = class UserSearchComponent {
                constructor(configService, userSearchService, toolbarService, translate) {
                    this.configService = configService;
                    this.userSearchService = userSearchService;
                    this.toolbarService = toolbarService;
                    this.translate = translate;
                    this.isHideUserSearchBody = true;
                    this.searchString = '';
                }
                ngOnInit() { }
                ngOnDestroy() {
                    if (this._subUserSearch) {
                        this._subUserSearch.unsubscribe();
                    }
                    if (this._subAddUserToMyList) {
                        this._subAddUserToMyList.unsubscribe();
                    }
                }
                searchUser(searchStr) {
                    this.searchString = searchStr;
                    this._subUserSearch = this.userSearchService.searchUsers(searchStr, this.configService.user.id).subscribe(data => {
                        console.info(data);
                        this.searchResult = data;
                        this.relationshipRequests = this.searchResult.relationshipRequests;
                        this.rRequestStatusNew = this.searchResult.rRequestStatusNew;
                        this.rRequestStatusAccepted = this.searchResult.rRequestStatusAccepted;
                        this.rRequestStatusDeclined = this.searchResult.rRequestStatusDeclined;
                        this.users = this.searchResult.users;
                    });
                }
                addUserToMyList(from, to) {
                    console.info('from=', from);
                    console.info('to=', to);
                    this._subAddUserToMyList = this.toolbarService.addUserToMyList(from, to).subscribe((data) => {
                        console.info(data);
                        if (data.status.isAdded == true && data.status.error == false) {
                            this.userSearchService.addUserToMyListEmit(from, to);
                            this.searchUser(this.searchString);
                        }
                    });
                }
                onShowHideUserSearchComponent(event) {
                    let searchInputElement = event.target.closest('#custom-search-input');
                    let searchBodyElement = event.target.closest('#custom-search-body');
                    this.isHideUserSearchBody = !!searchInputElement == true || !!searchBodyElement == true ? false : true;
                }
            };
            UserSearchComponent = __decorate([
                core_1.Component({
                    selector: 'userSearch',
                    templateUrl: 'app/userSearch/userSearch.component.html',
                    styleUrls: ['app/userSearch/userSearch.component.scss'],
                    host: {
                        '(document:click)': 'onShowHideUserSearchComponent($event)',
                    }
                }),
                __metadata("design:paramtypes", [config_service_1.ConfigService,
                    userSearch_service_1.UserSearchService,
                    toolbar_service_1.ToolbarService,
                    ng2_translate_1.TranslateService])
            ], UserSearchComponent);
            exports_1("UserSearchComponent", UserSearchComponent);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3VzZXJTZWFyY2gvdXNlclNlYXJjaC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFpQmEsbUJBQW1CLEdBQWhDO2dCQVlJLFlBQW1CLGFBQTRCLEVBQzVCLGlCQUFvQyxFQUNwQyxjQUE4QixFQUM5QixTQUEyQjtvQkFIM0Isa0JBQWEsR0FBYixhQUFhLENBQWU7b0JBQzVCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBbUI7b0JBQ3BDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtvQkFDOUIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7b0JBTnZDLHlCQUFvQixHQUFZLElBQUksQ0FBQztvQkFDckMsaUJBQVksR0FBVyxFQUFFLENBQUM7Z0JBS2dCLENBQUM7Z0JBRWxELFFBQVEsS0FBSSxDQUFDO2dCQUtiLFdBQVc7b0JBQ1AsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3RDLENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUMzQyxDQUFDO2dCQUNMLENBQUM7Z0JBU0QsVUFBVSxDQUFDLFNBQVM7b0JBQ2hCLElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDO29CQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDN0csT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7d0JBQ3pCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLG9CQUFvQixDQUFDO3dCQUNuRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDN0QsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUM7d0JBQ3ZFLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDO3dCQUN2RSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO29CQUN6QyxDQUFDLENBQUMsQ0FBQTtnQkFDTixDQUFDO2dCQU9ELGVBQWUsQ0FBQyxJQUFXLEVBQUUsRUFBUztvQkFDbEMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzVCLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUN4QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO3dCQUM3RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNuQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQzs0QkFDNUQsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQzs0QkFDckQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQ3ZDLENBQUM7b0JBQ0wsQ0FBQyxDQUFDLENBQUE7Z0JBQ04sQ0FBQztnQkFNRCw2QkFBNkIsQ0FBQyxLQUFLO29CQUMvQixJQUFJLGtCQUFrQixHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUM7b0JBQ3RFLElBQUksaUJBQWlCLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFFcEUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLENBQUMsQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQzFHLENBQUM7YUFDSixDQUFBO1lBOUVZLG1CQUFtQjtnQkFUL0IsZ0JBQVMsQ0FBQztvQkFDUCxRQUFRLEVBQUUsWUFBWTtvQkFDdEIsV0FBVyxFQUFFLDBDQUEwQztvQkFDdkQsU0FBUyxFQUFFLENBQUMsMENBQTBDLENBQUM7b0JBQ3ZELElBQUksRUFBRTt3QkFDRixrQkFBa0IsRUFBRSx1Q0FBdUM7cUJBQzlEO2lCQUNKLENBQUM7aURBY29DLDhCQUFhO29CQUNULHNDQUFpQjtvQkFDcEIsZ0NBQWM7b0JBQ25CLGdDQUFnQjtlQWZyQyxtQkFBbUIsQ0E4RS9COztRQUFBLENBQUMiLCJmaWxlIjoidXNlclNlYXJjaC91c2VyU2VhcmNoLmNvbXBvbmVudC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgRWxlbWVudFJlZn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqcy9TdWJzY3JpcHRpb25cIjtcclxuaW1wb3J0IHtUcmFuc2xhdGVTZXJ2aWNlfSBmcm9tIFwibmcyLXRyYW5zbGF0ZVwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7VXNlclNlYXJjaFNlcnZpY2V9IGZyb20gXCIuL3VzZXJTZWFyY2guc2VydmljZVwiO1xyXG5pbXBvcnQge1Rvb2xiYXJTZXJ2aWNlfSBmcm9tIFwiLi4vdG9vbGJhci90b29sYmFyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtTb2NrZXRTZXJ2aWNlfSBmcm9tIFwiLi4vc2hhcmVkU2VydmljZS9zb2NrZXQuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3VzZXJTZWFyY2gnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdhcHAvdXNlclNlYXJjaC91c2VyU2VhcmNoLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWydhcHAvdXNlclNlYXJjaC91c2VyU2VhcmNoLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgJyhkb2N1bWVudDpjbGljayknOiAnb25TaG93SGlkZVVzZXJTZWFyY2hDb21wb25lbnQoJGV2ZW50KScsXHJcbiAgICB9XHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgVXNlclNlYXJjaENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIHByaXZhdGUgX3N1YlVzZXJTZWFyY2g6IFN1YnNjcmlwdGlvbjtcclxuICAgIHByaXZhdGUgX3N1YkFkZFVzZXJUb015TGlzdDogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHVibGljIHNlYXJjaFJlc3VsdDogYW55O1xyXG4gICAgcHVibGljIHVzZXJzOiBhbnk7XHJcbiAgICBwdWJsaWMgcmVsYXRpb25zaGlwUmVxdWVzdHM6IGFueTtcclxuICAgIHB1YmxpYyByUmVxdWVzdFN0YXR1c05ldzogYW55O1xyXG4gICAgcHVibGljIHJSZXF1ZXN0U3RhdHVzRGVjbGluZWQ6IGFueTtcclxuICAgIHB1YmxpYyByUmVxdWVzdFN0YXR1c0FjY2VwdGVkOiBhbnk7XHJcbiAgICBwdWJsaWMgaXNIaWRlVXNlclNlYXJjaEJvZHk6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgcHVibGljIHNlYXJjaFN0cmluZzogc3RyaW5nID0gJyc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGNvbmZpZ1NlcnZpY2U6IENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgdXNlclNlYXJjaFNlcnZpY2U6IFVzZXJTZWFyY2hTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHRvb2xiYXJTZXJ2aWNlOiBUb29sYmFyU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UpIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQmVmb3JlIGRlc3Ryb3kgY29tcG9uZW50IHVuc3Vic2NyaWJlZCBmcm9tIGV2ZW50c1xyXG4gICAgICovXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZiAodGhpcy5fc3ViVXNlclNlYXJjaCkge1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJVc2VyU2VhcmNoLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLl9zdWJBZGRVc2VyVG9NeUxpc3QpIHtcclxuICAgICAgICAgICAgdGhpcy5fc3ViQWRkVXNlclRvTXlMaXN0LnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VhcmNoIHVzZXJzIGFuZCB3aXRoIHNlYXJjaGluZyB1c2VycyByZXR1cm4gXCJyZXF1ZXN0IHJlbGF0aW9uc2hpcHNcIiBmb3IgY3VycmVudCB1c2VyICh1c2VyIHRoYXQgbG9nZ2VkIGluIHRoZSBzeXN0ZW0pLlxyXG4gICAgICogVGhlIHJldHVybiBvZiB0aGUgcmVxdWVzdCBmb3IgdGhlIGFkZGl0aW9uIChcInJlbGF0aW9uc2hpcCByZXF1ZXN0c1wiKSBpcyBuZWNlc3NhcnkgdG8gc2VsZWN0IHRoZSB1c2Vyc1xyXG4gICAgICogd2hvIGhhdmUgYWxyZWFkeSBiZWVuIHNlbnQgdGhlIHJlcXVlc3QgZm9yIHRoZSBhZGRpdGlvbjogd2hvIGhhdmUgYWxyZWFkeSBhY2NlcHRlZCB0aGUgcmVxdWVzdCxcclxuICAgICAqIHdoaWNoIHJlamVjdGVkIHRoZSByZXF1ZXN0LCB3aGljaCBoYXZlIG5vdCB5ZXQgYWNjZXB0ZWQgdGhlIHJlcXVlc3QuXHJcbiAgICAgKiBAcGFyYW0gc2VhcmNoU3RyXHJcbiAgICAgKi9cclxuICAgIHNlYXJjaFVzZXIoc2VhcmNoU3RyKSB7XHJcbiAgICAgICAgdGhpcy5zZWFyY2hTdHJpbmcgPSBzZWFyY2hTdHI7XHJcbiAgICAgICAgdGhpcy5fc3ViVXNlclNlYXJjaCA9IHRoaXMudXNlclNlYXJjaFNlcnZpY2Uuc2VhcmNoVXNlcnMoc2VhcmNoU3RyLCB0aGlzLmNvbmZpZ1NlcnZpY2UudXNlci5pZCkuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8oZGF0YSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoUmVzdWx0ID0gZGF0YTtcclxuICAgICAgICAgICAgdGhpcy5yZWxhdGlvbnNoaXBSZXF1ZXN0cyA9IHRoaXMuc2VhcmNoUmVzdWx0LnJlbGF0aW9uc2hpcFJlcXVlc3RzO1xyXG4gICAgICAgICAgICB0aGlzLnJSZXF1ZXN0U3RhdHVzTmV3ID0gdGhpcy5zZWFyY2hSZXN1bHQuclJlcXVlc3RTdGF0dXNOZXc7XHJcbiAgICAgICAgICAgIHRoaXMuclJlcXVlc3RTdGF0dXNBY2NlcHRlZCA9IHRoaXMuc2VhcmNoUmVzdWx0LnJSZXF1ZXN0U3RhdHVzQWNjZXB0ZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuclJlcXVlc3RTdGF0dXNEZWNsaW5lZCA9IHRoaXMuc2VhcmNoUmVzdWx0LnJSZXF1ZXN0U3RhdHVzRGVjbGluZWQ7XHJcbiAgICAgICAgICAgIHRoaXMudXNlcnMgPSB0aGlzLnNlYXJjaFJlc3VsdC51c2VycztcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VuZCByZXF1ZXN0IHRvIGFkZCB1c2VyIHRvIG15IGxpc3QgKHNldCByZXF1ZXN0IHN0YXR1cyB0byBcIm5ld1wiKVxyXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGZyb21cclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB0b1xyXG4gICAgICovXHJcbiAgICBhZGRVc2VyVG9NeUxpc3QoZnJvbTpudW1iZXIsIHRvOm51bWJlcikge1xyXG4gICAgICAgIGNvbnNvbGUuaW5mbygnZnJvbT0nLCBmcm9tKTtcclxuICAgICAgICBjb25zb2xlLmluZm8oJ3RvPScsIHRvKTtcclxuICAgICAgICB0aGlzLl9zdWJBZGRVc2VyVG9NeUxpc3QgPSB0aGlzLnRvb2xiYXJTZXJ2aWNlLmFkZFVzZXJUb015TGlzdChmcm9tLCB0bykuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5pbmZvKGRhdGEpO1xyXG4gICAgICAgICAgICBpZiAoZGF0YS5zdGF0dXMuaXNBZGRlZCA9PSB0cnVlICYmIGRhdGEuc3RhdHVzLmVycm9yID09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJTZWFyY2hTZXJ2aWNlLmFkZFVzZXJUb015TGlzdEVtaXQoZnJvbSwgdG8pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hVc2VyKHRoaXMuc2VhcmNoU3RyaW5nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBvbiBjbGljayBieSBwYXJ0cyBvZiBkb2N1bWVudCBhcmVhIGFuZCBzaG93L2hpZGUgY29tcG9uZW50XHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgb25TaG93SGlkZVVzZXJTZWFyY2hDb21wb25lbnQoZXZlbnQpIHtcclxuICAgICAgICBsZXQgc2VhcmNoSW5wdXRFbGVtZW50ID0gZXZlbnQudGFyZ2V0LmNsb3Nlc3QoJyNjdXN0b20tc2VhcmNoLWlucHV0Jyk7XHJcbiAgICAgICAgbGV0IHNlYXJjaEJvZHlFbGVtZW50ID0gZXZlbnQudGFyZ2V0LmNsb3Nlc3QoJyNjdXN0b20tc2VhcmNoLWJvZHknKTtcclxuXHJcbiAgICAgICAgdGhpcy5pc0hpZGVVc2VyU2VhcmNoQm9keSA9ICEhc2VhcmNoSW5wdXRFbGVtZW50ID09IHRydWUgfHwgISFzZWFyY2hCb2R5RWxlbWVudCA9PSB0cnVlPyBmYWxzZSA6IHRydWU7XHJcbiAgICB9XHJcbn0iXX0=
