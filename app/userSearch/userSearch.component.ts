import {Component, OnInit, OnDestroy, ElementRef} from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {TranslateService} from "ng2-translate";
import {ConfigService} from "../sharedService/config.service";
import {UserSearchService} from "./userSearch.service";
import {ToolbarService} from "../toolbar/toolbar.service";
import {SocketService} from "../sharedService/socket.service";

@Component({
    selector: 'userSearch',
    templateUrl: 'app/userSearch/userSearch.component.html',
    styleUrls: ['app/userSearch/userSearch.component.scss'],
    host: {
        '(document:click)': 'onShowHideUserSearchComponent($event)',
    }
})

export class UserSearchComponent implements OnInit, OnDestroy {
    private _subUserSearch: Subscription;
    private _subAddUserToMyList: Subscription;
    public searchResult: any;
    public users: any;
    public relationshipRequests: any;
    public rRequestStatusNew: any;
    public rRequestStatusDeclined: any;
    public rRequestStatusAccepted: any;
    public isHideUserSearchBody: boolean = true;
    public searchString: string = '';

    constructor(public configService: ConfigService,
                public userSearchService: UserSearchService,
                public toolbarService: ToolbarService,
                public translate: TranslateService) {}

    ngOnInit() {}

    /**
     * Before destroy component unsubscribed from events
     */
    ngOnDestroy() {
        if (this._subUserSearch) {
            this._subUserSearch.unsubscribe();
        }
        if (this._subAddUserToMyList) {
            this._subAddUserToMyList.unsubscribe();
        }
    }

    /**
     * Search users and with searching users return "request relationships" for current user (user that logged in the system).
     * The return of the request for the addition ("relationship requests") is necessary to select the users
     * who have already been sent the request for the addition: who have already accepted the request,
     * which rejected the request, which have not yet accepted the request.
     * @param searchStr
     */
    searchUser(searchStr) {
        this.searchString = searchStr;
        this._subUserSearch = this.userSearchService.searchUsers(searchStr, this.configService.user.id).subscribe(data => {
            console.info(data);
            this.searchResult = data;
            this.relationshipRequests = this.searchResult.relationshipRequests;
            this.rRequestStatusNew = this.searchResult.rRequestStatusNew;
            this.rRequestStatusAccepted = this.searchResult.rRequestStatusAccepted;
            this.rRequestStatusDeclined = this.searchResult.rRequestStatusDeclined;
            this.users = this.searchResult.users;
        })
    }

    /**
     * Send request to add user to my list (set request status to "new")
     * @param {number} from
     * @param {number} to
     */
    addUserToMyList(from:number, to:number) {
        console.info('from=', from);
        console.info('to=', to);
        this._subAddUserToMyList = this.toolbarService.addUserToMyList(from, to).subscribe((data: any) => {
            console.info(data);
            if (data.status.isAdded == true && data.status.error == false) {
                this.userSearchService.addUserToMyListEmit(from, to);
                this.searchUser(this.searchString);
            }
        })
    }

    /**
     * Check on click by parts of document area and show/hide component
     * @param event
     */
    onShowHideUserSearchComponent(event) {
        let searchInputElement = event.target.closest('#custom-search-input');
        let searchBodyElement = event.target.closest('#custom-search-body');

        this.isHideUserSearchBody = !!searchInputElement == true || !!searchBodyElement == true? false : true;
    }
}