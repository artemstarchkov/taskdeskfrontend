System.register(["@angular/core", "@angular/http", "@angular/common", "@angular/router", "@angular/forms", "@angular/platform-browser", "./userSearch.service", "./userSearch.component"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, common_1, router_1, forms_1, platform_browser_1, userSearch_service_1, userSearch_component_1, UserSearchModule;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (forms_1_1) {
                forms_1 = forms_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (userSearch_service_1_1) {
                userSearch_service_1 = userSearch_service_1_1;
            },
            function (userSearch_component_1_1) {
                userSearch_component_1 = userSearch_component_1_1;
            }
        ],
        execute: function () {
            UserSearchModule = class UserSearchModule {
            };
            UserSearchModule = __decorate([
                core_1.NgModule({
                    imports: [
                        common_1.CommonModule,
                        http_1.HttpModule,
                        router_1.RouterModule,
                        forms_1.FormsModule,
                        platform_browser_1.BrowserModule
                    ],
                    providers: [
                        userSearch_service_1.UserSearchService
                    ],
                    declarations: [
                        userSearch_component_1.UserSearchComponent
                    ]
                })
            ], UserSearchModule);
            exports_1("UserSearchModule", UserSearchModule);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3VzZXJTZWFyY2gvdXNlclNlYXJjaC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUF3QmEsZ0JBQWdCLEdBQTdCO2FBQWlDLENBQUE7WUFBcEIsZ0JBQWdCO2dCQWY1QixlQUFRLENBQUM7b0JBQ04sT0FBTyxFQUFPO3dCQUNWLHFCQUFZO3dCQUNaLGlCQUFVO3dCQUNWLHFCQUFZO3dCQUNaLG1CQUFXO3dCQUNYLGdDQUFhO3FCQUNoQjtvQkFDRCxTQUFTLEVBQUU7d0JBQ1Asc0NBQWlCO3FCQUNwQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1YsMENBQW1CO3FCQUN0QjtpQkFDSixDQUFDO2VBQ1csZ0JBQWdCLENBQUk7O1FBQUEsQ0FBQyIsImZpbGUiOiJ1c2VyU2VhcmNoL3VzZXJTZWFyY2gubW9kdWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7SHR0cE1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge1JvdXRlck1vZHVsZX0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQge0Zvcm1zTW9kdWxlfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuaW1wb3J0IHtCcm93c2VyTW9kdWxlfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHtVc2VyU2VhcmNoU2VydmljZX0gZnJvbSBcIi4vdXNlclNlYXJjaC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7VXNlclNlYXJjaENvbXBvbmVudH0gZnJvbSBcIi4vdXNlclNlYXJjaC5jb21wb25lbnRcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiAgICAgIFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgSHR0cE1vZHVsZSxcclxuICAgICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgQnJvd3Nlck1vZHVsZVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIFVzZXJTZWFyY2hTZXJ2aWNlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgVXNlclNlYXJjaENvbXBvbmVudFxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlclNlYXJjaE1vZHVsZSB7IH0iXX0=
