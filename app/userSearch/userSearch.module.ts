import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from '@angular/platform-browser';
import {UserSearchService} from "./userSearch.service";
import {UserSearchComponent} from "./userSearch.component";

@NgModule({
    imports:      [
        CommonModule,
        HttpModule,
        RouterModule,
        FormsModule,
        BrowserModule
    ],
    providers: [
        UserSearchService
    ],
    declarations: [
        UserSearchComponent
    ]
})
export class UserSearchModule { }