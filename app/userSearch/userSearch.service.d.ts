import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/map";
import { ConfigService } from "../sharedService/config.service";
import { SocketService } from "../sharedService/socket.service";
export declare class UserSearchService {
    router: Router;
    http: Http;
    configService: ConfigService;
    socketService: SocketService;
    constructor(router: Router, http: Http, configService: ConfigService, socketService: SocketService);
    searchUsers(searchStr: string, currentUserId: any): Observable<Response>;
    addUserToMyListEmit(from: any, to: any): void;
}
