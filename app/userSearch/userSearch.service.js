System.register(["@angular/core", "@angular/router", "@angular/http", "rxjs/add/operator/map", "../sharedService/config.service", "../sharedService/socket.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, http_1, config_service_1, socket_service_1, UserSearchService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (socket_service_1_1) {
                socket_service_1 = socket_service_1_1;
            }
        ],
        execute: function () {
            UserSearchService = class UserSearchService {
                constructor(router, http, configService, socketService) {
                    this.router = router;
                    this.http = http;
                    this.configService = configService;
                    this.socketService = socketService;
                }
                searchUsers(searchStr, currentUserId) {
                    return this.http.post(this.configService.domen + 'user/searchuser', { searchStr: searchStr, currentUserId: currentUserId })
                        .map(res => res.json());
                }
                addUserToMyListEmit(from, to) {
                    this.socketService.socket.emit("add_user_to_my_list", { from: from, to: to });
                }
            };
            UserSearchService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [router_1.Router,
                    http_1.Http,
                    config_service_1.ConfigService,
                    socket_service_1.SocketService])
            ], UserSearchService);
            exports_1("UserSearchService", UserSearchService);
        }
    };
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3Rhc2tkZXNrZnJvbnRlbmQvYXBwL3VzZXJTZWFyY2gvdXNlclNlYXJjaC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFVYSxpQkFBaUIsR0FBOUI7Z0JBRUksWUFBbUIsTUFBYyxFQUNkLElBQVUsRUFDVixhQUE0QixFQUM1QixhQUE0QjtvQkFINUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtvQkFDZCxTQUFJLEdBQUosSUFBSSxDQUFNO29CQUNWLGtCQUFhLEdBQWIsYUFBYSxDQUFlO29CQUM1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtnQkFBRyxDQUFDO2dCQVE1QyxXQUFXLENBQUMsU0FBaUIsRUFBRSxhQUFrQjtvQkFDcEQsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLGlCQUFpQixFQUFFLEVBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUMsYUFBYSxFQUFDLENBQUM7eUJBQ25ILEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQyxDQUFDO2dCQU9NLG1CQUFtQixDQUFDLElBQUksRUFBRSxFQUFFO29CQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsRUFBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQyxDQUFDO2dCQUNoRixDQUFDO2FBQ0osQ0FBQTtZQTFCWSxpQkFBaUI7Z0JBRDdCLGlCQUFVLEVBQUU7aURBR2tCLGVBQU07b0JBQ1IsV0FBSTtvQkFDSyw4QkFBYTtvQkFDYiw4QkFBYTtlQUx0QyxpQkFBaUIsQ0EwQjdCOztRQUFBLENBQUMiLCJmaWxlIjoidXNlclNlYXJjaC91c2VyU2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7Um91dGVyfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7T2JzZXJ2YWJsZX0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQge0h0dHAsIEhlYWRlcnMsIFJlc3BvbnNlfSBmcm9tICdAYW5ndWxhci9odHRwJztcclxuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCI7XHJcbmltcG9ydCB7U3ViamVjdH0gZnJvbSBcInJ4anMvU3ViamVjdFwiO1xyXG5pbXBvcnQge0NvbmZpZ1NlcnZpY2V9IGZyb20gXCIuLi9zaGFyZWRTZXJ2aWNlL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7U29ja2V0U2VydmljZX0gZnJvbSBcIi4uL3NoYXJlZFNlcnZpY2Uvc29ja2V0LnNlcnZpY2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFVzZXJTZWFyY2hTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgaHR0cDogSHR0cCxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBjb25maWdTZXJ2aWNlOiBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHNvY2tldFNlcnZpY2U6IFNvY2tldFNlcnZpY2UpIHt9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWFyY2ggdXNlcnMgYnkgc2VhcmNoU3RyXHJcbiAgICAgKiBAcGFyYW0gc2VhcmNoU3RyXHJcbiAgICAgKiBAcGFyYW0gY3VycmVudFVzZXJJZFxyXG4gICAgICogQHJldHVybnMge09ic2VydmFibGU8Uj59XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzZWFyY2hVc2VycyhzZWFyY2hTdHI6IHN0cmluZywgY3VycmVudFVzZXJJZDogYW55KTogT2JzZXJ2YWJsZTxSZXNwb25zZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucG9zdCh0aGlzLmNvbmZpZ1NlcnZpY2UuZG9tZW4gKyAndXNlci9zZWFyY2h1c2VyJywge3NlYXJjaFN0cjogc2VhcmNoU3RyLCBjdXJyZW50VXNlcklkOmN1cnJlbnRVc2VySWR9KVxyXG4gICAgICAgICAgICAubWFwKHJlcyA9PiByZXMuanNvbigpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEVtaXQgZXZlbnQgdG8gcmVjZWl2ZXIsIHRoYXQgSSB3YW50IGFkZCB0aGF0J3MgdXNlciAocmVjZWl2ZXIpIHRvIG15IHVzZXJzIGxpc3QsIHZpYSBzb2NrZXQgY29ubmVjdGlvblxyXG4gICAgICogQHBhcmFtIGZyb21cclxuICAgICAqIEBwYXJhbSB0b1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYWRkVXNlclRvTXlMaXN0RW1pdChmcm9tLCB0bykge1xyXG4gICAgICAgIHRoaXMuc29ja2V0U2VydmljZS5zb2NrZXQuZW1pdChcImFkZF91c2VyX3RvX215X2xpc3RcIiwge2Zyb206IGZyb20sIHRvOiB0b30pO1xyXG4gICAgfVxyXG59Il19
