import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Headers, Response} from '@angular/http';
import "rxjs/add/operator/map";
import {Subject} from "rxjs/Subject";
import {ConfigService} from "../sharedService/config.service";
import {SocketService} from "../sharedService/socket.service";

@Injectable()
export class UserSearchService {

    constructor(public router: Router,
                public http: Http,
                public configService: ConfigService,
                public socketService: SocketService) {}

    /**
     * Search users by searchStr
     * @param searchStr
     * @param currentUserId
     * @returns {Observable<R>}
     */
    public searchUsers(searchStr: string, currentUserId: any): Observable<Response> {
        return this.http.post(this.configService.domen + 'user/searchuser', {searchStr: searchStr, currentUserId:currentUserId})
            .map(res => res.json());
    }

    /**
     * Emit event to receiver, that I want add that's user (receiver) to my users list, via socket connection
     * @param from
     * @param to
     */
    public addUserToMyListEmit(from, to) {
        this.socketService.socket.emit("add_user_to_my_list", {from: from, to: to});
    }
}