//--------------------------------------------------------------------------------
//
// Dependencies
//
//--------------------------------------------------------------------------------

var argv = require('yargs').argv,
    concat = require('gulp-concat'),                // Concat js
    del = require('del'),                           // Delete files/folders
    gulp = require('gulp'),                         // Gulp core
    sass = require('gulp-sass'),                    // Sass compiler
    sourcemaps = require('gulp-sourcemaps'),
    typescript = require('gulp-typescript'),
    uglify = require('gulp-uglify'),                // Uglifies the js
    electron = require('gulp-electron'),
    path = require('path'),
    pkg = require('./package.json'),
    autoprefixer = require('gulp-autoprefixer'),
    request = require('request'),
    source = require('vinyl-source-stream'),
    shell = require('gulp-shell'),
    unzip = require('gulp-unzip'),
    watchSequence = require('gulp-watch-sequence'),
    packager = require('electron-packager'),
    cp = require('child_process'),
    tslint = require("gulp-tslint");


//--------------------------------------------------------------------------------
//
// Private Variables
//
//--------------------------------------------------------------------------------

var target = {
    devApp: 'app/',
    prodApp: 'app/',
    testApp: 'test/',
    buildDir: 'build/',
    releaseDir: 'release/',
    outputDir: 'output/',
    bowerDir: 'bower_components'
};

//argv.appBuild can be "dev" or "test" or "live"
if (!argv.appBuild) {
    argv.appBuild = 'dev';
}

pkg.originalName = pkg.name;

//Set a build number for issue tracking
if (!argv.buildNum) {
    argv.buildNum = "0"
}
pkg.version = pkg.version + '.' + argv.buildNum;

var tsProject = typescript.createProject('tsconfig.json');

//--------------------------------------------------------------------------------
//
// Gulp Tasks
//
//--------------------------------------------------------------------------------
/**
 * Delete bower directory
 */
gulp.task('clean:bower', function () {
    del.sync([target.bowerDir]);
});

/**
 * bower task
 */
gulp.task('bower', ['clean:bower'], function () {
    if (!argv.bamboo) {
        cp.execSync('bower cache clean', {stdio: 'inherit'});
        cp.execSync('bower install', {stdio: 'inherit'});
    } else {
        cp.execSync('ant build', {stdio: 'inherit'});
    }
});

gulp.task('tslint', function () {
    gulp.src([target.devApp + '**/*.ts', '!' + target.devApp + '_mobile/**/*.ts'])
        .pipe(tslint({
            formatter: 'verbose',
            configuration: {
                rules: {
                    'variable-name': true,
                    'quotemark': [false, 'single']
                }
            }
        }))
        .pipe(tslint.report())
});

gulp.task('build:ts', function () {
    return gulp.src([target.devApp + '**/*.ts', '!' + target.devApp + '_mobile/**/*.ts'])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write())
        /*.pipe(uglify())*/
        .pipe(gulp.dest(target.prodApp));
});

gulp.task('build_test:ts', function () {
    return gulp.src(target.testApp + '**/*.spec.ts')
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write())
        /*.pipe(uglify())*/
        .pipe(gulp.dest('./'));
});

gulp.task('build:css', function () {
    return gulp.src(target.devApp + '**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer())
        .pipe(gulp.dest(target.devApp + './'));
});

gulp.task('build', ['build:ts', 'build:css']);

gulp.task('watch:ts', function () {
    var queue = watchSequence(1);
    gulp.watch(target.devApp + '**/*.ts', ['build:ts'], queue.getHandler('ts', 'html', 'reload'));
    gulp.watch(target.testApp + '**/*.ts', ['build_test:ts'], queue.getHandler('ts', 'html', 'reload'));
});

gulp.task('watch:scss', function () {
    gulp.watch(target.devApp + '**/*.scss', ['build:css']);
});

gulp.task('watch', ['watch:ts', 'watch:scss']);

// Unit Testing
gulp.task('build.test', ['clean.dev', 'tslint', 'build.assets.dev', 'build.js.test', 'build.index.dev']);

gulp.task('build.test.watch', ['build.test', 'watch.test']);

gulp.task('test', ['build.test', 'karma.start']);
//End Unit Testing

gulp.task('concatProps', function () {
    var locales = ['en_US', 'fr_FR'];
    locales.forEach(function (locale) {
        gulp.src(
            [
                'app/locale/' + locale + '/' + pkg.originalName + '-source.properties'
            ])
            .pipe(concat(pkg.originalName + '.properties'))
            .pipe(gulp.dest('app/locale/' + locale))
            .pipe(gulp.dest(target.buildDir + '/app/locale/' + locale));
    });
});

gulp.task('electron', function () {
    var params = {
        src: './build',
        packageJson: pkg,
        release: './release',
        cache: './cache',
        version: 'v1.4.0',
        packaging: true,
        platforms: ['win32-ia32', 'darwin-x64'],
        asar: false,
        platformResources: {
            darwin: {
                CFBundleDisplayName: pkg.name,
                CFBundleIdentifier: pkg.name,
                CFBundleName: pkg.name,
                CFBundleVersion: pkg.version
            },
            win: {
                "version-string": pkg.version,
                "file-version": pkg.version,
                "product-version": pkg.version
            }
        }
    };

    gulp.src("")
        .pipe(electron(params))
        .pipe(gulp.dest(""))
        .on('finish', function () {
            del([params.release + '/' + params.version + '/' + params.platforms[0], params.release + '/' + params.version + '/' + params.platforms[1]]);
        });
});

gulp.task('package', ['clean:output', 'postbuild', 'copySettings:prod'], function () {
    var packagerOptions = {
        dir: path.resolve(__dirname, "build"),
        out: path.resolve(__dirname, "output"),
        name: pkg.shortname,
        platform: 'win32',
        arch: 'ia32',
        version: '1.4.0',
        icon: path.resolve(__dirname, "app", "icons", argv.appType, ""),
        asar: false,
        overwrite: true,
        "app-version": pkg.version,
        "version-string": {
            CompanyName: pkg.author,
            FileDescription: pkg.description,
            ProductVersion: pkg.version,
            ProductName: pkg.name,
            OriginalFilename: pkg.name,
            InternalName: pkg.name,
            LegalCopyright: "(C) 2017 " + pkg.author
        }
    };
    packager(packagerOptions, function (error) {
        if (error) {
            throw error;
        }
    });
});

gulp.task('package-mac', ['clean:output', 'postbuild', 'copySettings:prod'], function () {
    var packagerOptions = {
        dir: path.resolve(__dirname, "build"),
        out: path.resolve(__dirname, "output"),
        name: pkg.name,
        platform: 'darwin',
        arch: 'x64',
        version: '1.4.0',
        icon: path.resolve(__dirname, "app", "icons", argv.appType, ""),
        asar: false,
        overwrite: true,
        "app-version": pkg.version,
        "app-copyright": "2017 " + pkg.author
    };
    packager(packagerOptions, function (error) {
        if (error) {
            throw error;
        }
    });
});

gulp.task('clean', function () {
    del.sync(target.buildDir);
    del.sync(target.releaseDir);
});

gulp.task('clean:output', function () {
    del.sync(target.outputDir);
});

/**
 * Copy task
 */
gulp.task('copyIcons', [], function () {
    return gulp.src('app/icons/' + argv.appType + '/**')
        .pipe(gulp.dest('build/app/icons'));
});

/**
 * Copy settings.json to local app
 */
gulp.task('copySettings', [], function () {
    gulp.src('app/settings/' + argv.appBuild + '/' + argv.appType + '/settings.json')
        .pipe(gulp.dest(target.devApp));
});

gulp.task('copySettings:prod', [], function () {
    gulp.src('app/settings/' + argv.appBuild + '/' + argv.appType + '/settings.json')
        .pipe(gulp.dest(target.buildDir + 'app/'));
});

gulp.task('sass:prod', function () {
    return gulp.src([target.devApp + '**/*.scss', '!app/bower_components/**'])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer())
        .pipe(gulp.dest(target.buildDir + 'app/'));
});

gulp.task('ts:prod', function () {
    return gulp.src([target.devApp + '**/*.ts'])
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(target.buildDir + 'app/'));
});

gulp.task('copy:packageJson', function () {
    gulp.src('package.json')
        .pipe(gulp.dest(target.buildDir));
});

gulp.task('copyBuildInfo', function () {
    cp.execSync('ant write.buildinfo -Dbuild.number=' + argv.buildNum);
});

gulp.task('copy:prod', ['copy:node-mods', 'copyBuildInfo', 'copySettings:prod'], function () {
    gulp.src(['app/**/*.html', '!app/bower_components/**'])
        .pipe(gulp.dest(target.buildDir + 'app'));
//    gulp.src('app/cw/**')
//        .pipe(gulp.dest(target.buildDir + 'app/cw'));
//    gulp.src('app/fonts/**')
//        .pipe(gulp.dest(target.buildDir + 'app/fonts'));
//    gulp.src('app/bower_components/activity-icon-font/css/**')
//        .pipe(gulp.dest(target.buildDir + 'app/bower_components/activity-icon-font/css'));
//    gulp.src('app/bower_components/sayhun-icon-font/css/**')
//        .pipe(gulp.dest(target.buildDir + 'app/bower_components/sayhun-icon-font/css'));
 //   gulp.src('app/bower_components/tl-b4_-ext-binaries/ffmpeg/**')
 //       .pipe(gulp.dest(target.buildDir + 'app/bin'));
    gulp.src('app/locale/**')
        .pipe(gulp.dest(target.buildDir + 'app/locale'));
    gulp.src('app/img/**')
        .pipe(gulp.dest(target.buildDir + 'app/img'));
//    gulp.src('app/styles/fonts/**')
//        .pipe(gulp.dest(target.buildDir + 'app/styles/fonts'));
    gulp.src('index.html')
        .pipe(gulp.dest(target.buildDir));
    gulp.src('main.js')
        .pipe(gulp.dest(target.buildDir));
    gulp.src('systemjs.config.js')
        .pipe(gulp.dest(target.buildDir));
});

//gulp.task('copy:node-mods', function () {
//    if (!argv.bamboo) {
//        return gulp.src('')
//            .pipe(shell('npm install --production --prefix ' + target.buildDir))
//            .pipe(gulp.dest(''));
//    } else {
//        cp.execSync('python bamboo.py copyMods', {stdio: 'inherit'});
//    }
//});

//gulp.task('postbuild', function () {
//    cp.execSync('python bamboo.py setTitleInHtml ' + pkg.name, {stdio: 'inherit'});
//    cp.execSync('python bamboo.py copyFfmpegToBuild', {stdio: 'inherit'});
//});

gulp.task('build:prod', ['clean', 'bower', 'ts:prod', 'sass:prod', 'copy:packageJson', 'copy:prod']);
