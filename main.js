//handle setupevents as quickly as possible
const setupEvents = require('./installers/setupEvents')
if (setupEvents.handleSquirrelEvent()) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

const electron = require('electron');

/**
 *  Module to create native browser window.
 */
const BrowserWindow = electron.BrowserWindow;

/**
 * Module to control application life.
 */
const app = electron.app;

/**
 *  Keep a global reference of the window object, if you don't, the window will
 *  be closed automatically when the JavaScript object is garbage collected.
 */
var mainWindow = null;

//var http = require('http').Server(app);
/*var http = require('https').createServer(app);

var io = require('socket.io')(http);

io.on('connection', function(socket){
    console.log('a user connected');
});

http.listen(3291, function(){
    console.log('listening on *:3291');
});*/

if (process.env.NODE_ENV === 'development') {
    /**
     * Adding context menu for development
     */
    require('electron-context-menu')({
        showInspectElement: true
    });
}

/**
 * This method will be called when Electron has finished
 * initialization and is ready to create browser windows.
 */
app.on('ready', function () {

    // Create the browser window.
    mainWindow = new BrowserWindow({
        'width': 1000,
        'minWidth': 993,
        'height': 600,
        'minHeight': 640,
        'resizable': true,
        'toolbar': true,
        'center': true,
        'frame': false,
        background: '#eee url(http://subtlepatterns.com/patterns/sativa.png)',
        show: false
        //'icon': __dirname + '/app/app.png'
    });

    global.userData = app.getPath('userData');

    // Remove default menu.
    mainWindow.setMenu(null);

    module.paths.push(__dirname + '/node_modules');

    //if (process.env.NODE_ENV === 'development') {
        //mainWindow.webContents.openDevTools();
    //}

    // Load the index.html of the app.
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    //mainWindow.loadURL('file://' + __dirname + '/unit-tests.html');

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });

    mainWindow.once('ready-to-show', () => {
        mainWindow.show()
    });

    global.sharedObject = {
        someProperty: 'default value'
    }
});

app.on('window-all-closed', app.quit);

app.on('before-quit', () => {
    mainWindow.removeAllListeners('close');
    mainWindow.close();
});

// Allows to send request to hosts with unverified certificate (*.transparent.local)
app.commandLine.appendSwitch('ignore-certificate-errors', false);