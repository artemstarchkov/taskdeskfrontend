(function (global) {
    // map tells the System loader where to look for things
    var map = {
        'app': 'app', // 'dist'
        '@angular': 'node_modules/@angular',
        'rxjs': 'node_modules/rxjs',
        'rx': 'node_modules/rx',
        'symbol-observable': 'node_modules/symbol-observable',
        'socket.io-client': 'node_modules/socket.io-client/dist/socket.io.js',
        "ng2-translate": "node_modules/ng2-translate/bundles/ng2-translate.umd.js",
        "ng4-loading-spinner": "node_modules/ng4-loading-spinner/ng4-loading-spinner.umd.js"
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'app': {main: 'boot', defaultExtension: 'js'},
        'rxjs': {defaultExtension: 'js'},
        'rx': {defaultExtension: 'js'},
        'symbol-observable': {defaultExtension: 'js'},
    };

    const AngularPackageNames = [
        '@angular/common',
        '@angular/compiler',
        '@angular/compiler-cli',
        '@angular/core',
        '@angular/forms',
        '@angular/http',
        '@angular/platform-browser',
        '@angular/platform-browser-dynamic',
        '@angular/platform-server',
        '@angular/router',
        '@angular/upgrade'
    ];

    // Add package entries for angular packages in the form '@angular/common': {main: 'index.js', defaultExtension: 'js'}
    AngularPackageNames.forEach(function (pkgName) {
        packages[pkgName] = {
            main: 'bundles/' + pkgName.split('/')[1] + '.umd.js',
            defaultExtension: 'js'
        };
    });

    var config = {
        defaultJSExtensions: true,
        map: map,
        packages: packages
    };

    System.config(config);
})(this);